<?php
/*
 * Copyright (c) Trilado Team (triladophp.org)
 * All rights reserved.
 */


/**
 * As rotas são para reescrita de URL. Veja um exemplo:
 * Route::add('^([\d]+)-([a-z0-9\-]+)$','home/view/$1/$2');
 *
 * Também é possível criar prefixos. Veja um exemplo:
 * Route::prefix('admin');
 */

Route::prefix('admin');

Route::add('^login','user/login');
Route::add('^logout','user/logout');
Route::add('^register$','user/register');
Route::add('^edit','user/edit');
Route::add('^forgot','user/forgot');
Route::add('^reset/(.*)','user/reset/$1');

Route::add('^tour$','home/tour');
Route::add('^price$','home/price');
Route::add('^contact$','home/contact');
Route::add('^help$','home/help');
Route::add('^subscribe$','home/subscribe');
Route::add('^team$','home/team');

Route::add('^admin(/[\d]+)?$','admin/admin/index/$1');
Route::add('^admin/send-daily-mails$','admin/admin/send-daily-mails');

Route::add('^event(/[\d]+)?(/([\d]+))?$','event/index/$1/$2');
Route::add('^account(/[\d]+)?(/([\d]+))?$','account/index/$1/$2');
Route::add('^workshop/([\d]+)(/([\d]+))?$','workshop/index/$1/$2');
Route::add('^manager/([\d]+)(/([\d]+))?$','manager/index/$1/$2');
Route::add('^paper(/([\d]+))*$','paper/index/$1/$2');
Route::add('^custom-field/([\d]+)(/([\d]+))?$','custom-field/index/$1/$2');
Route::add('^ticket/([\d]+)(/([\d]+))?$','ticket/index/$1/$2');
Route::add('^reviewer/([\d]+)(/([\d]+))?$','reviewer/index/$1/$2');
Route::add('^transfer(/[\d]+)?(/([\d]+))?$','transfer/index/$1/$2');


Route::add('^buy/([a-z0-9]{32})$', 'site/buy/$1');

//API
Route::add('^api/events/([\d]+)/([a-z\-]+)(/[\d]+)?/?$', 'api/$2/$1/$3');
Route::add('^api/manager/events/?$', 'api/manager_events');
Route::add('^api/manager/events/([\d]+)/([a-z\-]+)(/([0-9a-zA-Z]+))?/?$', 'api/manager_$2/$1/$4');
Route::add('^api/manager/auth/?$', 'api/manager_auth');

// Site
Route::add('^([0-9a-zA-Z\-\.\_]+)/?$', 'site/index/$1');
Route::add('^([0-9a-zA-Z\-\.\_]+)/embedded?$', 'site/iframe/$1');
