<!DOCTYPE html>
<html lang="pt-br">
    <head>
		<meta charset="utf-8">
		<title>EventMania</title>

		<meta name="globalsign-domain-verification" content="qr0bIVJtvjvpHVjdcdBTJsEGOpTjd0cVGl-JpZ67er" />

		<?php $version = '2.0.5beta' ?>
		<meta name="description" content="A melhor ferramenta para organizar seus eventos">
		<meta name="keywords" content="gerenciador, evento, eventmania, ticket, ingresso">

		<meta property="fb:admins" content="1276233101">
		<meta property="og:locale" content="pt_BR">
		<meta property="og:site_name" content="EventMania">
		<meta property="og:type" content="website">
		<meta property="og:image" content="">
		<meta property="og:title" content="EventMania">
		<meta property="og:description" content="A melhor ferramenta para organizar seus eventos">
		<meta property="og:url" content="http://eventmania.com.br/">

		<link href="~/css/jquery-ui-1.10.4.custom.css?<?= $version ?>" rel="stylesheet" media="all">
		<link href="~/css/bootstrap.css?<?= $version ?>" rel="stylesheet" media="all">
		<link href="~/css/flat.css?<?= $version ?>" rel="stylesheet" media="all">
		<link href="~/css/datepicker.css?<?= $version ?>" rel="stylesheet" media="all">
		<link href="~/css/style.css?<?= $version ?>" rel="stylesheet" media="all">
		<link href="~/css/chosen.min.css?<?= $version ?>" rel="stylesheet" media="all">

		<link rel="icon" type="image/png" href="~/img/favicon.png?2">
    </head>
    <body itemscope itemtype="http://schema.org/WebPage">
		<header itemscope itemtype="http://schema.org/WPHeader" class="header">
			<div class="header hidden-phone">
				<div class="container">
					<div class="row-fluid">
						<div class="span8">
							<a class="logo" href="~/"><h1 class="muted">EventMania</h1></a>
						</div>
						<div class="span4">
							<?php if(Auth::isLogged()): ?>
							<div class="logged">Olá, <b><?= $user->getFirstName() ?></b> <span id="session">(<?= Config::get('session_time') . ':00' ?>)</span></div>
							<?php endif ?>
						</div>
					</div>
				</div>
			</div>
			<div class="navbar navbar-inverse navbar-static-top">
				<div class="navbar-inner">
					<nav class="container" itemscope itemtype="http://schema.org/SiteNavigationElement">
						<?php if(Auth::isLogged()): ?>
						<div class="nav-collapse collapse">
							<ul class="nav">
								<li><a href="~/">Início</a></li>
								<li><a href="~/event/">Meus Eventos</a></li>
								<li><a href="~/ticket/my">Meus Pedidos</a></li>
								<li><a href="~/paper/">Artigos</a></li>

								<?php if(Auth::is('admin')): ?>
								<li><a href="~/admin">Admin</a></li>
								<?php endif ?>
							</ul>

							<ul class="nav pull-right">
								<li><a href="~/account">Minha Conta</a></li>
								<li><a href="~/logout">Sair</a></li>
							</ul>
						</div>
						<?php else: ?>
						<div class="nav-collapse collapse">
							<ul class="nav">
								<li><a href="~/">Início</a></li>
								<li><a href="~/tour">Como Funciona</a></li>
								<li><a href="~/price">Quanto Custa</a></li>
								<li><a href="~/contact">Contato</a></li>
								<li><a href="~/help">Ajuda</a></li>
							</ul>

							<ul class="nav pull-right">
								<li><a href="~/login" style="padding: 0"><span class="btn">Fazer Login</span></a></li>
							</ul>
						</div>
						<?php endif ?>
					</nav>
				</div>
			</div>
		</header>
		<div class="container">
			<?= FLASH ?>
			<div class="row-fluid">
				<?= CONTENT ?>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p>Copyright &copy; EventMania -
					<a href="~/">Início</a> |
					<a href="~/tour">Como Funciona</a> |
					<a href="~/price">Quanto Custa</a> |
					<a href="~/team">Equipe</a> |
					<a href="~/contact">Contato</a> |
					<a href="~/help">Ajuda</a>
				</p>
				<div class="row-fluid">
					<div class="span4">
						<div class="well">
							<p><b>Atendimento</b></p>
							<p>
								<b>Para particpantes</b><br>
								atendimento@eventmania.com.br
							</p>
							<p>
								<b>Para vendedores</b><br>
								suporte@eventmania.com.br
							</p>
						</div>
					</div>
					<div class="span8">
						<div id="sustainable_website"></div>
					</div>
				</div>
				<p>O uso deste site se caracteriza como a aceitação dos nossos termos de uso e política de privacidade.</p>
			</div>
		</footer>
		<?= Import::view(array(), '_snippet', 'loading') ?>
		<script>
			var ROOT = '~/';
			var FB_APPID = '<?= Config::get('facebook_appid') ?>';
			var TIME = <?= Config::get('session_time') ?>;
		</script>
		<?php if(isset($usemaps) && $usemaps === true): ?>
			<?php if (Config::get('enviroment') === 'production'): ?>
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAenjB5E8sWdxTSoKphqWyy6zjDsqikXiQ&sensor=false"></script>
			<?php else: ?>
			<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
			<?php endif ?>
		<?php endif ?>
		<script src="~/js/vendor/jquery.js?<?= $version ?>"></script>
		<script src="~/js/vendor/bootstrap.js?<?= $version ?>"></script>
		<script src="~/js/vendor/jquery-ui.custom.min.js?<?= $version ?>"></script>
		<script src="~/js/vendor/bootina.js?<?= $version ?>"></script>
		<script src="~/js/vendor/highcharts.js?<?= $version ?>"></script>
		<script src="~/js/vendor/chosen.js?<?= $version ?>"></script>
		<script src="~/js/app/functions.js?<?= $version ?>"></script>
		<script src="~/js/app/chart.js?<?= $version ?>"></script>
		<script src="~/js/app/layout.js?<?= $version ?>"></script>
		<script src="~/js/app/mask.js?<?= $version ?>"></script>
		<script src="~/js/app/match-reviewers.js?<?= $version ?>"></script>
		<script src="~/js/app/social.js?<?= $version ?>"></script>
		<script src="~/js/app/validate.js?<?= $version ?>"></script>
		<script src="~/js/app/wizard.js?<?= $version ?>"></script>
		<script src="~/js/form/account.js?<?= $version ?>"></script>
		<script src="~/js/form/customfield.js?<?= $version ?>"></script>
		<script src="~/js/form/event.js?<?= $version ?>"></script>
		<script src="~/js/form/manager.js?<?= $version ?>"></script>
		<script src="~/js/form/paper.js?<?= $version ?>"></script>
		<script src="~/js/form/participant.js?<?= $version ?>"></script>
		<script src="~/js/form/participant-checkin.js?<?= $version ?>"></script>
		<script src="~/js/form/participant-random.js?<?= $version ?>"></script>
		<script src="~/js/form/transfer.js?<?= $version ?>"></script>
		<script src="~/js/main.js?<?= $version ?>"></script>
		<script src="~/ckeditor/ckeditor.js"></script>
		<script src="~/js/vendor/sawpf.com-1.0.js"></script>
		<?php if (Config::get('enviroment') === 'production'): ?>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-42750858-1', 'eventmania.com.br');
		  ga('send', 'pageview');
		</script>
		<?php endif; ?>
		
		<?php if(Config::get('enviroment') === 'production' && App::$controller === 'HomeController'): ?>
		<script type="text/javascript" src="//selo.sitesustentavel.com.br/selo_sustentavel.js"></script>
		<?php endif ?>
    </body>
</html>
