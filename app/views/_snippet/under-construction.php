<div class="row-fluid">&nbsp;</div>
<div class="row-fluid">
    <div class="span6 text-right">
        <h1><?= $model->Title ?></h1>
        <p><?= $model->Message ?></p>
        <p><a href="javascript:void(0);" onclick="history.go(-1);return false;">Voltar</a></p>
    </div>
    <form action="~/subscribe" method="POST" class="span6 well">
        <h1>Avise-me quando estiver pronto!</h1>
        <div class="control">
            <input type="text" class="span9" name="Name" value="<?= $model->Name ?>" placeholder="Nome">
            <div class="input-append">
                <input type="text" class="span11 email" name="Email" value="<?= $model->Email ?>" placeholder="voce@exemplo.com.br">
                <button class="btn btn-primary">&nbsp;Me Avise</button>
            </div>
        </div>
        <input type="hidden" name="SubscriptionMessage" value="Uma e-mail será enviado assim que está funcionalidade estiver pronta!">
        <input type="hidden" name="Template" value="">
        <input type="hidden" name="Type" value="NotifyArticles">
        <input type="hidden" name="Subject" value="<?= $model->Title ?>">
        <input type="hidden" name="SendDate" value="">
    </form>
</div>