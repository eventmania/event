<h2>Termos de Uso</h2>
<p>O EventMania tem o direito de modificar estes termos, que se tornam efetivos imediatamente após a sua publicação no site.</p>

<h3>Satisfação</h3>
<p>O EventMania se esforça para fazer a comercialização de ingressos uma experiência fácil e conveniente. No entanto, não somos responsáveis por qualquer outro aspecto dos eventos promovidos através do nosso site e hot-sites vinculados. Se você esta insatisfeito com um evento que teve a venda dos ingressos através do EventMania entre em contato com o promotor do evento ou um representante válido do evento ou local.</p>

<h3>Responsabilidades como provedor de conteúdo</h3>
<p>O EventMania é um sistema para gerenciamento de eventos, portanto não se responsabiliza por qualquer conteúdo postado ou exibido em qualquer página dentro do site pelos produtores do evento. Tais informações não representam necessariamente os pontos de vista ou opiniões do EventMania ou de seus funcionários. Como tal, o EventMania não pode ser responsabilizada por eventuais promessas não cumpridas, acordos, contratos e também pela publicação de qualquer material ilegal ou ilícito, incluindo material difamatório ou que possa violar direitos autorais, por um promotor do evento. </p>

<h3>Conferência de informações</h3>
<p>Usuário de qualquer um dos serviços do EventMania se comprometem de fornecer informações precisas e autenticas. O EventMania fará todos os esforços para validar as informações de faturamento que nos é fornecida, no entanto, se não formos capazes de fazê-lo, nos reservamos o direito de cancelar seus ingressos ou proibir o uso deste sistema.</p>

<h3>Alteração de listagens de eventos</h3>
<p>O EventMania reserva-se o direito de eliminar qualquer evento ou publicação no seu site a qualquer momento, sob quaisquer circunstâncias, e por qualquer motivo.</p>

<h3>Taxas de conveniência</h3>
<p>Uma taxa não restituível de serviço/entrega/transação pode ser adicionada a cada ingresso ou a cada compra. Comprando ingressos através do site o comprador concorda no pagamento dessas taxas e compreende que elas não serão restituídas. </p>

<h3>Privacidade</h3>
<p>O EventMania entende e respeita o seu direito à privacidade. Não compartilhamos seus dados de e-mail com outras empresas/pessoas exceto ao promotor de eventos da qual você comprou o ingresso. Além disso, usaremos seu endereço de e-mail para recuperação de senha e fornecer informações sobre eventos e serviços que possam ser de seu interesse.</p>

<h3>Limites de compra</h3>
<p>Sob certas circunstâncias, a fim de desencorajar práticas injustas de compra de ingressos, os promotores pode estabelecer limites máximos para o número de bilhetes comprados por pessoa. O EventMania não é responsável por esses limites, nem somos capazes de contorná-los de qualquer maneira. Qualquer tentativa de evasão desses limites por um comprador de bilhetes irá resultar na anulação de todos os bilhetes que o comprador comprou de outra forma.</p>