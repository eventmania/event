<div class="pagination">
<?php if($count > 1): ?>
	<ul>
		<?php if($p > 1): ?>
		<li><a href="~/<?= $url .'/'. ($p - 1) ?>">&laquo;</a></li>
		<?php endif ?>

		<?php if($p > 7): ?>
		<li><a href="javascript:void(0);">...</a></li>
		<?php endif ?>

		<?php for($i = ($p > 7 ? $p - 6 : 1); $i <= $count && $i <= $p + 6; $i++): ?>
			<?php if($i == $p): ?>
			<li class="active"><span><?= $i ?></span></li>
			<?php else: ?>
			<li><a href="~/<?= $url .'/'. $i ?>"><?= $i ?></a></li>
			<?php endif ?>
		<?php endfor ?>

		<?php if($p < $count - 7): ?>
		<li><a href="javascript:void(0);">...</a></li>
		<?php endif ?>

		<?php if($p < $count): ?>
		<li><a href="~/<?= $url .'/'. ($p + 1) ?>">&raquo;</a></li>
		<?php endif ?>
	</ul>
<?php else: ?>
	<p class="muted">Página 1 de 1</p>
<?php endif ?>
</div>