<div class="row-fluid">
    <div class="span5">
        <h2>Painel de Administração</h2>
        <p class="muted">Olá <?= $user->getFirstName() ?></p>
    </div>
    <div class="span7">
        <div class="btn-group pull-right event-menu">
            <a href="~/admin/" class="btn <?= $s == 0 ? 'disabled' : '' ?>">Home</a>
            <a href="~/admin/user/" class="btn <?= $s == 1 ? 'disabled' : '' ?>">Usuários</a>
            <a href="~/admin/event/" class="btn <?= $s == 2 ? 'disabled' : '' ?>">Eventos</a>
            <a href="~/admin/event/banner/" class="btn <?= $s == 3 ? 'disabled' : '' ?>">Banners</a>
            <a href="~/admin/transfer/" class="btn <?= $s == 4 ? 'disabled' : '' ?>">Repasses</a>
			<a href="~/admin/error-log/" class="btn <?= $s == 5 ? 'disabled' : '' ?>">Logs</a>
        </div>
    </div>
</div>
<hr>