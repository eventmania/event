<ul class="wizard">
    <li <?= $i === 1 ? 'class="active"' : '' ?>>
        <span>1</span>
        <a href="<?= $hasLinks ? '~/event/edit/' . $eventId : 'javascript:void(0);' ?>">
            Informações do Evento
        </a>
    </li>
    <li <?= $i === 2 ? 'class="active"' : '' ?>>
        <span>2</span>
        <a href="<?= $hasLinks ? '~/layout/edit/' . $eventId : 'javascript:void(0);' ?>">
            Escolha o layout
        </a>
    </li>
    <li <?= $i === 3 ? 'class="active"' : '' ?>>
        <span>3</span>
        <a href="<?= $hasLinks ? '~/paper/edit/' . $eventId : 'javascript:void(0);' ?>">
            Submissão de Artigos
        </a>
    </li>
</ul>
