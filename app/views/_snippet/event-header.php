<div class="row-fluid">
	<div class="span5">
		<h2><?= $event->Name ?></h2>
		<p class="muted"><?= $event->getSchedule() ?></p>
	</div>
	<div class="span7">
		<div class="btn-group pull-right event-menu">
			<a href="~/event/about/<?= $event->Id ?>" class="btn <?= $s == 0 ? 'disabled' : '' ?>">Evento</a>
			<a href="~/ticket/index/<?= $event->Id ?>" class="btn <?= $s == 1 ? 'disabled' : '' ?>">Ingressos</a>
			<a href="~/participant/index/<?= $event->Id ?>" class="btn <?= $s == 2 ? 'disabled' : '' ?>">Participantes</a>
			<a href="~/sale/index/<?= $event->Id ?>" class="btn <?= $s == 3 ? 'disabled' : '' ?>">Vendas</a>
		</div>
	</div>
</div>
<hr>
