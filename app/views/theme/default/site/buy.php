<div class="span8">
	<div class="alert alert-block alert-info">
		<div id="timer"><?= Booked::TIME ?>:00</div>
		Sua inscrição foi reservada, você tem este tempo para preencher as informações abaixo. Se o tempo terminar, terá que fazer uma nova inscrição.
	</div>
	<form method="POST" action="" id="buy">
		<fieldset>
			<input type="hidden" id="TicketPrice" value="<?= $total ?>">
			<?= BForm::input('Nome Completo', 'Name', null, 'span8 name', array(), true) ?>
			<div class="row-fluid">
				<div class="span4">
					<?= BForm::input('E-mail', 'Email', null, 'span12 email', array(), true) ?>
				</div>
				<div class="span4">
					<?= BForm::input('Telefone', 'Card_Phone', null, 'span12 phone', array(), true) ?>
				</div>
			</div>
			<div class="sandbox"><?= $form->render() ?></div>
		</fieldset>

		<?php if($total > 0): ?>
		<button class="btn btn-large btn-success" id="Finish">Pagar com PagSeguro</button>
		<?php else: ?>
		<button class="btn btn-large btn-success" id="Finish">Finalizar Inscrição</button>
		<?php endif ?>
	</form>
	<script>
		var SECONDS = <?= $booked->getRemainder() ?>
	</script>
</div>
<div class="span4">
	<h2>Resumo da Compra</h2>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Descrição</th>
				<th>Valor</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($cart as $item): ?>
			<tr>
				<td><?= $item->getCurrent()->getTitle() ?></td>
				<td><?= $item->getSalePrice() ?></td>
			</tr>
			<?php endforeach ?>
			<tr class="total">
				<td>Total a Pagar</td>
				<td><?= Format::money($total) ?></td>
			</tr>
		</tbody>
	</table>

	<?= Import::view(array('event' => $event), '_snippet', 'sidebar') ?>
</div>