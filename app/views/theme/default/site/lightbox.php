<div class="span8">
	<div id="lightbox-loading">
		<h2>Aguarde...</h2>
		<p>O sistema está processando seu pedido...</p>
	</div>
	
	<div id="lightbox-success" class="hide">
		<h2>Pedido Finalizado</h2>
		<div class="alert alert-success">Seu pedido foi finalizado. Assim que confirmarmos o pagamento, enviaremos um e-mail com seu comprovante em anexo.</div>
	</div>
	<div id="lightbox-abort" class="hide">
		<h2>Houve Algum Problema?</h2>
	</div>
	
	<div id="lightbox-finish" class="hide">
		<p>Se tiver alguma dúvida em relação a esse evento, entre em contato com o organizador.</p>
		<a href="<?= $event->getUrl() ?>" class="btn btn-large btn-primary">Retornar ao Evento</a>
	</div>
	
	
	<script>
		var checkoutCode = '<?= $code ?>';
	</script>
</div>
<div class="span4">
	<?= Import::view(array('event' => $event), '_snippet', 'sidebar') ?>
</div>