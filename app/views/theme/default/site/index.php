<div class="span8">
	<h1><?= $event->Name ?></h1>
	<form method="POST" action="">
		<h2>Inscrição</h2>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Tipo de Ingresso</th>
					<th class="date">Disponível Até</th>
					<th class="price">Preço</th>
					<th class="fee">Taxa</th>
					<th class="check"></th>
				</tr>
			</thead>
			<tbody>
				<?php if($tickets->Count): ?>
					<?php foreach ($tickets->Data as $ticket): ?>
						<?php if(!$ticket->IsPrivate && $ticket->StartDate <= time()): ?>
						<tr class="<?= !$ticket->isAvailable() ? 'ticket-unavailable' : '' ?>">
							<td>
								<div class="ticket-name"><?= $ticket->Name ?></div>
								<small class="muted"><?= $ticket->Description ?></small>
							</td>
							<td><?= $ticket->getEndDate() .' '. $ticket->getEndHour() ?></td>
							<td><?= $event->newCart(array($ticket))->getNetPrice() ?></td>
							<td><?= $event->lastCart()->getFee() ?></td>
							<td>
								<?php if($ticket->isAvailable()): ?>
								<input type="radio" name="Ticket" value="<?= $ticket->Id ?>">
								<?php else: ?>
								<i class="icon-ban-circle" data-toggle="tooltip" title="Vendas Encerradas!"></i>
								<?php endif ?>
							</td>
						</tr>
						<?php endif ?>
					<?php endforeach ?>
				<?php else: ?>

				<?php endif ?>
			</tbody>
		</table>

		<?php if($workshops->Count): ?>
		<h3>Workshops</h3>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Título</th>
					<th class="date">Data do Minicurso</th>
					<th class="price">Preço</th>
					<th class="fee">Taxa</th>
					<th class="check"></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($workshops->Data as $workshop): ?>
				<tr>
					<td>
						<div class="ticket-name"><?= $workshop->Title ?></div>
						<small class="muted"><?= $workshop->Instructor ?></small>
					</td>
					<td><?= $workshop->getEndDate() .' '. $workshop->getEndHour() ?></td>
					<td><?= $event->newCart(array($workshop))->getNetPrice() ?></td>
					<td><?= $event->lastCart()->getFee() ?></td>
					<td>
						<?php if($workshop->isAvailable()): ?>
						<input type="radio" name="Workshop" value="<?= $workshop->Id ?>">
						<?php else: ?>
						<i class="icon-ban-circle" data-toggle="tooltip" title="Vendas Encerradas!"></i>
						<?php endif ?>
					</td>
				</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<?php endif; ?>

		<?php if($event->isPaid()): ?>
		<img src="~/theme/default/img/payments.png" alt="Métodos de Pagamento: Boleto, MasterCard e Visa">
		<?php endif ?>
		<button type="submit" class="btn btn-success pull-right">Continuar</button>
	</form>

	<div class="row-fluid">
		<div class="span12">
			<h2>Informações Adicionais</h2>
			<?= $event->Description ?>
		</div>
	</div>

	<?php if($event->HasPapers): ?>
	<div class="row-fluid">
		<div class="span12">
			<h2>Chamada de Trabalhos</h2>
			<div>
				<?= $event->CallForPapers ?>
			</div>
			<strong>Os trabalhos devem ser submetidos até <?= $event->getCurrentDeadLine() ?></strong>
			<p>Não sabe como enviar seu trabalho, leia as <a href="#modal-paper-instructions" data-toggle="modal">Instruuções</a>.</p>
		</div>
	</div>
	<?php endif; ?>
</div>
<div class="span4">
	<?= Import::view(array('event' => $event), '_snippet', 'sidebar') ?>
</div>
<div id="modal-paper-instructions" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Instruções para Submissão de Artigo</h3>
    </div>
    <div class="modal-body">
    	<div class="row-fluid">
	        <div class="span12">
		        <p>Para submeter artigos você deve iniciar uma inscrição (não é necessário confirmá-la) e acessar o <a href="~/user/login" target="_blank">EventMania</a>.</p>
		        <p>Após o entrar no EventMania você poderá acessar a área <a href="~/paper/" target="_blank">Artigos</a> e visualizar os eventos aos quais você pode enviar trabalhos. Para enviar seu artigo basta selecionar o evento desejado.</p>
	        </div>
    	</div>
    </div>
    <div class="modal-footer">
		<a href="javascript:void(0);" class="btn btn-primary" data-dismiss="modal">Entendi!</a>
    </div>
</div>
