<!DOCTYPE html>
<html lang="pt-br">
    <head>
		<meta charset="utf-8">
		<title>EventMania</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta name="description" content="A melhor ferramenta para organizar seus eventos">
		<meta name="keywords" content="gerenciador, evento, eventmania, ticket, ingresso">

		<link href="~/theme/default/css/bootstrap.css" rel="stylesheet" media="all">
		<link href="~/css/flat.css" rel="stylesheet" media="all">
		<link href="~/theme/default/css/style.css?v=0.2.0" rel="stylesheet" media="all">

		<link rel="icon" type="image/png" href="~/img/favicon.png">
    </head>
    <body itemscope itemtype="http://schema.org/WebPage" class="vevent embedded">
    	<?= FLASH ?>

		<div class="containter-fluid">
			<div class="row-fluid">
				<div class="span12">
					<form method="POST" action="<?= $event->getUrl() ?>" target="_blank">
						<img src="~/img/logo-mini.png" alt="Feito por EventMania" class="pull-right">
						<h2>Inscrição</h2>
						<table class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>Tipo de Ingresso</th>
									<th class="date">Disponível Até</th>
									<th class="price">Preço</th>
									<th class="check"></th>
								</tr>
							</thead>
							<tbody>
								<?php if($tickets->Count): ?>
									<?php foreach ($tickets->Data as $ticket): ?>
										<?php if(!$ticket->IsPrivate && $ticket->StartDate <= time()): ?>
										<tr class="<?= !$ticket->isAvailable() ? 'ticket-unavailable' : '' ?>">
											<td>
												<div class="ticket-name"><?= $ticket->Name ?></div>
												<small class="muted"><?= $ticket->Description ?></small>
											</td>
											<td><?= $ticket->getEndDate() .' '. $ticket->getEndHour() ?></td>
											<td><?= $event->newCart(array($ticket))->getSalePrice() ?></td>
											<td>
												<?php if($ticket->isAvailable()): ?>
												<input type="radio" name="Ticket" value="<?= $ticket->Id ?>">
												<?php else: ?>
												<i class="icon-ban-circle" data-toggle="tooltip" title="Vendas Encerradas!"></i>
												<?php endif ?>
											</td>
										</tr>
										<?php endif ?>
									<?php endforeach ?>
								<?php else: ?>

								<?php endif ?>
							</tbody>
						</table>

						<?php if($workshops->Count): ?>
						<h3>Workshops</h3>
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Título</th>
									<th class="date">Data do Minicurso</th>
									<th class="price">Preço</th>
									<th class="fee">Taxa</th>
									<th class="check"></th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($workshops->Data as $workshop): ?>
								<tr>
									<td>
										<div class="ticket-name"><?= $workshop->Title ?></div>
										<small class="muted"><?= $workshop->Instructor ?></small>
									</td>
									<td><?= $workshop->getEndDate() .' '. $workshop->getEndHour() ?></td>
									<td><?= $event->newCart(array($workshop))->getNetPrice() ?></td>
									<td><?= $event->lastCart()->getFee() ?></td>
									<td>
										<?php if($workshop->isAvailable()): ?>
										<input type="radio" name="Workshop" value="<?= $workshop->Id ?>">
										<?php else: ?>
										<i class="icon-ban-circle" data-toggle="tooltip" title="Vendas Encerradas!"></i>
										<?php endif ?>
									</td>
								</tr>
								<?php endforeach ?>
							</tbody>
						</table>
						<?php endif; ?>

						<?php if($event->isPaid()): ?>
						<img src="~/theme/default/img/payments.png" alt="Métodos de Pagamento: Boleto, MasterCard e Visa">
						<?php endif ?>
						<button type="submit" class="btn btn-warning pull-right">Continuar</button>
					</form>

					<?php if($event->HasPapers): ?>
					<div class="row-fluid">
						<div class="span12">
							<h2>Chamada de Trabalhos</h2>
							<div>
								<?= $event->CallForPapers ?>
							</div>
							<strong>Os trabalhos devem ser submetidos até <?= $event->getCurrentDeadLine() ?></strong>
							<p>Não sabe como enviar seu trabalho, leia as <a href="#modal-paper-instructions" data-toggle="modal">Instruuções</a>.</p>
						</div>
					</div>
					<?php endif; ?>
				</div>
				<div id="modal-paper-instructions" class="modal hide fade">
				    <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h3>Instruções para Submissão de Artigo</h3>
				    </div>
				    <div class="modal-body">
				    	<div class="row-fluid">
					        <div class="span12">
						        <p>Para submeter artigos você deve iniciar uma inscrição (não é necessário confirmá-la) e acessar o <a href="~/user/login" target="_blank">EventMania</a>.</p>
						        <p>Após o entrar no EventMania você poderá acessar a área <a href="~/paper/" target="_blank">Artigos</a> e visualizar os eventos aos quais você pode enviar trabalhos. Para enviar seu artigo basta selecionar o evento desejado.</p>
					        </div>
				    	</div>
				    </div>
				    <div class="modal-footer">
						<a href="javascript:void(0);" class="btn btn-primary" data-dismiss="modal">Entendi!</a>
				    </div>
				</div>
			</div>
		</div>
    </body>
</html>
