<hr>
<div class="row-fluid">
    <div class="span12">
        <h2>Conigurar o Tema Default</h2>
        <form id="manager-form" method="POST" action="~/layout/edit/<?= $model->Id ?>/default" enctype="multipart/form-data">
            <fieldset>
                <div class="row-fluid">
                    <div class="control-group span6">
                        <label class="control-label">Capa <small class="muted">(1170px X 450px)</small></label>
                        <div class="controls">
                            <a href="javascript:void(0);" class="btn btn-file-trigger">Selecionar Imagem</a>
                            <input type="file" name="Image" style="display: none;" />
                        </div>
                    </div>
                </div>
            </fieldset>
            <p></p>
            <?= Import::view(array('model' => $model), 'theme/_snippet', 'form-actions'); ?>
        </form>
    </div>
</div>
