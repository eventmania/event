<!DOCTYPE html>
<html lang="pt-br">
    <head>
		<meta charset="utf-8">
		<title>EventMania</title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<meta name="description" content="A melhor ferramenta para organizar seus eventos">
		<meta name="keywords" content="gerenciador, evento, eventmania, ticket, ingresso">

		<meta property="fb:admins" content="1276233101">
		<meta property="og:locale" content="pt_BR">
		<meta property="og:site_name" content="<?= $event->Name ?>">
		<meta property="og:type" content="website">

		<meta property="og:image" content="<?= $event->getImageUrl(200, 200) ?>">
		<meta property="og:title" content="<?= $event->Name ?>">
		<meta property="og:description" content="A melhor ferramenta para organizar seus eventos">
		<meta property="og:url" content="<?= $event->getUrl() ?>">

		<link href="~/theme/default/css/bootstrap.css" rel="stylesheet" media="all">
		<link href="~/theme/default/css/bootstrap-responsive.css" rel="stylesheet" media="all">
		<link href="~/theme/default/css/datepicker.css" rel="stylesheet" media="all">
		<link href="~/css/flat.css" rel="stylesheet" media="all">
		<link href="~/theme/default/css/style.css" rel="stylesheet" media="all">

		<link rel="icon" type="image/png" href="~/img/favicon.png">
    </head>
    <body itemscope itemtype="http://schema.org/WebPage" class="vevent">
		<div class="navbar navbar-inverse navbar-static-top">
			<div class="navbar-inner">
				<a class="brand" href="#">EventMania</a>
			</div>
		</div>

		<div class="container">
			<div class="row-fluid">
				<div class="span12">
					<header itemscope itemtype="http://schema.org/WPHeader" id="cover">
						<img id="image" src="<?= $event->getImageUrl(1170, 450) ?>" alt="">
					</header>

					<div id="content">
						<?= FLASH ?>
						<div class="row-fluid">
							<?= CONTENT ?>
						</div>
					</div>
					<footer>

					</footer>
				</div>
			</div>
		</div>
		<script>
			var ROOT = '~/';
			var SITE = '<?= $event->getUrl() ?>';
			var AKATUS_PUBLIC_TOKEN = '<?= Config::get('akatus_public_token') ?>';
		</script>
		<script src="~/theme/default/js/vendor/jquery.js"></script>
		<script src="~/theme/default/js/vendor/bootstrap.js"></script>
		<script src="~/theme/default/js/vendor/mask.js"></script>
		<script src="~/theme/default/js/vendor/bootina.js"></script>
		<script src="~/theme/default/js/app/timer.js"></script>
		<script src="~/theme/default/js/main.js"></script>
		<?php if(isset($lightbox) && $lightbox): ?>
		<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
		<script src="~/theme/default/js/form/lightbox.js?v=0.2"></script>
		<?php endif ?>
		<?php if(isset($event) && $event->MapType === 1): ?>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<?php endif ?>
    </body>
</html>
