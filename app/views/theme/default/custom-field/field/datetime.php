<div class="row-fluid snippet"><div class="span8">
    <div class="span8">
        <?= BForm::date($model->getTitle(), $model->getName(), $model->getDate(), 'span12', array(), $model->isRequired()) ?>
    </div>
    <div class="span4 control-group">
        <label>&nbsp;</label>
        <select name="<?= $model->getName() ?>" style="width: 100%" class="<?= $model->isRequired() ? 'required' : '' ?>">
            <option value="<?= $model->getValue() ?>"><?= $model->getValue() ?></option>
            <?php foreach($model->getContent() as $option): ?>
            <option value="<?= $option ?>"><?= $option ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div></div>