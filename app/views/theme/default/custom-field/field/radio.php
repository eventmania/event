<div class="row-fluid snippet">
    <div class="span8 control-group">
        <label class="control-label"><?= $model->getTitle() ?>&nbsp;
            <?php if($model->isRequired()): ?>
            <span class="required hide">*</span>
            <?php endif; ?>
        </label>
        <div class="controls">
            <div class="btn-group Content" data-toggle="buttons-radio">
                <?php foreach ($model->getContent() as $option): ?>
                <button type="button" value="<?= $option ?>" class="btn btn-small"><?= $option ?></button>
                <?php endforeach; ?>
            </div>
            <input name="<?= $model->getName() ?>" class="<?= $model->isRequired() ? 'required' : '' ?>" type="hidden" >
        </div>
    </div>
</div>