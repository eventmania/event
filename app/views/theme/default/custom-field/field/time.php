<div class="row-fluid snippet">
    <div class="span8 control-group">
        <label><?= $model->getTitle() ?>&nbsp;
            <?php if($model->isRequired()): ?>
            <span class="required">*</span>
            <?php endif; ?>
        </label>
        <select name="<?= $model->getName() ?>" style="width: 100%" class="<?= $model->isRequired() ? 'required' : '' ?>">
            <option value="<?= $model->getValue() ?>"><?= $model->getValue() ?></option>
            <?php foreach($model->getContent() as $option): ?>
            <option value="<?= $option ?>"><?= $option ?></option>
            <?php endforeach ?>
        </select>
    </div>
</div>