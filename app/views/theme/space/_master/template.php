<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <title>EventMania</title>

        <meta name="description" content="A melhor ferramenta para organizar seus eventos">
        <meta name="keywords" content="gerenciador, evento, eventmania, ticket, ingresso">

        <meta property="fb:admins" content="1276233101">
        <meta property="og:locale" content="pt_BR">
        <meta property="og:site_name" content="<?= $event->Name ?>">
        <meta property="og:type" content="website">

        <meta property="og:image" content="">
        <meta property="og:title" content="Título">
        <meta property="og:description" content="A melhor ferramenta para organizar seus eventos">
        <meta property="og:url" content="http://eventmania.com.br/<?= $event->Slug ?>">

        <link href="~/theme/space/css/bootstrap.css" rel="stylesheet" media="all">
        <link href="~/theme/space/css/style.css" rel="stylesheet" media="all">

        <link rel="icon" type="image/png" href="~/img/favicon.png">
    </head>
    <body itemscope itemtype="http://schema.org/WebPage" class="vevent">
        <?= CONTENT ?>
        <footer>
            <section id="eventmania">
                <div class="container">
                    <div class="row-fluid">
                        <div class="pull-left">
                            <a href="~/" class="btn btn-warning">Crie seu evento Grátis!</a>
                        </div>
                        <div class="pull-right">
                            Powered By: <a href="~/"><img src="~/img/logo.png"></a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </section>
            <section id="footer">
                <div class="container">
                    <div class="row-fluid">
                        <div class="span4">
                            <img src="<?= $event->getImageUrl(100, 40); ?>">
                            <p></p>
                            <p>
                                <strong><?= $event->Organizer ?></strong>
                                <br>
                                <a href="mailto:<?= $event->OrganizerEmail ?>"><?= $event->OrganizerEmail ?></a>
                            </p>
                        </div>
                        <div class="span4">
                            <h4>Explore</h4>
                            <ul>
                                <li><a href="#tickets">Inscrições</a></li>
                                <?php if($workshops->Count): ?><li><a href="#workshops">Workshops</a></li><?php endif; ?>
                                <?php if($event->MapType === 1): ?><li><a href="#address">Local</a></li><?php endif; ?>
                            </ul>
                        </div>
                        <div class="span4">
                            <h4>Junte-se a nós</h4>
                            <a href="#tickets" class="btn btn-primary">Faça sua Inscrição!</a>
                            <p></p>
                            <p class="muted">&copy; EventMania 2013-<?= date('Y') ?></p>
                        </div>
                    </div>
                </div>
            </section>
        </footer>
        <script src="~/theme/space/js/vendor/jquery.js"></script>
        <script src="~/theme/space/js/vendor/bootstrap.js"></script>
        <script src="~/theme/space/js/vendor/bootina.js"></script>
        <script src="~/theme/space/js/vendor/validate.js"></script>
        <script src="~/theme/space/js/vendor/mask.js"></script>
        <script src="~/theme/space/js/app/timer.js"></script>
        <script src="~/theme/space/js/form/buy.js"></script>
        <script src="~/theme/space/js/main.js"></script>
        <script src="~/theme/space/js/app/social.js"></script>
        <script src="~/theme/space/js/app/maps.js"></script>
        <script src="https://static.akatus.com/js/akatus.min.js"></script>
        <?php if(isset($event) && $event->MapType === 1): ?>
        <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
        <?php endif ?>
        <script type="text/javascript">
            var ROOT = '~/';
            var FB_APPID = '<?= Config::get('facebook_appid') ?>';
            var TIME = '<?= Config::get('session_time') ?>';
            var AKATUS_PUBLIC_TOKEN = '<?= Config::get('akatus_public_token') ?>';
        </script>
    </body>
</html>
