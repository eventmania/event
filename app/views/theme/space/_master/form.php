<hr>
<div class="row-fluid">
    <div class="span12">
        <h2>Conigurar o Tema Space</h2>
        <form id="manager-form" method="POST" action="~/layout/edit/<?= $model->Id ?>/space" enctype="multipart/form-data">
            <fieldset>
                <div class="row-fluid">
                    <div class="control-group span6">
                        <label class="control-label">Esquema de Cores</label>
                        <div class="controls btn-group" data-toggle="buttons-radio" name="ColorScheme">
                            <button class="btn <?= $model->getLayout()->ColorScheme ? ($model->getLayout()->ColorScheme == 'dark' ? 'active' : '') : 'active' ?>" value="dark">Dark</button>
                            <button class="btn <?= $model->getLayout()->ColorScheme == 'light' ? 'active' : '' ?>" value="light">Light</button>
                        </div>
                        <input type="hidden" name="ColorScheme" value="<?= $model->getLayout()->ColorScheme ? $model->getLayout()->ColorScheme : 'dark' ?>">
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="control-group span6">
                        <label class="control-label">Logotipo <small class="muted">(100px X 40px)</small></label>
                        <div class="controls">
                            <a href="javascript:void(0);" class="btn btn-file-trigger">Selecionar Imagem</a>
                            <input type="file" name="Image" style="display: none;" />
                        </div>
                    </div>
                    <div class="control-group span6">
                        <label class="control-label">Capa <small class="muted">(2000px X 768px)</small></label>
                        <div class="controls">
                            <a href="javascript:void(0);" class="btn btn-file-trigger">Selecionar Imagem</a>
                            <input type="file" name="Cover" style="display: none;" />
                        </div>
                    </div>
                </div>
            </fieldset>
            <p></p>
            <?= Import::view(array('model' => $model), 'theme/_snippet', 'form-actions'); ?>
        </form>
    </div>
</div>
