<div class="span8">
	<?php if($model->isConfirmed()): ?>
	<h2>Inscrição Confirmada</h2>
	<div class="alert alert-success">Detalhes do seu pedido foram enviados para seu e-mail.</div>
	
	<h2>Resumo da Compra</h2>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Participante</th>
				<th>Tipo</th>
				<th>Data</th>
				<th>Valor</th>
				<th>Taxa</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			<?php $cart = $event->newCart(array($model)) ?>
			<?php foreach ($cart as $item): ?>
			<tr>
				<td><?= $model->Name ?></td>
				<td><?= $item->getCurrent()->getName() ?></td>
				<td><?= $model->getDate() ?></td>
				<td><?= $item->getNetPrice() ?></td>
				<td><?= $item->getFee() ?></td>
				<td>
					<?= $item->getSalePrice() ?>
					<?php if($model->AuthorId == Participant::SITE_AUTHOR_ID && $model->Absorb): ?>
					<span class="required">*</span>
					<?php endif; ?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="4">&nbsp;</th>
				<th>Total</th>
				<th>
					<?= $event->lastCart()->reset()->getSalePrice() ?>
					<?php if($model->AuthorId == Participant::SITE_AUTHOR_ID && $model->Absorb): ?>
					<span class="required">*</span>
					<?php endif; ?>
				</th>
			</tr>
		</tfoot>
	</table>
	<?php else: ?>
	<h2>Inscrição em Aberto</h2>
	
	<div class="alert alert-success">Assim que seu pagamento for confirmado, enviaremos detalhes do seu pedido foram enviados para seu e-mail.</div>
	<?php endif ?>
	
	<p>Se tiver alguma dúvida em relação a esse evento, entre em contato com o organizador.</p>
	<a href="<?= $event->getUrl() ?>" class="btn btn-large btn-primary">Retornar ao Evento</a>
</div>
<div class="span4">
	<?= Import::view(array('event' => $event), '_snippet', 'sidebar') ?>
</div>