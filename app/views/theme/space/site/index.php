<?php if($event->getLayout()->Yin && $event->getLayout()->Yang): ?>
<style type="text/css">
    header .jumbotron, .nav li, .nav li a
    {
        color: <?= $event->getLayout()->Yin ?> !important;
    }
    .nav .divider
    {
        background-color: <?= $event->getLayout()->Yin ?> !important;
    }
    .navbar-inverse .navbar-inner, footer
    {
        background-color: <?= $event->getLayout()->Yang ?> !important;
    }
</style>
<?php endif; ?>
<form id="cart-form" method="POST">
    <h4>Carrinho</h4>
    <ul class="items">

    </ul>
    <div class="form-actions">
        <div class="pull-right">
            <a href="~/<?= $event->Slug ?>" class="btn">Esvaziar</a>
            <button type="submit" class="btn btn-primary">Comprar</button>
        </div>
    </div>
</form>
<header itemscope="" itemtype="http://schema.org/WPHeader" class="block-header" style="background-image: url('<?= $event->getLayout()->Cover ? $event->getLayout()->Cover->getImageUrl(2000, 768) : ImageService::getPlaceholderImage(2000, 768) ?>');">
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="navbar-inner">
            <nav class="container" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
                <div class="nav-collapse collapse">
                    <ul class="nav main-nav">
                        <li>
                            <a href="<?= $event->getUrl() ?>" class="logo-link">
                                <img src="<?= $event->getImageUrl(100, 40); ?>">
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li><?= $event->Name ?></li>
                    </ul>

                    <ul class="nav pull-right">
                        <li><a href="#tickets">Inscrições</a></li>
                        <?php if($workshops->Count): ?><li><a href="#workshops">Workshops</a></li><?php endif; ?>
                        <?php if($event->MapType === 1): ?><li><a href="#address">Local</a></li><?php endif; ?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>

    <div class="header">
        <div class="container">
            <div class="row-fluid text-center">
                <div class="jumbotron">
                    <h1><?= $event->Name ?></h1>
                    <p><?= $event->Description ?></p>
                </div>
                <p class="social-bar">
                    <a href="<?= $event->getUrl() ?>" data-toggle="fb-share" fb-img="<?= $event->getImageUrl() ?>" fb-redirect="<?= Request::getSite() ?>event/about/<?= $event->Id ?>" fb-name="<?= $event->Name ?>" fb-description="<?= $event->getShortDescription() ?>"><i class="icon-facebook"></i></a>
                    <a href="<?= $event->getUrl() ?>" data-toggle="twitter-share" twitter-redirect="<?= Request::getSite() ?>event/about/<?= $event->Id ?>"><i class="icon-twitter"></i></a>
                    <a href="<?= $event->getUrl() ?>" data-toggle="gplus-share"><i class="icon-gplus"></i></a>
                </p>
            </div>
        </div>
    </div>
</header>
<?= FLASH ?>
<section id="tickets" class="container">
    <div class="row-fluid">
        <div class="span12">
            <h1>Inscrições</h1>
        </div>
    </div>
    <div class="row-fluid">
    <?php if($tickets->Count): $i = -1; ?>
        <?php foreach ($tickets->Data as $ticket): ?>
            <?php if(!$ticket->IsPrivate): ?>
            <div class="span4 ticket">
                <div class="row-fluid">
                    <div class="span2">
                        <img src="~/img/favicon.png">
                    </div>
                    <div class="span10">
                        <h4><?= $ticket->Name ?></h4>
                        <h6>Compre até: <?= $ticket->getEndDate() .' '. $ticket->getEndHour() ?></h6>
                        <p><?= $ticket->Description ?></p>
                        <strong><?= $event->newCart(array($ticket))->getSalePrice() ?></strong>
                        <br><br>
                        <?php if($ticket->isAvailable()): ?>
                        <a href="javascript:void(0);" class="btn btn-primary add-to-cart" data-form-name="Ticket" data-name="<?= $ticket->Name ?> - <?= $event->lastCart()->getSalePrice() ?>" data-id="<?= $ticket->Id ?>" data-name="TicketId">Inscreva-se</a>
                        <?php else: ?>
                        <a href="javascript:void(0);" class="btn btn-primary disabled">Esgotado</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php if(++$i % 3 == 2): ?>
            </div><div class="row-fluid">
            <?php endif; ?>
            <?php endif ?>
        <?php endforeach ?>
    <?php else: ?>
    <div class="span12">
        <p>Ops, as inscrições ainda não começaram! =(</p>
    </div>
    <?php endif ?>
    </div>
</section>
<?php if($workshops->Count): ?>
<section id="workshops" class="container">
    <div class="row-fluid">
        <div class="span12">
            <h1>Workshops</h1>
        </div>
    </div>
    <div class="row-fluid">
        <?php foreach ($workshops->Data as $i => $w): ?>
        <div class="span4">
            <img src="<?= $w->getImageUrl(300, 200); ?>">
            <h3><a href="<?= $w->InstructorUrl ?>"><?= $w->Instructor ?></a></h3>
            <h5><?= $w->Title ?> - <?= $event->newCart(array($w))->getSalePrice() ?></h5>
            <p><?= $w->Description ?></p>
            <?php if($w->isAvailable()): ?>
            <a href="javascript:void(0);" class="btn btn-primary add-to-cart" data-form-name="Workshop" data-name="<?= $w->Title ?> - <?= $event->lastCart()->getSalePrice() ?>" data-id="<?= $w->Id ?>" data-name="WorkshopId">Participe</a>
            <?php else: ?>
            <a href="javascript:void(0);" class="btn btn-primary disabled">Esgotado</a>
            <?php endif; ?>
        </div>
        <?php if($i % 3 == 2): ?></div><div class="row-fluid"><?php endif; ?>
        <?php endforeach; ?>
    </div>
</section>
<div class="container"><hr></div>
<?php endif; ?>
<?php if($event->HasPapers): ?>
<section  class="container">
    <div class="row-fluid">
        <div class="span12">
            <h2>Chamada de Trabalhos</h2>
            <div>
                <?= $event->CallForPapers ?>
            </div>
            <strong>Os trabalhos devem ser submetidos até <?= $event->getCurrentDeadLine() ?></strong>
            <p>Não sabe como enviar seu trabalho, leia as <a href="#modal-paper-instructions" data-toggle="modal">Instruuções</a>.</p>
        </div>
    </div>
</section>
<?php if($event->MapType === 1): ?>
<section id="address" class="container">
    <div class="row-fluid">
        <div class="span12">
            <h1>Local</h1>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span8">
            <div id="MapsContainer" style="position: relative; <?= $event->MapType === 2 ? 'display: none;' : ''  ?>">
                <div id="maps"></div>
                <p class="muted">Ops, não temos um mapa disponível! =(</p>
            </div>
        </div>
        <div class="span4">
            <p><?= $event->getAddress() ?></p>
        </div>
        <script>
            var latitude = <?= $event->Latitude ?>;
            var longitude = <?= $event->Longitude ?>;
        </script>
    </div>
</section>
<?php endif; ?>
<?php endif; ?>

<div id="modal-paper-instructions" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Instruções para Submissão de Artigo</h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="span12">
                <p>Para submeter artigos você deve iniciar uma inscrição (não é necessário confirmá-la) e acessar o <a href="~/user/login" target="_blank">EventMania</a>.</p>
                <p>Após o entrar no EventMania você poderá acessar a área <a href="~/paper/" target="_blank">Artigos</a> e visualizar os eventos aos quais você pode enviar trabalhos. Para enviar seu artigo basta selecionar o evento desejado.</p>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0);" class="btn btn-primary" data-dismiss="modal">Entendi!</a>
    </div>
</div>
