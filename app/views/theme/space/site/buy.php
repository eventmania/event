<?= Import::view(array('event' => $event, 'workshops' => $workshops, 'tickets' => $tickets), '_snippet', 'small-header') ?>
<section class="container">
	<div class="row-fluid">
		<div class="span8">
			<div class="alert alert-block alert-info">
				<div id="timer"><?= Booked::TIME ?>:00</div>
					<small>Sua inscrição foi reservada, você tem este tempo para preencher as informações abaixo. Se o tempo terminar, terá que fazer uma nova inscrição.</small>
				</div>
				<form method="POST" action="" id="buy">
					<fieldset>
						<input type="hidden" id="TicketPrice" value="<?= $total ?>">
						<?= BForm::input('Nome Completo', 'Name', null, 'span8 name', array(), true) ?>
						<div class="row-fluid">
							<div class="span4">
								<?= BForm::input('E-mail', 'Email', null, 'span12 email', array(), true) ?>
							</div>
							<div class="span4">
								<?= BForm::input('Telefone', 'Card_Phone', null, 'span12 phone', array(), true) ?>
							</div>
						</div>
						<div class="sandbox"><?= $form->render() ?></div>
					</fieldset>

					<?php if($total > 0): ?>
		<fieldset>
			<legend>Meio de Pagamento</legend>

			<ul class="nav nav-tabs" id="payment-tab">
				<li class="active"><a href="#credito" data-toggle="tab">Cartão de Crédito</a></li>
				<li><a href="#tef" data-toggle="tab">Transferência</a></li>
				<li><a href="#boleto" data-toggle="tab">Boleto</a></li>
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="credito">
					<label>Selecione a bandeira do cartão:</label>
					<div class="payment-box">
						<label class="payment-method active">
							<input type="radio" name="Payment_Method" value="cartao_visa" checked="checked"> <img src="~/theme/default/img/payment/visa.png" alt="Visa">
							<span class="check badge badge-success"><i class="icon-ok icon-white"></i></span>
							Visa
						</label>
						<label class="payment-method">
							<input type="radio" name="Payment_Method" value="cartao_master"> <img src="~/theme/default/img/payment/master.png" alt="Master Card">
							MasterCard
						</label>
						<label class="payment-method">
							<input type="radio" name="Payment_Method" value="cartao_amex"> <img src="~/theme/default/img/payment/amex.png" alt="American Express">
							American Express
						</label>
						<label class="payment-method">
							<input type="radio" name="Payment_Method" value="cartao_diners"> <img src="~/theme/default/img/payment/diners.png" alt="Diners Club">
							Diners Club
						</label>
						<label class="payment-method">
							<input type="radio" name="Payment_Method" value="cartao_elo"> <img src="~/theme/default/img/payment/elo.png" alt="Elo">
							Elo
						</label>
					</div>
					<div>
						<div id="Payment_Card">
							<div class="row-fluid">
								<div class="span5">
									<?= BForm::input('Número do Cartão', 'Card_Number', null, 'span12 number', array('autocomplete' => 'off'), true) ?>
								</div>
								<div class="span3">
									<div class="control-group">
										<label class="control-label" for="Card_Month">Data de validade <span class="required">*</span></label>
										<div class="controls">
											<div class="row-fluid">
												<div class="span6">
													<input type="text" name="Card_Month" id="Card_Month" class="span12 number required" placeholder="MM" maxlength="2" autocomplete="off">
												</div>
												<div class="span6">
													<input type="text" name="Card_Year" id="Card_Year" class="span12 number required" placeholder="AA" maxlength="2" autocomplete="off">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span4">
									<?= BForm::input('CPF (dono do cartão)', 'Card_CPF', null, 'span12 cpf', array('autocomplete' => 'off'), true) ?>
								</div>
								<div class="span4">
									<?= BForm::input('Código de Segurança <i class="icon-question-sign" id="cvv-help"></i>', 'Card_CVV', null, 'span12 number', array('autocomplete' => 'off', 'maxlength' => 4), true) ?>
									<div id="cvv-help-content">
										<img src="~/theme/default/img/cvv.png" alt="">
										Informe os três últimos números localizados no verso do cartão.
									</div>
								</div>
							</div>
							<div class="row-fluid">
								<div class="span5">
									<?= BForm::input('Nome (como escrito no cartão)', 'Card_Name', null, 'span12 name', array('autocomplete' => 'off'), true) ?>
								</div>
								<div class="span3">
									<?= BForm::select('Parcelamento', 'Card_Installment', array('Calculando...'), null, 'span12', array(), true) ?>
								</div>
							</div>
						</div>
					</div>
					<button class="btn btn-large btn-warning finish" type="submit">Confirmar Pagamento</button>
				</div>
				<div class="tab-pane" id="tef">
					<label>Selecione seu banco:</label>
					<div class="payment-box">
						<label class="payment-method">
							<input type="radio" name="Payment_Method" value="tef_itau"> <img src="~/theme/default/img/payment/itau.png" alt="Itaú">
							Itaú
						</label>
						<label class="payment-method">
							<input type="radio" name="Payment_Method" value="tef_bradesco"> <img src="~/theme/default/img/payment/bradesco.png" alt="bradesco">
							Bradesco
						</label>
						<label class="payment-method">
							<input type="radio" name="Payment_Method" value="tef_bb"> <img src="~/theme/default/img/payment/bb.png" alt="Banco do Brasil">
							Banco do Brasil
						</label>
					</div>
					<button class="btn btn-large btn-warning finish" type="submit">Acessar Página do Banco</button>
				</div>
				<div class="tab-pane" id="boleto">
					<label>O pedido será confirmado somente após a aprovação do pagamento:</label>
					<div class="payment-box">
						<label class="payment-method">
							<input type="radio" name="Payment_Method" value="boleto"> <img src="~/theme/default/img/payment/boleto.png" alt="Boleto">
							Boleto
						</label>
					</div>
					<button class="btn btn-large btn-warning finish" type="submit">Gerar Boleto</button>
				</div>
			</div>
		</fieldset>
		<?php else: ?>
		<button class="btn btn-large finish btn-warning">Finalizar Inscrição</button>
		<?php endif ?>
				</form>
				<script>
					var SECONDS = '<?= $booked->getRemainder() ?>';
				</script>
			</div>
			<div class="span4">
				<h2>Resumo da Compra</h2>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>Descrição</th>
							<th>Valor</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($cart as $item): ?>
						<tr>
							<td><?= $item->getCurrent()->getTitle() ?></td>
							<td><?= $item->getSalePrice() ?></td>
						</tr>
						<?php endforeach ?>
						<tr class="total">
							<td>Total a Pagar</td>
							<td><?= Format::money($total) ?></td>
						</tr>
					</tbody>
				</table>

				<?= Import::view(array('event' => $event), '_snippet', 'sidebar') ?>
			</div>

	</div>
</section>
