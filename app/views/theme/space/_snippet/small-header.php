<header itemscope="" itemtype="http://schema.org/WPHeader" style="background-image: url('<?= $event->getLayout()->Cover ? $event->getLayout()->Cover->getImageUrl(2000, 768) : ImageService::getPlaceholderImage(2000, 768) ?>');">
    <div class="navbar navbar-inverse navbar-static-top">
        <div class="navbar-inner">
            <nav class="container" itemscope="" itemtype="http://schema.org/SiteNavigationElement">
                <div class="nav-collapse collapse">
                    <ul class="nav main-nav">
                        <li>
                            <a href="<?= $event->getUrl() ?>" class="logo-link">
                                <img src="<?= $event->getImageUrl(100, 40); ?>">
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li><?= $event->Name ?></li>
                    </ul>

                    <ul class="nav pull-right">
                        <li><a href="<?= $event->getUrl() ?>#tickets">Inscrições</a></li>
                        <?php if($workshops->Count): ?><li><a href="<?= $event->getUrl() ?>#workshops">Workshops</a></li><?php endif; ?>
                        <?php if($event->MapType === 1): ?><li><a href="<?= $event->getUrl() ?>#address">Local</a></li><?php endif; ?>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
<?= FLASH ?>
