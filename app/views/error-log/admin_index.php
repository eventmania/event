<?= Import::view(array('user' => $user, 's' => 5), '_snippet', 'admin-header'); ?>
<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Logs de Erro</h2>
		<form class="input-append pull-right controls-inline" method="GET" action="">
			<input type="text" name="q" value="<?= $q ?>" placeholder="" />
			<button type="submit" class="btn btn-primary">Pesquisar</button>
		</form>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="span1">#</th>
					<th class="span5">Nome</th>
					<th class="span3">Data</th>
					<th class="">Evento</th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $log): ?>
						<tr>
							<td><?= $log->Id ?></td>
							<td><a href="~/admin/error-log/view/<?= $log->Id ?>"><?= $log->Name ?></a></td>
							<td><?= $log->getDate() ?></td>
							<td><?= $log->EventId ?></td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="4" class="text-center">
							<p>Nenhum log de erro foi registrado! :D</p>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('admin/error-log/index', $model->Count, $p, $m) ?>
	</div>
</div>