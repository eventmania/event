<?= Import::view(array('user' => $user, 's' => 5), '_snippet', 'admin-header'); ?>
<div class="row-fluid">
	<div class="span12">
		<h2>Detalhes de Logs de Erro</h2>
		<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<th class="span3">Campo</th>
					<th>Descrição</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>ID</td>
					<td>#<?= $model->Id ?></td>
				</tr>
				<tr>
					<td>ID do Evento</td>
					<td><?= $model->EventId ?></td>
				</tr>
				<tr>
					<td>Data</td>
					<td><?= $model->getDate() ?></td>
				</tr>
				<tr>
					<td>IP</td>
					<td><?= htmlentities($model->IP) ?></td>
				</tr>
				<tr>
					<td>Navegador</td>
					<td><?= htmlentities($model->Browser) ?></td>
				</tr>
				<tr>
					<td>Participante</td>
					<td><?= $model->Name ?></td>
				</tr>
				<tr>
					<td>E-mail</td>
					<td><?= $model->Email ?></td>
				</tr>
				<tr>
					<td>Telefone</td>
					<td><?= $model->Phone ?></td>
				</tr>
				<tr>
					<td>Mensagem</td>
					<td><?= $model->Message ?></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>