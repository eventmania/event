<?= Import::view(array('s' => 3, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span4">
		<h2>Receita</h2>
		<div class="row-fluid">
			<table class="table table-bordered table-summary span12">
				<tbody>
					<tr>
						<td>
							<dl class="dl-horizontal">

								<dt>Vendas pelo Site:</dt>
								<dd><?= $event->getSiteCashier()->getGrossIncome(); ?></dd>											

								<div class="dl-horizontal-subitem">

									<dt>(-) Taxas:</dt>
									<dd><?= $event->getSiteCashier()->getIncomeFees(); ?></dd>
									
									<dt>TOTAL:</dt>
									<dd><?= $event->getSiteCashier()->getNetIncome(); ?></dd>
								
									<dt>A Receber:</dt>
									<dd><?= $event->getSiteCashier()->getRemaining(); ?></dd>

									<dt>Disponível:</dt>
									<dd><?= $event->getSiteCashier()->getCashOnHand(); ?></dd>

									<dt>Saque em Andamento:</dt>
									<dd><?= $event->getSiteCashier()->getWithdrawn(); ?></dd>

									<dt>Sacado:</dt>
									<dd><?= $event->getSiteCashier()->getWithdrawnCompleted(); ?></dd>
								</div>
								
								<dt>&nbsp;</dt><dd>&nbsp;</dd>

								<dt>Vendas Manuais:</dt>
								<dd><?= $event->getManualCashier()->getNetIncome(); ?></dd>
														
								<div class="dl-horizontal-subitem">
									<?php foreach($event->allManagers() as $manager): ?>
									<dt><?= $manager->Name ?>:</dt>
									<dd><?= $event->getManualCashier()->getManagerIncome($manager->UserId) ?></dd>
									<?php endforeach; ?>
								</div>
								
								<dt>&nbsp;</dt><dd>&nbsp;</dd>

								<dt>TOTAL:</dt>
								<dd><?= $event->getGrossIncome(); ?></dd>

								<dt>TOTAL Lq.:</dt>
								<dd><?= $event->getNetIncome(); ?></dd>
							</dl>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		
		<?php if($event->isPaid()): ?>
		<a name="transfer"></a>
		<h2>Repasse</h2>
		<div id="transfer-container">
			<?php if($event->getSiteCashier()->getCashOnHand(true) > 0): ?>
			<table class="table table-bordered table-striped table-summary">
				<tbody>
					<tr>
						<td>Faça saques do EventMania para sua conta bancária.</td>
					</tr>
					<tr>
						<td>
							<div class="text-center">
								<a  href="#modal-accounts" role="button" data-toggle="modal" class="btn btn-primary">Solicitar Saque de Repasse</a>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
			<?php endif ?>

			<?php if($transfer): ?>
			<?= Import::view(array('account' => $transfer), 'sale', 'account') ?>			
			<?php endif ?>
		</div>
		<?php endif ?>
	</div>
	<div class="span8">
		<h2 class="pull-left">Vendas</h2>
		
		<form method="GET" action="~/sale/index/<?= $event->Id ?>/" class="myform-search pull-right">
			<div class="input-append">
				<input type="text" name="q" value="<?= $q ?>" placeholder="Nome do comprador">
				<button class="btn" type="submit"><i class="icon-search"></i> Buscar</button>
			</div>
		</form>
		
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="span1">Nº</th>
					<th>Data</th>
					<th>Comprador</th>
					<th>Valor Lq.</th>
					<th>Taxa</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count):?>
				<?php foreach ($model->Data as $s):?>
				<tr>
					<td>
						<a href="#saleModal" class="btn-details" role="button" data-toggle="modal"><?= $s->getNumber() ?></a>
						<div class="hide">
							<div class="row-fluid">
								<div class="span12">
									<div class="well well-small">
										<p class="pull-right">
											<span class="label <?= $s->getStatusLabel() ?>"><?= $s->getStatus() ?></span>
										</p>
										<p>Nome do Comprador:</p>
										<p><b><?= Format::capitalize($s->Name) ?></b></p>
										<p><?= $s->Email ?></p>
									</div>
								</div>
							</div>
							<h4><strong>Resumo da Compra</strong></h4>
							<?= Import::view(array('model' => $s, 'date' => false, 'event' => $event), 'sale', 'summary'); ?>
						</div>
					</td>
					<td><?= $s->getDate() ?></td>
					<td><?= Format::capitalize($s->Name) ?></td>
					<td><?= $event->newCart(array($s))->getNetPrice() ?></td>
					<td><?= $event->lastCart()->getFee() ?></td>
					<td>
						<?= $event->lastCart()->getSalePrice() ?>
						<?php if($s->AuthorId == Participant::SITE_AUTHOR_ID && $s->Absorb): ?>
						<span class="required">*</span>
						<?php endif; ?>
					</td>
				</tr>
				<?php endforeach;?>
				<?php else:?>
				<tr>
					<?php if(!$q):?>
					<td colspan="6" class="text-center">Ainda não foi realizada nenhuma venda. Chame a atenção dos participantes, <a href="~/event/about/<?= $event->Id ?>/#share" class="btn btn-primary">Compartilhe seu Evento</a></td>
					<?php else:?>
					<td colspan="6" class="text-center">Nenhum resultado encontrado para sua busca. <a href="~/sale/index/<?= $event->Id ?>" class="btn btn-primary">Limpar Busca!</a></td>
					<?php endif;?>
				</tr>
				<?php endif;?>
			</tbody>
		</table>
		<?php if($event->Absorb && $event->getSiteCashier()->hasSales()): ?>
		<span class="required">*</span> As taxas dos ingressos que serão pagas ao EventMania estão inclusas nestes valores.
		<?php endif; ?>
		<?= Pagination::create('sale/index/' . $id, $model->Count, $p, $m); ?>
	</div>
</div>

<div id="saleModal" class="modal modal-large hide fade" tabindex="-1" role="dialog" aria-labelledby="saleModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h2 id="myModalLabel">Detalhes da Venda</h2>
	</div>
	<div class="modal-body">
		
	</div>
</div>


<div id="modal-accounts" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal-accounts-label" aria-hidden="true">
	<div class="modal-header">
		<h3 id="modal-accounts-label">Conta para Repasse</h3>
	</div>
	<div class="modal-body">
		<?php if(count($accounts)): ?>
		<form>
			<table class="table table-striped table-bordered table-small">
				<thead>
					<tr>
						<th></th>
						<th>Destino</th>
						<th>Detalhes</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($accounts as $ac): ?>
					<tr>
						<td><input type="radio" class="Account" name="Account" value="<?= $ac->Id ?>"></td>
						<td><?= $ac->getBank() ?></td>
						<td><?= $ac->getDetails() ?></td>
					</tr>
					<?php endforeach ?>
				</tbody>
			</table>

			<div class="row-fluid">
				<div class="span3">
					<div class="control-group">
						<label class="control-label" for="Value">Valor do Saque <span class="required">*</span></label>
						<div class="controls">
							<input name="Value" id="Value" value="" type="text" class="span12 required money" maxValue="<?= $event->getSiteCashier()->getCashOnHand(true); ?>">
						</div>
					</div>
				</div>
				<div class="span4"></div>
			</div>
		</form>
		<?php else: ?>
		<p class="text-center">
			<a href="~/account/add" class="btn btn-large btn-primary">Cadastre uma Conta Bancária!</a>
		</p>
		<?php endif ?>
	</div>
	<?php if(count($accounts)): ?>
	<div class="modal-footer">
		<button class="btn btn-primary" id="transfer-select" event-id="<?= $id ?>">Continuar</button>
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
	</div>
	<?php endif ?>
</div>