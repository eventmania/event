<table class="table table-bordered table-striped table-summary">
	<tbody>
		<tr>
			<td>Você pode acompanhar os repasses em andamento e ver histórico de repasses anteriores.</td>
		</tr>
		<tr>
			<td>
				<div class="text-center">
					<a  href="~/transfer/" role="button" data-toggle="modal" class="btn">Ver Histórico</a>
				</div>
			</td>
		</tr>
	</tbody>
</table>