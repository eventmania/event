<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Participante</th>
            <th>Tipo</th>
            <?php if($date): ?>
            <th>Data</th>
            <?php endif; ?>
            <th>Valor</th>
            <th>Taxa</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>
        <?php $cart = $event->newCart(array($model)) ?>
        <?php foreach ($cart as $key => $item): ?>
        <tr>
            <td><?= htmlentities(Format::capitalize($model->Name)) ?></td>
            <td><?= htmlentities($item->getCurrent()->getName()) ?></td>
            <?php if($date): ?>
            <td><?= $model->getDate() ?></td>
            <?php endif; ?>
            <td><?= $item->getNetPrice() ?></td>
            <td><?= $item->getFee() ?></td>
            <td>
                <?= $item->getSalePrice() ?>
                <?php if($model->AuthorId == Participant::SITE_AUTHOR_ID && $model->Absorb): ?>
                <span class="required">*</span>
                <?php endif; ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    <tfoot>
        <tr>
            <th colspan="<?= $date ? '4' : '3'; ?>">&nbsp;</th>
            <th>Total</th>
            <th>
                <?= $event->lastCart()->reset()->getSalePrice() ?>
                <?php if($model->AuthorId == Participant::SITE_AUTHOR_ID && $model->Absorb): ?>
                <span class="required">*</span>
                <?php endif; ?>
            </th>
        </tr>
    </tfoot>
</table>
<?php if($model->AuthorId == Participant::SITE_AUTHOR_ID && $model->Absorb): ?>
<span class="required">*</span> As taxas que ser&atilde;o pagas ao EventMania est&atilde;o inclusas nestes valores.
<?php endif; ?>