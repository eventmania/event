<?= Import::view(array('user' => $model, 's' => 0), '_snippet', 'admin-header'); ?>
<div class="row-fluid">
    <div class="span6">
        <h2>Notificações</h2>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <?php if($hasPendingEmails): ?>
        <div class="alert">
            <strong>E-mails Diários!</strong>
            Os e-mails de hoje ainda não foram enviados. Deseja enviá-los?
            <a href="~/admin/send-daily-mails/">Enviar E-mails</a>
        </div>
        <?php else: ?>
        <div class="alert alert-info">
            <strong>Parabéns!</strong>
            Parece que você leu todas as suas notificações.
        </div>
        <?php endif; ?>
    </div>
</div>