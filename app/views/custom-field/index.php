<?= Import::view(array('s' => 0, 'event' => $model), '_snippet', 'event-header') ?>
<div class="row-fluid">
    <div class="span8">
        <h2>Configurar Formulário de Inscrição</h2>

        <form id="wysiwyg-form" method="POST" action="" class="no-validate">
            <fieldset>
                <?= BForm::input('Nome', 'Name', null, 'span8', array(), true) ?>
                <?= BForm::input('Email', 'Email', null, 'span8 email', array(), true) ?>
                <div class="sandbox"><?= $form->render() ?></div>
            </fieldset>
            <fieldset id="real-fields-form" class="form-actions2">
                <button class="btn btn-primary" on-click="javascritpt:void(0);">Salvar Formulário</button>
                <a href="~/event/about/<?= $id ?>" class="pull-right">Cancelar</a>
            </fieldset>
        </form>
    </div>
    <div class="span4">
        <?= Import::view(array(
            'hasResponsability' => $form->hasResponsability()
        ), 'custom-field/modal', 'bootstrap') ?>
    </div>
</div>
<script type="text/javascript">
    var CustomFieldIndex = <?= $lastIndex ?>;
</script>