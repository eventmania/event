<div class="row-fluid snippet"><div class="span8">
    <div class="span12 control-group">
        <label class="checkbox">
            <input type="checkbox" name="<?= $model->getName() ?>" class="required" value=""> Concordo com os <a href="#modal-terms1" data-toggle="modal">Termos de Compromisso</a> deste evento.
            <input type="hidden" value="" name="<?= $model->getName() ?>">
        </label>
    </div>
</div>
<div id="modal-terms1" class="modal modal-custom-field hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Termos de Compromisso</h3>
    </div>
    <div class="modal-body row-fluid">
        <div class="span12">
            <p class="terms">
                <?= $model->getContent() ?>
            </p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0);" class="btn btn-primary" data-dismiss="modal">Entendi!</a>
    </div>
</div></div>