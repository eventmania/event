<a href="#modal-custom-field-text" data-toggle="modal" class="btn btn-block" style="text-align: left; padding-left: 10px;">
	<img src="~/img/form/text.png" alt=""> Texto
</a>
<a href="#modal-custom-field-number" data-toggle="modal" class="btn btn-block" style="text-align: left; padding-left: 10px;">
	<img src="~/img/form/number.png" alt=""> Número
</a>
<a href="#modal-custom-field-select" data-toggle="modal" class="btn btn-block" style="text-align: left; padding-left: 10px;">
	<img src="~/img/form/list.png" alt=""> Lista (Dropdown)
</a>
<a href="#modal-custom-field-radio" data-toggle="modal" class="btn btn-block" style="text-align: left; padding-left: 10px;">
	<img src="~/img/form/check.png" alt=""> Múltipla Escolha (Radio button)
</a>
<a href="#modal-custom-field-date" data-toggle="modal" class="btn btn-block" style="text-align: left; padding-left: 10px;">
	<img src="~/img/form/date.png" alt=""> Data e Hora
</a>
<a href="#modal-custom-field-cpf" data-toggle="modal" class="btn btn-block" style="text-align: left; padding-left: 10px;">
	<img src="~/img/form/cpf.png" alt=""> CPF/CNPJ
</a>
<a href="#modal-custom-field-responsability" data-toggle="modal" class="btn btn-block <?= $hasResponsability ? 'hide' : '' ?>" style="text-align: left; padding-left: 10px;">
	<img src="~/img/form/term.png" alt=""> Termos Compromisso
</a>
<a href="#modal-custom-field-phone" data-toggle="modal" class="btn btn-block" style="text-align: left; padding-left: 10px;">
	<img src="~/img/form/phone.png" alt=""> Telefone
</a>
<?= Import::view(array(), 'custom-field/modal', 'text') ?>
<?= Import::view(array(), 'custom-field/modal', 'number') ?>
<?= Import::view(array(), 'custom-field/modal', 'date') ?>
<?= Import::view(array(), 'custom-field/modal', 'cpf') ?>
<?= Import::view(array(), 'custom-field/modal', 'responsability') ?>
<?= Import::view(array(), 'custom-field/modal', 'select') ?>
<?= Import::view(array(), 'custom-field/modal', 'radio') ?>
<?= Import::view(array(), 'custom-field/modal', 'phone') ?>
