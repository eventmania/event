<form id="modal-custom-field-responsability" class="modal modal-custom-field hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Adicionar Termos de Compromisso</h3>
    </div>
    <div class="modal-body row-fluid">
        <div class="span11">
            <div class="row-fluid">
                <div class="span12">
                    <label class="control-label">Texto <span class="required">*</span></label>
                    <div class="control-group">
                        <textarea class="span12 Content" name="Content[]"></textarea>
                    </div>
                    <input name="Index[]" class="Index" type="hidden">
                    <input name="IsRequired[]" value="" class="IsRequired" type="hidden">
                    <input name="Type[]" class="Type" value="Responsability" type="hidden">
                    <input name="Title[]" class="Title" value="Termos de Compromisso" type="hidden">
                </div>
            </div>
            <div class="row-fluid snippet hide">
                <div class="span8">
                    <button type="button" class="btn btn-mini btn-danger btn-close"><i class="icon-white icon-remove"></i></button>
                    <label class="checkbox">
                        <input type="checkbox"> Concordo com os <a href="#modal-terms" data-toggle="modal">Termos de Compromisso</a> deste evento.
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0);" id="add-field" class="btn btn-primary" data-dismiss="modal">Adicionar</a>
        <a href="javascript:void(0);" class="btn" data-dismiss="modal">Cancelar</a>
    </div>
</form>
<div id="modal-terms" class="modal modal-custom-field hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Termos de Compromisso</h3>
    </div>
    <div class="modal-body row-fluid">
        <div class="span12">
            <p class="terms"></p>
        </div>
    </div>
    <div class="modal-footer">
        <a href="javascript:void(0);" class="btn btn-primary" data-dismiss="modal">Entendi!</a>
    </div>
</div>