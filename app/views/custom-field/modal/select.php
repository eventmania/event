<form id="modal-custom-field-select" class="modal modal-custom-field hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Adicionar Campo</h3>
    </div>
    <div class="modal-body row-fluid">
        <div class="span11">
            <div class="row-fluid">
                <div class="span12">
                    <label class="control-label">Título do Campo <span class="required">*</span></label>
                    <div class="control-group">
                        <div class="controls">
                            <input name="Title[]" type="text" class="Title span12 required" placeholder="Exemplo: Instituição, CPF, Cidade">
                        </div>
                    </div>
                    <input name="Index[]" class="Index" type="hidden">
                    <input name="IsRequired[]" class="IsRequired" type="hidden">
                    <input name="Type[]" class="Type" value="Select" type="hidden">
                    <input name="Content[]" class="Content" value="" type="hidden">
                </div>
            </div>
            <div class="row-fluid">
                <div class="control-group type span6">
                    <label class="control-label">Opções</label>
                    <div class="controls">
                        <input name="Option" type="text" class="Option span12" >
                    </div>
                </div>
                <div class="control-group required span3">
                    <label class="control-label">&nbsp;</label>
                    <div class="controls">
                        <button type="button" class="btn add-btn">Adicionar</button>
                    </div>
                </div>
                <div class="control-group required span3">
                    <label class="control-label">Obrigatório <span class="required">*</span></label>
                    <div class="controls">
                        <div class="btn-group" data-toggle="buttons-radio">
                            <button type="button" value="1" class="btn">Sim</button>
                            <button type="button" value="0" class="btn active">Não</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid"><ul class="content"></ul></div>
            <div class="row-fluid snippet hide">
                <div class="span8 control-group">
                    <button type="button" class="btn btn-mini btn-danger btn-close"><i class="icon-white icon-remove"></i></button>
                    <label class="control-label"><span class="field-label"></span>&nbsp;<span class="required hide">*</span></label>
                    <div class="controls">
                        <select class="Content span12"></select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
		<a href="javascript:void(0);" id="add-field" class="btn btn-primary" data-dismiss="modal">Adicionar</a>
        <a href="javascript:void(0);" class="btn" data-dismiss="modal">Cancelar</a>
    </div>
</form>