<form id="modal-custom-field-date" class="modal modal-custom-field hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Adicionar Campo de Data</h3>
    </div>
    <div class="modal-body row-fluid">
        <div class="span11">
            <div class="row-fluid">
                <div class="span12">
                    <label class="control-label">Título do Campo <span class="required">*</span></label>
                    <div class="control-group">
                        <div class="controls">
                            <input name="Title[]" type="text" class="Title span12 required" placeholder="Exemplo: Data de Nascimento">
                        </div>
                    </div>
                    <input name="Index[]" class="Index" type="hidden">
                    <input name="IsRequired[]" class="IsRequired" type="hidden">
                    <input name="Type[]" class="Type" value="Date" type="hidden">
                    <input name="Content[]" class="Content" value="" type="hidden">
                </div>
            </div>
            <div class="row-fluid">
                <div class="control-group required span4">
                    <label class="control-label">Obrigatório <span class="required">*</span></label>
                    <div class="controls">
                        <div class="btn-group" data-toggle="buttons-radio">
                            <button type="button" value="1" class="btn btn-small">Sim</button>
                            <button type="button" value="0" class="btn btn-small active">Não</button>
                        </div>
                    </div>
                </div>
                <div class="control-group type span8">
                    <label class="control-label">Formato</label>
                    <div class="controls">
                        <div class="btn-group" data-toggle="buttons-radio">
                            <button type="button" value="Date" class="btn btn-small active">Apenas Data</button>
                            <button type="button" value="Time" class="btn btn-small">Apenas Horário</button>
                            <button type="button" value="DateTime" class="btn btn-small">Data e Horário</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid snippet hide">
                <div class="span8">
                    <div class="row-fluid">
                        <div class="span9 control-group Date">
                            <button type="button" class="btn btn-mini btn-danger btn-close"><i class="icon-white icon-remove"></i></button>
                            <label class="control-label"><span class="field-label"></span>&nbsp;<span class="required hide">*</span></label>
                            <div class="controls">
                                <input value="" type="text" class="date span12">
                            </div>
                        </div>
                        <div class="span3 hide control-group Time">
                            <button type="button" class="btn btn-mini btn-danger btn-close"><i class="icon-white icon-remove"></i></button>
                            <label class="control-label hide"><span class="field-label"></span>&nbsp;<span class="required hide">*</span></label>
                            <label class="control-label show">&nbsp;</label>
                            <select style="width: 100%">
                                <option value="09:00">09:00</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
		<a href="javascript:void(0);" id="add-field" class="btn btn-primary" data-dismiss="modal">Adicionar</a>
        <a href="javascript:void(0);" class="btn" data-dismiss="modal">Cancelar</a>
    </div>
</form>