<?= Import::view(array('s' => 2, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span8">
		<h2>Sorteio de Brindes</h2>
		<div class="well">
			<p id="winnerMessage">Clique em "sortear" e veja o nome do felizardo.</p>
			<div id="winnerDetails" style="display: none;">
				<p>Parabéns:</p>
				<h1 id="winnerName"></h1>
				<p id="winnerEmail"></p>
			</div>
		</div>
		<button class="btn btn-primary" id="raffle" event-id="<?= $event->Id ?>">Sortear</button>
		<a href="~/event/about/<?= $id ?>" class="pull-right">Voltar</a>
	</div>
</div>
