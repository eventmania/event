<?= Import::view(array('s' => 2, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Check-in de Participantes</h2>
		<form method="GET" action="" class="myform-search pull-right">
			<div class="input-append">
				<input type="text" name="q" value="<?= $q ?>" placeholder="Nome do participante">
				<button class="btn" type="submit">Buscar</button>
			</div>

			<div class="btn-group" data-toggle="tooltip" title="Deixe um computador disponível para os próprios participantes fazerem check-in.">
				<a href="~/participant/auto-checkin/<?= $id ?>" class="btn">Auto Check-in</a>
			</div>
		</form>

		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="span1"></th>
					<th class="span4">Nome do Participante</th>
					<th class="span3">Ingresso</th>
					<th class="span2">Nº do Ingresso</th>
					<th class="span2">Hora da Entrada</th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $pr): ?>
					<tr>
						<td style="text-align: center;">
							<?php if($pr->CheckIn): ?>
							<a href="javascript:void(0)" class="btn btn-mini btn-success checkin" participant="<?= $pr->Id ?>" action="out" data-toggle="tooltip" title="Desfazer"><i class="icon-ok icon-white"></i></a>
							<?php else: ?>
							<a href="javascript:void(0)" class="btn btn-mini checkin" participant="<?= $pr->Id ?>" action="in" data-toggle="tooltip" title="Fazer Check-in"><i class="icon-check"></i></a>
							<?php endif ?>
						</td>
						<td><?= Format::capitalize($pr->Name) ?></td>
						<td><?= $pr->TicketName ?></td>
						<td><?= $pr->getNumber() ?></td>
						<td id="checkin-date-<?= $pr->Id ?>"><?= $pr->getCheckInDate() ?></td>
					</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<?php if(Request::get('q')): ?>
						<td colspan="5" class="text-center">Nenhum participante encontrado com <b><?= Request::get('q') ?></b>.</td>
						<?php else: ?>
						<td colspan="5" class="text-center">Ainda não foram adquiridos ingressos.</td>
						<?php endif ?>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('participant/checkin/' . $event->Id, $model->Count, $p, $m) ?>
	</div>
</div>
