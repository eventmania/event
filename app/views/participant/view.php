<?= Import::view(array('s' => 2, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span8">
		<h2>Detalhes do Participante</h2>
		<div class="well well-participant">
			<p>Nome:</p>
			<p><b><?= Format::capitalize($model->Name) ?></b></p>
			<p>E-mail:</p>
			<p><b><?= $model->Email ?></b></p>
			<p>&nbsp;</p>
			<?php if($model->isConfirmed()): ?>
				<a href="~/participant/edit/<?= $model->Id ?>" class="btn">Editar</a>
				<a href="~/participant/resend/<?= $model->Id ?>" class="btn">Reenviar Ingresso</a>
				<?php if($user->isAdmin($event->Id) && !$model->isCanceled() && $model->AuthorId != 0): ?>
				<a href="#cancelModal" role="button" data-toggle="modal" class="btn btn-danger">Cancelar Venda</a>
				<?php endif; ?>
			<?php endif ?>
			<a href="~/participant/index/<?= $event->Id ?>" class="pull-right">Voltar</a>
		</div>
	</div>
	<div class="span4">
		<h2>Ingresso</h2>
		<div class="well well-participant">
			<p class="pull-right"><span class="label <?= $model->getStatusLabel() ?>"><?= $model->getStatus() ?></span></p>
			<p>Tipo:</p>
			<p><b><?= $model->TicketName ?></b></p>
			<?php if($model->isConfirmed()): ?>
			<p>Número:</p>
			<p><b><?= $model->getNumber() ?></b></p>
			<?php endif ?>
			<p>Data da Compra:</p>
			<p><b><?= $model->getBuyDate() ?></b></p>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<h2>Resumo da Compra</h2>
		<?= Import::view(array('model' => $model, 'date' => true, 'event' => $event), 'sale', 'summary'); ?>
	</div>
</div>

<div id="cancelModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="cancelModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h2 id="myModalLabel">Atenção</h2>
	</div>
	<div class="modal-body">
		<p>Ao cancelar esta venda, todos os ingressos vinculados a ela serão cancelados e o pagamento será revertido.</p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Fechar</a>
		<a href="~/participant/cancel/<?= $model->Id ?>" class="btn btn-danger">Confirmar Cancelamento</a>
	</div>
</div>