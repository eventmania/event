<?= Import::view(array('s' => 2, 'event' => $event), '_snippet', $eventHeader) ?>
<div class="row-fluid">
	<div class="span8">
		<h2><?= $title ?></h2>
		<form method="POST" action="">
			<fieldset>
				<?= BForm::input('Nome Completo', 'Name', $model->Name, 'span12 name', array(), true) ?>
				<?= BForm::input('E-mail', 'Email', $model->Email, 'span12 email', array(), true) ?>
				<?php if($message): ?>
				<?= BForm::textarea('Mensagem', 'message', $message, 'span12', array(), true) ?>
				<?php endif; ?>
				<?php if($form): ?>
				<div class="sandbox"><?= $form->render() ?></div>
				<?php endif; ?>
			</fieldset>
			<fieldset class="row-fluid">
				<div class="span8">
					<div class="control-group">
						<label class="control-label" for="TicketId">Ingresso <span class="required">*</span></label>
						<div class="controls">
							<select name="TicketId" id="TicketId" class="span12 required">
								<?php $isOwner = $user->isAdmin($event->Id) ?>
								<option value="" data-price="0">Selecione um ingresso...</option>
								<?php foreach($tickets as $ticket): ?>
									<?php if(!$ticket->IsPrivate || $isOwner): ?>
									<option value="<?= $ticket->Id ?>" data-price="<?= $ticket->Price ?>"><?= $ticket->Name ?></option>
									<?php endif ?>
								<?php endforeach ?>
							</select>
						</div>
					</div>
					<?php if(count($workshops)): ?>
					<div class="control-group">
						<label class="control-label" for="WorkshopId">Workshop</label>
						<div class="controls">
							<select name="WorkshopId" id="WorkshopId" class="span12">
								<option value="" data-price="0">Selecione um workshop...</option>
								<?php foreach($workshops as $workshop): ?>
								<option value="<?= $workshop->Id ?>" data-price="<?= $workshop->Price ?>"><?= $workshop->Title ?></option>
								<?php endforeach ?>
							</select>
						</div>
					</div>
					<?php endif ?>
				</div>
				<div class="span4">
					<div class="control-group">
						<label class="control-label" for="TicketId">Valor da Compra</label>
						<div class="controls">
							<div id="participant-value">R$ 00,00</div>
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="row-fluid">
				<div class="span12">
					<button type="submit" class="btn btn-primary"><?= $btnLabel ?></button>
					<a href="~/participant/index/<?= $event->Id ?>" class="pull-right">Cancelar</a>
				</div>
			</fieldset>
		</form>
	</div>
</div>
