<?= Import::view(array('s' => 2, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span12">
		<h2>Editar Participante</h2>
		<form method="POST" action="">
			<fieldset>
				<?= BForm::input('Nome', 'Name', $model->Name, 'span8', array(), true) ?>
				<?= BForm::input('E-mail', 'Email', $model->Email, 'span8', array(), true) ?>
				<div class="sandbox"><?= $form->render() ?></div>
			</fieldset>
			<div class="row-fluid">
				<div class="span8">
					<button type="submit" class="btn btn-primary">Salvar</button>
					<a href="~/participant/view/<?= $id ?>" class="pull-right">Cancelar</a>
				</div>
			</div>
		</form>
	</div>
</div>
