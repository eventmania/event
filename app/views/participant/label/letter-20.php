<!DOCTYPE html>
<html>
	<head>
		<title>Etiquetas</title>
		<style>
			html {margin: 0px; margin-top: 45px; padding: 0;}
			body {font-family: 'Helvetica'; font-size: 12px; margin: 0; padding: 0;}
			h1 {font-size: 18px; margin: 0; padding: 0;}
			h2 {font-size: 18px; margin: 0; padding: 0; color: #888;}
			table {width: 100%; border-spacing: 15px 0;}
			table td {border: 1px dotted #fff; padding: 5px; height: 85px; text-align: center;}
			.name {font-size: 12px; line-height: 12px; text-transform: capitalize;}
			.number {font-size: 9px; line-height: 9px; padding: 0; margin: 0; margin-top: 10px; color: #333;}
		</style>
	</head>
	<body>
		<table>
			<tbody>
				<?php $count = count($participations->Data) ?>
				<?php foreach ($participations->Data as $i => $p): ?>
					<?php if($i % 2 == 0): ?>
					<tr>
					<?php endif ?>
						<td>
							<div class="name"><?= htmlentities(strtolower($p->Name)) ?></div>
							<div class="number"><?= $p->getNumber() ?></div>
						</td>
					<?php if($i + 1 % 2 == 0 || $i == $count - 1): ?>
					</tr>
					<?php endif ?>
				<?php endforeach ?>
			</tbody>
		</table>
	</body>
</html>