<?= Import::view(array('s' => 2, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Participantes</h2>
		<form method="GET" action="" class="myform-search pull-right">
			<div class="input-append">
				<input type="text" name="q" value="<?= $q ?>" placeholder="Nome do participante">
				<button class="btn" type="submit"><i class="icon-search"></i> Buscar</button>
			</div>
			<div class="btn-group">
				<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-cog"></i><span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="~/participant/add/<?= $id ?>">Adicionar Participante</a></li>
					<li><a href="~/participant/invite/<?= $event->Id ?>">Convidar</a></li>
					<li class="divider"></li>
					<li><a href="#modal-export" data-toggle="modal">Imprimir (PDF)</a></li>
					<li><a href="~/participant/excel/<?= $id ?>" target="_blank">Exportar (Excel)</a></li>
					<li><a href="~/participant/checkin/<?= $id ?>">Check-in</a></li>
					<li><a href="#modal-label" data-toggle="modal">Etiquetas</a></li>
				</ul>
			</div>
		</form>

		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Nome do Participante</th>
					<th>Ingresso</th>
					<th class="span3">Vendedor</th>
					<th class="span3">Data da Compra</th>
					<th class="span1">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $pr): ?>
					<tr>
						<td><a href="~/participant/view/<?= $pr->Id ?>"><?= Format::capitalize($pr->Name) ?></a></td>
						<td><?= $pr->TicketName ?></td>
						<td><?= Format::capitalize($pr->AuthorName) ?></td>
						<td><?= $pr->getBuyDate() ?></td>
						<td><span class="label <?= $pr->getStatusLabel() ?>"><?= $pr->getStatus() ?></span></td>
					</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<?php if(Request::get('q')): ?>
						<td colspan="5" class="text-center">Nenhum participante encontrado com <b><?= Request::get('q') ?></b>.</td>
						<?php else: ?>
						<td colspan="5" class="text-center">Ainda não foram comprados ingressos. Chame a atenção dos participantes, <a href="~/event/about/<?= $event->Id ?>/#share" class="btn btn-primary">Compartilhe seu Evento</a></td>
						<?php endif ?>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('participant/index/' . $event->Id, $model->Count, $p, $m) ?>
	</div>
</div>
<form id="modal-export" class="modal hide fade" action="~/participant/pdf/<?= $id ?>" method="POST" target="_blank">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3>Informações para Exportação</h3>
    </div>
    <div class="modal-body">
    	<div class="row-fluid">
			<div class="span3 control-group">
				<?= BForm::select('Ingresso', 'TicketFilter', $tickets); ?>
		    </div>
			<div class="span3 control-group">
				<?= BForm::select('Workshop', 'WorkshopFilter', $workshops); ?>
		    </div>
			<div class="span3 control-group">
				<?= BForm::select('Vendedor', 'AuthorFilter', $managers); ?>
		    </div>
			<div class="span3 control-group">
				<?= BForm::select('Status', 'StatusFilter', $status); ?>
		    </div>
    	</div>
    	<div class="row-fluid">
			<div class="span12 control-group">
		        <label class="control-label">
		        	Participante
		        </label>
		        <div class="controls">
		            <div class="btn-group" data-toggle="buttons-checkbox">
		                <button type="button" name="SelectedInfo[]" value="Name" class="btn btn-small">Nome</button>
		                <button type="button" name="SelectedInfo[]" value="Email" class="btn btn-small">Email</button>
		                <button type="button" name="SelectedInfo[]" value="Sign" class="btn btn-small">Espaço p/ Assinatura</button>
		                <button type="button" name="SelectedInfo[]" value="Status" class="btn btn-small">Status</button>
		                <button type="button" name="SelectedInfo[]" value="Workshop" class="btn btn-small">Workshop</button>
		            </div>
		        </div>
		    </div>
    	</div>
	    <div class="row-fluid">
	    	<div class="span12 control-group">
		        <label class="control-label">
		        	Controle
		        </label>
		        <div class="controls">
		            <div class="btn-group" data-toggle="buttons-checkbox">
		                <button type="button" name="SelectedInfo[]" value="TicketName" class="btn btn-small">Ingresso</button>
		                <button type="button" name="SelectedInfo[]" value="AuthorName" class="btn btn-small">Vendedor</button>
		                <button type="button" name="SelectedInfo[]" value="Number" class="btn btn-small">N&ordm; Ingresso</button>
		                <button type="button" name="SelectedInfo[]" value="Date" class="btn btn-small">Data da Compra</button>
		                <button type="button" name="SelectedInfo[]" value="CheckInDate" class="btn btn-small">Data do Checkin</button>
		            </div>
		        </div>
		    </div>
		</div>
		<div class="row-fluid">
	    	<div class="span12 control-group">
		        <label class="control-label">
		        	Extra
		        </label>
		        <div class="controls">
		            <div class="btn-group" data-toggle="buttons-checkbox">
					<?php $i = 0; ?>
					<?php foreach ($form->getFields() as $f): ?>
						<?php if($i && $i % 5 == 0): ?>
						</div></div></div></div>
						<div class="row-fluid">
					    	<div class="span12 control-group">
						        <div class="controls">
						            <div class="btn-group" data-toggle="buttons-checkbox">
						<?php endif; ?>
						<?php if($f->hasInformation()): $i++; ?>
			            <button type="button" name="SelectedInfo[]" value="<?= $f->getId() ?>" class="btn btn-small"><?= $f->getTitle() ?></button>
						<?php endif; ?>
					<?php endforeach; ?>
					</div>
		        </div>
		    </div>
		</div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" type="submit">Exportar</button>
        <a href="javascript:void(0);" class="btn" data-dismiss="modal">Cancelar</a>
    </div>
</form>


<!-- Modal -->
<div id="modal-label" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="labelModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="labelModalLabel">Formato de Etiqueta</h3>
	</div>
	<div class="modal-body">
		<div class="row-fluid">
			<div class="span3">
				<a href="~/participant/label/<?= $id ?>/letter-30" target="_blank">
					<img src="~/img/labels/letter-30.png" alt="">
				</a>	
				<div>Papel Carta</div>
			</div>
			<div class="span3">
				<a href="~/participant/label/<?= $id ?>/letter-20" target="_blank">
					<img src="~/img/labels/letter-20.png" alt="">
				</a>	
				<div>Papel Carta</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
	</div>
</div>