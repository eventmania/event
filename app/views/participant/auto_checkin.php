<!DOCTYPE html>
<html lang="pt-br">
    <head>
		<meta charset="utf-8">
		<title>EventMania</title>

		<?php $version = '2.0.5beta' ?>
		<meta name="description" content="A melhor ferramenta para organizar seus eventos">
		<meta name="keywords" content="gerenciador, evento, eventmania, ticket, ingresso">

		<link href="~/css/bootstrap.css?<?= $version ?>" rel="stylesheet" media="all">
		<link href="~/css/flat.css?<?= $version ?>" rel="stylesheet" media="all">
		<link href="~/css/style.css?<?= $version ?>" rel="stylesheet" media="all">

		<link rel="icon" type="image/png" href="~/img/favicon.png?2">
    </head>
    <body itemscope itemtype="http://schema.org/WebPage" class="checkin-body">
		<div class="navbar navbar-inverse navbar-static-top">
			<div class="navbar-inner" id="checkin-bar">
				<img src="~/img/logo-mini-dark.png" alt="">
			</div>
		</div>
		<div class="container checkin-container">
			<?= FLASH ?>
			<div class="row-fluid">
				<div class="row-fluid">
					<div class="span12">
						<h1><?= $event->Name ?></h1>
						<p class="muted"><?= $event->getSchedule() ?></p>
					</div>
				</div>
				<hr>
				<div class="row-fluid">
					<div class="checkin-box">
						<div class="" id="msg"></div>
						<div class="control-group">
							<div class="controls">
								<input type="text" maxlength="12" class="span12 checkin-number" value="" name="Number" id="Number" placeholder="Número do Ingresso">
								<input type="hidden" value="<?= $token ?>" id="Token">
							</div>
						</div>
						<button class="btn btn-large btn-primary" id="auto-checkin"><i class="icon-white icon-map-marker"></i> Check-in</button>
					</div>
				</div>
			</div>
		</div>
		<script>
			var ROOT = '~/';
			var FB_APPID = '<?= Config::get('facebook_appid') ?>';
			var TIME = <?= Config::get('session_time') ?>;
		</script>
		<script src="~/js/vendor/jquery.js?<?= $version ?>"></script>
		<script src="~/js/vendor/bootstrap.js?<?= $version ?>"></script>
		<script src="~/js/vendor/bootina.js?<?= $version ?>"></script>
		<script src="~/js/app/functions.js?<?= $version ?>"></script>
		<script src="~/js/app/validate.js?<?= $version ?>"></script>
		<script src="~/js/form/participant-checkin.js?<?= $version ?>"></script>	
		<script src="~/js/vendor/sawpf.com-1.0.js"></script>
    </body>
</html>
