<!DOCTYPE html>
<html>
	<head>
		<title><?= htmlentities($event->Name) ?></title>
		<style>
			body {font-family: 'Helvetica'; font-size: 12px; margin: 80px 0 40px 0; padding: 0;}
			h1 {font-size: 18px; margin: 0; padding: 0;}
			h2 {font-size: 18px; margin: 0; padding: 0; color: #888;}
			p {margin: 0;}
			table {width: 100%; border-collapse: collapse;}
			table th {background: #cccccc; font-weight: bold; text-align: left;}
			table td, table th {border: 1px solid #333; padding: 5px;}
			table tr:nth-child(odd) td {background: #f5f5f5;}
			.line {width: 100%;}
			.col8 {margin-right: 36%; float: left;}
			.col4 {margin-left: 70%;}
			#header {position: fixed; top: 10px;}
			#footer {position: fixed; bottom: 20px;}
			#footer .page {color: #888; margin: 0; padding: 0; text-align: right; font-size: 16px; font-weight: bold;}
			#footer .number:before {content: counter(page);}
			#footer .muted {color: #777; padding-top: 5px; font-size: 11px;}
		</style>
	</head>
	<body>
		<div class="line" id="header">
			<div class="col8">
				<h1><?= htmlentities($event->Name) ?></h1>
				<p><?= htmlentities($event->Address) ?></p>
			</div>
			<div class="col4">
				<h2>Data e Hor&aacute;rio</h2>
				<p><?= $event->getSchedule() ?></p>
			</div>
		</div>
		
		<div class="line" id="footer">
			<div class="col8">
				<img src="<?= WWWROOT . 'img/logo-mini.png' ?>" alt="EventMania">
				<div class="muted">A <b>melhor ferramenta</b> para organizar seus eventos.</div>
			</div>
			<div class="col4">
				<p class="page">P&aacute;gina <span class="number"></span></p>
			</div>
		</div>
		
		<table>
			<thead>
				<tr>
					<th style="width: 25px; font-family: 'DejaVu Sans'; font-size: 20px; padding: 0; text-align: center;">&#10004;</th>
					<?php foreach ($columns as $c): ?>
					<th><?= htmlentities($participations[0][$c]['Title']) ?></th>
					<?php endforeach; ?>
					<?php if($hasSignature): ?>
					<th style="width: 150px;">Assinatura</th>
					<?php endif; ?>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($participations as $p): ?>
				<tr>
					<td></td>
					<?php foreach ($columns as $c): ?>
					<td><?= htmlentities($p[$c]['Value']) ?></td>
					<?php endforeach; ?>
					<?php if($hasSignature): ?>
					<td>&nbsp;</td>
					<?php endif; ?>
				</tr>
			<?php endforeach ?>
			</tbody>
		</table>
	</body>
</html>