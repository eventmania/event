<div class="row-fluid">
    <div class="span12">
        <h2 class="pull-left"><?= $model->Title ?></h2>
        <a href="<?= $model->getUrl() ?>" class="btn pull-right controls-inline"><i class="icon-download"></i> Download</a>
        <div class="clearfix"></div>
        <?php if($showAuthors): ?>
        <p class="muted"><?= $model->getAuthors() ?></p>
        <?php endif; ?>
        <p><?= $model->Keywords ?></p>
        <p class="well"><?= $model->Summary ?></p>
    </div>
</div>
