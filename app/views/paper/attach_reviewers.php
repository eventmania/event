<?= Import::view(array('s' => -1, 'event' => $event), '_snippet', 'event-header') ?>
<div id="match-reviewers" class="row-fluid">
    <div class="span12">
        <h2 class="pull-left">Artigos <small>Alterações são Salvas Automáticamente</small></h2>

        <?php if($event->HasPapers): ?>
        <div class="myform-search toolbar pull-right">
            <div class="btn-group">
                <button class="btn btn-warning dropdown-toggle" data-toggle="dropdown"><i class="icon-white icon-cog"></i><span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li>
                        <a href="~/reviewer/raffle/<?= $event->Id ?>" data-toggle="tooltip" title="Selecionar aleatóriamente os revisores.">
                            Sortear Revisores
                        </a>
                    </li>
                    <li>
                        <a href="~/reviewer/notify/<?= $event->Id ?>" class="btn-tooltip" data-toggle="confirm" data-message="Tem certeza que deseja enviar e-mails para os revisores?" title="Enviar informações necessárias.">
                            Avisar Revisores <small>(via E-mail)</small>
                        </a>
                    </li>
                    <li>
                        <a href="~/reviewer/clear/<?= $event->Id ?>" class="btn-tooltip" data-toggle="confirm" data-message="Isso irá apagar todas as revisões já efetuadas até o momento. Deseja continuar?" title="Excluir todas as revisões.">
                            Limpar Revisões
                        </a>
                    </li>
                    <li>
                        <a href="~/paper/accept-all/<?= $event->Id ?>" data-toggle="confirm" data-message="Isso irá enviar os e-mails com as avaliações dos revisores aos autores. Se você estiver utilizando o sistema de re-submissão, serão enviados e-mails de notificação de re-submissão para os trabalhos marcados como aceitos. Deseja continuar?">
                            Aceitar os <?= $event->AcceptedPapers ?> melhores
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="~/paper/download-all/<?= $event->Id ?>">
                            Baixar Todos os Artigos
                        </a>
                    </li>
                    <li>
                        <a href="~/paper/export/<?= $event->Id ?>" target="_blank">
                            Exportar Dados dos Artigos
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="~/paper/edit/<?= $event->Id ?>">
                            Configurações
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <?php endif; ?>

        <table class="table table-bordered table-striped papers">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Palavras Chave</th>
                    <th class="span1">Revisores</th>
                </tr>
            </thead>
            <tbody>
                <?php if($model->Count): ?>
                    <?php foreach($model->Data as $paper): ?>
                    <tr class="paper" data-id="<?= $paper->Id ?>">
                        <td><a href="~/paper/view/<?= $paper->Id ?>"><?= $paper->Title ?></a></td>
                        <td><?= $paper->Keywords ?></td>
                        <td>
                            <button class="btn add-reviewer">Adicionar</button>
                            <div class="hide">
                                <ul class="select-reviewers">
                                <?php if($paper->Reviewers): ?>
                                    <?php foreach ($paper->Reviewers as $reviewer): ?>
                                        <li>
                                            <label class="checkbox">
                                                <input type="checkbox" class="add-reviewer-check" <?= $reviewer->IsChecked ? 'checked="checked"' : '' ?> data-managment-id="<?= $reviewer->ManagmentId ?>" data-paper-id="<?= $paper->Id ?>"><?= $reviewer->Name ?> <strong>(<?= $reviewer->CountReviews ?>)</strong>
                                            </label>
                                        </li>
                                    <?php endforeach; ?>
                                <?php else: ?>
                                    <li>
                                        <p>
                                            <span>Ainda não foi cadastrado nenhum revisor.</span>
                                            <br>
                                            <a href="~/paper/edit/<?= $event->Id ?>" class="btn btn-primary">Cadastrá-los Agora</a>
                                        </p>
                                    </li>
                                <?php endif; ?>
                                </ul>
                            </div>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <?php if($event->HasPapers): ?>
                        <td class="text-center" colspan="3">Ainda não foi submetido nenhum trabalho.</td>
                    <?php else: ?>
                        <td class="text-center" colspan="3">Seu evento ainda não possui gerenciamento de artigos. Ative agora a <a href="~/paper/edit/<?= $event->Id ?>" class="btn btn-primary">Submissão de Artigos</a></td>
                    <?php endif; ?>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?= Pagination::create('event/index', $model->Count, $p, $m) ?>
    </div>
</div>
