<section id="submissions" class="row-fluid">
    <div class="span12">
        <h2 class="pull-left">Submissão</h2>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Evento</th>
                    <th class="span2">Data Limite</th>
                    <th class="span3">Ações</th>
                </tr>
            </thead>
            <tbody>
                <?php if($model->Count): ?>
                    <?php foreach($model->Data as $paper): ?>
                    <tr>
                        <?php $event = Event::getMock(array('Slug' => $paper->EventSlug, 'CustomUrl' => $paper->EventCustomUrl)); ?>
                        <td>
                            <?php if($paper->File): ?>
                                <span><?= $paper->EventName ?></span><br>
                                <small><?= $paper->Title ?></small>
                            <?php else: ?>
                            <a href="~/paper/submit/<?= $paper->ParticipantId ?>"><?= $paper->EventName ?></a>
                            <?php endif; ?>
                        </td>
                        <td><?= $event->getCurrentDeadLine() ?></td>
                        <td>
                            <?php if($paper->File): ?>
                                <a href="~/paper/edit-submission/<?= $paper->Id ?>" class="btn btn-small" data-toggle="tooltip" title="Editar Submissão"><i class="icon-edit"></i></a>
                                <a href="<?= $paper->getUrl() ?>" class="btn btn-small" data-toggle="tooltip" title="Download"><i class="icon-download"></i></a>
                            <?php endif; ?>
                            <a href="<?= $event->getUrl() ?>" class="btn btn-small" data-toggle="tooltip" title="Ver Evento" target="_blank"><i class="icon-eye-open"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td class="text-center" colspan="3">Você não está participando de nenhum evento que aceite submissão de artigos.</td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?= Pagination::create('paper/my', $model->Count, $p, $m) ?>
    </div>
</section>
