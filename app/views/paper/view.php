<?= Import::view(array('s' => -1, 'event' => $event), '_snippet', 'event-header') ?>
<?= Import::view(array('model' => $model, 'showAuthors' => true), 'paper', 'summary') ?>
<?php if($reviews): ?>
    <div class="row-fluid">
        <div class="span12">
            <h2 class="pull-left">Avaliações</h2>
            <div class="toolbar pull-right controls-inline">
                <div class="controls btn-group dual-choice" data-toggle="buttons-radio" name="Decision">
                    <a href="~/paper/accept/<?= $model->Id ?>/1" onclick="javascript:void(0);" class="btn <?= $model->Decision ? 'active' : '' ?>" value="1">Aceito</a>
                    <a href="~/paper/accept/<?= $model->Id ?>/0" onclick="javascript:void(0);" class="btn <?= $model->Decision ? '' : 'active' ?>" value="0">Não Aceito</a>
                </div>
                <div class="btn-group">
                    <a href="~/paper/send-reviews/<?= $model->Id ?>" class="btn btn-primary" data-toggle="confirm" data-message="Isso enviará um e-mail ao autor com as avaliações recebidas e, caso você esteja utilizando o sistema de ressubmissão, as instruções para enviar a versão final do artigo. Deseja continuar?">
                        Enviar Avaliações
                    </a>
                </div>
            </div>
        </div>
    </div>
    <?php foreach($reviews as $review): ?>
    <div class="row-fluid">
        <div class="span4">
            <table class="table table-bordered table-summary">
                <tbody>
                    <tr>
                        <td>
                            <dl class="dl-horizontal">
                                <dt>Avaliador:</dt>
                                <dd><?= $review->UserName ?></dd>

                                <div class="dl-horizontal-subitem">
                                    <dt>Recomendação:</dt>
                                    <dd><?= $review->getRecommendation() ?></dd>
                                    <dt>Apresentação:</dt>
                                    <dd><?= $review->PresentationRate ?></dd>
                                    <dt>Conteúdo:</dt>
                                    <dd><?= $review->ContentRate ?></dd>
                                    <dt>Impacto:</dt>
                                    <dd><?= $review->ImpactRate ?></dd>
                                <dt>
                            </dl>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="span8">
            <strong>Comentários para o Autor</strong>
            <p><?= $review->PublicComments ? nl2br($review->PublicComments) : 'Sem comentários.' ?></p>
            <strong>Comentários para a Organização</strong>
            <p><?= $review->PrivateComments ? nl2br($review->PrivateComments) : 'Sem comentários.' ?></p>
        </div>
    </div>
    <?php endforeach; ?>
<?php else: ?>
    <div id="match-reviewers" class="row-fluid">
            <p>Esse trabalho ainda não tem revisores, quer escolhê-los?</p>
            <div class="paper">
                <button class="btn add-reviewer btn-primary">Adicionar Revisor</button>
                <div class="hide">
                    <ul class="select-reviewers">
                    <?php if($reviewers): ?>
                        <?php foreach ($reviewers as $reviewer): ?>
                            <li>
                                <label class="checkbox">
                                    <input type="checkbox" class="add-reviewer-check" data-managment-id="<?= $reviewer->Id ?>" data-paper-id="<?= $model->Id ?>"><?= $reviewer->Name ?> <strong>(<?= $reviewer->countReviews() ?>)</strong>
                                </label>
                            </li>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <li>
                            <p>
                                <span>Ainda não foi cadastrado nenhum revisor.</span>
                                <br>
                                <a href="~/paper/edit/<?= $event->Id ?>" class="btn btn-primary">Cadastrá-los Agora</a>
                            </p>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="~/paper/view/<?= $model->Id ?>" class="btn btn-block">Pronto!</a>
                    </li>
                    </ul>
                </div>
            </div>
        </div>
<?php endif; ?>
<div class="row-fluid">
    <div class="span12">
        <a href="~/paper/attach-reviewers/<?= $event->Id ?>" class="pull-right">Voltar</a>
    </div>
</div>
