<?= Import::view(array('s' => -1, 'event' => $event), '_snippet', 'event-public-header') ?>
<div class="row-fluid">
	<h2>Submeter Trabalho</h2>
</div>
<div class="row-fluid">
	<div class="span8">
		<form method="POST" action="" class="with-maps" enctype="multipart/form-data">
			<fieldset>
				<div class="row-fluid">
					<div class="span12">
						<?= BForm::input('Título', 'Title', $model->Title, 'span12', array(), true) ?>
					</div>
				</div>
				<?= BForm::input('Autores <small class="muted">separados por ponto e vírgula ( ; ) </small>', 'Authors', $model->Authors, 'span12', array('placeholder' => 'Fulano da Silva [fulanosilva@exemplo.com.br]')) ?>
				<div class="row-fluid">
					<div class="span9">
						<?= BForm::input('Palavras Chave <small class="muted">separadas por ponto e vírgula ( ; ) </small>', 'Keywords', $model->Keywords) ?>
					</div>
					<div class="span3 control-group">
						<label class="control-label">&nbsp;</label>
						<div class="controls">
							<a href="javascript:void(0);" class="btn btn-block btn-file-trigger">Selecionar Arquivo</a>
							<input type="file" name="File" style="display: none;" />
						</div>
					</div>
				</div>
				<?= BForm::textarea('Resumo', 'Summary', $model->Summary) ?>
			</fieldset>

			<a href="~/paper/my" class="pull-right">Cancelar</a>

			<button type="submit" class="btn btn-primary">Enviar</button>
		</form>
	</div>
	<div class="span4">
		<p>
			<strong>Não inclua</strong> seu nome na lista de autores.
		</p>
		<p>O nome dos autores e as palavras chave devem estar separados por ponto e virgulas.</p>
		<p>Caso deseje informar os e-mails dos autores utilize colchetes para identificá-los, ex: Fulano da Silva [fulano@exemplo.com.br]</p>
	</div>
</div>
