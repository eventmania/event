<?= Import::view(array('s' => -1, 'event' => $model), '_snippet', 'event-header') ?>
<div class="row-fluid">
    <div class="span8">
        <?= Import::view(array('i' => 3, 'eventId' => $model->Id ? $model->Id : '', 'hasLinks' => true), '_snippet', 'event-wizard') ?>

        <h2>Submissão de Artigos</h2>
    </div>
</div>
<div class="row-fluid">
    <div class="span8">
        <form method="POST" id="paper-form" action="">
            <fieldset>
                <div class="row-fluid">
                    <div class="control-group span6">
                        <label class="control-label">
                            Os particiantes poderão submeter artigos?
                        </label>
                        <div class="controls btn-group dual-choice" data-toggle="buttons-radio" name="HasPapers">
                            <button onclick="javascript:void(0);" class="btn <?= $model->HasPapers ? 'active' : '' ?>" value="1">Sim</button>
                            <button onclick="javascript:void(0);" class="btn <?= $model->HasPapers ? '' : 'active' ?>" value="0">Não</button>
                        </div>
                        <input type="hidden" name="HasPapers" value="<?= $model->HasPapers ?>">
                    </div>
                </div>
            </fieldset>
            <fieldset id="paper-config" <?= $model->HasPapers ? '' : 'style="display: none;"' ?>>
                <div class="row-fluid">
                    <div class="control-group span6">
                        <label class="control-label">
                            Final das Submissões
                            <span class="required">*</span>
                        </label>
                        <div class="controls">
                            <input <?= $model->DeadLine ? 'name="DeadLine"' : '' ?> id="DeadLine" value="<?= $model->getDeadLine() ?>" type="text" class="date span12 required valid" autocomplete="off">
                        </div>
                    </div>
                    <div class="control-group span5 <?= $model->FakeDeadLine ? '' : 'hide' ?> paper-has-fakedeadline">
                        <label class="control-label">
                            Final Falso das Submissões
                            <i class="icon-question-sign help-icon">
                                <div class="hide">
                                    <p>Final das submissões exibido inicialmente e prorrogado, quando vencido, para o Final das Submissões.</p>
                                </div>
                            </i>
                        </label>
                        <div class="controls">
                            <div class="input-append">
                                <input <?= $model->FakeDeadLine ? 'name="FakeDeadLine"' : '' ?> id="FakeDeadLine" value="<?= $model->getFakeDeadLine() ?>" type="text" class="date span12 required valid" autocomplete="off">
                                <button id="turn-off-fakedeadline" class="btn btn-danger" type="button" data-toggle="tooltip" title="Desativar prorrogação automática"><i class="icon-remove icon-white"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="control-group span6 <?= $model->FakeDeadLine ? 'hide' : '' ?> paper-hasnt-fakedeadline">
                        <label class="control-label">
                            Usar prorrogação automática?
                        </label>
                        <div class="controls btn-group dual-choice" data-toggle="buttons-radio" name="HasFakeDeadLine">
                            <button onclick="javascript:void(0);" class="btn <?= $model->FakeDeadLine ? 'active' : '' ?>" value="1">Sim</button>
                            <button onclick="javascript:void(0);" class="btn <?= $model->FakeDeadLine ? '' : 'active' ?>" value="0">Não</button>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="control-group span6 <?= $model->ResubmissionDeadLine ? '' : 'hide' ?> paper-has-resubmission">
                        <label class="control-label">
                            Prazo de Avaliação
                            <span class="required">*</span>
                            <i class="icon-question-sign help-icon">
                                <div class="hide">
                                    <p>Prazo para que os avaliadores realizem suas avaliações.</p>
                                </div>
                            </i>
                        </label>
                        <div class="controls">
                            <input <?= $model->RevisionDeadLine ? 'name="RevisionDeadLine"' : '' ?> id="RevisionDeadLine" value="<?= $model->getRevisionDeadLine() ?>" type="text" class="date span12 required valid" autocomplete="off">
                        </div>
                    </div>
                    <div class="control-group span5 <?= $model->ResubmissionDeadLine ? '' : 'hide' ?> paper-has-resubmission">
                        <label class="control-label">
                            Prazo de Resubmissão
                            <i class="icon-question-sign help-icon">
                                <div class="hide">
                                    <p>Prazo para reenvio de artigos após correções pelos autores.</p>
                                </div>
                            </i>
                        </label>
                        <div class="controls">
                            <div class="input-append">
                                <input <?= $model->ResubmissionDeadLine ? 'name="ResubmissionDeadLine"' : '' ?> id="ResubmissionDeadLine" value="<?= $model->getResubmissionDeadLine() ?>" type="text" class="date span12 required valid" autocomplete="off">
                                <button id="turn-off-resubmission" class="btn btn-danger" type="button" data-toggle="tooltip" title="Desativar submissão de correções"><i class="icon-remove icon-white"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="control-group alpha span12 <?= $model->ResubmissionDeadLine ? 'hide' : '' ?> paper-hasnt-resubmission">
                        <label class="control-label">
                            Habilitar prazo para submissão de correções (versão final)?
                        </label>
                        <div class="controls btn-group dual-choice" data-toggle="buttons-radio" name="HasResubmission">
                            <button onclick="javascript:void(0);" class="btn <?= $model->ResubmissionDeadLine ? 'active' : '' ?>" value="1">Sim</button>
                            <button onclick="javascript:void(0);" class="btn <?= $model->ResubmissionDeadLine ? '' : 'active' ?>" value="0">Não</button>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <?= BForm::input('Avaliadores p/ Trabalho', 'ReviewersPerPaper', $model->ReviewersPerPaper ? $model->ReviewersPerPaper : 1, 'span12', array(), true) ?>
                    </div>
                    <div class="span4">
                        <?= BForm::input('Qt. Trabalhos Aceitos', 'AcceptedPapers', $model->AcceptedPapers, 'span12', array(), true) ?>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                        <?= BForm::textarea('Chamada de Arigos', 'CallForPapers', $model->CallForPapers, 'span12 ckeditor', array(), true) ?>
                    </div>
                </div>

                <div class="row-fluid">
                    <h3>Avaliadores</h3>
                </div>
                <div class="row-fluid">
                    <fieldset>
                        <div class="span6 control-group">
                            <label class="control-label">Email do Avaliador</label>
                            <div class="controls input-append">
                                <input id="ReviewerEmail" type="text">
                                <button id="add-reviewer" class="btn" type="button">Adicionar</button>
                            </div>
                        </div>
                        <div class="span5 offset1 control-group">
                            <span>Avaliadores</span>
                            <table id="reviewers-list" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th class="span1">Ações</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($model->allReviewers() as $r): ?>
                                    <tr>
                                        <td>
                                            <input type="hidden" name="CurrentReviewerId[]" value="<?= $r->Id ?>">
                                            <span data-name="<?= $r->Name ?>"><?= $r->Name ?></span>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </fieldset>
            <hr/>
            <a href="~/event/<?= isset($id) ? 'about/' . $id : '' ?>" class="pull-right">Cancelar</a>
            <button type="submit" name="Status" class="btn btn-primary">Salvar</button>
        </form>
    </div>
    <div class="span4">
        <p>Você pode estabelecer um prazo para <strong>prorrogação automática</strong> da data limite para submissão de artigos, basta usar a opção de prorrogação automática e definir um final falso para as submissões.</p>
        <p>Você pode utilizar o sistema integrado para <strong>avaliação dos artigos</strong> submetidos. Os avaliadores poderã se cadastrar no eventmania e avaliar os artigos a eles designados. Para isso basta habilitar a opção de submissão de correções e definir os prazos indicados.</p>
    </div>
</div>

<?php if(App::$action == 'edit'): ?>
<div id="modal-reviewer" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal-reviewer-label" aria-hidden="true">
    <div class="modal-header">
        <h3 id="modal-reviewer-label">Convidar Moderador</h3>
    </div>
    <div class="modal-body">
        <p>Não foi encontrado um usuário com este e-mail, os usuários devem estar cadastrados para avaliar trabalhos. Deseja enviar um convite?</p>
        <div class="row-fluid">
            <div class="span12">
                <div class="control-group">
                    <label class="control-label" for="Name">Nome <span class="required">*</span></label>
                    <div class="controls">
                        <input name="Name" id="Name" value="" type="text" class="span12 required">
                        <input type="hidden" id="EventId" value="<?= $eventId ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" id="reviewer-invite">Enviar Convite</button>
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
    </div>
</div>
<?php endif ?>
