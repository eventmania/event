<div class="row-fluid">
	<?= Import::view(array('s' => 0), '_snippet', 'account-menu') ?>
	<div class="span8">
		<h2 class="pull-left">Informações Bancárias</h2>
		<a href="~/account/add" class="btn btn-primary pull-right controls-inline">Cadastrar Conta</a>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="span2">Destino</th>
					<th>Detalhes</th>
					<th class="span1"></th>
				</tr>
			</thead>
			<tbody>
				<?php if(isset($model)): ?>
					<?php foreach($model as $a): ?>
					<tr>
						<td><?= $a->getBank() ?></td>
						<td><?= $a->getDetails() ?></td>
						<td>
							<a href="~/account/edit/<?= $a->Id ?>" class="btn btn-mini btn-tooltip" title="Editar conta"><i class="icon-edit"></i></a>
						</td>
					</tr>
					<?php endforeach ?>
				<?php else: ?>
					<tr>
						<td colspan="3" class="text-center">
							<a href="~/account/add" class="btn btn-primary">Cadastre uma Conta Bancária!</a>
						</td>
					</tr>
				<?php endif ?>
			</tbody>
		</table>
	</div>
</div>