<div class="row-fluid">
	<?= Import::view(array('s' => 0), '_snippet', 'account-menu') ?>
	<div class="span4">
		<h2>Cadastrar Conta</h2>
		<form method="POST" action="" id="account-form">
			<fieldset>
				<?= BForm::select('Modo de Recebimento', 'Bank', $model->getBanks(), $model->Bank, 'span12', array(), true) ?>
				<?= BForm::input('Agência', 'Agency', $model->Agency, 'span12', array(), true) ?>
			</fieldset>
			<fieldset id="no-moip" class="<?= $model->Type === 0 ? 'hide' : '' ?>">
				<div class="row-fluid">
					<div class="span4">
						<?= BForm::select('Tipo', 'AccountType', $model->getAccountTypes(), $model->AccountType, 'span12', array(), true) ?>
					</div>
					<div class="span8">
						<?= BForm::input('Número da Conta', 'Account', $model->Account, 'span12', array(), true) ?>
					</div>
				</div>
				<?= BForm::input('Nome do Titular', 'Name', $model->Name, 'span12', array(), true) ?>
				<div class="row-fluid">
					<div class="span4">
						<div class="control-group">
							<div class="controls">
								<select class="span12 required valid" id="TypeCPF">
									<option value="CPF">CPF</option>
									<option value="CNPJ">CNPJ</option>
								</select>
							</div>
						</div>
					</div>
					<div class="span8">
						<div class="control-group">
							<div class="controls">
								<input type="text" class="span12 required" value="<?= $model->CPF ?>" id="CPF" name="CPF">
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			
			<a href="~/account" class="pull-right">Cancelar</a>
			<button type="submit" class="btn btn-primary">Salvar</button>
		</form>
	</div>
	<div class="span4">
		<h2>&nbsp;</h2>
		<div class="well well-small">
			<p>
				<b>MoIP</b><br>
				Recebimento gratuito e em X dias após a confirmação de pagamento.
			</p>
			<p>
				<b>Banco do Brasil</b><br>
				Recebimento gratuito em X dias após o finalização do evento.
			</p>
			<p>
				<b>Outros Bancos</b><br>
				Taxa de R$ X pela transfência e em X dias após a finalização do evento.
			</p>
		</div>
	</div>
</div>