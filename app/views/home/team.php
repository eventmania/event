<h1>Equipe</h1>
<ul class="team">
	<li>
		<img src="https://avatars1.githubusercontent.com/u/1905937?s=128" alt="" class="img-circle">
		<p>Diego Oliveira</p>
	</li>
	<li>
		<img src="https://avatars3.githubusercontent.com/u/146581?s=128" alt="" class="img-circle">
		<p>Valdirene Neves</p>
	</li>
	<li>
		<img src="~/img/tour/icon-organizer.png" alt="" class="img-circle">
		<p>Você</p>
	</li>
</ul>

<hr>
<div class="team-join">
	<h1>Junte-se a Nossa Equipe</h1>
	<p>Nós gostamos de enfrentar desafios e estamos procurando pessoas que queiram fazer parte dessa equipe.</p>
	<p>Você tem interesse?</p>
	<a href="~/contact" class="btn btn-primary btn-large">Entrar em Contato</a>
</div>
