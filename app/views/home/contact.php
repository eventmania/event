<div class="row-fluid">
    <div class="span12">
        <h1>Fale Conosco</h1>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <h2>Procure-nos on-line...</h2>
        <ul class="contact-social">
            <li>
                <a href="https://www.facebook.com/eventmaniaoficial" target="_blank">
                    <img class="img-circle" alt="" src="~/img/social/facebook-xxx.png">
                </a>
            </li>
            <li>
                <a href="https://twitter.com/eventmanya" target="_blank">
                    <img class="img-circle" alt="" src="~/img/social/twitter-xxx.png">
                </a>
            </li>
        </ul>
        <div class="well">
            <p>
                <b>Participantes</b><br>
                atendimento@eventmania.com.br
            </p>
            <p>
                <b>Vendedores</b><br>
                suporte@eventmania.com.br
            </p>
        </div>
    </div>
    <div class="span6">
        <form id="contact-form" method="POST" action="">
            <h2>...Ou simplesmente mande um "Olá"!</h2>
            <div class="control-group">
                <input type="text" name="Name" placeholder="Nome" class="span10" />
            </div>
            <div class="control-group">
                <input type="text" name="Mail" placeholder="E-mail" class="span10 email" />
            </div>
            <div class="control-group">
                <input type="text" name="Subject" placeholder="Assunto" class="span10" />
            </div>
            <div class="control-group">
                <textarea class="span12" name="Content">Olá, </textarea>
            </div>
            <input type="submit" class="btn btn-primary pull-right" value="Enviar Mensagem" />
        </form>
    </div>
</div>
