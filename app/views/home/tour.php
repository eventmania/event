<h1>Como Funciona</h1>
<div class="row-fluid">
	<ul class="thumbnails tour">
		<li class="span3">
			<div class="thumbnail">
				<img src="~/img/tour/icon-organizer.png">
				<div class="caption">
					<h3>Organizador</h3>
					<p>O Organizador do Evento utiliza o EventMania para criar um página para seu evento e disponibilizar inscrições on-line para os participantes.</p>
				</div>
			</div>
		</li>
		<li class="span3">
			<div class="thumbnail">
				<img src="~/img/tour/icon-participant.png">
				<div class="caption">
					<h3>Participante</h3>
					<p>Através da página do evento os participantes podem acessar o EventMania para realizar suas inscrições.</p>
				</div>
			</div>
		</li>
		<li class="span3">
			<div class="thumbnail">
				<img src="~/img/tour/icon-ticketss.png">
				<div class="caption">
					<h3>EventMania</h3>
					<p>O EventMania atua como uma interface entre os participantes que realizarão suas inscrições e diversas formas de pagamentos.</p>
				</div>
			</div>
		</li>
		<li class="span3">
			<div class="thumbnail">
				<img src="~/img/tour/icon-creditcards.png">
				<div class="caption">
					<h3>Pagamento On-line</h3>
					<p>Os pagamentos são realizados pelo <a href="http://pagseguro.com.br" target="_blank">PagSeguro</a>. Após a liberação (~14 dias) do dinheiro o Organizador pode solicitar o saque ao EventMania.</p>
				</div>
			</div>
		</li>
	</ul>
</div>
<hr>
<h1>Com o EventMania você pode:</h1>
<div class="tour">
	<div class="row-fluid">
		<div class="span3 thumbnail">
			<img src="~/img/tour/icon-share.png">
		</div>
		<div class="span9">
			<h2>Compartilhar</h2>
			<p>O EventMania disponibiliza uma página para seu evento, com todas as informações relevante e mais algumas que você pode adicionar. Dessa forma você pode compartilhar a página do seu evento em diversas Redes Sociais, uma excelente forma de tornar seu evento conhecido.</p>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span9 text-right">
			<h2>Inscrever On-Line</h2>
			<p>Os participantes do seu evento poderão acessar a página do seu evento e realizar as inscrições totalmente on-line. E não se preocupe, as informações necessárias para as incrições são personalizáveis, assim você poderá perguntar o que desejar para seus participantes na hora de se inscreverem.</p>
		</div>
		<div class="span3 thumbnail">
			<img src="~/img/tour/icon-responsive.png">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3 thumbnail">
			<img src="~/img/tour/icon-chart.png">
		</div>
		<div class="span9">
			<h2>Criar Relatórios</h2>
			<p>Para facilitar o controle e execução do seu evento, o EventMania disponibiliza uma série de relatórios de vendas e de participantes. Você pode selecionar participantes por modalidade de inscrição, workshops, status de inscrição e etc. Após selecioá-los você pode exportar seus dados em PDF (para utilizar como uma lista de assinaturas, por exemplo), ou para o Excel (para trabalhar os dados mais fácilmente).</p>
		</div>
	</div>
	<div class="row-fluid">
		<div class="span9 text-right">
			<h2>Distribuir Atividades</h2>
			<p>Para lhea auxiliar a coordenar seu evento o EventMania permite que você adicione moderadores ao evento. Cada moderador possui atividades específicas, que são refletidas nas informações que ele poderá acessar dentro da área de gerenciamento do evento.</p>
		</div>
		<div class="span3 thumbnail">
			<img src="~/img/tour/icon-organizers.png">
		</div>
	</div>
	<div class="row-fluid">
		<div class="span3 thumbnail">
			<img src="~/img/tour/icon-presentation.png">
		</div>
		<div class="span9">
			<h2>Workshops, Artigos e Palestras</h2>
			<p>O EventMania tem como objetivo principal auxiliar o desenvolvimento de eventos acadêmicos e afins. Nesse caso existem áreas de gerenciamento para as principais atrações desse tipo de eventos, como Workshops, Submição de Artigos e Palestras.</p>
		</div>
	</div>
</div>