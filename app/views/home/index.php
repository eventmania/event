	</div>
</div>
<div class="main">
	<div id="carousel" class="carousel slide">
		<ol class="carousel-indicators">
			<?php if(!Auth::isLogged()): ?>
			<li data-target="#carousel" data-slide-to="0" class="active"></li>
			<?php endif ?>
			<li data-target="#carousel" data-slide-to="1" class="<?= Auth::isLogged() ? 'active' : '' ?>"></li>
		</ol>
		<!-- Carousel items -->
		<div class="carousel-inner">
			<?php if(!Auth::isLogged()): ?>
			<div class="active item">
				<div class="container">
					<div class="row-fluid">
						<div class="span12">
							<div class="x">
								<div class="row-fluid">
									<div class="span7">
										<h2>A <b>melhor ferramenta</b> para organizar seus eventos</h2>
										<p>Organizar um evento é bem mais que vender ingressos. Com o EventMania você pode gerenciar todo seu evento, participantes, workshops e, até mesmo, submissão de artigo.</p>
									</div>
									<div class="span4 offset1">
										<form method="POST" action="~/register">
											<fieldset>
												<?= BForm::input('Nome Completo', 'Name') ?>
												<?= BForm::input('E-mail', 'Email') ?>
												<div class="row-fluid">
													<div class="span6">
														<?= BForm::password('Senha', 'Password') ?>
													</div>
													<div class="span6">
														<?= BForm::password('Confirmar Senha', 'PasswordConfirm') ?>
													</div>
												</div>
												<button type="submit" class="btn btn-success btn-block btn-large">Criar um Evento</button>
												<p>Ao clicar em "Criar um Evento", você estará aceitando os <a href="">termos de uso</a> e <a href="">política de privacidade</a>.</p>
											</fieldset>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php endif ?>
			<div class="item <?= Auth::isLogged() ? 'active' : '' ?>">
				<div class="container">
					<div class="row-fluid">
						<div class="span12">
							<div class="x">
								<div class="row-fluid">
									<div class="span7">
										<h2>Aplicativo <b>Android</b> para organizadores</h2>
										<p>Com o aplicativo Android, os organizadores podem fazer check-in dos participantes com apenas uma foto do ingresso. É simples, fácil e rápido.</p>
										<div>
											<a href="https://play.google.com/store/apps/details?id=br.com.eventmania.checkin" target="_blank"><img src="~/img/playstore.png" alt="Disponível no Google Play"></a>
										</div>
									</div>
									<div class="span4 offset1">
										<div style="position: relative; height: 400px; ">
											<img src="~/img/mobile.png" alt="" style="position: absolute; left: 0; bottom: 0;">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div class="row-fluid">
		<h1>Eventos em Destaque</h1>
		<div class="row-fluid">
            <ul class="thumbnails">
        	<?php if ($model): ?>
            	<?php for($i = 0; $i < 3; $i++): ?>
            	<?php $event = $model[$i]; ?>
            	<li class="span4">
					<article class="thumbnail">
						<a href="<?= $event->getUrl() ?>">
							<img src="<?= $event->getImageUrl(300, 150) ?>" alt="<?= $event->Name ?>">
							<section>
								<h2><?= $event->getName(25) ?></h2>
								<div class="row-fluid">
									<div class="span6"><?= $event->getShortDate() ?></div>
									<div class="span6 text-right"><i class="icon icon-time"></i> <?= $event->getStartHour() ?>hs</div>
								</div>
							</section>
							<section class="details">
								<?= $event->getAddress() ?>
							</section>
						</a>
					</article>
				</li>
            	<?php endfor ?>
        	<?php endif; ?>
            </ul>
		</div>
	</div>
</div>

<div class="container">
	<div class="">
		<div clas="">
			<div class="used">
				<hr>
				<h1>Parceiros</h1>
				<div class="partners">
					<a href="http://pagseguro.com.br" target="_blank"><img src="~/img/partners/pagseguro.png" alt="PagSeguro"></a>
					<a href="http://sitesustentavel.com.br" target="_blank"><img src="~/img/partners/sitesustentavel.png" alt="Site Sustentável"></a>
					<a href="http://linode.com" target="_blank"><img src="~/img/partners/linode.png" alt="Linode"></a>
				</div>
			</div>
		</div>
	</div>
	<div class="row-fluid testimonials">
		<div class="span4">
			<div class="well">
				<blockquote>
					<p>&ldquo;O Eventmania revolucionou nossa forma de fazer e pensar em eventos. Seu sistema prático e objetivo facilitou muito nossa vida, Eventmania é show!&rdquo;</p>
				</blockquote>
				<div class="media">
					<a class="pull-left" href="#">
						<img class="media-object img-circle" src="~/img/testimonials/zenil.jpg" alt="Zenil Drumond">
					</a>
					<div class="media-body">
						<cite class="testimonials-name">Zenil Drumond</cite>
						<div class="testimonials-desc">Organizador do CONEJUR</div>
					</div>
				</div>
			</div>
		</div>
		<div class="span4">
			<div class="well">
				<blockquote>
					<p>&ldquo;Uma plataforma organizada, com funcionalidades específicas para criação de eventos. Suporte técnico rápido em sanar nossas dúvidas. Facilidade ao estudante para se inscrever e pagar a inscrição.&rdquo;</p>
				</blockquote>
				<div class="media">
					<a class="pull-left" href="#">
						<img class="media-object img-circle" src="~/img/testimonials/danierick.jpg" alt="Danierick Nascimento">
					</a>
					<div class="media-body">
						<cite class="testimonials-name">Danierick Nascimento</cite>
						<div class="testimonials-desc">Organizador do III SAEEL UFT</div>
					</div>
				</div>
			</div>
		</div>
		<div class="span4">
			<div class="well">
				<blockquote>
					<p>&ldquo;Com o EventMania é muito simples fazer a inscrição em um evento, basta o participante seguir alguns passos.&rdquo;</p>
				</blockquote>
				<div class="media">
					<a class="pull-left" href="#">
						<img class="media-object img-circle" src="~/img/testimonials/cristiane.jpg" alt="Cristiane Mello">
					</a>
					<div class="media-body">
						<cite class="testimonials-name">Cristiane Mello</cite>
						<div class="testimonials-desc">Participante do CONTEM</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
