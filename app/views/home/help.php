<div class="row-fluid">
    <div class="span9 offset3">
        <h1><strong>Pergunts Frequentes</strong></h1>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <ul class="nav nav-list affix" data-spy="affix" data-offset-top="600">
            <li><a href="#help-general"><strong>Geral</strong></a></li>
            <li><a href="#help-payment"><strong>Pagamentos</strong></a></li>
            <li><a href="#help-setup"><strong>Taxa de SetUp</strong></a></li>
            <li><a href="#help-center"><strong>Central de Atendimento</strong></a></li>
        </ul>
    </div>
    <div class="span9">
        <h1 id="help-general">Geral</h1>
        <h3>O que é o EventMania?</h3>
        <p>O EventMania é uma plataforma para automação de atividades de gerenciamento e inscrições on-line de eventos, principlmente eventos acadêmicos, como: congressos, encontros, simpósios, etc. Nossos serviços substituem os modelos de gerenciamento manual de eventos, permitindo que os organidores controlem o evento a partir de uma série de relatórios e outras facilidades disponíveis.</p>
        <h3>Quem pode usar o EventMania?</h3>
        <p>Qualquer pessoa que queira promover um evento de qualquer porte, desde festas de aniversário entre amigos à encontros nacionais de estudantes, ou feiras de músicas.</p>
        <h3>Eu terei acesso aos dados dos paricipantes do evento?</h3>
        <p>Com o EventMania, todos os dados que forem solicitados aos participantes do seu evento podem ser exportados em dois formatos, PDF ou CSV (Excel). Ao criar um evento, o organizador poderá definir quais dados deverão ser informados para participar do evento, em seguida, do painel de controle do evento é possível exportar esses dados.</p>
        
        <h1 id="help-payment">Pagamentos</h1>
        <h3>Como ocorrem os pagametos?</h3>
        <p>O EventMania opera com pagamentos intermediados por intermediadores de transações, como o <a href="https://site.akatus.com/" target="_blank">Akatus</a>. Ao realizar uma inscrição, as transações financeiras ocorrem dentro do ambiente do intermediador. Sempre que uma inscrição é paga o dinheiro fica com o intermidiador por determinado prazo de liberação (aproximadamente 14 dias) e em seguida o dinheiro pode ser sacado.</p>
        <h3>Como ocorrem os repasses do meu dinheiro?</h3>
        <p>Os organizadores podem solicitar repasse do dinheiro após o período de liberação da transação (aproximadamente 14 dias), após essa solicitação o dinheiro é repassado aos organizadores em até 5 dias úteis.</p>
        <h3>Quanto Custa?</h3>
        <p>No EventMania você apensa paga pelas transações efetivadas. Os preços são cobrados como uma porcentagem do valor das compras (existe uma taxa mínima), você pode consultar a tabela de preços na seção <a href="~/price">Quanto Custa</a>.</p>
        
        <h1 id="help-setup">Taxa de SetUp</h1>
        <h3>Existe alguma taxa de SetUp?</h3>
        <p>Não, você não paga nada para começar a utilizar os serviços do EventMania. Os únicos custos gerados são os referentes às transações financeiras das vendas das inscrções.</p>
        
        <h1 id="help-center">Central de Atendimento</h1>
        <h3>Não achei minha pergunta aqui, e agora?</h3>
        <p>Você pode tentar encontrar na nossa <a href="http://atendimento.eventmania.com.br">Central de Atendimento</a>, uma comunidade on-line onde os usuários do EventMania podem colaborar entre si ou serem ajudados pela nossa equipe. Se preferir você também pode entrar em contato conosco na área de <a href="~/contact">Contato</a>.</p>
    </div>
</div>