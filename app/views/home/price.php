<h1>Quanto Custa</h1>
<div class="row-fluid">
	<div class="span6">
		<h2>Está organizando um evento gratuito?</h2>
		<div class="well">
			<h3>Grátis</h3>
			<p>Para as inscrições gratuitas disponibilizamos todas as ferramentas com nenhum custo!</p>
			<p>
				<a href="~/register" class="btn btn-success">Criar Conta</a>
			</p>
		</div>
		
		<h2>Meios de pagamento</h2>
		<img src="~/img/icones.png?2" alt="">
	</div>
	<div class="span6">
		<h2>Está organizando um evento pago?</h2>
		<div class="well">
			<h3>Taxa de <?= Event::DEFAULT_FEE ?>% <small>(mínimo de <?= Format::money(Event::MINIMUM_FEE) ?> por item da compra)</small></h3>
			<p>Cobramos uma taxa de apenas <?= Event::DEFAULT_FEE ?>% em cima dos itens (ingressos, minicursos, etc.) vendidos. Se não vendeu, não paga!</p>
			<p>
				<a href="~/register"  class="btn btn-success">Criar Conta</a>
			</p>
		</div>
		
		<h2>Exemplo de taxa de ingresso</h2>
		
		<p>A taxa cobrada por ingresso já inclui o valor cobrado pelas administradoras de cartões de crédito.</p>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Preço do seu ingresso</th>
					<th>Taxa do EventMania</th>
					<th>Total</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Grátis</td>
					<td>R$ 0,00</td>
					<td><span class="label label-info">Grátis</span></td>
				</tr>
				<tr>
					<td>R$ 10,00</td>
					<td><?= Format::money(Event::MINIMUM_FEE) ?></td>
					<td><?= Format::money(10 + Event::MINIMUM_FEE) ?></td>
				</tr>
				<tr>
					<td>R$ 50,00</td>
					<td><?= Format::money(50 * Event::DEFAULT_FEE / 100) ?></td>
					<td><?= Format::money((50 * Event::DEFAULT_FEE / 100) + 50) ?></td>
				</tr>
			</tbody>
		</table>
		<p>Você pode optar por arcar com a taxa ou transferi-la aos participantes.</p>
	</div>
</div>