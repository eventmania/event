<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <div>
            De: <?= $name ?> &lt;<?= $mail ?>&gt;
        </div>
        <div>
            <?= nl2br($content) ?>
        </div>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>
