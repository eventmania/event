<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá,</p>
        <p>Você é um dos revisores de artigos do evento <?= $event->Name ?> e por isso está recebendo esse e-mail com as instruções para avaliação dos artigos.</p>
        <p>
            Para avaliar os artigos do evento, basta entrar no <a href="<?= Request::getSite() ?>/reviewer/<?= $event->Id ?>">EventMania</a> e selecionar o menu Artigos.
            Dentro da seção de artigos você poderá visualizar os eventos dos quais faz parte como revisor e selecionar um evento para ver os trabalhos submetidos que você deve avaliar.
            Você tem até o dia <strong><?= $event->getRevisionDeadLine() ?></strong> para realizar as suas avaliações.
        </p>
        <p>
            A avaliação é composta por quatro notas:
        </p>
        <ul>
            <li>
                Nota de Apresentação: que se refere a forma como o conteúdo do trabalho é apresentado, incluindo organização, coerência, coesão e gramática.
            </li>
            <li>
                Nota do Conteúdo: que representa a qualidade do conteúdo em relação ao tema abordado e nível de aprofundamento das técnicas e metodologias apresentadas.
            </li>
            <li>
                Nota de Impacto: que reflete a importância do trabalho para a área abordada.
            </li>
            <li>
                <strong>Nota de Recomendação</strong>: é a principal nota da avaliação e informa o quanto você indica o trabalho para publicação.
            </li>
        </ul>
        <p>
            Além das notas, se preferir, você pode deixar comentários para os autores do trabalho e comentários para a organização do evento.
        </p>
        <p>Bons Eventos =)</p>
        <p>Atenciosamente,<br />Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>
