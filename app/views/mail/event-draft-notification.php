<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá,</p>
        <p>sou a Roberta, parece que você criou um evento e não o publicou. Caso tenha encontrado algum problema para publicá-lo ou precise de ajuda é só me avisar respondendo a este e-mail.</p>
        <p>Bons Eventos =)</p>
        <p>Atenciosamente,<br>Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>