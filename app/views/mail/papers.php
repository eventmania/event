<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá <?= $name ?>,</p>
        <p>Você solicitou fazer o Download dos artigos enviados para o evento <?= $event->Name ?>, seguem abaixo os links para download.</p>
        <ul>
            <?php foreach ($papers as $p): ?>
            <li><?= $p->getUrl() ?></li>
            <?php endforeach; ?>
        </ul>
        <p>Bons Eventos =)</p>
        <p>Atenciosamente,<br>Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>
