<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá <?= $paper->ParticipantName ?>,</p>
        <p>estamos felizes em informar que seu trabalho <?= $paper->Title ?> foi aceito para publicação no evento <?= $event->Name ?>. As avaliações recebidas seguem abaixo:</p>
        <?php foreach ($paper->Comments as $i => $c): ?>
            <div>
                <h3>Avaliação</h3>
                <hr>
                <strong>Recomendação:</strong>
                <p><?= $recommendations[$i] ?></p>
                <strong>Comentários:</strong>
                <p><?= nl2br($c) ?></p>
            </div>
        <?php endforeach; ?>
        <?php if($needRevision): ?>
            <p>As correções sugeridas pelos revisores podem ser feitas e enviadas até <?= $event->getResubmissionDeadLine() ?>, pelo link <a href="<?= Request::getSite() ?>paper/resubmit/<?= $paper->PaperId ?>"><?= Request::getSite() ?>paper/resubmit/<?= $paper->PaperId ?></a>.</p>
        <?php endif; ?>
        <p>Obrigada por submeter seu trabalho.</p>
        <p>Bons Eventos =)</p>
        <p>Atenciosamente,<br>Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>
