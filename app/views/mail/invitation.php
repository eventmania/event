<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <div>
            <?= nl2br($message) ?>
        </div>
        <p>
            <a href="<?= Request::getSite() ?>participant/edit-invitation/<?= $model->getNumber() ?>">Inscreva-se Aqui!</a>
        </p>
        <p>O link para editar suas informações apenas ficará disponível por <?= Participant::INVITATION_DAYS_REMAINING ?> dias. Após esse período apenas o organizador poderá editá-las.</p>
        <p>Você está recebendo este e-mail por indicação do organizador do <?= $event->Name ?>, <?= $event->Organizer ?><?= $event->OrganizerEmail ? " &lt;$event->OrganizerEmail&gt;" : '' ?>.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>
