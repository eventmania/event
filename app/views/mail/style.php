.container
{
    width: 100%;
    background: #f5f5f5;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    font-size: 13px;
}
.header
{
    padding: 25px 25px 0 25px;
}
.body
{
    padding: 0 25px;
}
.footer
{
    background: #1b1b1b;
    border-top: 5px solid #FE781E;
    color: #999999;
    font-size: 11px;
    padding: 0 25px 25px 25px;
}
a
{
    color: #e66d1c;
    text-decoration: none;
    margin-bottom: 10px;
    text-decoration: underline;
}
ul
{
    margin: 0;
    padding: 0;
}
