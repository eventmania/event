<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <div>
            <?= nl2br($message) ?>
        </div>
        <p>
            <a href="<?= $event->getUrl() ?>">Inscreva-se Aqui!</a>
        </p>
        <p>Você está recebendo este e-mail por indicação do organizador do <?= $event->Name ?>, <?= $event->Organizer ?><?= $event->OrganizerEmail ? " &lt;$event->OrganizerEmail&gt;" : '' ?>.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>
