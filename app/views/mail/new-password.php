<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá <?= $name ?>,</p>
        <p>foi solicitado que sua senha fosse redefinida e agora lhe enviei uma nova senha gerada automaticamente. A sua nova senha é <strong><?= $password ?></strong>.</p>
        <p>Caso não tenha sido você quem solicitou a nova senha para a equipe do EventMania, recomendamos que a use e a altere imediatamente.</p>
        <p>Qualquer outro problema nos avise.</p>
        <p>Bons Eventos =)</p>
        <p>Atenciosamente,<br>Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>