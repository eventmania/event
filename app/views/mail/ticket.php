<div class="container">
    <div class="header">
        <img src="<?= Request::getSite(); ?>/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá <?= $name ?>,</p>
        <p>
            segue em anexo o seu ingresso em PDF. Por favor, imprima-o e leve-o com você no dia do evento.
        </p>
        <p>
            Caso tenha alguma dúvida basta me avisar! Ou se preferir pode consultar a nossa <a href="<?= Request::getSite() ?>/help">Ajuda</a> no site.
        </p>
        <p>Bons eventos =)</p>
        <p>Atenciosamente,<br>Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>