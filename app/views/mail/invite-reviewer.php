<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>img/logo.png" />
    </div>
    <div class="body">
        <p>Olá,</p>
        <p>
            você foi convidado(a) pelo(a) <?= $event->OwnerName ?> para se cadastrar no EventMania e ser avaliador(a) de artigos do evento <b><?= $event->Name ?></b>.
            Acesse o seguinte endereço para efetuar o cadastro 
            <a href="<?= Request::getSite() ?>user/invited/<?= $code ?>"><?= Request::getSite() ?>user/invited/<?= $code ?></a>
        </p>
        <p>
            Caso tenha alguma dúvida basta me avisar!.
        </p>
        <p>Bons eventos =)</p>
        <p>Atenciosamente,<br>Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>