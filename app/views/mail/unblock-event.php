<div class="container">
    <div class="header">
        <img src="~/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá <?= $name ?>,</p>
        <p>
            Seu evento <?= $event->Name ?> foi desbloqueado, qualquer problema nos avise.
        </p>
        <p>Bons Eventos =)</p>
        <p>Atenciosamente,<br/>Equipe do EventMania.</p>
    </div>
    <div class="footer">
        <p>Copyright © EventMania
        <br><a href="mailto:faleconosco@eventmania.com">faleconosco@eventmania.com</a></p>
    </div>
</div>