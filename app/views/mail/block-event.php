<div class="container">
    <div class="header">
        <img src="~/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá <?= $name ?>,</p>
        <p>
            Seu evento <?= $event->Name ?> foi bloqueado, para saber o motivo e soliciar a sua liberação entre em contato com o suporte (suporte@eventmania.com.br).
        </p>
        <p>Atenciosamente,<br/>Equipe do EventMania.</p>
    </div>
    <div class="footer">
        <p>Copyright © EventMania
        <br><a href="mailto:faleconosco@eventmania.com">faleconosco@eventmania.com</a></p>
    </div>
</div>