<div class="container">
    <div class="header">
        <img src="<?= Request::getSite(); ?>/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá,</p>
        <p>
            me chamo Roberta e vi que você criou a pouco uma conta no EventMania. 
            Estou feliz que você tenha se juntado a nós para ajudarmos a promover seus eventos.
        </p>
        <p>
            Caso tenha alguma dúvida basta me avisar! Ou se preferir pode consultar a nossa <a href="<?= Request::getSite() ?>/help">Ajuda</a> no site.
        </p>
        <p>Bons eventos =)</p>
        <p>Atenciosamente,<br>Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>