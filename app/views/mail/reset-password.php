<div class="container">
    <div class="header">
        <img src="<?= Request::getSite() ?>/img/logo.png" />
    </div>
    <div class="body">
        <p>Olá <?= $name ?>,</p>
		<p>Recentemente uma solicitação para redefinir sua senha foi feita em nosso site. Se você não fez isso, por favor, ignore o e-mail.</p>
		<p>Para redefinir sua senha, por favor, visite abaixo:</p>
		<p><a href="<?= Request::getSite() ?>reset/<?= $code ?>"><?= Request::getSite() ?>reset/<?= $code ?></a></p>
        <p>Qualquer outro problema nos avise.</p>
        <p>Bons Eventos =)</p>
        <p>Atenciosamente,<br>Roberta, Equipe do EventMania.</p>
    </div>
    <?= Import::view(array(), 'mail/_snippet', 'mail-footer') ?>
</div>