<?= Import::view(array('user' => $user, 's' => 1), '_snippet', 'admin-header'); ?>
<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Usuários</h2>
		<form class="input-append pull-right controls-inline" method="GET" action="">
			<input type="text" name="q" value="<?= $q ?>" placeholder="Nome do usuário ou e-mail" />
			<button type="submit" class="btn btn-primary">Pesquisar</button>
		</form>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="span1">Status</th>
					<th>Nome</th>
					<th class="span4">Email</th>
					<th class="span2"></th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $user): ?>
						<tr>
							<td><span class="badge <?= $user->Status ? 'badge-success' : 'badge-important' ?>">&nbsp;</span></td>
							<td><a href="~/event/about/<?= $user->Id ?>"><?= $user->Name ?></a></td>
							<td><?= $user->Email; ?></td>
							<td>
								<a href="~/admin/user/block-switch/<?= $user->Id ?>" class="btn btn-mini"><i class="<?= $user->Status ? 'icon-ban-circle' : 'icon-ok-circle' ?>"></i></a>
								<a href="~/admin/user/reset-password/<?= $user->Id ?>" class="btn btn-mini"><i class="icon-asterisk"></i></a>
								<a href="~/admin/user/login-as/<?= $user->Id ?>" class="btn btn-mini"><i class="icon-user"></i></a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="4" class="text-center">
							<p>Nenhum usuário encontrado!</p>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('admin/users/index', $model->Count, $p, $m) ?>
	</div>
</div>