<div class="span7 terms">
	<?= Import::view(array(), '_snippet', 'terms') ?>
</div>
<div class="span5">
	<h2>Criar nova Conta</h2>
	<form method="POST" action="">
		<fieldset>
			<?= BForm::input('Nome Completo', 'Name', $model->Name, 'span12', array(), true) ?>
			<?= BForm::input('E-mail', 'Email', $model->Email, 'span12 email', array(), true) ?>
			<div class="row-fluid">
				<div class="span6">
					<?= BForm::password('Senha', 'Password', null, 'span12', array(), true) ?>
				</div>
				<div class="span6">
					<?= BForm::password('Confirmar Senha', 'PasswordConfirm', null, 'span12', array(), true) ?>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Criar Conta</button>
		</fieldset>
    </form>
</div>