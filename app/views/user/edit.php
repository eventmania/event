<div class="row-fluid">
	<?= Import::view(array('s' => 3), '_snippet', 'account-menu') ?>
	<div class="span8">
		<h2>Minha Conta</h2>	
		<form method="POST" action="">
			<fieldset>
				<?= BForm::input('Nome Completo', 'Name', $model->Name, 'span12', array(), true) ?>
				<?= BForm::input('E-mail', 'Email', $model->Email, 'span12', array(), true) ?>
				
				<div class="alert alert-info">
					Caso não queira mudar a senha deixe os campos abaixo em branco.
				</div>
				
				<div class="row-fluid">
					<div class="span6">
						<?= BForm::password('Senha', 'Password') ?>
					</div>
					<div class="span6">
						<?= BForm::password('Confirmar Senha', 'PasswordConfirm') ?>
					</div>
				</div>
				
			</fieldset>			
			<button type="submit" class="btn btn-primary">Salvar</button>
		</form>
	</div>
</div>