<div class="span5">
	<h2>Redefinição de Senha</h2>
	<form method="POST" action="">
		<fieldset>
			<?= BForm::password('Nova Senha', 'Password', null, 'span12', array(), true) ?>
			<?= BForm::password('Confirmar Senha', 'PasswordConfirm', null, 'span12', array(), true) ?>
			<button type="submit" class="btn btn-primary">Redefinir Senha</button>
			<a href="~/login" class="pull-right">Voltar para login</a>
		</fieldset>
    </form>
</div>
<div class="span5 pull-right">
	<h2>Não possui uma conta?</h2>
	<a href="~/register" class="btn btn-large btn-block btn-success">Criar uma conta grátis!</a>
</div>