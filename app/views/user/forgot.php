<div class="span5">
	<h2>Recuperar Senha</h2>
	<form method="POST" action="">
		<fieldset>
			<?= BForm::input('E-mail', 'Email', null, 'span12 email', array(), true) ?>
			<button type="submit" class="btn btn-primary">Recuperar Senha</button>
			<a href="~/login" class="pull-right">Voltar para login</a>
		</fieldset>
    </form>
</div>
<div class="span5 pull-right">
	<h2>Não possui uma conta?</h2>
	<a href="~/register" class="btn btn-large btn-block btn-success">Criar uma conta grátis!</a>
</div>