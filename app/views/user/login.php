<div class="span5">
	<h2>Entrar</h2>
	<form method="POST" action="">
		<fieldset>
			<?= BForm::input('E-mail', 'Email', null, 'span12 email', array(), true) ?>
			<?= BForm::password('Senha', 'Password', null, 'span12', array(), true) ?>
			<button type="submit" class="btn btn-primary">Entrar</button>
			<a href="~/forgot" class="pull-right">Esqueci minha senha</a>
		</fieldset>
    </form>
</div>
<div class="span5 pull-right">
	<h2>Não possui uma conta?</h2>
	<a href="~/register" class="btn btn-large btn-block btn-success">Criar uma conta grátis!</a>
</div>