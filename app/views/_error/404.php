<div class="span6">
	<h2>Ops! Página não encontrada</h2>
    <?php if($model): ?>
    <p><?= $model ?> Se você clicou em algum link que lhe trouxe aqui, por favor, informe-nos esse problema.</p>
    <?php else: ?>
	<p>A página que você tentou acessar não foi encontrada. Se você clicou em algum link que lhe trouxe aqui, por favor, informe-nos esse problema.</p>
    <?php endif; ?>
	<p class="error-number">404</p>
</div>
