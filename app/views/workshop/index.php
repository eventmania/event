<?= Import::view(array('s' => 0, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Workshops</h2>
		<a href="~/workshop/add/<?= $event->Id ?>" class="btn btn-primary pull-right controls-inline">Criar Workshop</a>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="span5">Nome</th>
					<th class="span3">Instrutor</th>
					<th class="span2">Preço</th>
					<th>Inscritos</th>
					<th class="span1">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $workshop): ?>
						<tr>
							<td><a href="~/workshop/edit/<?= $workshop->Id ?>"><?= $workshop->Title ?></a></td>
							<td><?= $workshop->Instructor ?></td>
							<td><?= $workshop->getFormatedPrice() ?></td>
							<td>
								<div class="row-fluid">
									<div class="span8">
										<div class="progress progress-info">
											<div class="bar" style="width: <?= $workshop->getParticipantsPercentage() ?>%"><?= $workshop->ParticipantsAmount ?></div>
										</div>
									</div>
									<div class="span4">de <?= $workshop->Amount ?></div>
								</div>
							</td>
							<td>
								<a href="~/workshop/remove/<?= $workshop->Id ?>" class="btn btn-mini btn-danger" data-toggle="tooltip" title="Remover"><i class="icon-remove icon-white"></i></a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="6" class="text-center">
							<a href="~/workshop/add/<?= $event->Id ?>" class="btn btn-primary controls-inline">Crie um Workshop!</a>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('workshop/index', $model->Count, $p, $m) ?>
	</div>
</div>
