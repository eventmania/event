<?= Import::view(array('s' => 0, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<h2>Criar Workshop</h2>
</div>
<div class="row-fluid">
	<div class="span8">
		<form method="POST" action="" class="with-maps" enctype="multipart/form-data">
			<fieldset>
				<div class="row-fluid">
					<div class="span12">
						<?= BForm::input('Título do Workshop', 'Title', $model->Title, 'span12', array('placeholder' => 'Escolha um título curto e chamativo'), true) ?>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span9">
						<?= BForm::input('Instrutor', 'Instructor', $model->Instructor, 'span12', array(), true) ?>
					</div>
					<div class="span3 control-group">
						<label class="control-label">&nbsp;</label>
						<div class="controls">
							<a href="javascript:void(0);" class="btn btn-block btn-file-trigger">Selecionar Imagem</a>
							<input type="file" name="Image" style="display: none;" />
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span12">
						<?= BForm::input('Url Instrutor', 'InstructorUrl', $model->InstructorUrl) ?>
					</div>
				</div>
				<div class="row-fluid">
					<?php if($model->count(Participant::ALL_STATUSES) > 0): ?>
					<div class="span6">
						<?= BForm::readonly('Valor', 'Price', $model->getFormatedPrice(false), 'span12 money', array('placeholder' => '0,00', 'data-content' => 'Este workshop já possui vendas ou está em processo de compra. Seu valor não pode ser alterado.'), true) ?>
					</div>
					<?php else: ?>
					<div class="span6">
						<?= BForm::input('Valor', 'Price', $model->getFormatedPrice(false), 'span12 money', array('placeholder' => '0,00'), true) ?>
					</div>
					<?php endif ?>
					<div class="span6">
						<?= BForm::input('Vagas', 'Amount', $model->Amount, 'span12 number', array(), true) ?>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span4">
						<?= BForm::date('Data do Início', 'StartDate', $model->getStartDate()) ?>
					</div>
					<div class="span2">
						<label>&nbsp;</label>
						<select name="StartHour" style="width: 100%">
							<option value="<?= $model->getStartHour() ?>"><?= $model->getStartHour() ?></option>
							<?php for($i = 0; $i < 24; $i++): ?>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00</option>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30</option>
							<?php endfor ?>
						</select>
					</div>
					<div class="span4">
						<?= BForm::date('Data do Fim', 'EndDate', $model->getEndDate()) ?>
					</div>
					<div class="span2">
						<label>&nbsp;</label>
						<select name="EndHour" style="width: 100%">
							<option value="<?= $model->getEndHour() ?>"><?= $model->getEndHour() ?></option>
							<?php for($i = 0; $i < 24; $i++): ?>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00</option>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30</option>
							<?php endfor ?>
						</select>
					</div>
				</div>
				<?= BForm::textarea('Descrição', 'Description', $model->Description) ?>
			</fieldset>

			<a href="~/workshop/index/<?= $model->EventId ?>" class="pull-right">Cancelar</a>

			<button type="submit" name="Status" value="1" class="btn btn-primary"><?= $submitLabel ?></button>
		</form>
	</div>
	<div class="span4">
		<?php if(isset($imageUrl)): ?>
		<a href="<?= $model->InstructorUrl ?>" target="_blank">
			<img src="<?= $imageUrl ?>">
		</a>
		<?php endif ?>
	</div>
</div>
