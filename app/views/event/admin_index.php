<?= Import::view(array('user' => $user, 's' => 2), '_snippet', 'admin-header'); ?>
<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Eventos</h2>
		<form class="input-append pull-right controls-inline" method="GET" action="">
			<input type="text" name="q" value="<?= $q ?>" placeholder="Nome do evento, usuário ou e-mail" />
			<button type="submit" class="btn btn-primary">Pesquisar</button>
		</form>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="span1">Status</th>
					<th class="span4">Nome</th>
					<th class="span1">Data</th>
					<th>Contato</th>
					<th class="span2"></th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $event): ?>
						<tr>
							<td><span class="badge <?= $event->Status == -1 ? 'badge-important' : ($event->Status ? 'badge-success' : 'badge-warning') ?>">&nbsp;</span></td>
							<td><a href="~/event/about/<?= $event->Id ?>"><?= $event->Name ?></a></td>
							<td><?= $event->getStartDate(); ?></td>
							<td><?= $event->OwnerName . ' &lt;' . $event->OwnerEmail . '&gt;' ?></td>
							<td>
								<a href="~/admin/event/block-switch/<?= $event->Id ?>" class="btn btn-mini" title="<?= $event->Status ? 'Bloquear' : 'Desbloquear' ?>"><i class="<?= $event->Status != -1 ? 'icon-ban-circle' : 'icon-ok-circle' ?>"></i></a>
								<a href="~/event/edit/<?= $event->Id ?>" class="btn btn-mini" title="Editar"><i class="icon-edit"></i></a>
								<a href="~/<?= $event->getSlug() ?>" class="btn btn-mini" title="Visualizar página"><i class="icon-eye-open"></i></a>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="5" class="text-center">
							Nenhum evento encontrado.
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('admin/event/index', $model->Count, $p, $m) ?>
	</div>
</div>