<?= Import::view(array('s' => 0, 'event' => $model), '_snippet', 'event-header') ?>
<div class="row-fluid">
    <div class="span8">
        <h2>Enviar E-mails</h2>

        <form id="mail-share-form" method="POST" action="">
            <fieldset>
                <?= BForm::input('Título do Convite', 'Title', null, 'span12', array(), true) ?>
                <?= BForm::textarea('Mensagem', 'Text', null, 'span12', array('rows' => '10'), true) ?>
                <?= BForm::textarea('Destinatários', 'Recipients', null, 'span12', array(), true) ?>
            </fieldset>

            <a href="~/event/about/<?= $model->Id ?>" class="pull-right">Cancelar</a>

            <button type="submit" class="btn btn-primary">Enviar Emails</button>
        </form>
    </div>
    <div class="span4">
    </div>
</div>