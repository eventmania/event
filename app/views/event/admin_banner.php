<?= Import::view(array('user' => $user, 's' => 3), '_snippet', 'admin-header'); ?>
<form id="banner-choser" method="POST" action="">
    <div class="row-fluid">
        <div class="span12">
            <h2 class="pull-left">Banners</h2>
            <button class="btn btn-primary pull-right controls-inline" type="submit">Salvar</button>
        </div>
    </div>
    <div class="row-fluid">
        <ul class="thumbnails">
            <?php if ($model): ?>
            <?php foreach ($model as $i => $event): ?>
            <li class="span3">
                <article class="thumbnail" href="<?= $event->getUrl() ?>">
                    <a href="<?= $event->getUrl() ?>">
                        <img src="<?= $event->getImageUrl(220, 185) ?>" alt="<?= $event->Name ?>">
                    </a>
                    <section>
                        <?= BForm::select('Evento', 'EventId['. ($i + 1) .']', $comingEvents, $event->Id, 'span12 chosen', array(), true) ?>
                    </section>
                </article>
            </li>
            <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</form>