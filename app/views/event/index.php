<div class="span12">
	<h2 class="pull-left">Meus Eventos</h2>
	<a href="~/event/add" class="btn btn-primary pull-right controls-inline">Criar Evento</a>
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th class="span1">Status</th>
				<th class="span4">Nome</th>
				<th class="span1">Data</th>
				<th>Cidade</th>
				<th class="span3">Vendidos</th>
				<th style="width: 60px;"></th>
			</tr>
		</thead>
		<tbody>
			<?php if($model->Count): ?>
				<?php foreach ($model->Data as $event): ?>
					<tr>
						<td><span class="badge <?= $event->Status == -1 ? 'badge-important' : ($event->Status ? 'badge-success' : 'badge-warning') ?>">&nbsp;</span></td>
						<td><a href="~/event/about/<?= $event->Id ?>"><?= $event->Name ?></a></td>
						<td><?= $event->getStartDate(); ?></td>
						<td><?= $event->getCity() ?></td>
						<td>
							<div class="row-fluid">
								<div class="span8">
									<div class="progress progress-info">
										<div class="bar" style="width: <?= str_replace(',', '.', $event->getSoldTicketPercentage()) ?>%"><?= $event->countSoldTickets() ?></div>
									</div>
								</div>
								<div class="span4">de <?= $event->countTickets() ?></div>
							</div>
						</td>
						<td>
							<a href="~/event/edit/<?= $event->Id ?>" class="btn btn-mini" data-toggle="tooltip" title="Editar"><i class="icon-edit"></i></a>
							<?php if($event->Visibility): ?>
							<a href="~/<?= $event->getSlug() ?>" class="btn btn-mini" target="_blank" data-toggle="tooltip" title="Visualizar página"><i class="icon-eye-open"></i></a>
							<?php else: ?>
							<a href="javascript:void(0)" class="btn btn-mini disabled" data-toggle="tooltip" title="Evento privado não tem site"><i class="icon-lock"></i></a>
							<?php endif ?>
						</td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="6" class="text-center">
						<a href="~/event/add" class="btn btn-primary controls-inline">Crie seu primeiro evento!</a>
					</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>
	<?= Pagination::create('event/index', $model->Count, $p, $m) ?>
</div>
