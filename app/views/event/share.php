<?= Import::view(array('s' => 0, 'event' => $model), '_snippet', 'event-header') ?>
<div class="row-fluid">
    <div class="span12">
        <h2 class="pull-left">Compartilhar</h2>
        <div class="clearfix"></div>
        <div class="row-fluid well">
            <div class="span4">
                <ul class="social-bar">      
                    <li><span><img src="~/img/social/facebook.png"></span>Facebook</li>
                    <li><span><img src="~/img/social/twitter.png"></span>Twitter</li>
                    <li><span><img src="~/img/social/gplus.png"></span>G+</li>
                    <li><span><img src="~/img/social/email.png"></span>E-mail</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div id="fb-root"></div>