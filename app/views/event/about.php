<?= Import::view(array('s' => 0, 'event' => $model), '_snippet', 'event-header') ?>

<div class="row-fluid">
    <div class="span12">
    	<div class="well">
	        <div class="control-group span2">
	            <label class="control-label">
	                Evento Visível?
	                <i class="icon-question-sign help-icon">
	                    <div class="hide">
	                        <p>O evento é considerado visível quando não é <strong>Privado</strong> e foi <strong>Publicado</strong> (não está salvo como rascunho).</p>
	                    </div>
	                </i>
	            </label>
	            <div class="controls btn-group dual-choice" data-toggle="buttons-radio" name="Public">
	                <a href="~/event/toggle/<?= $model->Id ?>/1" class="btn <?= $model->isVisible() ? 'active' : '' ?>" value="1">Sim</a>
	                <a href="~/event/toggle/<?= $model->Id ?>/0" class="btn <?= $model->isVisible() ? '' : 'active' ?>" value="0">Não</a>
	            </div>
	        </div>
	        <div class="control-group">
	            <label class="control-label">
	                Endereço do Site
	            </label>
	            <div class="controls">
	            	<div class="input-append">
						<input class="span8" type="text" value="<?= $model->getUrl() ?>">
						<a class="btn" href="<?= $model->getUrl() ?>" target="_blank">Acessar Site</a>
					</div>
	            </div>
	        </div>
    	</div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row-fluid" id="panel">
	<div class="span2">
		<a href="~/event/edit/<?= $model->Id ?>" class="btn btn-large btn-block" data-toggle="tooltip" title="Altere as informações do evento.">
			<img src="~/img/tour/icon-edit.png" alt="" style="width: 50%">
			<p>Editar Evento</p>
		</a>
	</div>
	<div class="span2">
		<a href="~/custom-field/<?= $model->Id ?>" class="btn btn-large btn-block" data-toggle="tooltip" title="Defina quais campos o participante deve preencher.">
			<img src="~/img/tour/icon-form.png" alt="" style="width: 50%">
			<p>Configurar Campos</p>
		</a>
	</div>
	<div class="span2">
		<a href="~/workshop/<?= $model->Id ?>" class="btn btn-large btn-block" data-toggle="tooltip" title="Seu evento terá workshops?">
			<img src="~/img/tour/icon-presentation.png" alt="" style="width: 50%">
			<p>Workshops</p>
		</a>
	</div>
	<div class="span2">
		<a href="~/manager/<?= $model->Id ?>" class="btn btn-large btn-block" data-toggle="tooltip" title="Defina os demais organizadores do evento e suas devidas permissões.">
			<img src="~/img/tour/icon-organizers.png" alt="" style="width: 50%">
			<p>Moderadores</p>
		</a>
	</div>
	<div class="span2">
		<a href="~/paper/attach-reviewers/<?= $model->Id ?>" class="btn btn-large btn-block" data-toggle="tooltip" title="Gerencie os artigos submetidos ao evento.">
			<img src="~/img/tour/icon-paper.png" alt="" style="width: 50%">
			<p>Artigos</p>
		</a>
	</div>
	<div class="span2">
		<a href="~/participant/random/<?= $model->Id ?>" class="btn btn-large btn-block" data-toggle="tooltip" title="Realize sorteio entre os participantes.">
			<img src="~/img/tour/icon-present.png" alt="" style="width: 50%">
			<p>Sorteio</p>
		</a>
	</div>
</div>

<div class="row-fluid" id="statistic">
	<div class="span6">
		<h2>Estatísticas</h2>
	</div>
</div>

<div class="row-fluid" id="statistic">
	<div class="span3">
		<span class="icon"><i class="icon-white icon-calendar"></i></span>
		<span class="title">Dias para o evento</span>
		<span class="value"><?= $model->getRemainingDays() ?></span>
	</div>
	<div class="span3">
		<span class="icon"><i class="icon-white icon-signal"></i></span>
		<span class="title">Visitas à página</span>
		<span class="value"><?= $model->Hits ?></span>
	</div>
	<div class="span3">
		<span class="icon"><i class="icon-white icon-user"></i></span>
		<span class="title">Participantes</span>
		<span class="value"><?= $model->countParticipants() ?></span>
	</div>
	<div class="span3">
		<span class="icon"><i class="icon-white icon-shopping-cart"></i></span>
		<span class="title">Arrecadação</span>
		<span class="value"><?= $model->getNetIncome(); ?></span>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<div id="chart"></div>
	</div>
</div>

<div class="row-fluid" id="share">
    <div class="span6">
        <h2>Compartilhe seu Evento</h2>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="row-fluid">
            <div class="well span6">
                <p>Uma ótima maneira de tornar seu evento conhecido é através das redes sociais. Faça sua escolha e começe a divulgar seu evento.</p>
            </div>
            <div class="span6">
                <ul class="social-bar">
                    <li>
                        <a href="<?= $model->getUrl() ?>" data-toggle="fb-share" fb-img="<?= $model->getImageUrl() ?>" fb-redirect="<?= Request::getSite() ?>event/about/<?= $model->Id ?>" fb-name="<?= $model->Name ?>" fb-description="<?= $model->getShortDescription() ?>">
                            <span><img src="~/img/social/facebook.png"></span>Facebook
                        </a>
                    </li>
                    <li>
                        <a href="<?= $model->getUrl() ?>" data-toggle="twitter-share" twitter-redirect="<?= Request::getSite() ?>event/about/<?= $model->Id ?>">
                            <span><img src="~/img/social/twitter.png"></span>Twitter
                        </a>
                    </li>
                    <li>
                        <a href="<?= $model->getUrl() ?>" data-toggle="gplus-share">
                            <span><img src="~/img/social/gplus.png"></span>G+
                        </a>
                    </li>
                    <li>
                        <a href="~/event/mail-share/<?= $model->Id ?>">
                            <span><img src="~/img/social/email.png"></span>E-mail
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
	var dataChart = [
		<?php foreach ($report as $r): ?>
		[Date.UTC(<?= $r->PurchaseYear ?>,<?= $r->PurchaseMonth ?>,<?= $r->PurchaseDay ?>),<?= $r->Amount ?>],
		<?php endforeach; ?>
	];
</script>
