<?php if($action === 'Editar'): ?>
<?= Import::view(array('s' => 0, 'event' => $model), '_snippet', 'event-header') ?>
<?php endif; ?>
<div class="row-fluid">
	<div class="span8">
		<h2><?= $action ?> Evento</h2>

		<?= Import::view(array('i' => 1, 'eventId' => $model->Id ? $model->Id : '', 'hasLinks' => $action !== 'Criar'), '_snippet', 'event-wizard') ?>

		<form method="POST" id="event-form" action="" class="with-maps">
			<fieldset>
				<?= BForm::input('Título do Evento', 'Name', $model->Name, 'span12', array('placeholder' => 'Escolha um título curto e chamativo', 'data-slug' => $model->Slug), true) ?>
				<div class="row-fluid">
					<div class="control-group">
						<label for="Slug">Url</label>
						<div class="control">
							<div class="input-prepend span12">
								<span class="add-on">http://eventmania.com.br/</span>
								<input class="event-slug" name="Slug" id="Slug" value="<?= $model->Slug ?>" type="text">
								<input type="hidden" name="Id" id="Id" value="<?= $model->Id ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span9">
						<?= BForm::input('Endereço', 'Address', $model->Address, 'span12', $model->MapType === 2 ? array('readonly' => 'readonly','placeholder' => 'O mapa não será apresentado para eventos online') : array('placeholder' => 'Local onde o evento será realizado')) ?>
						<input type="hidden" name="Latitude" id="Latitude" value="<?= $model->Latitude ?>">
						<input type="hidden" name="Longitude" id="Longitude" value="<?= $model->Longitude ?>">
					</div>
					<div class="span3">
						<label class="only-label checkbox">
							<input type="hidden" name="IsOnline" value="0">
							<input type="checkbox" name="IsOnline" value="1" id="eventOnline" <?= $model->MapType === 2 ? 'checked' : '' ?>>
							Evento online
						</label>
					</div>
				</div>

				<div class="row-fluid">
					<div class="span4">
						<?= BForm::date('Data do Início', 'StartDate', $model->getStartDate()) ?>
					</div>
					<div class="span2">
						<label>&nbsp;</label>
						<select name="StartHour" style="width: 100%">
							<option value="<?= $model->getStartHour() ?>"><?= $model->getStartHour() ?></option>
							<?php for($i = 0; $i < 24; $i++): ?>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00</option>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30</option>
							<?php endfor ?>
						</select>
					</div>
					<div class="span4">
						<?= BForm::date('Data do Fim', 'EndDate', $model->getEndDate()) ?>
					</div>
					<div class="span2">
						<label>&nbsp;</label>
						<select name="EndHour" style="width: 100%">
							<option value="<?= $model->getEndHour() ?>"><?= $model->getEndHour() ?></option>
							<?php for($i = 0; $i < 24; $i++): ?>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00</option>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30</option>
							<?php endfor ?>
						</select>
					</div>
				</div>
				<?= BForm::textarea('Descrição do Evento', 'Description', $model->Description, 'span12 ckeditor') ?>

				<div class="row-fluid">
					<div class="span6">
						<?= BForm::input('Nome do Organizador', 'Organizer', $model->Organizer ? $model->Organizer : $user->Name, 'span12', array(), true) ?>
					</div>
					<div class="span6">
						<?= BForm::input('Email do Organizador', 'OrganizerEmail', $model->OrganizerEmail ? $model->OrganizerEmail : $user->Email, 'span12', array(), true) ?>
					</div>
				</div>
			</fieldset>
			<?php if($action === 'Criar'): ?>
			<fieldset>
				<legend>Adicionar Ingressos</legend>
				<table class="table table-bordered table-ticket">
					<thead>
						<tr>
							<th class="span6">Título do Ingresso <span class="required">*</span></th>
							<th class="span2">Qtd. <span class="required">*</span></th>
							<th class="span2">Preço <span class="required">*</span></th>
							<th class="span2"></th>
						</tr>
					</thead>
					<tbody>

					</tbody>
					<tfoot>
						<tr>
							<td colspan="4">
								<button type="button" class="btn btn-small" id="btnAddTicket"><i class="icon-plus-sign"></i> Ingresso pago</button>
								<button type="button" class="btn btn-small" id="btnAddTicketFree"><i class="icon-plus-sign"></i> Ingresso grátis</button>

								<span class="pull-right hide"data-toggle="tooltip" title="É possível definir um limite de venda de ingressos para o evento.">Capacidade total:
									<a href="javascript:void(0)" id="maxCapacity">0</a>
									<input type="text" name="Capacity" id="maxCapacityInput" style="display: none">
								</span>
							</td>
						</tr>
					</tfoot>
				</table>
			</fieldset>
			<?php endif; ?>
			<div class="row-fluid">
				<div class="control-group span6">
					<label class="control-label">
						Quem paga as taxas do serviço?
						<i class="icon-question-sign help-icon">
							<div class="hide">
								<div>
									<h3>Você</h3>
									<p>As taxas dos serviços do EventMania são inclusas nos valores dos ingressos, dessa forma os ingressos são apresentados aos participantes com os preços que você definiu e as taxas serão descontadas desses preços.</p>
									<h3>Participantes</h3>
									<p>As taxas dos serviços do EventMania são adicionadas aos valores dos ingressos que você definiu e cobradas dos participantes. Dessa forma você receberá por cada ingresso exatamente o valor que definiu para eles.</p>
								</div>
							</div>
						</i>
					</label>
					<div class="controls btn-group" data-toggle="buttons-radio" name="Absorb">
						<button onclick="javascript:void(0);" class="btn <?= $model->Absorb ? 'active' : '' ?>" value="1">Você</button>
						<button onclick="javascript:void(0);" class="btn <?= $model->Absorb ? '' : 'active' ?>" value="0">Participantes</button>
					</div>
					<input type="hidden" name="Absorb" value="<?= strval($model->Absorb) ?>">
				</div>
				<div class="control-group span6">
					<label class="control-label">
						Seu evento é...
						<i class="icon-question-sign help-icon">
							<div class="hide">
								<div>
									<h3>Público</h3>
									<p>Possui um site de vendas aberto a qualquer pessoa com a URL.</p>
									<h3>Privado</h3>
									<p>Não possui um site de vendas.</p>
								</div>
							</div>
						</i>
					</label>
					<div class="controls btn-group dual-choice" data-toggle="buttons-radio" name="Visibility">
						<button onclick="javascript:void(0);" class="btn <?= $model->Visibility ? 'active' : '' ?>" value="1">Público</button>
						<button onclick="javascript:void(0);" class="btn <?= $model->Visibility ? '' : 'active' ?>" value="0">Privado</button>
					</div>
					<input type="hidden" name="Visibility" value="<?= strval($model->Visibility) ?>">
				</div>
			</div>
			<hr/>

			<a href="~/event/<?= isset($id) ? 'about/' . $id : '' ?>" class="pull-right">Cancelar</a>

			<button type="submit" name="Status" value="1" class="btn btn-primary">Publicar</button>
			<button type="submit" name="Status" value="0" class="btn">Salvar Rascunho</button>
			<button type="submit" name="Status" value="2" class="btn">Escolher Layout <i class="icon-chevron-right"></i></button>
		</form>
	</div>
	<div class="span4">
		<div id="MapsContainer" style="position: relative; <?= $model->MapType === 2 ? 'display: none;' : ''  ?>">
			<div id="maps"></div>
			<p class="muted">O mapa não será exibido se o endereço não for preenchido.</p>
		</div>
	</div>
</div>
<table class="hide" id="ticketTemplate">
	<tr class="ticket-basic">
		<td>
			<div class="control-group">
				<div class="controls">
					<input name="Ticket_Title[]" type="text" class="span12 required" placeholder="Ex.: Congresso, Meia-entrada, VIP">
				</div>
			</div>
		</td>
		<td>
			<div class="control-group">
				<div class="controls">
					<input name="Ticket_Amount[]" type="text" class="span12 minirequired number" placeholder="Ex.: 100" data-toggle="tooltip" title="Quantidade de ingressos">
				</div>
			</div>
		</td>
		<td>
			<div class="control-group">
				<div class="controls">
					<input name="Ticket_Price[]" type="text" class="span12 minirequired money" placeholder="0,00">
				</div>
			</div>
		</td>
		<td>
			<a href="javascript:void(0)" class="btn btn-small btn-ticket-details" data-toggle="tooltip" title="Definir detalhes"><i class="icon-cog"></i></a>
			<a href="javascript:void(0)" class="btn btn-danger btn-small btn-ticket-remove" data-toggle="tooltip" title="Excluir"><i class="icon-white icon-remove"></i></a>
		</td>
	</tr>
	<tr class="ticket-details hide">
		<td colspan="4">
			<div class="row-fluid">
				<div class="">
					<div class="row-fluid">
						<div class="span12">
							<div class="control-group">
								<label>Descrição</label>
								<div class="controls">
									<input name="Ticket_Description[]" type="text" class="span12" placeholder="">
								</div>
							</div>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span3">
							<?= BForm::date('Início das Vendas', 'Ticket_StartDate[]', $model->getStartDate()) ?>
						</div>
						<div class="span3">
							<label>&nbsp;</label>
							<select name="Ticket_StartHour[]" style="width: 100%">
								<?php for($i = 0; $i < 24; $i++): ?>
								<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00</option>
								<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30</option>
								<?php endfor ?>
								<option value="23:59">23:59</option>
							</select>
						</div>
						<div class="span3">
							<?= BForm::date('Fim das Vendas', 'Ticket_EndDate[]', $model->getEndDate(), 'span12 react', array('data-target' => '#StartDate')) ?>
						</div>
						<div class="span3">
							<label>&nbsp;</label>
							<select name="Ticket_EndHour[]" style="width: 100%">
								<option value="23:59">23:59</option>
								<?php for($i = 0; $i < 24; $i++): ?>
								<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00</option>
								<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30</option>
								<?php endfor ?>
							</select>
						</div>
					</div>
					<div class="row-fluid">
						<div class="span6">
							<div class="control-group">
								<label>Visibilidade</label>
								<div class="controls">
									<label class="checkbox inline">
										<input type="hidden" name="Ticket_Visibility[]" value="0"  class="ticket-visibility-hide">
										<input type="checkbox" class="ticket-visibility-check"> Ingresso restrito a convidados
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>
</table>
<?php if($model->MapType === 1): ?>
<script>
	var latitude = <?= $model->Latitude ? $model->Latitude : 'undefined' ?>;
	var longitude = <?= $model->Longitude ? $model->Longitude : 'undefined' ?>;
</script>
<?php endif; ?>
