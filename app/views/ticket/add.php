<?= Import::view(array('s' => 1, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span8">
		<h2>Criar Ingresso</h2>
		<form method="POST" action="">
			<fieldset>

				<div class="row-fluid">
					<div class="span8">
						<?= BForm::input('Título do Ingresso', 'Name', $model->Name, 'span12', array('placeholder' => 'Exemplo: Congresso, Meia-entrada, VIP'), true) ?>
					</div>
					<div class="span2">
						<?= BForm::input('Qtd.', 'Amount', $model->Amount, 'span12 number', array('placeholder' => '100'), true) ?>
					</div>
					<?php if(App::$action == 'edit' && $model->countSolds(true) > 0): ?>
					<div class="span2">
						<?= BForm::readonly('Preço', 'Price', $model->getFormatedPrice(false), 'span12 money use-popover', array('placeholder' => '0,00', 'data-content' => 'Este ingresso já possui vendas ou está em processo de compra. Seu valor não pode ser alterado.'), true) ?>
					</div>
					<?php else: ?>
					<div class="span2">
						<?= BForm::input('Preço', 'Price', $model->getFormatedPrice(false), 'span12 money', array('placeholder' => '0,00'), true) ?>
					</div>
					<?php endif ?>
				</div>

				<?= BForm::input('Descrição', 'Description', $model->Description) ?>
				<div class="row-fluid">
					<div class="span3">
						<?= BForm::date('Início das Vendas', 'StartDate', $model->getStartDate(), 'span12') ?>
					</div>
					<div class="span3">
						<label>&nbsp;</label>
						<select name="StartHour" style="width: 100%">
							<option value="<?= $model->getStartHour() ?>"><?= $model->getStartHour() ?></option>
							<?php for($i = 0; $i < 24; $i++): ?>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00</option>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30</option>
							<?php endfor ?>
							<option value="23:59">23:59</option>
						</select>
					</div>

					<div class="span3">
						<?= BForm::date('Fim das Vendas', 'EndDate', $model->getEndDate(), 'span12') ?>
					</div>
					<div class="span3">
						<label>&nbsp;</label>
						<select name="EndHour" style="width: 100%">
							<option value="<?= $model->getEndHour() ?>"><?= $model->getEndHour() ?></option>
							<?php for($i = 0; $i < 24; $i++): ?>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:00</option>
							<option value="<?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30"><?= str_pad($i, 2, '0', STR_PAD_LEFT) ?>:30</option>
							<?php endfor ?>
							<option value="23:59">23:59</option>
						</select>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<div class="control-group">
							<label>Visibilidade</label>
							<div class="controls">
								<label class="checkbox">
									<input type="hidden" name="Ticket_Visibility" value="0">
									<input type="checkbox" name="Ticket_Visibility" value="1" <?= $model->IsPrivate ? 'checked' : '' ?>> Ingresso restrito a convidados
								</label>
							</div>
						</div>
					</div>
				</div>
				<?php if($event->Absorb): ?>
				<strong><span>*</span> As taxas dos ingressos estão inclusas no Preço informado.</strong>
				<?php endif; ?>
				<hr>
				<button type="submit" class="btn btn-primary">Salvar</button>
				<a href="~/ticket/index/<?= $event->Id ?>" class="pull-right">Cancelar</a>
			</fieldset>
		</form>
	</div>
</div>
