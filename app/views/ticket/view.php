<div class="row-fluid">
	<div class="span8">
		<h2>Detalhes do Pedido</h2>
		<div class="well well-participant">
			<p>Nome:</p>
			<p><b><?= $model->Name ?></b></p>
			<p>E-mail:</p>
			<p><b><?= $model->Email ?></b></p>
			<p>&nbsp;</p>
			<?php if($model->isConfirmed()): ?>
			<a href="~/ticket/pdf/<?= $model->Id ?>" class="btn btn-primary">Imprimir</a>
			<?php endif ?>
			<a href="~/ticket/my/" class="pull-right">Voltar</a>
		</div>
	</div>
	<div class="span4">
		<h2>Ingresso</h2>
		<div class="well well-participant">
			<p class="pull-right"><span class="label <?= $model->getStatusLabel() ?>"><?= $model->getStatus() ?></span></p>
			<p>Tipo:</p>
			<p><b><?= $model->TicketName ?></b></p>
			<?php if($model->isConfirmed()): ?>
			<p>Número:</p>
			<p><b><?= $model->getNumber() ?></b></p>
			<?php endif ?>
			<p>Data da Compra:</p>
			<p><b><?= $model->getBuyDate() ?></b></p>
		</div>
	</div>
</div>
<div class="row-fluid">
	<div class="span12">
		<h2>Resumo da Compra</h2>
		<?= Import::view(array('model' => $model, 'date' => true, 'event' => $event), 'sale', 'summary'); ?>
	</div>
</div>