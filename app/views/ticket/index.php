<?= Import::view(array('s' => 1, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span4">
		<h2 class="pull-left">Resumo as Vendas</h2>
		<table class="table table-bordered table-summary">
			<tbody>
				<tr>
					<td>
						<dl class="dl-horizontal">
							<dt>Total Confirmado:</dt>
							<dd><?= $event->countSoldTickets() ?></dd>

							<?php if($model->Count): ?>
							<dt>Quantidade por Tipo:</dt>
							<dd>&nbsp;</dd>
							<div class="dl-horizontal-subitem">
								<?php foreach ($model->Data as $t): ?>
								<dt><?= $t->Name ?>:</dt>
								<dd>
									<div class="row-fluid">
										<div class="span6">
											<div class="progress progress-info">
												<div class="bar" style="width: <?= $t->getSoldPercentage() ?>%"><?= $t->countSolds() ?></div>
											</div>
										</div>
										<div class="span6">de <?= $t->Amount ?></div>
									</div>
								</dd>
								<?php endforeach; ?>
								<dt>TOTAL:</dt>
								<dd><?= $event->countSoldTickets() ?></dd>
							</div>

							<dt>Aguardando Pagto.:</dt>
							<dd><?= $event->countUnpaidTickets() ?></dd>
							<dt>Cancelados:</dt>
							<dd><?= $event->countCanceledTickets() ?></dd>
							<?php endif; ?>
						</dl>
					</td>
				</tr>
			</tbody>
		</table>

		<div class="control-group">
			<label class="control-label">
				Quem paga as taxas do serviço?
				<i class="icon-question-sign help-icon">
					<div class="hide">
						<div class="row-fluid">
							<h3>Você</h3>
							<p>As taxas dos serviços do EventMania são inclusas nos valores dos ingressos, dessa forma os ingressos são apresentados aos participantes com os preços que você definiu e as taxas serão descontadas desses preços.</p>
							<h3>Participantes</h3>
							<p>As taxas dos serviços do EventMania são adicionadas aos valores dos ingressos que você definiu e cobradas dos participantes. Dessa forma você receberá por cada ingresso exatamente o valor que definiu para eles.</p>
						</div>
					</div>
				</i>
			</label>
			<div class="controls btn-group">
				<a href="~/event/set-absorb/<?= $event->Id ?>/1" class="btn <?= $event->Absorb ? 'disabled' : '' ?>">Você</a>
				<a href="~/event/set-absorb/<?= $event->Id ?>/0" class="btn <?= !$event->Absorb ? 'disabled' : '' ?>">Participantes</a>
			</div>
		</div>
	</div>
	<div class="span8">
		<h2 class="pull-left">Ingressos</h2>
		<a href="~/ticket/add/<?= $event->Id ?>" class="btn btn-primary pull-right controls-inline">Criar Ingresso</a>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Tipo de Ingresso</th>
					<th>Termina</th>
					<th>Valor</th>
					<th>Taxa</th>
					<th>Total</th>
					<th>Qtde.</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $t): ?>
					<tr>
						<td>
							<a href="~/ticket/edit/<?= $t->Id ?>"><?= $t->Name ?></a>
						</td>
						<td><?= $t->getEndDate() ?></td>
						<td><?= $event->newCart(array($t))->getNetPrice() ?></td>
						<td><?= $event->lastCart()->getFee() ?></td>
						<td>
							<?= $event->lastCart()->getSalePrice() ?>
							<?php if($event->Absorb): ?>
							<span class="required">*</span>
							<?php endif; ?>
						</td>
						<td><?= $t->Amount ?></td>
						<td>
							<div class="btn-group dual-choice">
								<a href="~/ticket/show/<?= $t->Id ?>" class="btn btn-mini <?= $t->IsPrivate ? '' : 'active' ?>"><i class="icon-ok-circle" data-toggle="tooltip" title="<?= $t->IsPrivate ? 'Tornar Público' : '' ?>"></i></a>
								<a href="~/ticket/hide/<?= $t->Id ?>" class="btn btn-mini <?= $t->IsPrivate ? 'active' : '' ?>"><i class="icon-ban-circle" data-toggle="tooltip" title="<?= $t->IsPrivate ? '' : 'Tornar Privado' ?>"></i></a>
							</div>
						</td>
					</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td class="text-center" colspan="7"><a href="~/ticket/add/<?= $event->Id ?>" class="btn btn-primary">Crie seu primeiro ingresso</a></td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?php if($event->Absorb): ?>
		<span class="required">*</span> As taxas dos ingressos que serão pagas ao EventMania estão inclusas nestes valores.
		<?php endif; ?>
	</div>
</div>
