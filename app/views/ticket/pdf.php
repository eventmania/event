<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title><?= 'Ingresso #' . $model->Id ?></title>
		<style>
			body {font-family: 'Helvetica'; font-size: 12px; margin: 40px 10px; padding: 0;}
			h1 {font-size: 21px; margin: 10px 0; padding: 4px; text-align: center; font-weight: normal;}
			h2 {font-weight: normal; margin: 10px 0;}
			img {margin: 0; padding: 0;}
			.label {font-size: 11px; color: #888; padding: 4px; text-transform: uppercase;}
			.value {font-size: 14px; font-weight: bold; padding: 4px; margin-bottom: 6px;}
			.number {font-size: 16px; font-weight: bold; padding: 4px; margin-bottom: 6px;}
			.address {font-weight: normal; font-size: 12px;}
			
			.table {width: 100%; border-collapse: collapse; border-radius: 8px; overflow: hidden;}
			.table th {background: #fff; font-weight: bold; text-align: left;}
			.table td, .table th {border: 1px solid #ccc; padding: 5px;}
			
			.table-striped {border-radius: 3px;}
			.table-striped th, .table-striped td {padding: 6px;}
			.table-striped tr:nth-child(even) td {background: #f8f8f8;}
			
			.left {width: 40%; float: left;}
			.right {margin-left: 52%; height: 100px;}
			
			.column-right {width: 150px; text-align: center; margin-top: 0;}
			
			.date {color: #888; float: right; text-align: right; font-size: 11px; font-style: italic;}
			
			.hr {background: none; border: 0; border-top: 1px dotted #ccc; margin: 50px 0; padding: 4px; color: #888;}
			
			#logo {margin-top: 40px;}
		</style>
	</head>
	<body>
		<table class="table">
			<tbody>
				<tr>
					<td>
						<h1><?= htmlentities($model->EventName) ?></h1>
					</td>
					<td class="column-right" rowspan="4">
						<img src="data:image/png;base64,<?= $qrcode ?>" alt="QRCode">
						<div class="number"><?= $model->getNumber() ?></div>
						<br>
						<img src="<?= WWWROOT . 'img/logo-mini.png' ?>" alt="EventMania" id="logo">
					</td>
				</tr>
				<tr>
					<td>
						<div class="left">
							<div class="label">Data e Hor&aacute;rio</div>
							<div class="value"><?= htmlentities($event->getSchedule()) ?></div>
						</div>
						<div class="right">
							<div class="label">Local</div>
							<div class="value address"><?= htmlentities($event->Address) ?></div>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="label">Participante</div>
						<div class="value"><?= htmlentities($model->Name) ?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div class="date">Comprado em <?= $model->getBuyDate() ?></div>
						
						<div class="label">Ingresso</div>
						<div class="value"><?= htmlentities($model->TicketName) ?></div>
					</td>
				</tr>
			</tbody>
		</table>
	
		<div class="hr">Por favor, imprimir e levar este ingresso ao evento.</div>
		
		<div class="row-fluid">
			<div class="span12">
				<h2>Resumo da Compra</h2>
				<?= Import::view(array('model' => $model, 'date' => true, 'event' => $event), 'sale', 'summary'); ?>
			</div>
		</div>
	</body>
</html>
