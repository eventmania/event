<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Meus Pedidos</h2>
		<table class="table table-bordered table-striped table-my-tickers">
			<thead>
				<tr>
					<th>Tipo de Ingresso</th>
					<th class="span3">Data da Compra</th>
					<th class="span2">Valor</th>
					<th class="span2">Status</th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $pr): ?>
					<tr>
						<td>
							<a href="~/ticket/view/<?= $pr->Id ?>"><?= $pr->TicketName ?></a>
							<div class="muted"><?= $pr->EventName ?></div>
						</td>
						<td><?= $pr->getBuyDate() ?></td>
						<td><?= $pr->Cart->getSalePrice(); ?></td>
						<td><span class="label <?= $pr->getStatusLabel() ?>"><?= $pr->getStatus() ?></span></td>
					</tr>
					<?php endforeach; ?>
				<?php else: ?>
				<tr>
					<td colspan="4" class="text-center">Você ainda não comprou nenhum ingresso.</td>
				</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('ticket/my', $model->Count, $p, $m) ?>
	</div>
</div>