<?= Import::view(array('s' => 0, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span8">
		<h2>Adicionar Moderador</h2>

		<form id="manager-form" method="POST" action="">
			<fieldset>
				<?= BForm::input('Email do Moderador', 'Email', $model->Email, 'span12', $blockEmailEdit ? array('disabled' => 'disabled') : array(), true) ?>
				<div class="<?= $model->Name ? 'show' : 'hide' ?>">
					<?= BForm::input('Nome do Moderador', 'Name', $model->Name, 'span12', array('disabled' => 'disabled')) ?>
				</div>
				<?= BForm::select('Permissões', 'Role', $roles, $model->Role, 'span6', array(), true) ?>
			</fieldset>

			<a href="~/manager/<?= $event->Id ?>" class="pull-right">Cancelar</a>

			<button type="submit" class="btn btn-primary"><?= $submitLabel ?></button>
		</form>
	</div>
	<div class="span4">
		<div class="well well-small">
			<p>
				<b>Vizualizador</b><br>
				Consegue apenas visualizar o andatamento do evento.
			</p>
			<p>
				<b>Trocador</b><br>
				Consegue efetuar a troca de ingresso (ponto de troca).
			</p>
			<p>
				<b>Vendedor</b><br>
				Consegue cadastrar participantes manualmente. Porém não tem acesso aos ingressos privados.
			</p>
			<p>
				<b>Administrador</b><br>
				Possui "super poderes" e consegue realizar tudo dentro do evento.
			</p>
		</div>
	</div>
</div>

<?php if(App::$action == 'add'): ?>
<div id="modal-manager" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="modal-manager-label" aria-hidden="true">
	<div class="modal-header">
		<h3 id="modal-manager-label">Convidar Moderador</h3>
	</div>
	<div class="modal-body">
		<p>Não foi encontrado um usuário com este e-mail. Deseja convidá-lo para ser moderador?</p>
		<div class="row-fluid">
			<div class="span12">
				<div class="control-group">
					<label class="control-label" for="Name">Nome <span class="required">*</span></label>
					<div class="controls">
						<input name="Name" id="Name" value="" type="text" class="span12 required">
						<input type="hidden" id="EventId" value="<?= $eventId ?>">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn btn-primary" id="manager-invite">Enviar Convite</button>
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
	</div>
</div>
<?php endif ?>