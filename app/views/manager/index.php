<?= Import::view(array('s' => 0, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Moderadores</h2>
		<a href="~/manager/add/<?= $event->Id ?>" class="btn btn-primary pull-right controls-inline">Adicionar Moderador</a>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th class="">Nome</th>
					<th class="span3">Permissão</th>
					<th class="span1">&nbsp;</th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach ($model->Data as $manager): ?>
						<tr>
							<?php if($manager->isOwner()): ?>
							<td><?= $manager->Name ?></td>
							<?php else: ?>
							<td><a href="~/manager/edit/<?= $manager->Id ?>"><?= $manager->Name ?></a></td>
							<?php endif ?>
							<td><?= $manager->getRole() ?></td>
							<td>
							<?php if(!$manager->isOwner()): ?>
								<div class="btn-group dual-choice">
									<a href="~/manager/block-switch/<?= $manager->Id ?>" class="btn btn-mini <?= $manager->isActive() ? 'active' : '' ?>"><i class="icon-ok-circle" data-toggle="tooltip" title="<?= $manager->isActive() ? '' : 'Desbloquear' ?>"></i></a>
									<a href="~/manager/block-switch/<?= $manager->Id ?>" class="btn btn-mini <?= $manager->isActive() ? '' : 'active' ?>"><i class="icon-ban-circle" data-toggle="tooltip" title="<?= $manager->isActive() ? 'Bloquear' : '' ?>"></i></a>
								</div>
							<?php endif ?>
							</td>
						</tr>
					<?php endforeach; ?>
				<?php else: ?>
					<tr>
						<td colspan="3" class="text-center">
							<a href="~/manager/add" class="btn btn-primary controls-inline">Adicione um Moderador!</a>
						</td>
					</tr>
				<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('manager/index', $model->Count, $p, $m) ?>
	</div>
</div>
