<?= Import::view(array('event' => $event), '_snippet', 'event-public-header') ?>
<div id="match-reviewers" class="row-fluid">
    <div class="span12">
        <h2 class="pull-left">Artigos</h2>
        <table class="table table-bordered table-striped papers">
            <thead>
                <tr>
                    <th>Título</th>
                    <th>Palavras Chave</th>
                    <th class="span1">Download</th>
                </tr>
            </thead>
            <tbody>
                <?php if($model->Count): ?>
                    <?php foreach($model->Data as $review): ?>
                    <tr>
                        <td><a href="~/reviewer/review/<?= $review->Id ?>"><?= $review->Title ?></a></td>
                        <td><?= $review->Keywords ?></td>
                        <td>
                            <a href="<?= $review->getUrl() ?>" class="btn"><i class="icon-download"></i></a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                <tr>
                    <td class="text-center" colspan="3">Ainda não foi submetido nenhum trabalho.</td>
                </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?= Pagination::create('reviewer/event', $model->Count, $p, $m) ?>
    </div>
</div>
