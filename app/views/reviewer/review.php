<?= Import::view(array('event' => $event), '_snippet', 'event-public-header') ?>
<?= Import::view(array('model' => $model, 'showAuthors' => false), 'paper', 'summary') ?>

<div class="row-fluid">
    <div class="span12">
    <h2>Avaliação</h2>
    <form method="POST" action="">
        <fieldset>
            <div class="row-fluid">
                <div class="span3">
                    <?= BForm::select('Nota de Apresentação <small class="muted">(peso 1)</small>', 'PresentationRate', Review::$Scores, $model->PresentationRate, 'span12', array(), true) ?>
                </div>
                <div class="span3">
                    <?= BForm::select('Nota de Conteúdo <small class="muted">(peso 1)</small>', 'ContentRate', Review::$Scores, $model->ContentRate, 'span12', array(), true) ?>
                </div>
                <div class="span3">
                    <?= BForm::select('Nota de Impacto <small class="muted">(peso 1)</small>', 'ImpactRate', Review::$Scores, $model->ImpactRate, 'span12', array(), true) ?>
                </div>
                <div class="span3">
                    <?= BForm::select('<strong>Recomendação</strong> <small class="muted">(peso 3)</small>', 'Recommendation', Review::$Recommendations, $model->Recommendation, 'span12', array(), true) ?>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <?= BForm::textarea('Comentários para o autor', 'PublicComments', $model->PublicComments) ?>
                </div>
                <div class="span6">
                    <?= BForm::textarea('Comentários para a organização', 'PrivateComments', $model->PrivateComments) ?>
                </div>
            </div>
            <a href="~/reviewer/<?= $event->Id ?>" class="pull-right">Cancelar</a>
            <button type="submit" name="Status" value="<?= Review::PUBLISHED_STATUS ?>" class="btn btn-primary">Enviar Avaliação</button>
            <button type="submit" name="Status" value="<?= Review::DRAFT_STATUS ?>" class="btn">Salvar Rascunho</button>
        </fieldset>
    </form>
    </div>
</div>
