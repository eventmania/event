<section id="reviews" class="row-fluid">
    <div class="span12">
        <h2 class="pull-left">Revisões</h2>

        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Evento</th>
                    <th class="span2">Data Limite</th>
                    <th class="span2">Ver Evento</th>
                </tr>
            </thead>
            <tbody>
                <?php if($model->Count): ?>
                    <?php foreach($model->Data as $event): ?>
                    <tr>
                        <td><a href="~/reviewer/<?= $event->Id ?>"><?= $event->Name ?></a></td>
                        <td><?= $event->getRevisionDeadLine() ?></td>
                        <td><a href="<?= $event->getUrl() ?>" target="_blank" class="btn btn-small" data-toggle="tooltip" title="Avaliar"><i class="icon-eye-open"></i></a></td>
                    </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="3">Você não é revisor de nenhum evento.</td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <?= Pagination::create('reviewer/my', $model->Count, $p, $m) ?>
    </div>
</section>
