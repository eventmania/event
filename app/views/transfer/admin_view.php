<?= Import::view(array('user' => $user, 's' => 4), '_snippet', 'admin-header'); ?>
<div class="row-fluid">
	<div class="span12">
		<?php if($model): ?>
		<?= Import::view(array('transfer' => $model), 'transfer', 'details') ?>
		
		<h3>Alteração de Status</h3>
		<form method="POST" action="">
			<div class="row-fluid">
				<div class="span5">
					<?= BForm::select('Alterar Status', 'Status', Transfer::getStatuses(), null, 'span12', array(), true) ?>
					<?= BForm::textarea('Anotação', 'Description') ?>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Salvar Alteração</button>
		</form>
		<?php elseif($event->isPaid()): ?>
		<div class="alert">Este evento não cadastrou uma conta para receber os repasses.</div>
		<?php else: ?>
		<div class="alert">Este evento não possui ingressos pagos.</div>
		<?php endif ?>
	</div>
</div>