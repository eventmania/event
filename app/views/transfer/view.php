<?= Import::view(array('s' => 0, 'event' => $event), '_snippet', 'event-header') ?>
<div class="row-fluid">
	<div class="span12">
		<?php if($model): ?>
			<?= Import::view(array('transfer' => $model), 'transfer', 'details') ?>
		<?php elseif($event->isPaid()): ?>
			<div class="alert alert-block">
				<h3 class="alert-heading">Dados Bancários não Definidos!</h3>
				<p>Você deve fornecer as informações bancárias que irão receber os repasses deste evento.</p>
				<p>
					<a href="~/sale/index/<?= $id ?>/#transfer" class="btn btn-primary">Informar Dados Bancários!</a>
				</p>
			</div>
		<?php else: ?>
		<div class="alert alert-block">
			<h3>Atenção!</h3>
			O repasse é a transferência do que foi arrecadado pelo e evento, por meio do EventMania, com as devidas taxas adminitrativas descontadas.
			Detectamos que seu evento não possui ingressos com valor cobrado, portanto não repasse esse evento.
		</div>
		<?php endif ?>
	</div>
</div>