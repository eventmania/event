<div class="row-fluid">
	<?= Import::view(array('s' => 1), '_snippet', 'account-menu') ?>
	<div class="span8">
		<h2>Informações Bancárias</h2>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Evento</th>
					<th>Valor</th>
					<th>Status</th>
				</tr>
			</thead>
			<tbody>
				<?php if($model->Count): ?>
					<?php foreach($model->Data as $transfer): ?>
					<tr>
						<td>
							<a href="~/transfer/view/<?= $transfer->Id ?>"><?= $transfer->EventName ?></a>
						</td>
						<td>
							<?= $transfer->Value > 0 ? Format::money($transfer->Value) : '' ?>
						</td>
						<td>
							<?= $transfer->getStatus() ?>
						</td>
					</tr>
					<?php endforeach ?>
				<?php endif ?>
			</tbody>
		</table>
		<?= Pagination::create('transfer/index', $model->Count, $p, $m) ?>
	</div>
</div>