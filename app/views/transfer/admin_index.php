<?= Import::view(array('user' => $user, 's' => 4), '_snippet', 'admin-header'); ?>
<div class="row-fluid">
	<div class="span12">
		<h2 class="pull-left">Repasses</h2>
		<form class="input-append pull-right controls-inline" method="GET" action="">
			<input type="text" name="q" value="<?= $q ?>" placeholder="Nome do usuário ou evento" />
			<button type="submit" class="btn btn-primary">Pesquisar</button>
		</form>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>Evento</th>
					<th>Fim do Evento</th>
					<th>Valor</th>
					<th class="span3">Status</th>
				</tr>
			</thead>
			<tbody>
			<?php if($model->Count): ?>
				<?php foreach ($model->Data as $transfer): ?>
				<tr>
					<td>
						<a href="~/admin/transfer/view/<?= $transfer->Id ?>"><?= $transfer->EventName ?></a>
					</td>
					<td>
						<?php if(time() > $transfer->EventEndDate): ?>
							<s><?= Format::date($transfer->EventEndDate) ?></s>
						<?php else: ?>
							<?= Format::date($transfer->EventEndDate) ?>
						<?php endif ?>
					</td>
					<td><?= Format::money($transfer->Value) ?></td>
					<td><?= $transfer->getStatus() ?></td>
				</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td colspan="4" class="text-center">Nenhum organizador solicitou repasse.</td>
				</tr>
			<?php endif; ?>
			</tbody>
		</table>
		<?= Pagination::create('admin/transfer/index', $model->Count, $p, $m) ?>
	</div>
</div>