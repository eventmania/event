<h2>Detalhes do Repasse</h2>
<dl class="dl-horizontal">
	<dt>Status</dt>
	<dd><?= $transfer->getStatus() ?></dd>

	<dt>Código da Transação</dt>
	<dd><?= $transfer->Code ?></dd>

	<dt>Total</dt>
	<dd><?= $transfer->Value ? Format::money($transfer->Value) : '' ?></dd>
</dl>

<h3>Saque para</h3>
<dl class="dl-horizontal">
	<dt>Favorecido</dt>
	<dd><?= $transfer->Name ?></dd>

	<dt>CPF</dt>
	<dd><?= $transfer->CPF ?></dd>

	<dt>Código do Banco</dt>
	<dd><?= $transfer->Bank ?></dd>

	<dt>Nome do Banco</dt>
	<dd><?= $transfer->getBank() ?></dd>

	<dt>Tipo de Conta</dt>
	<dd><?= $transfer->getAccountType() ?></dd>

	<dt>Agência</dt>
	<dd><?= $transfer->Agency ?></dd>

	<dt>Conta</dt>
	<dd><?= $transfer->Account ?></dd>
</dl>

<h3>Histórico de Mudanças de Status</h3>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>Data</th>
			<th>Status</th>
			<th>Anotação</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($transfer->getHistory()->Data as $history): ?>
			<tr>
				<td><?= Format::datetime($history->Date) ?></td>
				<td><?= $history->getStatus() ?></td>
				<td><?= $history->Description ?></td>
			</tr>
		<?php endforeach ?>
	</tbody>
</table>
