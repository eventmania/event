<?= Import::view(array('s' => 0, 'event' => $model), '_snippet', 'event-header') ?>
<div class="span8">
    <h2>Escolha o Layout</h2>

    <?= Import::view(array('i' => 2, 'eventId' => $model->Id ? $model->Id : '', 'hasLinks' => true), '_snippet', 'event-wizard') ?>

    <div class="row-fluid">
        <ul class="nav theme-selctor">
            <li>
                <a href="default">
                    <img src="~/img/theme-icons/default.png">
                    <br>
                    <span>Default</span>
                </a>
            </li>
            <li>
                <a href="space">
                    <img src="~/img/theme-icons/space.png">
                    <br>
                    <span>Space</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="row-fluid">
        <div class="form-container"><?= $form ?></div>
    </div>

    <script type="text/javascript">
        var EventId = <?= $model->Id ?>;
    </script>
</div>
