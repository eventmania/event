<?php

class MailFormater
{
    public static function import($template, $style, $vars = array())
    {
        $css = Import::view(array(), $style['controller'], $style['view']);
        $css = self::proccessCss($css);

        require_once(App::$root . 'app/vendors/phpQuery/phpQuery.php');

        $html = Import::view($vars, $template['controller'], $template['view']);
        $doc = phpQuery::newDocument(self::resolveUrl($html));

        foreach ($css as $selector => $rules)
            $doc->find($selector)->attr('style', $rules);

        return $doc->htmlOuter();
    }

    public static function proccessCss($css, $breakProperties = false)
    {
        $regex =  '/(?<selector>(?:(?:[^,{]+),?)*?)\{(?:(?<name>[^}:]+):?(?<value>[^};]+);?)*?\}/';
        preg_match_all($regex, $css, $matches);
        $rulesStr = array_shift($matches);

        $rules = array();
        foreach ($rulesStr as $r)
        {
            $r = str_replace("\n", '', $r);
            preg_match('/^(.*)\{(.*)\}$/', $r, $matches);

            $selector = trim($matches[1]);
            $properties = trim($matches[2]);

            if($breakProperties)
            {
                preg_match_all('/(.*?)\:(.*?)\;/', $properties, $matches);

                $properties = array_map('trim', $matches[1]);
                $values = array_map('trim', $matches[2]);

                $rules[$selector] = array_combine($properties, $values);
            }
            else
            {
                $rules[$selector] = $properties;
            }

        }

        return $rules;
    }

    public static function resolveUrl($html)
    {
        return str_replace(array('"~/', "'~/"), array('"'. App::$rootVirtual, "'". App::$rootVirtual), $html);
    }
}
