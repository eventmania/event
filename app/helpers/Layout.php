<?php

class Layout
{
    public $Id;

    private $_data = array();

    public function __set($prop, $value)
    {
        $this->_data[$prop] = $value;
    }

    public function __get($prop)
    {
        if(isset($this->_data[$prop]))
            return $this->_data[$prop];
        return '';
    }

    public function __construct($id, $config)
    {
        $this->Id = $id;
        $config = json_decode($config);

        if($config)
            foreach ($config as $key => $value)
            {
                if(is_object($value) && isset($value->Image))
                    $this->_data[$key] = new ImageService($value->Image);
                else
                    $this->_data[$key] = $value;
            }
    }

    public function toJson()
    {
        return json_encode($this->_data);
    }
}