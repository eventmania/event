<?php
class Table
{
	private $link;
	private $page;
	private $actual;
	private $order;
	
	public function __construct($link, $page, $actual, $order)
	{
		$this->link = $link;
		$this->page = $page;
		$this->actual = $actual;
		$this->order = strtoupper($order);
	}
	
	public function th($title, $column)
	{
		$icon = '';
		$type = 'ASC';
		if($this->actual == $column)
		{
			if($this->order == 'ASC')
			{
				$icon = '<i class="icon-chevron-up pull-right"></i>';
				$type = 'DESC';
			}
			else
			{
				$icon = '<i class="icon-chevron-down pull-right"></i>';
			}
		}
		return '<th><a href="~/' . $this->link . '/' . $this->page . '/' . $column . '/' . $type . '">' . $title . $icon . '</a></th>';
	}
}
