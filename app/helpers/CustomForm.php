<?php

class CustomForm
{
    protected $_fields = array();

    protected $_fieldIds = array();

    protected $_hasResponsability = false;

    public $Exists = true;

    public function __construct($fields = array())
    {
        if($fields)
            foreach ($fields as $f)
                $this->add($f);
        else
            $this->Exists = false;
    }

    public function setData($data)
    {
        foreach ($data as $d)
        {
            $field = $this->getField($d->Index);
            $field->setValue($d->Value);
        }
    }

    public function add($field)
    {
        $fieldType = $field->Type . 'CustomField';
        $f = new $fieldType();

        $f->setContent($field->Content);
        $f->setTitle($field->Title);
        $f->setRequired($field->IsRequired);
        $f->setId($field->Id);

        $this->_hasResponsability = $this->_hasResponsability || $field->Type == 'Responsability';

        $this->_fields[$field->Index] = $f;
        array_push($this->_fieldIds, $field->Id);
    }

    public function render()
    {
        $str = '';
        foreach ($this->_fields as $f)
            $str .= $f->render();
        return $str;
    }

    public function exists()
    {
        return $this->Exists;
    }

    public function hasResponsability()
    {
        return $this->_hasResponsability;
    }

    public function deleteFields($fields, $eventId)
    {
        $ids = array_keys($fields);
        foreach ($this->_fieldIds as $id)
        {
            if(array_search($id, $ids) === false)
            {
                $field = Field::get($id);
                if($field->EventId == $eventId)
                    $field->delete();
            }
        }
    }

	public function deleteAllFields($fields, $eventId)
    {
        $ids = array_keys($fields);
        foreach ($this->_fieldIds as $id)
        {
            if(array_search($id, $ids) === false)
            {
                $field = Field::get($id);
                if($field->EventId == $eventId)
                    $field->delete();
            }
        }
    }

    public function getFields()
    {
        return $this->_fields;
    }

    public function getField($index)
    {
        return $this->_fields[$index];
    }

    public function getLastIndex()
    {
        if($this->_fields)
        {
            $indexes = array_keys($this->_fields);
            return max($indexes) + 1;
        }

        return 0;
    }
}
