<?php
abstract class GenericCustomField implements ICustomField
{
    public $Id = 0;

    public $Value = '';

    public $Content = '';

    public $Title = '';

    public $IsRequired = 0;

    const FIELD_PREFIX = 'CustomField';

    protected function _render($view, $model)
    {
        return Import::view(array(
            'model' => $model
        ), 'custom-field/field', $view);
    }

    abstract function render();

    public function setValue($value)
    {
        $this->Value = $value;
    }

    public function setContent($content)
    {
        $this->Content = $content;
    }

    public function setTitle($title)
    {
        $this->Title = $title;
    }

    public function setRequired($isRequired)
    {
        $this->IsRequired = $isRequired;
    }

    public function setId($id)
    {
        $this->Id = $id;
    }

    public function hasInformation()
    {
        return true;
    }

    public function getValue()
    {
        return $this->Value;
    }

    public function getContent()
    {
        return $this->Content;
    }

    public function getTitle()
    {
        return $this->Title;
    }

    public function isRequired()
    {
        return $this->IsRequired;
    }

    public function getId()
    {
        return $this->Id;
    }

    public function getName()
    {
        return self::FIELD_PREFIX . "[$this->Id]";
    }
}