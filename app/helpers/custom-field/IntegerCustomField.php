<?php
class IntegerCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('integer', $this);
    }
}