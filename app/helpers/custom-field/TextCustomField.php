<?php
class TextCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('text', $this);
    }
}