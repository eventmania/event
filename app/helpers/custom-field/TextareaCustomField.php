<?php
class TextareaCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('textarea', $this);
    }
}