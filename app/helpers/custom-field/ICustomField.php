<?php

interface ICustomField
{
    public function render();

    public function setValue($value);

    public function setContent($content);

    public function setTitle($title);

    public function setRequired($isRequired);

    public function setId($id);

    public function hasInformation();

    public function getValue();

    public function getContent();

    public function getTitle();

    public function isRequired();

    public function getId();

    public function getName();
}