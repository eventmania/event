<?php
class DoubleCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('double', $this);
    }
}