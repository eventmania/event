<?php
class CpfCnpjCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('cpfcnpj', $this);
    }
}