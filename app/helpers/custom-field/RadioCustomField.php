<?php
class RadioCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('radio', $this);
    }

    public function setContent($content)
    {
        $options = explode(';', $content);
        array_shift($options);

        $this->Content = array_combine($options, $options);
    }
}