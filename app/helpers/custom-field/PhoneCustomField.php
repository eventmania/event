<?php
class PhoneCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('phone', $this);
    }
}