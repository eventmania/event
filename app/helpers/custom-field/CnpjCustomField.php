<?php
class CnpjCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('cnpj', $this);
    }
}