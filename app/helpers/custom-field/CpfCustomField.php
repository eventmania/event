<?php
class CpfCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('cpf', $this);
    }
}