<?php
class TimeCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('time', $this);
    }

    public function setContent($content)
    {
        $options = array();
        
        for($i = 0; $i < 24; $i++)
        {
            $time = str_pad($i, 2, '0', STR_PAD_LEFT) . ':' . '00';
            $options[$time] = $time;

            $time = str_pad($i, 2, '0', STR_PAD_LEFT) . ':' . '30';
            $options[$time] = $time;
        }

        $this->Content = $options;
    }
}