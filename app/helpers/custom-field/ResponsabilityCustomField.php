<?php
class ResponsabilityCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('responsability', $this);
    }

    public function hasInformation()
    {
        return false;
    }
}