<?php
class DateCustomField extends GenericCustomField implements ICustomField
{
    public function render()
    {
        return $this->_render('date', $this);
    }
}