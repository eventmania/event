$(document).ready(function() {
    $('#paper-form .btn-group[name="HasPapers"] button').click(function(){
        if($(this).val() == '1'){
            $('#paper-config').show();
        }else{
            $('#paper-config').hide();
        }
    });

    $('#turn-off-resubmission').click(function() {
        var self = $(this);
        var row = self.closest('.row-fluid');
        row.find('.paper-has-resubmission').hide();
        row.find('.paper-hasnt-resubmission').show();
        row.find('input').val((new Date()).format());
        row.find('.btn-group[name="HasResubmission"] button[value="0"]').click();
    });

    $('#turn-off-fakedeadline').click(function() {
        var self = $(this);
        var row = self.closest('.row-fluid');
        row.find('.paper-has-fakedeadline').hide();
        row.find('.paper-hasnt-fakedeadline').show();
        row.find('.paper-has-fakedeadline input').val((new Date()).format());
        row.find('.btn-group[name="HasFakeDeadLine"] button[value="0"]').click();
    });

    $('#paper-form .btn-group[name="HasFakeDeadLine"] button').click(function(){
        if($(this).val() == '1'){
            $('.paper-has-fakedeadline').show();
            $('.paper-hasnt-fakedeadline').hide();
            $('.paper-has-fakedeadline input').each(function() {
                var self = $(this);
                self.attr('name', self.attr('id'));
            });
        }else{
            $('.paper-has-fakedeadline').hide();
            $('.paper-hasnt-fakedeadline').show();
            $('.paper-has-fakedeadline input').each(function() {
                $(this).removeAttr('name');
            });
        }
    });

    $('#paper-form .btn-group[name="HasResubmission"] button').click(function(){
        if($(this).val() == '1'){
            $('.paper-has-resubmission').show();
            $('.paper-hasnt-resubmission').hide();
            $('.paper-has-resubmission input').each(function() {
                var self = $(this);
                self.attr('name', self.attr('id'));
            });
        }else{
            $('.paper-has-resubmission').hide();
            $('.paper-hasnt-resubmission').show();
            $('.paper-has-resubmission input').each(function() {
                $(this).removeAttr('name');
            });
        }
    });

    $('.btn-reviewer-remove').click(function() {
        $(this).closest('tr').remove();
    });

    $('#paper-form #add-reviewer').click(function(){
        var self = $(this);
        var email = self.parent().find('input').val();
        self.val('');
        $.get(ROOT + 'user/consult/' + email, function(data){
            if(data.d){
                if($('#reviewers-list span[data-name="'+data.d.Name+'"]').length) {
                    app.modal.alert('Esse revisor já foi incluído.');
                } else {
                    $('#reviewers-list').append(
                        $('<tr>').append(
                            $('<td>').append(
                                $('<input type="hidden">').val(data.d.Id).attr('name', 'ReviewerId[]'),
                                $('<span data-name="'+data.d.Name+'">').text(data.d.Name)
                            ),
                            $('<td>').append(
                                $('<a title="Remover" data-toggle="tooltip" class="btn btn-danger btn-small btn-reviewer-remove" href="javascript:void(0);" data-original-title="Remover">')
                                .append('<i class="icon-white icon-remove"></i>')
                                .click(function() {
                                    $(this).closest('tr').remove();
                                })
                            )
                        )
                    );
                }
            }else{
                var id, modal;
                id = $('#EventId').val();

                modal = $('#modal-reviewer');
                modal.find('#reviewer-invite').click(function() {
                    var name = modal.find('#Name').val();
                    if(name != '' && name.length > 5) {
                        $('.alert').hide();
                        $.post(ROOT + 'user/invite/reviewer', {Id: id, Email: email, Name: name}, function(data){
                            if(data.d.success != undefined) {
                                $('#reviewers-list').append(
                                    $('<tr>').append(
                                        $('<td>').append(
                                            $('<input type="hidden">').val(data.d.id).attr('name', 'ReviewerId[]'),
                                            $('<span data-name="'+ name +'">').text(name)
                                        ),
                                        $('<td>').append(
                                            $('<a title="Remover" data-toggle="tooltip" class="btn btn-danger btn-small btn-reviewer-remove" href="javascript:void(0);" data-original-title="Remover">')
                                            .append('<i class="icon-white icon-remove"></i>')
                                            .click(function() {
                                                $(this).closest('tr').remove();
                                            })
                                        )
                                    )
                                );
                                modal.modal('hide');
                            } else {
                                app.alert('#modal-reviewer .modal-body', 'error', data.d.error);
                            }
                        });
                    } else {
                        app.alert('#modal-reviewer .modal-body','Por favor, informe um nome maior.');
                    }
                });
                modal.modal('show');
            }
        });
    });
});
