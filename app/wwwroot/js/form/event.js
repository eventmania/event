$(document).ready(function() {
	/*## Form Event ##*/
    $('#eventOnline').change(function() {
        if($(this).is(':checked')) {
            $('#MapsContainer').fadeOut();
            $('#Address').attr('readonly', 'readonly').attr('placeholder', 'O mapa não será apresentado para eventos online');
        } else {
            $('#MapsContainer').fadeIn();
            $('#Address').removeAttr('readonly', 'readonly').attr('placeholder', 'Local onde o evento será realizado');
        }
    });

    $('#maxCapacity').click(function() {
        $(this).hide();
        $('#maxCapacityInput').show();
    });

    $('#maxCapacityInput').blur(function() {
        $(this).hide();
        $('#maxCapacity').show().text($(this).val());
    });

    $('.btnRemoveTicket').click(function() {
        $(this).parent().parent().parent().remove();
    });
    $('.btnDetailsTicket').click(function() {
        var details = $(this).closest('.fieldsTicket').find('.ticketDetails').toggle();
    });

    $('#btnAddTicket, #btnAddTicketFree').click(function(e) {
		var ref = new Date().getTime(),
			self = $(this),
			fieldsInternal = $('#ticketTemplate').clone().find('tr').attr('ref', ref);
        $('[data-toggle="tooltip"]', fieldsInternal).tooltip();
        $('.number', fieldsInternal).focusout(function() {
            var element = $(this);
            element.val(element.val().replace(/\D/g, ''));
        });

		if(self.attr('id') === 'btnAddTicket') {
			$('.money', fieldsInternal).maskMoney({symbol: '', thousands: '.', decimal: ',', allowZero: true});
		} else {
			$('.money', fieldsInternal).attr('type','hidden').val('0,00').after('Grátis');
		}

		$('.date', fieldsInternal).datepicker({
			format: "dd/mm/yyyy",
			language: "pt-BR",
			autoclose: true
		});
		$('.ticket-visibility-check', fieldsInternal).change(function() {
			$('.ticket-visibility-hide', fieldsInternal).val($(this).is(':checked') ? 1 : 0);
		});
        $('.btn-ticket-remove', fieldsInternal).click(function() {
            $('tr[ref="'+ ref +'"]').remove();
        });

		$('.btn-ticket-details', fieldsInternal).click(function() {
            $('.ticket-details[ref="'+ ref +'"]').toggleClass('hide');
        });
        $('.table-ticket tbody').append(fieldsInternal);
    });
	$('#btnAddTicket').click();

    $('#event-form #Name, #event-form #Slug').change(function(){
        var self = $(this);
        var str = self.val();
        var eventId = $('#Id').val() || '0';

        if(self.attr('id') == 'Name' &&  $('#Name').attr('data-slug'))
            return false;
        else
            $('#Name').attr('data-slug', '1');

        $.ajax({
            url: ROOT + 'event/check-slug/' + eventId + '/',
            data: {Slug: str},
            success: function(data){
                if(data.d.slug){
                    $('#Slug').val(data.d.slug);
                }
            },
            beforeSend: function(){
                $('#Slug').val('Aguarde...').attr('disabled', 'disabled');
            },
            complete: function(){
                $('#Slug').removeAttr('disabled');
            }
        });
    });

    $('#event-form #StartDate').change(function() {
        $('.react[data-target="#StartDate"]').val($(this).val());
    });

    $('.react[data-target="#StartDate"]').change(function() {
        $(this).attr('data-target', '');
    });
});
