$(document).ready(function() {
    //## Titles ##//
    $('.modal-custom-field .Title').change(function(){
        var self = $(this);
        self.closest('.modal-body').find('.snippet span.field-label').text(self.val());
    });

    //## Requireds ##//
    $('.modal-custom-field .required button').click(function(){
        var self = $(this);
        var value = self.val();
        var body = self.closest('.modal-body');
        body.find('.IsRequired').val(value);

        if(value)
            body.find('.snippet span.required').show();
        else
            body.find('.snippet span.required').hide();
    });

    //## Types ##//
    $('.modal-custom-field .type button').click(function(){
        var self = $(this);
        var type = self.val();
        var body = self.closest('.modal-body');
        body.find('.Type').val(type);
        body.find('.snippet .control').hide();
        body.find('.snippet .' + type).show();
    });

    //## Contents ##//
    $('.modal-custom-field .Content').change(function(){
        var self = $(this);
        var value = self.val();
        var body = self.closest('.modal-body');
        body.find('.show-default-value').text(value);
    });

    //## Add Snnipet ##//
    $('.modal-custom-field .btn-primary').click(function(){
        var self = $(this);
        var body = self.closest('.modal');
        var snippet = body.find('.snippet').clone().show();
        var div = $('<div class="hide"></div>');

        if(!body.valid())
            return false;

        body.find('.Index').val(CustomFieldIndex++);

        // text area bugado clona sem copiar o conteúdo... ¬¬
        var textarea = body.find('textarea.Content');
        var value = textarea.val();
        textarea.clone().val(value).appendTo(div);

        body.find('input[type="hidden"], input[name="Title[]"]').clone().appendTo(div);
        div.appendTo(snippet);
        snippet.appendTo('#wysiwyg-form .sandbox');

        snippet.find('.btn-close').click(function(){
            $(this).closest('.snippet').remove();
        });

        // clear
        body.find('input[type="text"][name="Title[]"]').val('');
        body.find('.Content').val('');
        body.find('.snippet select.Content').empty();
        body.find('.snippet .btn-group.Content').empty();
        body.find('ul.content').empty();
    });

    //## Add Option ##//
    $('.modal-custom-field .add-btn').click(function(){
        var self = $(this);
        var body = self.closest('.modal-body');
        var option = body.find('.Option');
        var value = option.val();
        var content = body.find('input.Content');

        if(value){
            body.find('ul.content').append($('<li>').text(value));
            content.val(content.val() + ';' + value);
            option.val('');
            body.find('.snippet select.Content').append(
                $('<option>').attr('value', value).text(value)
            );
            body.find('.snippet .btn-group.Content').append(
                $('<button type="button" class="btn">').attr('value', value).text(value)
            );
        }
    });

    //## Date Types ##//
    $('#modal-custom-field-date .type button').click(function(e){
        var self = $(this);
        var value = self.val();
        var body = self.closest('.modal-body');
        if(value == 'Date'){
            body.find('.snippet .Time, .snippet label.hide, .snippet .Time .btn-close').hide();
            body.find('.snippet .Date, .snippet label.show, .snippet .Date .btn-close').show();
        }else if(value == 'Time'){
            body.find('.snippet .Date, .snippet label.show, .snippet .Date .btn-close').hide();
            body.find('.snippet .Time, .snippet label.hide, .snippet .Time .btn-close').show();
        }else{
            body.find('.snippet .Date, .snippet label.show, .snippet .Time, .snippet .Time .btn-close').show();
            body.find('.snippet label.hide, .snippet .Date .btn-close').hide();
        }
    });

    //## Cpf Types ##//
    $('#modal-custom-field-cpf .type button').click(function(){
        var self = $(this);
        var value = self.text();
        var body = self.closest('.modal-body');
        body.find('.Title').val(value).change();
    });

    //## Responsability ##//
    $('#modal-custom-field-responsability textarea').change(function(){
        $('a[href="#modal-custom-field-responsability"]').remove();
        $('#modal-terms .terms').text($(this).val());
    });

    //## Remove Fields ##//
    $('#wysiwyg-form-gotit').click(function(){
        $('.sandbox .snippet').each(function(){
            $(this).find('.control-group:last')
            .prepend(
                $('<button type="button" class="btn btn-mini btn-danger btn-close"><i class="icon-white icon-remove"></i></button>').click(function(){
                    var self = $(this);
                    app.modal.confirm('Este capo será excluído, ao salvar o formulário as informações dos participantes referentes ao campo serão apagadas. Tem certeza que deseja continuar?', function(){
                        var isResponsability = self.closest('.row-fluid').find('#modal-terms1').length;
                        if(isResponsability)
                            $('a[href="#modal-custom-field-responsability"]').removeClass('hide');
                        self.closest('.row-fluid').remove();
                    });
                })
            );
        });
        $(this).closest('.alert').remove();
    });
});
