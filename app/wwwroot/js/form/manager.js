$(document).ready(function() {
    $('#manager-form #Email').change(function(){
        var email = $(this).val();
        $.get(ROOT + 'user/consult/' + email, function(data){
            var user = data.d;
            if(user){
                $('#Name').val(user.Name).closest('.hide').show();
                $('button[type="submit"]').removeAttr('disabled').removeClass('disabled');
            } else {
                var id, role, modal;
                id = $('#EventId').val();
                role = $('#Role').val();

                modal = $('#modal-manager');
                modal.find('#manager-invite').click(function() {
                    var name = modal.find('#Name').val();
                    if(name != '' && name.length > 5) {
                        $('.alert').hide();
                        $.post(ROOT + 'user/invite/', {Id: id, Email: email, Role: role, Name: name}, function(data){
                            if(data.d.success != undefined) {
                                app.alert('#manager-form', 'success', data.d.success);
                                modal.modal('hide');
                            } else {
                                app.alert('#modal-manager .modal-body', 'error', data.d.error);
                            }
                        });
                    } else {
                        app.alert('#modal-manager .modal-body','Por favor, informe um nome maior.');
                    }
                });
                modal.modal('show');
                $('button[type="submit"]').attr('disabled', 'disabled').addClass('disabled');
            }
        });
    });
});