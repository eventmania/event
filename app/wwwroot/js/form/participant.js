$(document).ready(function() {
	/*## Participant Add ##*/
	$('#WorkshopId, #TicketId').change(function(event) {
		var value = 0, valueTicket = 0, valueWorkshop = 0;
		valueTicket = parseFloat($('#TicketId option:checked').attr('data-price'));
		valueWorkshop = parseFloat($('#WorkshopId option:checked').attr('data-price'));
		value = valueTicket;
		if(!isNaN(valueWorkshop))
			value = valueTicket + valueWorkshop;
		$('#participant-value').text('R$ ' + value.toFixed(2).toString().replace('.', ','));
	});
});