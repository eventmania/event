$(document).ready(function() {
    $('#account-form #TypeCPF').change(function(){
        var type = $(this).val(),
			field = $('#CPF');
		
		if(type === 'CPF') {
			field.mask('999.999.999-99');
		} else {
			field.mask('99.999.999/9999-99');
		}
    }).change();
	
	$('#account-form #Bank').change(function(){
        var bank = $(this).val();
		if(bank === 'Akatus') {
			$('#no-moip').hide();
			$('label[for="Agency"]').html('E-mail <span class="required">*</span>');
		} else {
			$('#no-moip').show();
			$('label[for="Agency"]').html('Agência <span class="required">*</span>');
		}
		//$('#account-form input[type="text"]').val('');
    }).change();
});