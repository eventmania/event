$(document).ready(function() {
	$('#transfer-select').click(function() {
		var id = $('.Account:checked').val();
		var value = $('#Value').val();
		var max = parseFloat($('#Value').attr('maxValue'));
		if(id != undefined) {
			if(value != '' && value != '0' && value != '0,00') {
				if(parseFloat(value.replace(/\./g, '').replace(',','.')) <= max) {

					var eventId = $(this).attr('event-id');

					$('#modal-accounts').modal('hide');

					$.post(ROOT + 'transfer/set/' + eventId + '/', {Id: id, Value: value})
					.done(function(data){
						if(data.d.Account) {
							$('#transfer-container').html(data.d.HTML);
							$('.alert').hide();
						} else if(data.d.Error) {
							app.modal.alert(data.d.Error);
						} else {
							app.modal.alert('Ocorreu um erro e não foi possível salvar as informações.');
						}
					});
				} else {
					app.alert('#modal-accounts .modal-body','Saldo insuficiente para realizar este saque.');
				}
			} else {
				app.alert('#modal-accounts .modal-body','Por favor, informe um valor para saque.');
			}
		} else {
			app.alert('#modal-accounts .modal-body','Por favor, selecione uma conta.');
		}
	});
});