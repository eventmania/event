$(document).ready(function() {
	/*## Participant Checkin ##*/
    $('.checkin').click(function(e){
        var self, icon, id;

		self = $(this);
		icon = self.find('i');
		if(icon.hasClass('icon-white'))
			icon.attr('class','icon-refresh icon-white icon-animate');
		else
			icon.attr('class','icon-refresh icon-animate');

        id = self.attr('participant');
        $.post(ROOT + 'participant/check/' + self.attr('action') + '/' + id, function(data) {
			if(data.d) {
				if(data.d.CheckIn === 1) {
					self
						.attr('class', 'btn btn-mini btn-success')
						.attr({action: 'out', title: 'Desfazer'})
						.tooltip('fixTitle');

					icon.attr('class','icon-ok icon-white');
				} else {
					self
						.attr('class', 'btn btn-mini')
						.attr({action: 'in', title: 'Fazer Check-in'})
						.tooltip('fixTitle');

					icon.attr('class','icon-check');
				}
				$('#checkin-date-' + data.d.Id).html(data.d.CheckInDate);
			} else {

			}
        });
    });

	/*## Participant AutoCheckin ##*/
    $('#auto-checkin').click(function(e){
        var self, field, token, msg;

		self = $(this);
		field = $('#Number');
		token = $('#Token');
		msg = $('#msg');

		if(field.val() !== '') {
			msg.attr('class', '').html('');
			self.attr('disabled', 'disabled').find('i').attr('class','icon-refresh icon-white icon-animate');
console.log(field.length);
			$.post(ROOT + 'participant/auto-check/', {Number: field.val(), Token: token.val()}, function(data) {
				if(data.d) {
					if(data.d.Name) {
						msg.attr('class', 'alert alert-success').html('Check-in de <b>' + data.d.Name + '</b> às ' + data.d.Date + '.');
					} else {
						msg.attr('class', 'alert alert-error').html(data.d.Error);
					}
					self.removeAttr('disabled').find('i').attr('class','icon-white icon-map-marker');
					field.val('');
				} else {
					msg.attr('class', 'alert alert-error').html('Ocorreu algum erro ao tentar fazer o check-in.');
				}
			});
		} else {
			msg.attr('class', 'alert alert-error').html('Número inválido.');
		}
    });
});
