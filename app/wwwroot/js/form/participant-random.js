$(document).ready(function() {
	/*## Participant Random ##*/
    $('#raffle').click(function(){
        $('#winnerMessage').html('Aguarde...');
        
        var id = $(this).attr('event-id');
        $.post(ROOT + 'participant/random/' + id, function(data) {
            $('#winnerMessage').hide();
            $('#winnerDetails').show();
            $('#winnerName').html(data.d.Name);
            $('#winnerEmail').html(data.d.Email);
        });
    });
});