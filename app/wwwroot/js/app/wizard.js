$(document).ready(function() {
	/*## Wizard ##*/
    $('.btnNext').click(function proximoPasso() {
        if($('form').valid()) {
            var active = $('.wizard .active').removeClass('active');
            var step = parseInt(active.find('span').text());
            $('[data-step="'+ step +'"]').hide().next().show();
            active.next().addClass('active');
        }
    });
    
    $('.btnPrev').click(function passoAnterior() {
        var active = $('.wizard .active').removeClass('active');
        var step = parseInt(active.find('span').text());
        $('[data-step="'+ step +'"]').hide().prev().show();
        active.prev().addClass('active');
    });
});