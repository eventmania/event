Date.prototype.format = function () {
	return this.getDate().toString().pad(2) + '/' + this.getMonth().toString().pad(2) + '/' + this.getFullYear();
};

String.prototype.pad = function (length, pad, type) {
	pad = pad || '0';
	type = type || 'left';

	if(this.length >= length)
		return this;

	pad = new Array(length - this.length + 1).join(pad);
	if(type === 'left')
		return pad + this;
	return this + pad;
};

Array.prototype.contains = function(object) {
	var i = this.length;
	while (i--) {
		if (this[i] === object) {
			return true;
		}
	}
	return false;
};

Array.prototype.containsAll = function(objects) {
	var i = this.length, contains = false;
	while (i--) {
		for(var j in objects) {
			if (this[i] === objects[j]) {
				contains = true;
			}
		}
	}
	return contains;
};

$(document).ready(function() {
    $.fn.popout = function(selector, strategy){
        return $(this).on('click', function (e) {
            $(selector).each(function ( ) {
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                    if(strategy === 'hide')
                        $(this).parent().find('.popover').addClass('hide').hide();
                    else
                        $(this).popover('hide');
                }
            });
        });
    };
	
	
	window.Timer = (function(){
        var fday = function (d){
        if (d < 10)
                d = '0' + d;
            return d;
        };

        if (TIME !== 0) {
            var time = new Date(0, 0, 0, 0, TIME);
            var refresh = setInterval(function() {
                time.setSeconds(time.getSeconds() - 1);

                if(time.getHours() === 0) {
                    $("#session").html('(' + fday(time.getMinutes()) + ':' + fday(time.getSeconds()) + ')');
                } else {
                    $("#session").html('(expirou)');
                    clearInterval(refresh);
                }
            }, 1000);
        }

        return {
            fday: fday
        };
    })();
});