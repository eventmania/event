$(document).ready(function() {
    $('#match-reviewers .paper .add-reviewer').each(function(){
        var self = $(this);
        var html = self.parent().find('.hide').html();

        var sumReviews = function(selector, value) {
            $(selector).parent().find('strong').each(function() {
                var self = $(this);
                var number = parseInt(self.text().replace(/[^0-9]/, ''));
                self.text('(' + (number+value) + ')');
            });
        };

        self.popover({
            html: true,
            trigger: 'manual',
            placement: 'left',
            content: html
        }).parent().delegate('.add-reviewer-check', 'click', function(e) {
            e.preventDefault();
            var self = $(this);
            var managerId = self.attr('data-managment-id');
            var paperId = self.attr('data-paper-id');
            if(self.is(':checked')) {
                $.post(ROOT + 'reviewer/add/' + paperId + '/' + managerId, function(data){
                    if(!data.d){
                        app.modal.alert('Este avaliador já está definido para este trabalho.');
                    } else {
                        sumReviews('.add-reviewer-check[data-managment-id="'+ managerId +'"]', 1);
                    }
                    self.prop('checked', true);
                });
            } else {
                $.post(ROOT + 'reviewer/remove/' + paperId + '/' + managerId, function(data){
                    if(!data.d) {
                        app.modal.confirm('Esta avaliação já foi realizada, deseja mesmo excluí-la?', function(){
                            $.post(ROOT + 'reviewer/remove/' + paperId + '/' + managerId + '/1', function(){
                                self.removeAttr('checked').change();
                                sumReviews('.add-reviewer-check[data-managment-id="'+ managerId +'"]', -1);
                            });
                        });
                    } else {
                        self.removeAttr('checked').change();
                        sumReviews('.add-reviewer-check[data-managment-id="'+ managerId +'"]', -1);
                    }
                });
            }
        });

        self.click(function(){
            var popover = self.parent().find('.popover');

            if(popover.length) {
                if(popover.is('.hide')) {
                    popover.removeClass('hide').show();
                } else {
                    popover.addClass('hide').hide();
                }
            } else {
                self.popover('show');
            }
        }).click().click(); // criar todos os popover e esconde-los
    });

    $('body').popout('#match-reviewers .paper .add-reviewer', 'hide');
});
