$(document).ready(function() {
	/*## Chart ##*/
    Highcharts.setOptions({
        lang: {
            months: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            weekdays: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado']
        }
    });
    
    if($('#chart').length > 0) {
        $('#chart').highcharts({
            chart: {
                zoomType: 'x',
                spacingRight: 0,
                spacingLeft: 0,
                height: 180
            },
            credits: {
                enabled: false
            },
            colors: [
                '#4bb1cf'
            ],
            title: {
                text: ''
            },
            subtitle: {
                text: document.ontouchstart === undefined ? '' : ''
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 24 * 3600000, // fourteen days
                title: {
                    text: null
                },
                labels: {
                    formatter: function() {
                        return Timer.fday(new Date(this.value).getDate());
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function() {
                        return this.value;
                    }
                }
            },
            tooltip: {
                pointFormat: '<b>{point.y:,.0f}</b> inscrições'
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {x1: 0, y1: 0, x2: 0, y2: 1},
                        stops: [
                            [0, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.40).get('rgba')],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.05).get('rgba')]
                        ]
                    },
                    lineWidth: 2,
                    marker: {
                        enabled: true,
                        fillColor: '#ffffff',
                        lineColor: '#4bb1cf',
                        lineWidth: 2
                    },
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 2
                        }
                    },
                    threshold: null
                }
            },
            series: [{
                    type: 'area',
                    data: dataChart
                }]
        });
    }
});