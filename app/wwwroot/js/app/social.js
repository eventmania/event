$(document).ready(function() {
    $.fn.fbshare = (function(config){
        var getFbUrl = function(FBVars){
            var url = 'http://www.facebook.com/dialog/feed?app_id=' + FBVars.fbAppId +
            '&link=' + FBVars.fbShareUrl +
            '&picture=' + encodeURIComponent(FBVars.fbShareImg) +
            '&name=' + encodeURIComponent(FBVars.fbShareName) + 
            '&caption=' + encodeURIComponent(FBVars.fbShareCaption) + 
            '&description=' + encodeURIComponent(FBVars.fbShareDesc) + 
            '&redirect_uri=' + FBVars.baseURL + 
            '&display=popup';

            return url;
        };

        return $(this).each(function(){
            var self = $(this);

            var fbVars = {
                fbAppId: config.appId,
                fbShareUrl: self.attr('href') || config.url,
                fbShareImg: self.attr('fb-img') || config.img,
                fbShareName: self.attr('fb-name') || config.name,
                fbShareCaption: self.attr('fb-caption') || config.caption,
                fbShareDesc: self.attr('fb-description') || config.description,
                baseURL: self.attr('fb-redirect') || config.redirect
            };

            self.click(function(e){
                e.preventDefault();
                window.open(getFbUrl(fbVars), 
                    'feedDialog',
                    'toolbar=0,status=0,width=550,height=435'
                );
            });
        });
    });

    $.fn.twittershare = (function(config){
        var getUrl = function(Vars){
            var url = 'https://twitter.com/intent/tweet?related=' + Vars.related +
                        '&text=' + encodeURIComponent(Vars.text) +
                        '&url=' + encodeURIComponent(Vars.link) +
                        '&via=' + Vars.via +
                        '&original_referer=' + encodeURIComponent(Vars.redirectUrl);
            return url;
        }

        return $(this).each(function(){
            var self = $(this);

            var vars = {
                link: self.attr('href') || config.link,
                text: self.attr('twitter-text') || config.text,
                related: self.attr('twitter-related') || config.related,
                via: self.attr('twitter-via') || config.via,
                redirectUrl: self.attr('twitter-redirect') || config.redirectUrl,
            };

            self.click(function(e){
                e.preventDefault();
                window.open(getUrl(vars), 
                    'feedDialog',
                    'toolbar=0,status=0,width=500,height=260'
                );
            });
        });
    });

    $.fn.gplusshare = (function(config){
        var getUrl = function(vars){
            var url = 'https://plus.google.com/share?url=' + vars.link;
            return url;
        };

        return $(this).each(function(){
            var self = $(this);

            var vars = {
                link: self.attr('href') || config.link,
            };

            self.click(function(e){
                e.preventDefault();
                window.open(getUrl(vars), 
                    'feedDialog',
                    'toolbar=0,status=0,width=500,height=436'
                );
            });
        });
    });

    $('a[data-toggle="fb-share"]').fbshare({
        appId: FB_APPID,
        caption: 'Compre seu ingresso pelo EventMania, é fácil e rápido.',
        description: '',
    });

    $('a[data-toggle="twitter-share"]').twittershare({
        text: 'Compre seu ingresso pelo EventMania, é fácil e rápido.',
        via: 'eventmanya',
        related: 'eventmanya'
    });

    $('a[data-toggle="gplus-share"]').gplusshare({
        link: ''
    });
});