$(document).ready(function() {
    $('.cnpj').mask('99.999.999/9999-99');
	$('.cpf').mask('999.999.999-99');
    $('.cep').mask('99.999-999');
    
    $('.date').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        autoclose: true
    }).on('changeDate', function(ev){
		var self = this;
		setTimeout(function() {
			if($(self).valid()) {
			}
		}, 50);
	}).attr('autocomplete', 'off');

    
    $('.number').focusout(function() {
        var element = $(this);
        element.val(element.val().replace(/\D/g, ''));
    });
    
    $('.money').maskMoney({symbol: '', thousands: '.', decimal: ',', allowZero: true});
    
    $('.phone').focusout(function() {
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');
    
    $('.cpfcnpj').focus(function(){
        var self = $(this);
        var cpf = self.val().replace(/\D/g, '');
        self.unmask();
        self.val(cpf);
    }).focusout(function() {
        var cpf, element;
        element = $(this);
        element.unmask();
        cpf = element.val().replace(/\D/g, '');
        if (cpf.length > 11) {
            element.mask('99.999.999/9999-99');
        } else {
            element.mask('999.999.999-99');
        }

        if(element.val() == '')
            element.unmask();
    });
});