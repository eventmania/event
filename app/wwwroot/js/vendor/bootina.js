(function(global) {
	"use strict";
	var app = {};
	app.modal = {};
	app.modal.alert = function(message) {
		var key = 'appModalAlert';
		var box = $('#' + key);
		if(box.length === 0) {
			box = $('<div id="appModalAlert" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="appModalAlertLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="appModalAlertLabel">Atenção</h3></div><div class="modal-body"><p id="appModalAlertMessage"></p></div><div class="modal-footer"><button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button></div></div>');
		}
		$('#' + key + 'Message', box).html(message);
		box.modal('show');
	};
	app.modal.confirm = function(message, yes) {
		var key = 'appModalConfirm';
		var box = $('#' + key);
		if(box.length === 0) {
			box = $('<div id="appModalConfirm" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="appModalConfirmLabel" aria-hidden="true"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h3 id="appModalConfirmLabel">Atenção</h3></div><div class="modal-body"><p id="appModalConfirmMessage"></p></div><div class="modal-footer"><button class="btn btn-primary" id="appModalConfirmYes" data-dismiss="modal" aria-hidden="true">Sim</button><button class="btn" data-dismiss="modal" aria-hidden="true">Não</button></div></div>');
		}
		$('#' + key + 'Message', box).html(message);
		$('#' + key + 'Yes', box).unbind('click').bind('click', yes);
		box.modal('show');
	};
	app.request = {};
	app.request.create = function(type, url, params, success, error) {
		//verifica se o argumento 'params' foi passado
		if(typeof params === 'function') {
			if(typeof success === 'function') {
				error = success;
			}
			success = params;
		}
		var request = $.ajax({
			url: url,
			type: type,
			data: params,
			dataType: 'json'
		});
		request.done(function(response) {
			success(response);
		});
		request.fail(function(jqXHR, textStatus) {
			if(typeof error === 'function') {
				error(jqXHR, textStatus);
			}
		});
	};
	app.request.post = function(url, params, success, error) {
		this.create('POST', url, params, success, error);
	};
	app.request.get = function(url, params, success, error) {
		this.create('GET', url, params, success, error);
	};
	app.form = {};
	app.form.get = function(selector) {
		var form = $(selector);
		var data = {};
		$('input, textarea, select', form).each(function(i, o) {
			var property = $(o).attr('name');
			var value = $(o).val();
			data[property] = value;
		});
		return data;
	};
	app.form.set = function(selector, data) {
		var form = $(selector);
		for(var prop in data) {
			$('[name="' + prop + '"]', form).val(data[prop]);
		}
	};
	app.form.submit = function(selector, success, error) {
		var data = this.get(selector);
		var type = $(selector).attr('method').toUpperCase();
		var url = $(selector).attr('action');
		app.request.create(type, url, data, success, error);
	};
	app.form.onSubmit = function(selector, success, error) {
		$(selector).submit(function(e) {
			e.preventDefault();
			app.form.submit(selector, success, error);
		});
	};
	app.form.checkToggle = function(selector, target) {
		$(selector).change(function() {
			if ($(this).is(':checked')) {
				$(target).hide();
			} else {
				$(target).show();
			}
		});
	};
	app.alert = function(target, type, message) {
		if(typeof message === 'undefined') {
			message = type;
			type = 'alert';
		} else {
			type = 'alert alert-' + type;
		}
		var key = 'appAlert';
		var box = $('#' + key);
		if(box.length === 0) {
			box = $('<div class="' + type + '" id="' + key + '"></div>').prependTo(target);
		}
		box.html('').append('<button type="button" class="close" data-dismiss="alert">&times;</button>').append(message);
		box.alert();
	};
	app.error = function(target, message) {
		this.alert(target, 'error', message);
	};

	global.Bootina = {
		create: function() {
			global.app = app;
			return app;
		},
		extend: function(prop) {
			for (var name in prop) {
				app[name] = prop[name];
			}
		}
	};

	return global.Bootina;
}(window));