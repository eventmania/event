$(document).ready(function() {
	/*## Initialize ##*/
    Bootina.create(window);

    $.fn.attachEvents = function(){
        var self = $(this);
        self.find('.btn-tooltip').tooltip();
        self.find('[data-toggle="tooltip"]').tooltip();
        self.find('.btn-group[data-toggle="buttons-checkbox"] button').click(function(e){
            e.preventDefault();
            var self = $(this);
            var name = self.attr('name');
            var value = self.attr('value');
            if(!self.hasClass('active')){
                self.closest('.btn-group').parent()
                .append(
                    $('<input type="hidden">').attr('name', name).val(value)
                );
            }else{
                self.closest('.btn-group').parent()
                .find('input[type="hidden"][name="'+name+'"][value="'+value+'"]').remove();
            }
        });
        self.find('.btn-group[data-toggle="buttons-radio"] button').click(function(e){
            e.preventDefault();
            var self = $(this);
            var control = self.closest('.btn-group').parent();
            var hidden = control.find('input[type="hidden"]');
            if(!hidden.length){
                hidden = $('<input type="hidden">').attr('name', self.attr('name'));
                hidden.appendTo(control);
            }
            hidden.val(self.attr('value'));
        });

        self.find('button[data-toggle="remove"]').click(function(){
            var self = $(this);
            var target = self.attr('data-target');
            self.closest(target).remove();
        });

        self.find('a.btn-details').click(function(){
            var self = $(this);
            var content = self.parent().find('> .hide');
            var modal = $(self.attr('href'));
            modal.find('.modal-body').empty().append(content.html());
        });

        self.find('a.btn-file-trigger').click(function(e){
            e.preventDefault();
            $(this).parent().find('input[type="file"]').click();
        });

        self.find('.chosen').chosen({no_results_text: 'Nenhum resultado para'});

        self.find('.help-icon').each(function(){
            var self = $(this);
            var html = self.find('> .hide').html();
            self.popover({
                html: true,
                trigger: self.attr('data-trigger') || 'hover',
                placement: self.attr('data-placement') || 'right',
                content: html
            });
        });

        self.find('a[data-toggle="confirm"]').click(function(e) {
            e.preventDefault();
            var self = $(this);
            app.modal.confirm(self.attr('data-message') || 'Tem certeza que deseja continuar com esta ação?', function() {
                window.location = self.attr('href');
            });
        });

        return self;
    };

    $('body').popout('.help-icon').attachEvents();

	$('.use-popover').popover();

    /*## Google Maps ##*/
    if($('#maps').length > 0) {
        initialize();

        function loadMap(address) {
            geocoder.geocode({'address': address + ', Brasil', 'region': 'BR'}, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();

                        //$('#Address').val(results[0].formatted_address);
                        $('#Latitude').val(latitude);
                        $('#Longitude').val(longitude);

                        var location = new google.maps.LatLng(latitude, longitude);
                        marker.setPosition(location);
                        map.setCenter(location);
                        map.setZoom(zoom);
                    }
                }
            });
        }

        $('#Address').blur(function() {
            if($(this).val() !== '')
                loadMap($(this).val());
        });

        google.maps.event.addListener(marker, 'drag', function () {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        //$('#Address').val(results[0].formatted_address);
                        $('#Latitude').val(marker.getPosition().lat());
                        $('#Longitude').val(marker.getPosition().lng());
                    }
                }
            });
        });
		
		var findResult = function(results, name){
            var i, j, component, type;
			for(i in results.address_components) {
				component = results.address_components[i];
				for(j in component.types) {
					type = component.types[j];
					if(type === name)
						return component['short_name'];
				}
			}
        };
		
		var findResultFull = function(results){
            var i, component, address = [], invalids = ['street_number', 'country', 'postal_code'];
			for(i in results.address_components) {
				component = results.address_components[i];
				if(component.types !== undefined && !component.types.containsAll(invalids)) {
					if(!address.contains(component['short_name']))
						address.push(component['short_name']);
				}
			}
			return address.join(', ');
        };

        $("#Address").autocomplete({
            source: function (request, response) {
                geocoder.geocode({ 'address': request.term + ', Brasil', 'region': 'BR' }, function (results, status) {
					response($.map(results, function (item) {
						
						var city = findResult(item, 'locality');
						var state = findResult(item, 'administrative_area_level_1');
						
                        return {
                            label: findResultFull(item),
                            value: findResultFull(item), //item.formatted_address,
                            latitude: item.geometry.location.lat(),
                            longitude: item.geometry.location.lng()
                        };
                    }));
                });
            },
            select: function (event, ui) {
                $("#Latitude").val(ui.item.latitude);
                $("#Longitude").val(ui.item.longitude);
                var location = new google.maps.LatLng(ui.item.latitude, ui.item.longitude);
                marker.setPosition(location);
                map.setCenter(location);
                map.setZoom(zoom);
            }
        });
    }
});

/*## Google Maps ##*/
var geocoder;
var map;
var marker;
var zoom = 15;

function initialize() {
	var latitude	= window.latitude || -10.184649;
	var longitude	= window.longitude || -48.333675;
    var latlng = new google.maps.LatLng(latitude, longitude);
    var options = {
        zoom: zoom,
        center: latlng,
        scrollwheel: false,
        navigationControl: false,
        scaleControl: false,
        draggable: true,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false
    };

    map = new google.maps.Map(document.getElementById('maps'), options);

    geocoder = new google.maps.Geocoder();

    marker = new google.maps.Marker({
        map: map,
        draggable: true
    });

    marker.setPosition(latlng);
}