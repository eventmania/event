var isOpenLightbox = PagSeguroLightbox({
    code: checkoutCode
}, {
    success: function(transactionCode) {
	$('#lightbox-success, #lightbox-finish').removeClass('hide');
	$('#lightbox-loading').addClass('hide');
    },
    abort: function() {
	$('#lightbox-abort, #lightbox-finish').removeClass('hide');
	$('#lightbox-loading').addClass('hide');
    }
});
if (!isOpenLightbox) {
    location.href = "https://pagseguro.uol.com.br/v2/checkout/payment.html?code=" + checkoutCode;
}