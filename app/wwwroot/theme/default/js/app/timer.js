window.Timer = (function(){
	var fday = function (d){
	if (d < 10)
			d = '0' + d;
		return d;
	};

	if (typeof SECONDS !== 'undefined') {
		var time = new Date(0, 0, 0, 0, 0, SECONDS);
		var refresh = setInterval(function() {
			time.setSeconds(time.getSeconds() - 1);

			if(time.getHours() == 0) {
				document.getElementById('timer').innerHTML = fday(time.getMinutes()) + ':' + fday(time.getSeconds());
			} else {
				clearInterval(refresh);
				var app = Bootina.create();
				app.modal.alert('O tempo de reserva da sua inscrição expirou. Por favor, realize um novo pedido.', function() {
					window.location = SITE;
				});
			}
		}, 1000);
	}

	return {
		fday: fday
	};
})();