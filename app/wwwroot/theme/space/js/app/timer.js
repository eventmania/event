window.Timer = (function(){
	var fday = function (d){
	if (d < 10)
			d = '0' + d;
		return d;
	};

	if (typeof SECONDS !== 'undefined') {
		var time = new Date(0, 0, 0, 0, 0, SECONDS);
		var refresh = setInterval(function() {
			time.setSeconds(time.getSeconds() - 1);

			if(time.getHours() == 0) {
				document.getElementById('timer').innerHTML = fday(time.getMinutes()) + ':' + fday(time.getSeconds());
			} else {
				clearInterval(refresh);
				alert('Expirou!');
			}
		}, 1000);
	}

	return {
		fday: fday
	};
})();