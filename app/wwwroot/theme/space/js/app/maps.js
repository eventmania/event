$(document).ready(function(){
    /*## Google Maps ##*/
    if($('#maps').length > 0) {
        initialize();

        function loadMap(address) {
            geocoder.geocode({'address': address + ', Brasil', 'region': 'BR'}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var latitude = results[0].geometry.location.lat();
                        var longitude = results[0].geometry.location.lng();

                        $('#Address').val(results[0].formatted_address);
                        $('#Latitude').val(latitude);
                        $('#Longitude').val(longitude);

                        var location = new google.maps.LatLng(latitude, longitude);
                        marker.setPosition(location);
                        map.setCenter(location);
                        map.setZoom(zoom);
                    }
                }
            });
        }

        $('#Address').blur(function() {
            if($(this).val() != "")
                loadMap($(this).val());
        });

        google.maps.event.addListener(marker, 'drag', function () {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        //$('#Address').val(results[0].formatted_address);
                        $('#Latitude').val(marker.getPosition().lat());
                        $('#Longitude').val(marker.getPosition().lng());
                    }
                }
            });
        });
    }
});

/*## Google Maps ##*/
var geocoder;
var map;
var marker;
var zoom = 15;

function initialize() {
    var latitude    = window.latitude || -10.184649;
    var longitude   = window.longitude || -48.333675;
    var latlng = new google.maps.LatLng(latitude, longitude);
    var options = {
        zoom: zoom,
        center: latlng,
        scrollwheel: false,
        navigationControl: false,
        scaleControl: false,
        draggable: true,
        mapTypeControl: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: false
    };

    map = new google.maps.Map(document.getElementById('maps'), options);

    geocoder = new google.maps.Geocoder();

    marker = new google.maps.Marker({
        map: map,
        draggable: true
    });

    marker.setPosition(latlng);
}
