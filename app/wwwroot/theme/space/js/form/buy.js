$(document).ready(function() {

	var form = $('#buy');

	var config = {
		sandbox: true,
		publicToken: AKATUS_PUBLIC_TOKEN
	};

    Akatus.init(form, config);

	var cvv = $('#cvv-help-content');
	$('#cvv-help').popover({
		title: 'Código de Segurança',
		placement: 'top',
		html: true,
		content: function() {
			return cvv.show();
		}
	});

	$('#payment-tab a').click(function(event) {
		event.preventDefault();
		$(this).tab('show');
		$($(this).attr('href')).find('input:eq(0)').attr('checked', true).change();
	});

	$('.payment-method input').change(function() {
		var self = $(this),
			card = $('#Payment_Card');
		if(self.val() === 'boleto') {
			card.addClass('hide');
		} else if(/^tef_/.test(self.val())) {
			card.addClass('hide');
		} else {
			card.removeClass('hide');

			var ticket = $('#TicketPrice');
			var installment = $('#Card_Installment');
			installment.html('<option>Calculando...</option>');
			$.get(ROOT + 'site/installment/' + self.val() + '/' + ticket.val(), function(data) {
				var i,
					options = '<option>Selecione</option>',
					parcelas = data.resposta.parcelas;
				for(i in parcelas) {
					options += '<option value="' + parcelas[i].quantidade + '">' + parcelas[i].quantidade + 'x R$ ' + parseFloat(parcelas[i].valor).toFixed(2).toString().replace('.', ',') + '</option>';
				}
				installment.html(options);
			});
		}
		$('.payment-method').removeClass('active').find('.check').remove();
		self.parent().addClass('active').append('<span class="check badge badge-success"><i class="icon-ok icon-white"></i></span>');
	});
});
