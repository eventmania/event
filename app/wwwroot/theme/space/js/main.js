$(document).ready(function() {
	$('[data-toggle="tooltip"]').tooltip();

    $('.add-to-cart').click(function(){
        var self = $(this);
        var formName = self.attr('data-form-name');
        var id = self.attr('data-id');
        var name = self.attr('data-name');

        $('#cart-form input[name="'+ formName +'"]').closest('li').remove();

        $('#cart-form .items').append(
            $('<li>').append(
                $('<input type="hidden">').attr('name', formName).val(id),
                $('<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>')
                .click(function(){
                    $(this).closest('li').remove();
                }),
                $('<span>').text(name)
            )
        );
    });

	$.validator.setDefaults({
		errorPlacement: function(error, element) {
			// if the input has a prepend or append element, put the validation msg after the parent div
			if (element.parent().hasClass('input-prepend') || element.parent().hasClass('input-append')) {
				error.insertAfter(element.parent());
			// else just place the validation message immediatly after the input
			} else {
				error.insertAfter(element);
			}
		},
		errorElement: "small", // contain the error msg in a small tag
		wrapper: "div", // wrap the error message and small tag in a div
		highlight: function(element) {
			$(element).closest('.control-group').addClass('error'); // add the Bootstrap error class to the control group
		},
		success: function(element) {
			$(element).closest('.control-group').removeClass('error'); // remove the Boostrap error class from the control group
		}
	});

	/*## Validation ##*/
    $.validator.addClassRules({
        required: {
            required: true,
            minlength: 1
        },
        date: {
            required: true,
            date: true
        },
        email: {
            required: true,
            email: true
        },
		name: {
            required: true,
            fullname: true
        },
		cartao_visa: {
			required: true,
			visa: true
        },
		cartao_visa: {
			required: true,
			visa: true
        },
		cartao_master: {
			required: true,
			master: true
        },
		cartao_amex: {
			required: true,
			amex: true
        },
		cartao_diners: {
			required: true,
			diners: true
        },
		cartao_elo: {
			required: true,
			elo: true
		}
    });

    var validate = $('form').validate();


	/*## Mask ##*/
	$('.cpf').mask('999.999.999-99');

	$('.phone').focusout(function() {
        var phone, element;
        element = $(this);
        element.unmask();
        phone = element.val().replace(/\D/g, '');
        if (phone.length > 10) {
            element.mask("(99) 99999-999?9");
        } else {
            element.mask("(99) 9999-9999?9");
        }
    }).trigger('focusout');

	$('.number').keydown(function(e) {
		// Allow: backspace, delete, tab, escape, enter and .
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
				// Allow: Ctrl+A
						(e.keyCode == 65 && e.ctrlKey === true) ||
						// Allow: home, end, left, right
								(e.keyCode >= 35 && e.keyCode <= 39)) {
			// let it happen, don't do anything
			return;
		}
		// Ensure that it is a number and stop the keypress
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});

    if($('#maps').length > 0) {
        initialize();
    }
});

/*## Google Maps ##*/
var geocoder;
var map;
var marker;
var zoom = 15;

function initialize() {
    var latitude    = window.latitude || -10.184649;
    var longitude   = window.longitude || -48.333675;
    var latlng = new google.maps.LatLng(latitude, longitude);
    var options = {
        zoom: zoom,
        center: latlng,
        scrollwheel: true,
        navigationControl: true,
        scaleControl: false,
        draggable: false,
        mapTypeControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true
    };

    map = new google.maps.Map(document.getElementById('maps'), options);

    geocoder = new google.maps.Geocoder();

    marker = new google.maps.Marker({
        map: map,
        draggable: false
    });

    marker.setPosition(latlng);
}
