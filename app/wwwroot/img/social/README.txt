Source: http://veodesign.com/2011/en/09/07/somicro-27-free-simple-social-media-icons/

Somicro is licensed under Creative Commons Attribution Share-Alike 3.0 Unported. You are free to share, use and modify this work for personal or commercial purposes. You do not have to credit us in your work or on your site but a linkback to Veodesign.com is always appreciated. This icon pack may not be redistributed or sold for profit.

License: http://creativecommons.org/licenses/by-sa/3.0/legalcode
