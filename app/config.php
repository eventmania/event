<?php
/*
 * Copyright (c) Trilado Team (triladophp.org)
 * All rights reserved.
 */


/**
 * Arquivo de configuração
 *
 */

/**
 * Define o tipo do debug
 */
Config::set('debug', array(
	'type'	=> 'off', //pode assumir os seguintes valores: off, local, network e all
	'query'	=> false, //pode assumir false, para desativar, ou um valor para a query ?debug=seu-valor-seguro
	'sql'	=> false,
));

include 'config.private.php';

/**
 * Master Page padrão
 */
Config::set('default_master', 'template');

/**
 * Controller padrão
 */
Config::set('default_controller', 'Home');

/**
 * Controller padrão para páginas de erro. Defina como NULL para não utilizar controler de erro
 */
Config::set('error_controller', 'Error');

/**
 * Action padrão
 */
Config::set('default_action', 'index');

/**
 * Página de login
 */
Config::set('default_login', '~/user/login');

/**
 * Charset padrão
 */
Config::set('charset', 'UTF-8');

/**
 * Linguagem padrão
 */
Config::set('default_lang', 'pt-br');

/**
 * Formato padrão da data.
 */
Config::set('date_format', 'd/m/Y');

/**
 * Chave de segurança (deve ser alterada)
 */
Config::set('salt', 'dfksaDAadcA3%2wd{aA]/mdsd\adsdsSS124tds');

/**
 * Define se as requisições via dispositivo móvel irão carregar os templates específicos, se existirem, para versão móvel
 */
Config::set('auto_mobile', true);

/**
 * Define se as requisições via tablet irão carregar os templates  específicos, se existirem, para versão tablet
 */
Config::set('auto_tablet', false);

/**
 * Define se as requisições AJAX devem retornar automaticamente conteúdo em JSON
 */
Config::set('auto_ajax', false);

/**
 * Define se actions acessadas com .xml devem retorna automaticamente conteúdo em XML
 */
Config::set('auto_dotxml', true);

/**
 * Define se actions acessadas com .json devem retorna automaticamente conteúdo em JSON
 */
Config::set('auto_dotjson', true);

/**
 * Define as configurações de cache
 */
Config::set('cache', array(
	'enabled'	=> false,
	'type'		=> 'file',
	'host'		=> 'localhost',
	'port'		=> '',
	'page'		=> false,
	'time'		=> 10
));

/**
 * Registrar diretórios de arquivos de código fonte, para autoload
 */
Config::set('directories', array(
	'controller' 				=> App::$root . 'app/controllers',
	'model'				 		=> App::$root . 'app/models',
	'helper' 					=> App::$root . 'app/helpers',
	'helper/custom-field' 		=> App::$root . 'app/helpers/custom-field',
	'vendor' 					=> App::$root . 'app/vendors',
	'akatus' 					=> App::$root . 'app/vendors/Akatus',
));

/**
 * Registrar diretórios de arquivos de código fonte, para autoload
 */
Config::set('modules', array(
	'example' 		=> App::$root . 'app/modules/example/',
));

Config::set('session_time', 30);

Config::set('timezone', 'America/Araguaina');

Config::set('locale', 'pt_BR');

Config::set('currency', array(
	'currency' => 'R$',
	'dec_separator' => ',',
	'thousand_separator' => '.',
	'decimals' => 2
));

Config::set('use_coockies', true);

Config::set('mail_atendimento', 'atendimento@eventmania.com.br');

date_default_timezone_set(Config::get('timezone'));

setlocale (LC_ALL, Config::get('locale'));
