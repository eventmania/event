<?php
class TransferController extends AppController
{
	public function index($p = 1)
	{
		$m = 20;
		$transfers = ViewTransfer::search($p, $m, 'Id', 'DESC', array(
			'UserId' => $this->User->Id
		));
		$this->_set('m', $m);
		return $this->_view($transfers);
	}

    public function view($id)
    {
		$transfer = Transfer::get($id);
		if(!$transfer)
			throw new PageNotFoundException('Repasse não encontrado');

		$event = $transfer->getEvent();

		$this->checkAuthorization($event, Manager::ROLE_OWNER, 'Você não tem permissão para visualizar informações de repasse deste evento.');

		$this->_set('event', $event);
		$this->_set('model', $transfer);

		return $this->_view();
    }

	public function set($id)
	{
		$data = new stdClass;
		try
		{
			if(!Request::isPost() || !Request::isAjax())
				throw new BadRequestException('Requisição mal feita.');

			$event = Event::get($id);
        	if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

			$this->checkAuthorization($event, Manager::ROLE_OWNER, 'Você não tem permissão para visualizar informações de repasse deste evento.');

			if(count($event->getTransfers()))
				throw new UnauthorizedException('Informações já cadastradas para este evento.', 401);

			$account = Account::get(Request::post('Id'));
			if(!$account || $account->UserId != $this->User->Id)
				throw new PageNotFoundException('Conta bancária não encontrada.');

			$value = Format::moneyToDouble(Request::post('Value'));
			if($value <= 0)
				throw new PageNotFoundException('Valor inválido para saque.');

			if($value > $event->getSiteCashier()->getCashOnHand(true))
				throw new PageNotFoundException('Saldo insuficiente para realizar este saque.');

			$transfer = new Transfer();
			$transfer->setAccount($account);
			$transfer->generateCode();
			$transfer->UserId = (int)$this->User->Id;
			$transfer->Date = time();
			$transfer->Value = $value;
			$transfer->Status = 0;
			$transfer->EventId = (int)$event->Id;
			$transfer->save();

			$history = new TransferHistory($transfer);
			$history->save();

			$data->Account = $account;
			$data->HTML = Import::view(array('account' => $transfer), 'sale', 'account');
		}
		catch(HTTPException $e)
		{
			$data->Error = $e->getMessage();
		}
		catch(Exception $e)
		{
			$data->Error = 'Ocorreu um erro e não foi possível salvar as informações.';
		}
		return $this->_json($data);
	}

	/** @Auth("admin") */
	public function admin_index($p = 1)
	{
		$m = 20;
		$q = Format::get('q');

		$transfers = ViewTransfer::search($p, $m, 'Id', 'DESC', array(
			'Name' => "%$q%",
			'UserName' => "%$q%",
			'UserEmail' => "%$q%",
			'EventName' => "%$q%",
		));

		$this->_set('m', $m);
		$this->_set('q', $q);

		return $this->_view($transfers);
	}

	/** @Auth("admin") */
	public function admin_view($id)
	{
		$transfer = Transfer::get($id);

		if(!$transfer)
			throw new PageNotFoundException('Repasse não encontrado.');

		$event = $transfer->getEvent();

		if(Request::isPost())
		{
			$data = $this->_data();

			$history = new TransferHistory();
			$history->TransferId = (int)$transfer->Id;
			$history->UserId = (int)$this->User->Id;
			$history->Date = time();
			$history->Status = (int)$data->Status;
			$history->Description = $data->Description;
			$history->save();

			$this->_flash('alert alert-success', 'Status alterado com sucesso.');
		}

		$this->_set('event', $event);
		$this->_set('model', $transfer);

		return $this->_view();
	}
}
