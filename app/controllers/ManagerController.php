<?php
class ManagerController extends AppController
{
    public $Event;

    public $Manager;

    public function beforeRender()
    {
        parent::beforeRender();

        $id = end(App::$args['params']);

        if(App::$args['action'] === 'index' || App::$args['action'] === 'add')
        {
            $this->Event = Event::get($id);
            if(!$this->Event) throw new PageNotFoundException('O evento não foi encontrado.');
            $this->Manager = new ViewManager($this->Event->Id);
        }
        else
        {
            $this->Manager = ViewManager::get($id);
            $this->Event = $this->Manager->getEvent();
            if(!$this->Event) throw new PageNotFoundException('O evento não foi encontrado.');
        }

        $this->checkAuthorization($this->Event, Manager::ROLE_ADMIN, 'Você não tem permissão para visualizar esta área.');
    }
    public function index($eventId, $p = 1)
    {
        $m = 20;
        $this->_set('m', $m);
        $this->_set('event', $this->Event);
        return $this->_view($this->Event->getManagers($p, $m));
    }

    public function add($eventId)
    {
        if(Request::isPost())
        {
            $email = Format::post('Email');
            $role = (int)Request::post('Role');

            try
            {
                $user = User::getByEmail($email);
				
				if(Manager::exists($this->Event->Id, $user->Id))
					throw new ValidationException('Moderador já cadastrado.');
				
				if($role == Manager::ROLE_OWNER)
					throw new ValidationException('Você não pode cadastrar um "Criador".');
				
                $this->Event->addManager($user, $role);

                $this->_flash('alert alert-success', 'Moderador adicionado com sucesso.');
                return $this->_redirect('~/manager/' . $this->Event->Id);
            }
			catch(ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
            catch (Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao tentar adicionar o moderador.');
            }
        }

        $this->_set('event', $this->Event);
        $this->_set('blockEmailEdit', false);
        $this->_set('submitLabel', 'Adicionar');
        $this->_set('roles', Manager::getRoles());
        return $this->_view($this->Manager);
    }

    public function edit($id)
    {
		$manager = $this->Manager->getManagment();
		if($manager->isOwner())
			throw new ForbiddenException('O Criador do evento não pode ser editado.');
				
        if(Request::isPost())
        {
            try
            {
				if((int)Request::post('Role') == Manager::ROLE_OWNER)
					throw new ValidationException('Você não pode salvar como Criador do evento.');
                
                $manager->setRole(Request::post('Role'));
                $manager->save();

                $this->_flash('alert alert-success', 'Moderador editado com sucesso.');
                return $this->_redirect('~/manager/' . $manager->EventId);
            }
			catch(ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
            catch (Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao tentar editar o moderador.');
            }
        }

        $this->_set('event', $this->Event);
        $this->_set('blockEmailEdit', true);
        $this->_set('submitLabel', 'Salvar');
        $this->_set('roles', Manager::getRoles());
        return $this->_view('add', $this->Manager);
    }

    public function block_switch($id)
    {
        try
        {
            $manager = $this->Manager->getManagment();
			if($manager->isOwner())
				throw new ValidationException('O Criador do evento não pode ser bloqueado.');
			
			$manager->blockSwitch();
            $this->_flash('alert alert-success', 'Status alterado com sucesso.');
        }
		catch(ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch(Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao alterar o status do moderador.');
        }

        return $this->_redirect('~/manager/' . $this->Manager->EventId);
    }
}
