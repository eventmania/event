<?php

class ApiController extends Controller
{
	private $user;

	private function auth()
	{
		$allowed = false;
		$message = 'Não autorizado';
		if(isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
		{
			$message = 'Dados incorretos';

			$user = User::getByEmail($_SERVER['PHP_AUTH_USER']);
			if($user && $user->checkPassword($_SERVER['PHP_AUTH_PW']))
			{
				$this->user = $user;
				$allowed = true;
			}
		}

		if(!$allowed)
		{
			header("WWW-Authenticate: Basic realm=\"API Interna\"");
			header("HTTP\ 1.0 401 Unauthorized");
			header('Content-Type: application/json');

			$data = new stdClass;
			$data->Error = $message;
			echo json_encode($data);
			exit;
		}
	}

	private function json($data)
	{
		header('Content-Type: application/json');
		echo json_encode($data);
		exit;
	}

	private function error($number = 500, $message = 'Ocorreu um erro')
	{
		$errors = array();
		$errors[400] = 'Bad Request';
		$errors[401] = 'Unauthorized';
		$errors[402] = 'Payment Required';
		$errors[403] = 'Forbidden';
		$errors[404] = 'Not Found';
		$errors[405] = 'Method Not Allowed';
		$errors[406] = 'Not Acceptable';
		$errors[407] = 'Proxy';
		$errors[408] = 'Request Timeout';
		$errors[500] = 'Internal Server Error';
		$errors[501] = 'Not Implemented';
		$errors[502] = 'Bad Gateway';
		$errors[503] = 'Service Unavailable';
		$errors[504] = 'Gateway Timeout';

		header('HTTP/1.1 ' . $number . ' ' . $errors[$number]);
			
		$data = new stdClass;
		$data->Error = $message;
		return $this->json($data);
	}

	public function events($id = null)
	{
		if($id)
		{
			$event = Event::get($id);
			if(!$event || !$event->Visibility)
				throw new PageNotFoundException('Evento não encontrado');
			$events = array($event);
		}
		else
		{
			$events = Event::getPublicComing();
		}
		
		array_map(function($event) {
			unset($event->Visibility);
			unset($event->Status);
			unset($event->Hits);
			unset($event->UserId);
			unset($event->PaymentWay);
			unset($event->Absorb);
			unset($event->LayoutConfig);
			unset($event->MapType);
			unset($event->CreateDate);
			unset($event->Layout);
			unset($event->Capacity);
			unset($event->TransferId);
			unset($event->MinimumFee);
			unset($event->DeadLine);
			unset($event->FakeDeadLine);
			unset($event->IsSinglePurchase);
			unset($event->SpotlightIndex);
			unset($event->OrganizerEmail);
		}, $events);
		
		if($id)
			$events = array_shift($events);
		
		return $this->json($events);
	}
	
	public function tickets($id)
	{
		$event = Event::get($id);
		if(!$event || !$event->Visibility)
			throw new PageNotFoundException('Evento não encontrado');
		
		$tickets = Ticket::search(1, 20, null, null, array(
			'EventId' => $event->Id,
			'IsPrivate' => 0
		), 'AND')->Data;
		
		array_map(function($ticket) {
			unset($ticket->EventId);
			unset($ticket->Amount);
			unset($ticket->ReservedAmount);
			unset($ticket->IsPrivate);
			unset($ticket->HasPapers);
		}, $tickets);
		
		return $this->json($tickets);
	}
	
	public function workshops($id)
	{
		$event = Event::get($id);
		if(!$event || !$event->Visibility)
			throw new PageNotFoundException('Evento não encontrado');
		
		$workshops = $event->getWorkshops(1, 100)->Data;
		
		array_map(function($workshop) {
			unset($workshop->ParticipantsAmount);
			unset($workshop->EventId);
			unset($workshop->Amount);
		}, $workshops);
		
		return $this->json($workshops);
	}
	
	public function manager_auth()
	{
		$this->auth();
		unset($this->user->Password);
		unset($this->user->Role);
		unset($this->user->Status);
		unset($this->user->CreateDate);
		unset($this->user->LastLoginDate);
		unset($this->user->Newsletter);
		unset($this->user->CodeConfirmation);
		unset($this->user->Roles);

		$this->json($this->user);
	}
	
	public function manager_events($id = null)
	{
		try
		{
			$this->auth();

			$events = ViewEventManager::search(1, 100, 'Id', 'DESC', array(
				'ManagerId' => $this->user->Id,
			));

			array_map(function($event) {
				unset($event->Visibility);
				unset($event->Status);
				unset($event->Hits);
				unset($event->UserId);
				unset($event->PaymentWay);
				unset($event->Absorb);
				unset($event->LayoutConfig);
				unset($event->MapType);
				unset($event->CreateDate);
				unset($event->Layout);
				unset($event->Capacity);
				unset($event->TransferId);
				unset($event->MinimumFee);
				unset($event->DeadLine);
				unset($event->FakeDeadLine);
				unset($event->IsSinglePurchase);
				unset($event->SpotlightIndex);
				unset($event->OrganizerEmail);
			}, $events->Data);
			
			return $this->json($events->Data);
		}
		catch(HTTPException $e)
		{
			return $this->error($e->getCode(), $e->getMessage());
		}
		catch(Exception $e)
		{
			return $this->error();
		}
	}
	
	public function manager_participants($id, $p = 1)
	{
		try
		{
			$this->auth();
			
			$event = Event::get($id);
			if(!$event)
				throw new PageNotFoundException('Evento não encontrado');
			
			$participants = ViewParticipant::eventSearchConfirmed($event->Id, $p, 100);
			
			array_map(function($participant) {
				unset($participant->EventId);
				unset($participant->AuthorId);
				unset($participant->EventName);
				unset($participant->AuthorName);
				unset($participant->MinimumFee);
				unset($participant->WorkshopPrice);
				unset($participant->Date);
				unset($participant->Price);
				unset($participant->PaymentWay);
				unset($participant->Absorb);
				unset($participant->CheckIn);
				unset($participant->Status);
			}, $participants->Data);
			
			return $this->json($participants);
		}
		catch(HTTPException $e)
		{
			return $this->error($e->getCode(), $e->getMessage());
		}
		catch(Exception $e)
		{
			return $this->error();
		}
	}

	public function manager_checkin($id, $number)
	{
		try
		{
			$this->auth();
			
			$event = Event::get($id);
			if(!$event)
				throw new PageNotFoundException('Evento não encontrado');

			$participant = Participant::getByNumber($number);
			if(!$participant || ViewParticipant::getByNumber($number)->EventId != $id)
				throw new PageNotFoundException('Ingresso não encontrado');

			if(!$participant->isConfirmed())
				throw new PageNotFoundException('Ingresso não confirmado');

			if($participant->CheckIn)
				throw new PageNotFoundException('Check-in realizado anteriormente');

			$participant->CheckIn = 1;
			$participant->CheckInDate = time();
			$participant->save();

			$data = new stdClass();
			$data->Name = $participant->Name;
			$data->Date = date('H:i', $participant->CheckInDate);

			return $this->json($data);
		}
		catch(HTTPException $e)
		{
			return $this->error($e->getCode(), $e->getMessage());
		}
		catch(Exception $e)
		{
			return $this->error();
		}
	}
}