<?php

class PaperController extends AppController
{
    public function attach_reviewers($eventId, $p = 1)
    {
        $event = Event::get($eventId);
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não pode acessar essa área.');

        $m = 20;
        $this->_set('event', $event);
        $this->_set('m', $m);
        return $this->_view($event->allPapersAndReviewers($p, $m));
    }

    public function edit($eventId)
    {
        $event = Event::get($eventId);
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão pra editar esse evento.');

        if(Request::isPost())
        {
            try
            {
                $redirect = '~/event/about/' . $event->Id;

                $event->AcceptedPapers = (int) Request::post('AcceptedPapers');
                $event->HasPapers = (int) Request::post('HasPapers');
                $event->CallForPapers = Format::stripTags(Request::post('CallForPapers'));
                $event->setFakeDeadLine(Request::post('FakeDeadLine'));
                $event->setDeadLine(Request::post('DeadLine'));
                $event->setResubmissionDeadLine(Request::post('ResubmissionDeadLine'));
                $event->setRevisionDeadLine(Request::post('RevisionDeadLine'));
                $event->ReviewersPerPaper = (int) Request::post('ReviewersPerPaper');
                $event->checkReviewers(Request::post('CurrentReviewerId'));
                $event->addReviewers(Request::post('ReviewerId'));

                $event->save();
                $this->_flash('alert alert-success', 'As configurações de artigos foram salvas com sucesso.');
                return $this->_redirect($redirect);
            }
            catch (ValidationException $e)
            {
                $this->_flash('alert alert-error', $e->getMessage());
            }
            catch (Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao tentar salvar as configurações de submissão de artigos.' . $e->getMessage());
            }
        }

        return $this->_view($event);
    }

    public function index()
    {
        $p = 1;
        $m = 10;
        $submissions = $this->User->getSubmissionEvents($p, $m);
        $reviews = $this->User->getReviewEvents($p, $m);

        $this->_set('submissions', $submissions);
        $this->_set('reviews', $reviews);
        $this->_set('p', $p);
        $this->_set('m', $m);
        return $this->_view();
    }

    public function my($p = 1)
    {
        $m = 20;
        $this->_set('m', $m);
        return $this->_view($this->User->getSubmissionEvents($p, $m));
    }

    public function submit($participantId)
    {
        $participant = ViewParticipant::get($participantId);

        if(!$participant)
            throw new PageNotFoundException('Inscrição não encontrada');

        $event = $participant->getEvent();

        if(!$event->checkDeadLine())
        {
            $this->_flash('alert alert-error', 'O prazo de submissão acabou.');
            return $this->_back();
        }

        if(Request::isPost())
        {
            try
            {
                $file = Request::file('File');

                if(!$file['tmp_name'])
                    throw new ValidationException('Você deve enviar um arquivo.');

                $paper = $this->_data(new Paper());
                $paper->ParticipantId = (int) $participantId;
                $paper->setFile($file['tmp_name']);
                $paper->save();
                $this->_flash('alert alert-success', 'Trabalho submetido com sucesso.');
                return $this->_redirect('~/paper/my/');
            }
            catch (ValidationException $e)
            {
                $this->_flash('alert alert-error', $e->getMessage());
            }
            catch (Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao enviar o arquivo.');
            }
        }

        $this->_set('event', $event);
        return $this->_view(new Paper());
    }

    public function edit_submission($paperId)
    {
        $paper = Paper::get($paperId);

        if(!$paper)
            throw new PageNotFoundException('Artigo não encontrado');

        $event = $paper->getEvent();

        if(!$event->checkDeadLine())
            throw new ForbiddenException('O prazo de submissão acabou.');

        if(Request::isPost())
        {
            try
            {
                $paper = $this->_data($paper);

                $file = Request::file('File');

                if($file['tmp_name'])
                    $paper->setFile($file['tmp_name']);

                $paper->save();
                $this->_flash('alert alert-success', 'Trabalho submetido com sucesso.');
                return $this->_redirect('~/paper/my/');
            }
            catch (ValidationException $e)
            {
                $this->_flash('alert alert-error', $e->getMessage());
            }
            catch (Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao enviar o arquivo.');
            }
        }

        $this->_set('event', $event);
        return $this->_view('submit', $paper);
    }

    public function resubmit($paperId)
    {
        $paper = Paper::get($paperId);

        if(!$paper)
            throw new PageNotFoundException('Artigo não encontrado');

        $event = $paper->getEvent();

        if(!$event->checkResubmissionDeadLine() || !$paper->NeedRevision)
            throw new ForbiddenException('Artigo não habilitado para revisão.');

        if(Request::isPost())
        {
            try
            {
                $paper = $this->_data($paper);

                $file = Request::file('File');

                if($file['tmp_name'])
                    $paper->setFile($file['tmp_name']);

                $paper->save();
                $this->_flash('alert alert-success', 'Trabalho resubmetido com sucesso.');
                return $this->_redirect('~/paper/my/');
            }
            catch (ValidationException $e)
            {
                $this->_flash('alert alert-error', $e->getMessage());
            }
            catch (Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao enviar o arquivo.');
            }
        }

        $this->_set('event', $event);
        return $this->_view('submit', $paper);
    }

    public function view($paperId)
    {
        $paper = ViewPaper::get($paperId);

        if(!$paper)
            throw new PageNotFoundException('Artigo não encontrado');

        $event = $paper->getEvent();
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão pra acessar esta área.');

        $reviews = $paper->allReviews();

        if(!$reviews[0]->UserId)
        {
            $reviews = array();
            $this->_set('reviewers', $event->allReviewers());
        }

        $this->_set('event', $event);
        $this->_set('reviews', $reviews);
        return $this->_view($paper);
    }

    public function download_all($eventId)
    {
        $event = Event::get($eventId);
        if(!$event) throw new PageNotFoundException('Evento não encontrado');
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão pra acessar esta opção.');

        try
        {
            $event->sendPapersByMail($this->User);
            $this->_flash('alert alert-success', 'Será enviado um e-mail com os links para download dos trabalhos, assim você pode utilizar um gerênciador de downloads para baixar os artigos, ou se preferir acessar as URLs do navegador.');
        }
        catch (ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch (Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao enviar e-amil com os artigos.');
        }

        return $this->_back();
    }

    public function accept($paperId, $decision)
    {
        $paper = Paper::get($paperId);
        if(!$paper) throw new PageNotFoundException('Artigo não encontrado');

        $event = $paper->getEvent();
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão pra acessar esta área.');

        try
        {
            $paper->Decision = intval($decision);
            $paper->save();
            $this->_flash('alert alert-success', 'A situação do artigo foi alterada.');
        }
        catch (ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch (Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao alterar o status do artigo.');
        }

        return $this->_back();
    }

    public function accept_all($eventId)
    {
        $event = Event::get($eventId);
        if (!$event) throw new PageNotFoundException('Evento não encontrado');
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão pra acessar esta opção.');

        try
        {
            $event->sendReviews();
            $this->_flash('alert alert-success', 'Foram enviados os e-mails com as avaliações dos artigos.');
        }
        catch (ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch (Exception $e)
        {
            throw $e;
            $this->_flash('alert alert-error', 'Ocorreu um erro ao enviar e-amil com as avaliações dos artigos.');
        }

        return $this->_back();
    }

    public function send_reviews($paperId)
    {
        $paper = ViewPaper::get($paperId);
        if(!$paper) throw new PageNotFoundException('Artigo não encontrado');

        $event = $paper->getEvent();
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão pra acessar esta área.');

        try
        {
            $paper->sendReviews();
            $this->_flash('alert alert-success', 'O e-mail com as avaliações foi enviado com sucesso.');
        }
        catch (ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch (Exception $e)
        {
            throw $e;
            $this->_flash('alert alert-error', 'Ocorreu um erro ao enviar e-amil com as avaliações do artigo.');
        }

        return $this->_back();
    }

    public function export($eventId)
    {
        $event = Event::get($eventId);
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão pra acessar esta área.');

        $papers = $event->allPapers();

        $this->_headers(array(
            'Content-type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="Lista de Participantes - ' . $event->Name . '.csv"',
            'Pragma' => 'no-cache',
            'Expires' => '0',
        ));

        $this->_set('separator', ';');
        $this->_set('nl', NL);
        return $this->_partial($papers);
    }
}
