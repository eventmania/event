<?php
class AdminController extends AppController
{
    /** @Auth("admin") */
    public function __construct(){}

    public function admin_index()
    {
        $hasPendingEmails = Subscription::hasPendingEmails();

        $this->_set('hasPendingEmails', $hasPendingEmails);
        return $this->_view($this->User);
    }

    public function admin_send_daily_mails()
    {
        try 
        {
            Subscription::sendDailyMails();
            $this->_flash('alert alert-success', 'Os e-mails diários foram enviados.');        
        } 
        catch (Exception $e) 
        {   
            $this->_flash('alert alert-error', 'Ocorreu um erro ao tentar enviar os e-mails diários. ' . $e->getMessage());
        }

        return $this->_back();
    }
}