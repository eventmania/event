<?php
class EventController extends AppController
{
	const EVENT_DRAFT_SUBSCRITION = 'NotifyEventDraft';

	public function index($p = 1)
	{
		$m = 10;
		$events = ViewEventManager::search($p, $m, 'Id', 'DESC', array(
			'ManagerId' => $this->User->Id,
			'ManagerStatus' => 1,
		), 'AND', true);

		$this->_set('m', $m);
		return $this->_view($events);
	}

	public function about($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event);

		$this->_set('report', $event->getTicketsByDateReport()->Data);
		return $this->_view($event);
	}

	public function add()
	{
		if(Request::isPost())
		{
			$event = null;

			try
			{
				$redirect = '~/event/';
				$event = $this->_data(new Event($this->User->Id));

				if($event->Status == 2)
				{
					$event->Status = 0;
					$redirect = '~/layout/edit/{id}';
				}

				$event->Description = Format::stripTags(Request::post('Description'));
				$event->setStartDate(Request::post('StartDate'), Request::post('StartHour'));
				$event->setEndDate(Request::post('EndDate'), Request::post('EndHour'));
				$event->setSlug(Format::post('Slug'));
				$event->MapType = Request::post('IsOnline') ? 2 : (Request::post('Address') ? 1 : 0);
				$event->save();

				$redirect = str_replace('{id}', $event->Id, $redirect);

				$tickets = $event->addTickets(Format::post('Ticket_Title'), Request::post('Ticket_Amount'), Request::post('Ticket_Price'), Format::post('Ticket_Description'), Request::post('Ticket_StartDate'), Request::post('Ticket_StartHour'), Request::post('Ticket_EndDate'), Request::post('Ticket_EndHour'), Request::post('Ticket_Visibility'));
				$event->addManager($this->User, Manager::ROLE_OWNER);

				Subscription::unsubscribe(UserController::REGISTER_SUBSCRITION, $this->User->Id, 'User');

				if(!$event->Status)
				{
					$subs = new Subscription($this->User->Name, $this->User->Email, 'Publique seu Evento!', time() + DAY, self::EVENT_DRAFT_SUBSCRITION, 'event-draft-notification', $event->Id, 'Event');
					$subs->save();
				}

				if(!$event->isValidAbsortion())
				{
					$this->_flash('alert alert-error', 'Para absorver a taxa do serviço todos os ingressos devem ter valor superior a '. Format::money($event->MinimumFee) .'. A política de absrção de taxas foi alterada para que os participantes pagem as taxas.');
					$event = Event::get($event->Id);
					$event->Status = 0;
					$event->Absorb = 0;
					$event->save();
					return $this->_redirect('~/ticket/' . $event->Id);
				}

				$this->_flash('alert alert-success', 'Evento salvo com sucesso.');
				return $this->_redirect($redirect);
			}
			catch(ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch(Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao criar o evento, tente novamente mais tarde.' . $e->getMessage());
				if($event && $event->Id)
					Event::get($event->Id)->delete();
			}
		}

		$this->_set('action', 'Criar');
		$this->_set('usemaps', true);
		return $this->_view(new Event());
	}

	public function edit($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão para editar informações deste evento.');

		if(Request::isPost())
		{
			try
			{
				$newStatus = Request::post('Status');
				unset($_POST['Status']);

				$newAbsorb = Request::post('Absorb');
				unset($_POST['Absorb']);

				$redirect = '~/event/edit/' . $id;

				if($newStatus == 2)
				{
					$newStatus = $event->Status;
					$redirect = '~/layout/edit/' . $event->Id;
				}

				$event = $this->_data($event);
				$event->setStatus($newStatus);
				$event->setAbsorb($newAbsorb);
				$event->setStartDate(Request::post('StartDate'), Request::post('StartHour'));
				$event->setEndDate(Request::post('EndDate'), Request::post('EndHour'));
				$event->Description = Format::stripTags(Request::post('Description'));
				$event->MapType = Request::post('IsOnline') ? 2 : (Request::post('Address') ? 1 : 0);
				$event->save();
            	$event->checkDraftSubscription();

				if($event->isBlocked())
					$this->_flash('alert alert-warning', 'Seu evento foi bloqueado, entre em contato com o suporte para saber a razão (suporte@eventmania.com.br).');
				else
					$this->_flash('alert alert-success', 'Evento editado com sucesso.');

				return $this->_redirect($redirect);
			}
			catch(ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch(Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao editar o evento, tente novamente mais tarde.');
			}
		}

		$this->_set('usemaps', true);
		$this->_set('action', 'Editar');
		return $this->_view('add', $event);
	}

	public function share($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		return $this->_view($event);
	}

	public function toggle($id, $status)
	{
		$event = Event::get($id);
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');
		$this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão para editar informações deste evento.');

		try {
			$event->toggle($status);
			$this->_flash('alert alert-success', 'Status do evento alterado.');
		} catch (ValidationException $e) {
			$this->_flash('alert alert-error', $e->getMessage());
		} catch (Exception $e) {
			$this->_flash('alert alert-error', 'Ocorreu um erro ao alterar o status do evento.');
		}

		return $this->_back();
	}

	public function check_slug($eventId = 0)
	{
		$slug = Event::slugify(urldecode(Request::get('Slug')), intval($eventId));
		return $this->_json(array(
			'slug' => $slug
		));
	}

	public function set_absorb($eventId, $absorb)
	{
		$event = Event::get($eventId);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão para editar informações deste evento.');

		try
		{
			$event->setAbsorb($absorb);
			$this->_flash('alert alert-success', 'A política de pagamento de taxas foi alterada com sucesso.');
		}
		catch (ValidationException $e)
		{
			$this->_flash('alert alert-error', $e->getMessage());
		}
		catch (Exception $e)
		{
			$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar alterar a política de pagamentos de taxas.' . $e->getMessage());
		}

        return $this->_back();
	}

	public function mail_share($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		if(Request::isPost())
		{
			try
			{
				$message = nl2br(Format::post('Text'));
				$subject = Format::post('Title');
				$recipients = Mail::importRecipients(Format::post('Recipients'));

				$response = Mail::broadcast($recipients, $subject, array(
					'controller' => 'mail',
					'view' => 'share',
					'vars' => array(
						'message' => $message,
						'event' => $event
					)
				), $event->Organizer, $event->OrganizerEmail);

				if($response === false)
				{
					$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível enviar os e-mails.');
				}
				else if(count($response))
				{
					$emails = implode(', ', $response);
					$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível enviar para os seguintes e-mails: ' . $emails);
				}
				else
				{
					$this->_flash('alert alert-success', 'Seus convites foram enviados!');
				}

				return $this->_redirect('~/event/about/' . $event->Id);
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar enviar seus convites, tente novamente mais tarde!');
			}
		}

		return $this->_view($event);
	}

	/** @Auth("admin") */
	public function admin_index($p = 1)
	{
		$m = 20;
		$q = Format::get('q');
		$events = ViewEventOwner::search($p, $m, 'Id', 'DESC', array(
			'Name' => "%$q%",
			'OwnerName' => "%$q%",
			'OwnerEmail' => "%$q%",
		));

		$this->_set('m', $m);
		$this->_set('q', $q);
		$this->_set('p', $p);
		$this->_set('user', $this->User);
		return $this->_view($events);
	}

	/** @Auth("admin") */
	public function admin_block_switch($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		if($event)
		{
			try
			{
				$event->blockSwitch();
				$this->_flash('alert alert-success', 'Evento editado com sucesso.');
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar bloquear o evento.' . $e->getMessage());
			}
		}

        return $this->_back();
	}

	/** @Auth("admin") */
	public function admin_banner()
	{
		if(Request::isPost())
		{
			try
			{
				Event::setSpotlights(Request::post('EventId'));
				$this->_flash('alert alert-success', 'Os banners foram alterados com suesso.');
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar alterar os banners.' . $e->getMessage());
			}
		}

		$events = Event::getSpotlights();

		$nullableEvent = new Event();
		$nullableEvent->Name = 'Vazio';
		$nullableEvent->Id = '';
		$events = array_pad($events, 4, $nullableEvent);

		$comingEvents = Event::getComing();
		array_push($comingEvents, $nullableEvent);
		$comingEvents = ArrayFormat::map($comingEvents, 'Id', 'Name');

		$this->_set('comingEvents', $comingEvents);
		$this->_set('user', $this->User);
		return $this->_view($events);
	}
}
