<?php
class ErrorController extends AppController
{
	public function error403($number = '', $message = '', $file = '', $line = '', $trace = '', $details = '')
	{
		return $this->_view('_error', '403', $message);
	}

	public function error404($number = '', $message = '', $file = '', $line = '', $trace = '', $details = '')
	{
		return $this->_view('_error', '404', $message);
	}

	public function error500($number = '', $message = '', $file = '', $line = '', $trace = '', $details = '')
	{
		return $this->_view('_error', '500', $message);
	}
}
