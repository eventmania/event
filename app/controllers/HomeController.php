<?php
class HomeController extends Controller
{
	public $User;

	public function beforeRender()
	{
		if(Auth::isLogged())
		{
			$this->User = User::cast(Session::get('user'));
			$this->_set('user', $this->User);
		}
	}

	public function index()
	{
		$events = Event::getSpotlights();

		$nullableEvent = new Event();
		$nullableEvent->Name = 'Apareça Aqui!';
		$events = array_pad($events, 4, $nullableEvent);

		return $this->_view($events);
	}

	public function tour()
	{
		return $this->_view();
	}

	public function price()
	{
		return $this->_view();
	}

	public function help()
	{
		return $this->_view();
	}

	public function subscribe()
	{
		if(Request::isPost())
		{
			try
			{
				$subs = $this->_data(new Subscription());
				$subs->save();
				$this->_flash('alert alert-success', Format::post('SubscriptionMessage'));
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-success', 'Ocorreu um erro ao tentar fazer sua inscrição para receber ao e-mail.');
			}
		}

		$this->_back();
	}

    protected function _back()
    {
        header('Location: '. $_SERVER["HTTP_REFERER"]);
        exit;
    }

    public function team()
    {
    	return $this->_view();
    }

    public function contact()
    {
    	if(Request::isPost())
    	{
    		try
    		{
	    		$mail = new Mail(Request::post('Name'), 'site@eventmania.com.br');
				$mail->setSubject(Request::post('Subject'));
				$mail->addTo('atendimento@eventmania.com.br', 'Equie do Eventmania');
				$mail->setTemplate(array(
					'controller' => 'mail',
					'view' => 'contact',
				), array(
					'content' => Request::post('Content'),
					'name' => Request::post('Name'),
					'mail' => Request::post('Mail'),
				), array(
					'controller' => 'mail',
					'view' => 'style',
				));

				$mail->send();

				$this->_flash('alert alert-success', 'Mensagem enviado com sucesso!');
    		}
    		catch (Exception $e)
    		{
				$this->_flash('alert alert-success', 'Ocorreu um erro ao enviar a mensagem!');
    		}
    	}

    	return $this->_view();
    }
}
