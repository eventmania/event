<?php
class AccountController extends AppController
{
    public function index()
    {
        return $this->_view($this->User->getAccounts());
    }
	
	public function add()
	{
		$account = new Account();
		if(Request::isPost())
		{
			try
			{
				$data = $this->_data($account);
				$account->UserId = (int)$this->User->Id;

				if(!$account->checkBank($data->Bank))
					throw new ValidationException('Modo de Recebimento inválido.', 1);
				
				$account->Type = $data->Bank == 'MoIP' ? 0 : 1;
				$account->IsMain = 0;
				$account->save();
				
				$this->_flash('alert alert-success', 'Conta salva com sucesso.');
				$this->_redirect('~/account');
			}
			catch (ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível salvar a conta.');
			}
		}
		return $this->_view($account);
	}
	
	public function edit($id)
	{
		$account = Account::get($id);
		
		if(!$account || $account->UserId != $this->User->Id)
			throw new PageNotFoundException('Conta não encontrada.');
		
		if(Request::isPost())
		{
			try
			{
				$data = $this->_data();

				$account->Type = $data->Bank == Account::PAYMENT_TOOL ? 0 : 1;
				$account->Bank = $data->Bank;
				$account->Agency = $data->Agency;
				$account->Account = $data->Account;
				$account->AccountType = (int)$data->AccountType;
				$account->Name = $data->Name;
				$account->CPF = $data->CPF;
				
				$account->save();
				
				$this->_flash('alert alert-success', 'Conta salva com sucesso.');
				$this->_redirect('~/account');
			}
			catch (ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível salvar a conta.');
			}
		}
		return $this->_view('add', $account);
	}
}