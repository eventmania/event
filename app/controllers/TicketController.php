<?php
class TicketController extends AppController
{
	public function index($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event);

		$tickets = $event->getTickets();

		$this->_set('event', $event);
		return $this->_view($tickets);
	}

	public function add($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão para editar informações deste evento.');

		$ticket = new Ticket($event->Id,
			Format::post('Name'),
			Request::post('Amount'),
			Request::post('Price'),
			Format::post('Description'),
			Request::post('StartDate', date('d/m/Y')),
			Request::post('StartHour', date('07:00')),
			Request::post('EndDate', date('d/m/Y', $event->StartDate)),
			Request::post('EndHour', date('23:59')),
			Request::post('Ticket_Visibility')
		);

		if(Request::isPost())
		{
			try
			{
				if(!$event->isValidAbsortion($ticket))
					throw new ValidationException('Para absorver a taxa do serviço o ingresso deve ter valor superior a '. Format::money($event->MinimumFee) .'.');

				$ticket->save();

				$this->_flash('alert alert-success', 'Ingresso criado com sucesso.');
				return $this->_redirect('~/ticket/index/' . $id);
			}
			catch(ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch(Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao criar o ingresso.');
			}
		}

		$this->_set('event', $event);
		return $this->_view($ticket);
	}

	public function edit($id)
	{
		$ticket = Ticket::get($id);
		$event = $ticket->getEvent();

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão para editar informações deste evento.');

		if(Request::isPost())
		{
			try
			{
				$price = (double) str_replace(',', '.', Request::post('Price'));
				if($ticket->countSolds(true) !== 0 && $ticket->Price != $price)
					throw new ValidationException('Já foram vendidos ingressos desse tipo, logo o preço não pode mais ser alterado.');

				$ticket->update(
					Format::post('Name'),
					Request::post('Amount'),
					Format::post('Description'),
					Request::post('StartDate'),
					Request::post('StartHour'),
					Request::post('EndDate'),
					Request::post('EndHour'),
					Request::post('Ticket_Visibility')
				);

				$ticket->setPrice(Request::post('Price'));

				if(!$event->isValidAbsortion($ticket))
					throw new ValidationException('Para absorver a taxa do serviço o ingresso deve ter valor superior a '. Format::money($event->MinimumFee) .'.');

				$ticket->save();

				$this->_flash('alert alert-success', 'Ingresso editado com sucesso.');
				return $this->_redirect('~/ticket/index/' . $ticket->EventId);
			}
			catch(ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch(Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao editar o ingresso.');
			}
		}

		$this->_set('event', $event);
		return $this->_view('add', $ticket);
	}

	public function hide($id)
	{
		$ticket = Ticket::get($id);
		$event = $ticket->getEvent();

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão para editar informações deste evento.');

		try
		{
			$ticket->hide();
		}
		catch(Exception $e)
		{
			$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar editar o ingresso.');
		}

		$this->_flash('alert alert-success', 'Ingresso alterado com sucesso.');
		return $this->_redirect('~/ticket/index/' . $event->Id);
	}

	public function show($id)
	{
		$ticket = Ticket::get($id);
		$event = $ticket->getEvent();

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão para editar informações deste evento.');

		try
		{
			$ticket->show();
		}
		catch(Exception $e)
		{
			$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar editar o ingresso.');
		}

		$this->_flash('alert alert-success', 'Ingresso alterado com sucesso.');
		return $this->_redirect('~/ticket/index/' . $event->Id);
	}

	public function my($p = 1)
	{
		$m = 10;
		$participations = ViewParticipant::search($p, $m, 'Id', 'DESC', array(
			'Email' => $this->User->Email
		));

		if($participations->Count)
			foreach ($participations->Data as $key => $value)
			{
				$mockEvent = new Event();
				$mockEvent->Id = $value->Id;
				$mockEvent->Absorb = $value->Absorb;
				$mockEvent->PaymentWay = $value->PaymentWay;
				$mockEvent->MinimumFee = $value->MinimumFee;

				$value->Cart = $mockEvent->newCart(array($value));
			}

		$this->_set('m', $m);
		return $this->_view($participations);
	}

	public function view($id)
	{
		$participant = ViewParticipant::get($id);

		if(!$participant || $participant->Email != $this->User->Email)
			throw new PageNotFoundException('Pedido não foi encontrado');

		$event = $participant->getEvent();

		$this->_set('event', $event);
		return $this->_view($participant);
	}

	public function pdf($id)
	{
		$participant = ViewParticipant::get($id);

		if(!$participant || !$participant->isConfirmed() || $participant->Email != $this->User->Email)
			throw new PageNotFoundException('Pedido não foi encontrado');

		$this->_headers(array(
			'Pragma' => 'no-cache',
			'Expires' => '0',
			'Content-Type' => 'application/pdf',
		));
		echo $participant->getTicketPDF();
		exit(0);
	}
}
