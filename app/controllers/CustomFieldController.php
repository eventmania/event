<?php
class CustomFieldController extends AppController
{
    public $Event;

    public function index($id)
    {
        $this->Event = Event::get($id);

        if(!$this->Event) throw new PageNotFoundException('O evento não foi encontrado.');

        $this->checkAuthorization($this->Event, Manager::ROLE_ADMIN, 'Você não tem permissão para editar informações deste evento.');

        $form = $this->Event->getForm();
        if($form->exists())
            $this->_flash('alert', Import::view(array(), 'custom-field', 'warning'));

        if(Request::isPost())
        {
            try
            {
				$form->deleteFields(Format::post(GenericCustomField::FIELD_PREFIX, array()), $this->Event->Id);
                Field::saveForm($this->Event->Id, Format::post('Title'), Format::post('Type'), Format::post('IsRequired'), Format::post('Index'), Format::post('Content'));
                $this->_flash('alert alert-success', 'O formulário foi salvo com sucesso.');
                $this->_redirect('~/event/about/' . $this->Event->Id);
            }
            catch (Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao tentar salvar o formulário.' . $e->getMessage());
            }
        }

        $this->_set('form', $form);
        $this->_set('lastIndex', $form->getLastIndex());
        return $this->_view($this->Event);
    }
}
