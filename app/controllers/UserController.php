<?php
class UserController extends AppController
{
	const REGISTER_SUBSCRITION = 'NotifyRegisterWithoutPublish';

	/** @Auth("*") */
	public function login()
	{
		if(Request::isPost())
		{
			$user = User::getByEmail(Request::post('Email'));

			if($user && $user->checkPassword(Request::post('Password')))
			{
				if($user->isActive())
				{
					Session::set('user', $user);
					Auth::set(User::$Roles[$user->Role]);

					if(Request::get('next'))
						$this->_redirect('~/' . Request::get('next'));
					
					$this->_redirect('~/event');
				}
				else
				{
					$this->_flash('alert alert-error', 'Por favor, confirme sua conta por e-mail.');
				}
			}
			else
			{
				$this->_flash('alert alert-error', 'Usuário ou senha incorretos.');
			}
		}

		return $this->_view();
	}
	
	/** @Auth("*") */
	public function register()
	{
		$user = new User();
		if(Request::isPost())
		{
			try
			{
				$this->_data($user);
				$pass = Request::post('Password');
				$passConfirm = Request::post('PasswordConfirm');

				if($pass != $passConfirm)
					throw new ValidationException('As senhas devem ser iguais.', 400);

				if(!User::isAvailable(Request::post('Email')))
					throw new ValidationException('Já existe um usuário cadastrado com este e-mail.', 400);
				
				$user->setPassword($pass);
				$user->Role = 0;
				$user->Status = 1;
				$user->save();

				$response = $user->sendWelcomeMail();
				
				Session::set('user', $user);
				Auth::set(User::$Roles[$user->Role]);

				$subs = new Subscription($user->Name, $user->Email, 'Seja Bem-Vindo!', time() + DAY, self::REGISTER_SUBSCRITION, 'event-draft-notification', $user->Id, 'User');
				$subs->save();

				$this->_flash('alert alert-success', 'Seu cadastro foi realizado com sucesso. Agora você pode criar eventos.');
				return $this->_redirect('~/event/add');
			}
			catch(ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch(Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar criar seu usuário.');
			}
		}
		return $this->_view($user);
	}

	/** @Auth("*") */
	public function confirm($code)
	{
		$user = User::getByCode($code);
		if($user)
		{
			if($user->Status === 0)
			{
				try
				{
					$user->Status = 1;
					$user->CityId = NULL;
					$user->save();

					$this->_flash('alert alert-success', 'Bem-vindo! Sua conta foi ativada, agora você pode criar ou participar de um evento.');
				}
				catch(Exception $e)
				{
					$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar ativar sua conta. Por favor, tente novamente.');
				}
			}
			else
			{
				$this->_flash('alert alert-error', 'Essa conta não pode ser confirma, pois já foi confirmada antes ou está bloqueada.');
			}
		}
		else
		{
			$this->_flash('alert alert-error', 'Não conseguimos encontrar seu usuário, tente efetuar o cadastro novamente.');
		}

		$this->_redirect('~/user/login');
	}
	
	/** @Auth("*") */
	public function forgot()
	{
		if(Request::isPost())
		{
			try
			{
				$user = User::getByEmail(Request::post('Email'));
				if(!$user)
					throw new ValidationException('E-mail não cadastrado.', 1);
				$user->resetPassword();
				$this->_flash('alert alert-success', 'Você receberá um e-mail para redefinição de senha.');
			} 
			catch (ValidationException $e) 
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch (Exception $e) 
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível recuperar sua senha.');
			}
		}
		return $this->_view();
	}
	
	/** @Auth("*") */
	public function reset($code)
	{
		$user = User::single(array(
			'CodeConfirmation' => $code
		));
		
		if(!$user)
			throw new PageNotFoundException('Código de redefinição inválido.');
		
		if(Request::isPost())
		{
			try
			{
				$pass = Request::post('Password');
				$passConfirm = Request::post('PasswordConfirm');

				if($pass != $passConfirm)
					throw new ValidationException('As senhas devem ser iguais.', 400);
				
				$user->setPassword($pass);
				$user->CodeConfirmation = '';
				$user->save();
				$this->_flash('alert alert-success', 'Sua senha foi redefinida com sucesso.');
				$this->_redirect('~/login');
			} 
			catch (ValidationException $e) 
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch (Exception $e) 
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível redefinir sua senha.');
			}
		}
		return $this->_view();
	}
	
	public function logout()
	{
		Auth::clear();
		Session::clear();
		$this->_redirect('~/');
	}
	
	public function edit()
	{
		$user = User::get($this->User->Id);
		if(Request::isPost())
		{
			try 
			{
				$form = $this->_data();
				$user->Name		= $form->Name;
				$user->Email	= $form->Email;
				
				if($form->Password != $form->PasswordConfirm)
					throw new ValidationException('Os campos "Senha" e "Confirmar Senha" estão diferentes.', 1);
				
				if($form->Password)
					$user->setPassword($form->Password);

				$user->save();
				Session::set('user', $user);
				
				$this->_flash('alert alert-success', 'Dados salvos com sucesso.');
			}
			catch (Exception $e) 
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível salvar os dados.');
			}
		}
		return $this->_view($user);
	}

	public function consult($email)
	{
		$user = User::getByEmail($email);

		if($user)
		{
			$clearUser = new StdClass();
			$clearUser->Email = $user->Email;
			$clearUser->Id = $user->Id;
			$clearUser->Name = $user->Name;

			return $this->_json($clearUser);
		}

		return $this->_json(null);
	}

	public function invite($type = 'manager')
	{
		try
		{
			if(!Request::isPost())
				throw new BadRequestException('Requisição mal feita.');

			$event = ViewEventOwner::single(array(
				'Id' => Request::post('Id'),
			));
			if(!$event)
				throw new PageNotFoundException('Evento não encontrado.');

			$this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não tem permissão para convidar moderadores.');

			$user = User::getByEmail(Request::post('Email'));
			if($user)
				throw new ValidationException('Usuário já cadastrado.');
			
			$role = (int)Request::post('Role');

			if($type == 'manager')
			{
				if (!array_key_exists($role, Manager::getRoles()))
					throw new ValidationException('Permissão inválida.');
			}
			else if($type == 'reviewer')
			{
				$role = Manager::ROLE_REVIEWER;
			}

			$user = new User();				
			$user->Name = Format::post('Name');
			$user->Email = Format::post('Email');
			$user->setPassword('suck my eggs! se tu tá vendo isso aqui é porque é viadão!');
			$user->Role = 0;
			$user->Status = 0;
			$user->CodeConfirmation = md5(uniqid(rand(1, 999), true));
			$user->save();

			$manager = new Manager();
			$manager->UserId = (int)$user->Id;
			$manager->EventId = (int)$event->Id;
			$manager->Role = $role;
			$manager->Status = Manager::WAITING_STATUS;
			$manager->save();

			$response = $user->sendInviteMail($event, $type);

			return $this->_json(array('success' => 'Convite enviado com sucesso.', 'id' => $user->Id));
		}
		catch(ValidationException $e)
		{
			return $this->_json(array('error' => $e->getMessage()));
		}
		catch(Exception $e)
		{
			return $this->_json(array('error' => $e->getMessage()));
		}
	}

	/** @Auth("*") */
	public function invited($token)
	{
		$user = User::single(array(
			'CodeConfirmation' => $token,
		));

		if(!$user)
			$this->_redirect('~/register');

		if(Request::isPost())
		{
			try 
			{
				$form = $this->_data();
				$user->Name		= $form->Name;
				
				if($form->Password != $form->PasswordConfirm)
					throw new ValidationException('Os campos "Senha" e "Confirmar Senha" estão diferentes.');
				
				if($form->Password)
					$user->setPassword($form->Password);

				$user->Status = 1;
				$user->CodeConfirmation = '';
				$user->save();

				$managers = Manager::search(1, 10, 'Id', 'ASC', array(
					'UserId' => $user->Id,
					'Status' => Manager::WAITING_STATUS
				))->Data;

				foreach ($managers as $manager)
				{
					$manager->Status = Manager::ACTIVE_STATUS;
					$manager->save();
				}

				Session::set('user', $user);
				Auth::set(User::$Roles[$user->Role]);
				
				$this->_flash('alert alert-success', 'Seu cadastro está completo. Seja bem-vindo ao EventMania.');

				$this->_redirect('~/event');
			}
			catch (ValidationException $e) 
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch (Exception $e) 
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível salvar os dados.');
			}
		}
		return $this->_view($user);
	}

	/** @Auth("admin") */
	public function admin_index($p = 1)
    {
        $m = 20;
        $q = Format::get('q');
        $users = User::search($p, $m, 'Name', 'ASC', array(
            'Name' => "%$q%",
            'Email' => "%$q%",
        ));

        $this->_set('m', $m);
        $this->_set('p', $p);
        $this->_set('q', $q);
        $this->_set('user', $this->User);
        return $this->_view($users);
    }

	/** @Auth("admin") */
	public function admin_block_switch($id)
	{
		try 
		{
			$user = User::get($id);
			$user->blockSwitch();
			$this->_flash('alert alert-success', 'Status alterado com sucesso.');
		} 
		catch (Exception $e) 
		{
			$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar alterar o status do usuário.');
		}

		return $this->_redirectBack();
	}

	/** @Auth("admin") */
	public function admin_reset_password($id)
	{
		try 
		{
			$user = User::get($id);
			$user->resetPassword();
			$this->_flash('alert alert-success', 'A senha foi resetada com sucesso. Foi enviado um e-mail de notificação ao usuário com a nova senha gerada.');
		} 
		catch (Exception $e) 
		{
			$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar redefinir a senha.');
		}

		return $this->_redirectBack();
	}

	/** @Auth("admin") */
	public function admin_login_as($id)
	{
		try 
		{
			$user = User::get($id);
			Session::set('user', $user);
			Auth::set(User::$Roles[$user->Role]);
			$this->_flash('alert', "<b>Atenção:</b> Agora voçê está acessando como $user->Name &lt;$user->Email&gt;");
		} 
		catch (Exception $e) 
		{
			$this->_flash('alert alert-error', 'Ocorreu um erro ao tentar alterar o usuário de acesso.');
		}
		
		return $this->_redirect('~/event');
	}
}