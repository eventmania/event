<?php

class LayoutController extends AppController
{
    public function edit($id, $layout = '')
    {
        $event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não pode editar informações desse evento.');

        if(Request::isPost())
        {
            $redirect = '~/event/about/' . $event->Id;
            if(Request::post('Status') === '2')
                $redirect = '~/paper/edit/' . $event->Id;

            $plugin = ucfirst($layout) . 'Layout';
            return $this->$plugin->save($event, $redirect);
        }

        if($event->Layout)
            $this->_set('form', Import::view(array(
                'model' => $event
            ), 'theme/'. $event->Layout .'/_master', 'form'));
        else
            $this->_set('form', '');

        return $this->_view($event);
    }

    public function get_form($eventId, $layout)
    {
        $event = Event::get($eventId);
        return $this->_partial('theme/'. $layout .'/_master', 'form', $event);
    }
}
