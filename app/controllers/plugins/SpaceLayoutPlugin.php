<?php

class SpaceLayoutPlugin extends LayoutPlugin
{
    public function save($event, $redirect)
    {
        try
        {
            $event->Layout = 'space';
            $file = Request::file('Image');
            if($file)
                $event->saveImage($file['tmp_name'], 100, 40);

            $layout = $event->getLayout();
            if(!$layout->Cover)
                $layout->Cover = new ImageService();
            $file = Request::file('Cover');
            if($file)
                $layout->Cover->saveImage($file['tmp_name'], 2000, 768);

            $layout->ColorScheme = Format::post('ColorScheme');
            if($layout->ColorScheme == 'light')
            {
                $layout->Yin = 'black';
                $layout->Yang = 'white';
            }
            else
            {
                $layout->Yin = 'white';
                $layout->Yang = 'black';
            }

            $event->save();
            $event->checkDraftSubscription();

            if($event->Status == -1)
                $this->_flash('alert alert-warning', 'Seu evento foi bloqueado, entre em contato com o suporte para saber a razão (suporte@eventmania.com.br).');
            else
                $this->_flash('alert alert-success', 'Layout salvo com sucesso.');
        }
        catch (Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao tentar salvar o layout.');
        }

        return $this->_redirect($redirect);
    }
}
