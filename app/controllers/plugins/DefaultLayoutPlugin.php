<?php

class DefaultLayoutPlugin extends LayoutPlugin
{
    public function save($event, $redirect)
    {
        try
        {
            $event->Layout = 'default';
            $file = Request::file('Image');
            if($file)
                $event->saveImage($file['tmp_name'], 1170, 450);

            $event->save();
            $event->checkDraftSubscription();

            if($event->Status == -1)
                $this->_flash('alert alert-warning', 'Seu evento foi bloqueado, entre em contato com o suporte para saber a razão (suporte@eventmania.com.br).');
            else
                $this->_flash('alert alert-success', 'Layout salvo com sucesso.');
        }
        catch (Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao tentar salvar o layout.');
        }

        return $this->_redirect($redirect);
    }
}
