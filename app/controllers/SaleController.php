<?php
class SaleController extends AppController
{
	public function index($id, $p = 1)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

        $m = 20;
        $q = Format::get('q');

        $participants = $event->getParticipants($p, $m, $q);

		$this->_set('event', $event);
		$this->_set('transfer', array_shift($event->getTransfers()));
		$this->_set('accounts', $this->User->getAccounts());
        $this->_set('m', $m);
        $this->_set('q', $q);
        $this->_set('p', $p);

		return $this->_view($participants);
	}
}
