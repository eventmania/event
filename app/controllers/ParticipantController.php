<?php
class ParticipantController extends AppController
{
	public function index($id, $p = 1)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event);

		$m = 25;
		$q = Format::get('q', '');
		$participations = $event->getParticipants($p, $m, $q);

		// pegar dados para os filtros
		$workshops = $event->getWorkshops(1, 100);
		$managers = $event->getManagers(1, 100);
		$tickets = $event->getTickets(1, 100);
		$status = Participant::getStatuses();

		// ajustar dados para criar selects
		$workshops = ArrayFormat::map($workshops->Data, 'Id', 'Title');
		$managers = ArrayFormat::map($managers->Data, 'UserId', 'Name');
		$tickets = ArrayFormat::map($tickets->Data, 'Id', 'Name');

		// adicionar opcao vazia
		$workshops[''] = 'Sem Filtro';
		$managers[''] = 'Sem Filtro';
		$tickets[''] = 'Sem Filtro';
		$status[''] = 'Sem Filtro';

		// enviar dados para a view
		$this->_set('workshops', $workshops);
		$this->_set('managers', $managers);
		$this->_set('tickets', $tickets);
		$this->_set('status', $status);
		$this->_set('event', $event);
		$this->_set('form', $event->getForm());
		$this->_set('m', $m);
		$this->_set('q', $q);

		return $this->_view($participations);
	}

	public function add($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event, Manager::ROLE_ADDER, 'Você não tem permissão para cadastrar participantes.');

		if(Request::isPost())
		{
			try
			{
				$participant = $this->_data(new Participant($this->User->Id));
				$participant->checkFieldsData(Format::post('CustomField', array()));
				$participant->AuthorId = $this->User->Id;
				$participant->save();
				$participant->addFieldsData(Format::post('CustomField', array()));

				$this->_flash('alert alert-success', 'Participante cadastrado com sucesso.');

				$this->_redirect('~/participant/view/' . $participant->Id);
			}
			catch(Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao cadastrar o participant.');
			}
		}

		$this->_set('event', $event);
		$this->_set('eventHeader', 'event-header');
		$this->_set('message', '');
		$this->_set('form', $event->getForm());
		$this->_set('btnLabel', 'Salvar');
		$this->_set('title', 'Adicionar Participante');
		$this->_set('tickets', $event->getTickets()->Data);
		$this->_set('workshops', $event->getWorkshops()->Data);
		return $this->_view(new Participant());
	}

	public function invite($id)
	{
		$event = Event::get($id);
		if(!$event) throw new PageNotFoundException('Evento não foi encontrado');
		$this->checkAuthorization($event);

		if(Request::isPost())
		{
			try
			{
				$participant = $this->_data(new Participant());
				$participant->Status = Participant::WAITING_STATUS;
				$participant->InvitationStatus = Participant::INVITATION_SEND;
				$participant->AuthorId = $this->User->Id;
				$participant->save();
				$participant->invite(Format::post('message'));
				$this->_flash('alert alert-success', 'O e-mail de convite foi enviado com sucesso. Atenção, o link para que o participante edite os dados de sua inscrição apenas ficará disponível por '. Participant::INVITATION_DAYS_REMAINING .' dias.');
			}
			catch (ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch(Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao convidar o participante.');
			}
		}

		$this->_set('event', $event);
		$this->_set('eventHeader', 'event-header');
		$this->_set('title', 'Convidar');
		$this->_set('btnLabel', 'Convidar');
		$this->_set('form', '');
		$this->_set('message', 'Olá, gostaria que você participasse do '. $event->Name .'.');
		$this->_set('tickets', $event->getTickets()->Data);
		$this->_set('workshops', $event->getWorkshops()->Data);
		return $this->_view('add', new Participant());
	}

	/**
	 * @Auth("*")
	 */
	public function edit_invitation($number)
	{
		$participant = Participant::getByNumber($number);
		if(!$participant || !$participant->InvitationStatus || $participant->Date + Participant::INVITATION_DAYS_REMAINING * DAY < time())
			throw new PageNotFoundException('Participante não foi encontrado');
		$event = $participant->getEvent();

		if(Request::isPost())
		{
			try
			{
				$status = $participant->InvitationStatus;

				$participant->Name = Format::post('Name');
				$participant->Email = Format::post('Email');
				$participant->Status = Participant::FINALIZED_STATUS;
				$participant->InvitationStatus = Participant::INVITATION_FINALIZED;
				$participant->save();
				$participant->editFieldsData(Format::post('CustomField', array()));

				if($status === Participant::INVITATION_SEND)
				{
					$p = ViewParticipant::single(array('Id' => $participant->Id));
					$p->sendTicketMail();
				}

				$this->_flash('alert alert-success', 'Inscrição finalizada com sucesso.');
			}
			catch (ValidationException $e)
			{
				$this->_flash('alert alert-error', $e->getMessage());
			}
			catch(Exception $e)
			{
				exit(var_dump($e->getMessage()));
				$this->_flash('alert alert-error', 'Ocorreu um erro ao finalizar a inscrição.');
			}
		}

		$form = $event->getForm();
		$form->setData($participant->getCustomData());

		$this->_set('event', $event);
		$this->_set('eventHeader', 'event-public-header');
		$this->_set('title', 'Editar Inscrição');
		$this->_set('btnLabel', 'Salvar');
		$this->_set('form', $form);
		$this->_set('message', '');
		$this->_set('tickets', array());
		$this->_set('workshops', array());
		return $this->_view('add', $participant);
	}

	public function edit($id)
	{
		$participant = Participant::get($id);

		$event = $participant->getEvent();
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');
		$this->checkAuthorization($event, Manager::ROLE_ADDER, 'Você não tem permissão para editar participantes.');

		if(Request::isPost())
		{
			try
			{
				$data = $this->_data();
				$participant->Name = $data->Name;
				$participant->Email = $data->Email;
				$participant->save();
				$participant->editFieldsData(Format::post('CustomField', array()));

				$this->_flash('alert alert-success', 'Participante editado com sucesso.');
			}
			catch(Exception $e)
			{
				$this->_flash('alert alert-error', 'Ocorreu um erro ao editar o participant.');
			}
		}

		$form = $event->getForm();
		$form->setData($participant->getCustomData());

		$this->_set('event', $event);
		$this->_set('form', $form);
		$this->_set('tickets', $event->getTicketsAssoc());
		return $this->_view($participant);
	}

	public function view($id)
	{
		$participant = ViewParticipant::get($id);

		if(!$participant) throw new PageNotFoundException('Participante não foi encontrado');

		$event = $participant->getEvent();

		$this->checkAuthorization($event);

		$this->_set('event', $event);
		$this->_set('user', $this->User);
		return $this->_view($participant);
	}

	public function random($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event);

		if(Request::isPost() && Request::isAjax())
		{
			$participants = $event->allParticipants();
			$participant = $participants[rand(0, count($participants) - 1)];

			$this->_set('model', $participant);
			return $this->_json($participant); //naum esta funcionando corretamente...
		}

		$this->_set('event', $event);
		return $this->_view();
	}

	public function resend($id)
	{
		$participant = ViewParticipant::get($id);
		if(!$participant)
			throw new PageNotFoundException('Participante não foi encontrado');

		if(!$participant->isConfirmed())
			throw new PageNotFoundException('Participante não confirmado');

		$event = $participant->getEvent();
		$this->checkAuthorization($event, Manager::ROLE_ADDER);

		$participant->sendTicketMail();

		$this->_redirect('~/participant/view/' . $id);
	}

	public function cancel($id)
	{
		$participant = Participant::get($id);
		if(!$participant)
			throw new PageNotFoundException('Participante não foi encontrado');

		$event = $participant->getEvent();
		$this->checkAuthorization($event, Manager::ROLE_ADMIN);

		try
		{
			if($participant->AuthorId == 0)
				throw new ValidationException('Somente inscrições manuais podem ser canceladas.');


			if($participant->isCanceled())
				throw new ValidationException('Inscrição já cancelada.');

			$participant->Status = Participant::ORG_CANCELED_STATUS;
			$participant->save();

			$this->_flash('alert alert-success', 'Inscrição cancelada com sucesso.');

			$this->_redirect('~/participant/index/' . $event->Id);
		}
		catch (ValidationException $e)
		{
			$this->_flash('alert alert-error', $e->getMessage());
			$this->_redirect('~/participant/view/' . $participant->Id);
		}
		catch (Exception $e)
		{
			$this->_flash('alert alert-error', 'Ocorreu um erro e não foi possível cancelar a venda.');
			$this->_redirect('~/participant/view/' . $participant->Id);
		}
	}

	public function pdf($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event);

		$participations = $event->getParticipationsData(Format::post('TicketFilter'), Format::post('AuthorFilter'), Format::post('WorkshopFilter'), Format::post('StatusFilter'));

		if(!$participations)
		{
			$this->_flash('alert', 'Nenhum participante foi encontrado para exportação, tente mudar os filtros utilizados.');
			return $this->_redirect('~/participant/index/' . $event->Id);
		}

		$columns = Format::post('SelectedInfo');
		if(!$columns)
			$columns = array('Name', 'Email', 'Sign');

		$hasSignature = array_search('Sign', $columns);

		if($hasSignature !== false)
			unset($columns[$hasSignature]);

		require_once(App::$root . 'app/vendors/dompdf/dompdf_config.inc.php');

		$content = Import::view(array(
			'event' => $event,
			'participations' => $participations,
			'columns' => $columns,
			'hasSignature' => $hasSignature !== false,
		), 'participant', 'pdf');

		$dompdf = new DOMPDF();
		$dompdf->load_html($content);
		$dompdf->set_paper('a4', count($columns) > 4 ? 'landscape' : 'portrait');
		$dompdf->render();
		$dompdf->add_info('Title', 'Lista de Participantes - ' . $event->Name);
		$dompdf->add_info('Author', 'EventMania');

		$this->_headers(array(
			'Pragma' => 'no-cache',
			'Expires' => '0',
		));

		$dompdf->stream('Lista de Participantes - ' . $event->Name . '.pdf', array('Attachment' => true));
		exit(0);
	}

	public function excel($id)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event);

		$participations = $event->getParticipationsData();

		if(!$participations)
		{
			$this->_flash('alert', 'Nenhum participante foi encontrado para exportação.');
			return $this->_redirect('~/participant/index/' . $event->Id);
		}

		$columns = ArrayFormat::selectedProperty($participations[0], 'Title');
		$columns = array_unique($columns);

		$this->_headers(array(
			'Content-type' => 'text/csv',
			'Content-Disposition' => 'attachment; filename="Lista de Participantes - ' . $event->Name . '.csv"',
			'Pragma' => 'no-cache',
			'Expires' => '0',
		));

		$this->_set('separator', ';');
		$this->_set('nl', NL);
		$this->_set('columns', $columns);
		return $this->_partial($participations);
	}
	
	public function label($id, $format)
	{
		$accepteds = array(
			'letter-30',
			'letter-20',
		);
		
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event);
		
		if(!in_array($format, $accepteds))
		{
			$this->_flash('alert alert-error', 'Formato de etiqueta inválido.');
			return $this->_redirect('~/participant/index/' . $event->Id);
		}

		$participations = $event->getParticipants(1, 5000);

		if(!$participations->Data)
		{
			$this->_flash('alert', 'Nenhum participante foi encontrado para exportação.');
			return $this->_redirect('~/participant/index/' . $event->Id);
		}
		
		require_once(App::$root . 'app/vendors/dompdf/dompdf_config.inc.php');

		$content = Import::view(array(
			'participations' => $participations,
		), 'participant', 'label/' . $format);

		$dompdf = new DOMPDF();
		$dompdf->load_html($content);
		$dompdf->set_paper(explode('-', $format)[0], 'portrait');
		$dompdf->render();
		$dompdf->add_info('Title', 'Etiquetas - ' . $event->Name);
		$dompdf->add_info('Author', 'EventMania');

		$this->_headers(array(
			'Pragma' => 'no-cache',
			'Expires' => '0',
		));

		$dompdf->stream('Etiquetas - ' . $event->Name . '.pdf', array('Attachment' => true));
		exit(0);
	}

	public function checkin($id, $p = 1)
	{
		$event = Event::get($id);

        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

		$this->checkAuthorization($event);

		$m = 30;
		$q = Format::get('q', '');
		$participations = $event->getConfirmedParticipants($p, $m, $q);

		$this->_set('event', $event);
		$this->_set('m', $m);
		$this->_set('q', $q);
		return $this->_view($participations);
	}

	public function check($check, $id)
	{
		try
		{
			$participant = Participant::get($id);
			$event = $participant->getEvent();

        	if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');

			$this->checkAuthorization($event, Manager::ROLE_EXCHANGER, 'Você não tem permissão para fazer o check-in.');

			if(!$participant->isConfirmed())
				throw new Exception('Ingresso não confirmado.');

			if($check == 'in')
			{
				$participant->CheckIn = 1;
				$participant->CheckInDate = time();
				$participant->save();
			}
			else if($check == 'out')
			{
				$participant->CheckIn = 0;
				$participant->CheckInDate = 0;
				$participant->save();
			}
			$participant->CheckInDate = $participant->getCheckInDate();
		}
		catch (Exception $e)
		{
			$error = new stdClass;
			$error->Error = $e->getMessage();
			return $this->_json($error);
		}
		return $this->_json($participant);
	}

	/**
	 * @Auth("*")
	 */
	public function auto_checkin($id)
	{
		session_set_cookie_params(60 * 60 * 6);

		$tokenKey = 'Checkin.Token';

		if(!preg_match('/^([\d]+)$/', $id))
		{
			$session = Session::get($tokenKey);
			if(!$session || $session->Token != $id)
			{
				$this->_flash('alert alert-error', 'Token inválido. A organização do evento deve realizar um novo login.');

				Session::clear();
				Auth::clear();
				$this->_redirect('~/login');
			}
			$event = Event::get($session->Event->Id);
		}
		else
		{
			$event = Event::get($id);

			if(!$event)
				throw new PageNotFoundException('O evento não foi encontrado.');

			$this->checkAuthorization($event, Manager::ROLE_EXCHANGER, 'Você não tem permissão para habilitar o auto check-in.');

			Session::clear();
			Auth::clear();

			$session = new stdClass();
			$session->Event = $event;
			$session->Token = md5(uniqid(rand(1, 999), true));
			Session::set($tokenKey, $session);

			$this->_redirect('~/participant/auto-checkin/' . $session->Token . '/');
		}

		$this->_set('token', $session->Token);
		$this->_set('event', $event);
		return $this->_partial();
	}

	/**
	 * @Auth("*")
	 */
	public function auto_check()
	{
		session_set_cookie_params(60 * 60 * 6);

		$data = new stdClass();
		try
		{
			$token = Request::post('Token');
			$session = Session::get('Checkin.Token');
			if(!$session || $session->Token != $token)
			{
				Session::clear();
				Auth::clear();

				throw new Exception('Token inválido. A organização do evento deve realizar um novo <a href="' . App::$rootVirtual . '">login</a>.');
			}

			if(!Request::isPost() || !Request::isAjax() )
				throw new BadRequestException('Requisição mal feita.');

			if(!Request::post('Number'))
				throw new BadRequestException('Número inválido.');

			$number = Format::post('Number');

			$participant = Participant::getByNumber($number);
			if(!$participant)
				throw new Exception('Ingresso não encontrado.');

			if(!$participant->isConfirmed())
				throw new Exception('Ingresso não confirmado.');

			if($participant->CheckIn)
				throw new Exception('Check-in realizado anteriormente.');

			$participant->CheckIn = 1;
			$participant->CheckInDate = time();
			$participant->save();

			$data->Name = $participant->Name;
			$data->Date = date('H:i', $participant->Date);
		}
		catch (Exception $e)
		{
			$data->Error = $e->getMessage();
		}
		return $this->_json($data);
	}
}
