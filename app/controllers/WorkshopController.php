<?php
class WorkshopController extends AppController
{
    public $Event;

    public $Workshop;

    public function beforeRender()
    {
        parent::beforeRender();

        $id = end(App::$args['params']);

        if(App::$args['action'] === 'index' || App::$args['action'] === 'add')
        {
            $this->Event = Event::get($id);
            if(!$this->Event) throw new PageNotFoundException('O evento não foi encontrado.');
            $this->Workshop = new Workshop($this->Event->Id);
        }
        else
        {
            $this->Workshop = Workshop::get($id);
            $this->Event = $this->Workshop->getEvent();
            if(!$this->Event) throw new PageNotFoundException('O evento não foi encontrado.');
        }

    }

    public function index($id, $p = 1)
    {
        $this->checkAuthorization($this->Event, Manager::ROLE_VIEWER, 'Você não tem permissão para visualizar esta área.', '~/event/' . $this->Event->Id);

        $m = 20;
        $this->_set('event', $this->Event);
        $this->_set('p', $p);
        $this->_set('m', $m);
        return $this->_view($this->Event->getWorkshops($p, $m));
    }

    public function add($eventId)
    {
        $this->checkAuthorization($this->Event, Manager::ROLE_ADMIN, 'Você não tem permissão para visualizar esta área.', '~/workshop/' . $this->Event->Id);

        if(Request::isPost())
        {
            try
            {
                $file = Request::file('Image');

                $workshop = $this->_data($this->Workshop);

                if(!$this->Event->isValidAbsortion($workshop))
                    throw new ValidationException('Você está absorvendo as taxas do serviço de vendas de inscrições, logo todos os itens vendidos devem ter preços supreriores à '. Format::money($this->Event->MinimumFee) .'.');
                $workshop->setStartDate(Request::post('StartDate'), Request::post('StartHour'));
                $workshop->setEndDate(Request::post('EndDate'), Request::post('EndHour'));
                $workshop->saveImage($file['tmp_name']);
                $workshop->save();

                $this->_flash('alert alert-success', 'Workshop criado com sucesso.');
                return $this->_redirect('~/workshop/' . $workshop->EventId);
            }
            catch(ValidationException $e)
            {
                $this->_flash('alert alert-error', $e->getMessage());
            }
            catch(Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao criar o workshop, tente novamente mais tarde.');
            }
        }

        $this->_set('submitLabel', 'Criar');
        $this->_set('event', $this->Event);
        return $this->_view($this->Workshop);
    }

    public function edit($id)
    {
        $this->checkAuthorization($this->Event, Manager::ROLE_ADMIN, 'Você não tem permissão para visualizar esta área.', '~/workshop/' . $this->Event->Id);

        if(Request::isPost())
        {
            try
            {
                $workshop = $this->_data($this->Workshop);
                $workshop->setStartDate(Request::post('StartDate'), Request::post('StartHour'));
                $workshop->setEndDate(Request::post('EndDate'), Request::post('EndHour'));

                $workshop->setPrice(Request::post('Price'));

                $file = Request::file('Image');
                if($file['tmp_name'])
                    $workshop->saveImage($file['tmp_name']);

                if(!$this->Event->isValidAbsortion($workshop))
                    throw new ValidationException('Você está absorvendo as taxas do serviço de vendas de inscrições, logo todos os itens vendidos devem ter preços supreriores à '. Format::money($this->Event->MinimumFee) .'.');

                if($workshop->count(Participant::ALL_STATUSES) !== 0)
                    throw new ValidationException('Já foram vendidos workshops desse tipo, logo o preço não pode mais ser alterado.');

				$workshop->save();

                $this->_flash('alert alert-success', 'Workshop salvo com sucesso.');
                return $this->_redirect('~/workshop/' . $workshop->EventId);
            }
            catch(ValidationException $e)
            {
                $this->_flash('alert alert-error', $e->getMessage());
            }
            catch(Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao criar o workshop, tente novamente mais tarde.');
            }
        }

        $this->_set('event', $this->Event);
        $this->_set('submitLabel', 'Salvar');
        $this->_set('imageUrl', $this->Workshop->getImageUrl(300, 200));
        return $this->_view('add', $this->Workshop);
    }

    public function remove($id)
    {
        $this->checkAuthorization($this->Event, Manager::ROLE_ADMIN, 'Você não tem permissão para visualizar esta área.', '~/workshop/' . $this->Event->Id);

        try
        {
            if($this->Workshop->count(Participant::ALL_STATUSES) !== 0)
                throw new ValidationException('Já foram vendidos workshops desse tipo, logo ele não pode ser excluído.');

            $this->Workshop->delete();
            $this->_flash('alert alert-success', 'Workshop excluído com sucesso.');
        }
        catch(ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch(Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao excluir o workshop, tente novamente mais tarde.');
        }

        return $this->_redirect('~/workshop/' . $this->Workshop->EventId);
    }
}
