<?php

require_once App::$root . 'app/vendors/PagSeguroLibrary/PagSeguroLibrary.php';

class SiteController extends Controller
{
	private $Event;

	public function index($id)
	{
		if(preg_match('/^([\d]+)$/', $id))
			$this->Event = Event::get($id);
		else
			$this->Event = Event::single(array('Slug' => $id));

        if(!$this->Event || !$this->Event->isVisible() || $this->Event->isBlocked())
			throw new PageNotFoundException('O evento não foi encontrado.');

        $layout = $this->Event->Layout;
		$this->Template->setDirectory(App::$root . "app/views/theme/$layout/");

		if(Request::isPost())
		{
			try
			{
				if(!Request::post('Ticket'))
					throw new ValidationException('Por favor, selecione um ingresso.');

				$ticket = Ticket::get(Request::post('Ticket'));
				if(!$ticket || $ticket->IsPrivate)
					throw new ValidationException('Ingresso não encontrado.');

				if($ticket->EventId != $this->Event->Id)
					throw new ValidationException('O ingresso <b>' . $ticket->Name . '</b> não pertence ao evento <b>' . $this->Event->Name . '</b>.');

				if(!$ticket->isAvailable())
					throw new ValidationException('O ingresso <b>' . $ticket->Name . '</b> não está mais disponível.');

				$cart = new ShoppingCart($this->Event);
				$cart->add($ticket);

				if(Request::post('Workshop'))
				{
					$workshop = Workshop::get(Request::post('Workshop'));
					if(!$workshop)
						throw new ValidationException('Workshop não encontrado.');

					if($workshop->EventId != $this->Event->Id)
					throw new ValidationException('O workshop <b>' . $workshop->Title . '</b> não pertence ao evento <b>' . $this->Event->Name . '</b>.');

					if(!$workshop->isAvailable())
						throw new ValidationException('O ingresso <b>' . $workshop->Title . '</b> não está mais disponível.');

					$cart->add($workshop);
				}
				$booked = $cart->saveBooked();

				$this->_redirect('~/buy/' . $booked->Code);
			}
			catch(ValidationException $e)
			{
				$this->_flash('alert alert-block alert-error', $e->getMessage());
			}
			catch (Exception $e)
			{
				$this->_flash('alert alert-block alert-error', 'Ocorreu um erro e não foi possível fazer a sua reserva.');
			}
		}

		$this->_set('event', $this->Event);
		$this->_set('workshops', $this->Event->getWorkshops(1, 100));
		$this->_set('tickets', $this->Event->getTickets());
		return $this->_view();
	}

	public function buy($code)
	{
		$booked = Booked::single(array(
			'Code' => $code
		));

		if(!$booked)
			throw new PageNotFoundException('Página não encontrada.');

		try
		{
			$event = Event::get($booked->EventId);

			if($booked->isExpired())
			{
				$this->_flash('alert alert-block alert-error','Você excedeu o tempo limite de sua reserva. Por favor, efetue uma nova compra.');
				header('Location: ' . $event->getUrl());
				exit;
			}
			
			$shopping = new ShoppingCart($event);

			$ticket = null;
			$workshop =  null;

			foreach ($booked->getItems() as $item)
			{
				$clazz = $item->ItemType;
				$instance = $clazz::get($item->ItemId);
				$shopping->add($instance);
				if($clazz == 'Ticket')
					$ticket = $instance;
				else if($clazz == 'Workshop')
					$workshop = $instance;
			}

			$total = 0;
			foreach($shopping as $shop)
				$total += $shop->getSalePrice(true);

			$form = $event->getForm();

			if(Request::isPost())
			{
				if(strlen(Format::post('Name')) < 10)
					throw new ValidationException('Por favor, informe seu nome completo.');

				if(!preg_match('/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/', Format::post('Email')))
					throw new ValidationException('Por favor, informe seu e-mail corretamente.');

				if(strlen(Format::post('Card_Phone')) < 6)
					throw new ValidationException('Por favor, informe seu telefone corretamente.');
			
				$participant = new Participant();

				$participant->checkFieldsData(Format::post('CustomField', array()));
				
				$participant->Name = Format::post('Name');
				$participant->Email = Format::post('Email');
				$participant->TicketId = (int)$ticket->Id;
				$participant->Date = time();
				$participant->Status = 0;
				$participant->AuthorId = 0;
				$participant->PaymentMethod = Format::post('PaymentMethod', 'free');
				if($workshop)
					$participant->WorkshopId = (int)$workshop->Id;
				$participant->save();

				$participant->addFieldsData(Format::post('CustomField', array()));

				if($total > 0)
				{
					
					////// Start PagSeguro //////
	

					// Instantiate a new payment request
					$paymentRequest = new PagSeguroPaymentRequest();

					// Set the currency
					$paymentRequest->setCurrency("BRL");

					// Add an item for this payment request
					foreach($shopping as $item)
						$paymentRequest->addItem($item->getCurrent()->Id, $event->Name .' - '. $item->getCurrent()->getTitle(), 1, $item->getSalePrice(true));
					

					// Set a reference code for this payment request. It is useful to identify this payment
					// in future notifications.
					$paymentRequest->setReference($participant->Id);

					// Set shipping information for this payment request
					$sedexCode = PagSeguroShippingType::getCodeByType('NOT_SPECIFIED');
					$paymentRequest->setShippingType($sedexCode);

					$phones = explode(' ', Request::post('Card_Phone'));
					// Set your customer information.
					$paymentRequest->setSender(
						$participant->Name, $participant->Email, Format::strToNumber($phones[0]), Format::strToNumber($phones[1])
					);

					// Set the url used by PagSeguro to redirect user after checkout process ends
					$paymentRequest->setRedirectUrl($event->getUrl());


					// Another way to set checkout parameters
					$paymentRequest->addParameter('notificationURL', 'https://www.eventmania.com.br/site/notify2');

					try 
					{

						/*
						 * #### Credentials #####
						 * Replace the parameters below with your credentials (e-mail and token)
						 * You can also get your credentials from a config file. See an example:
						 * $credentials = PagSeguroConfig::getAccountCredentials();
						 */
						$credentials = new PagSeguroAccountCredentials(Config::get('pagseguro_email'), Config::get('pagseguro_token'));

						// Register this payment request in PagSeguro to obtain the payment URL to redirect your customer.
						//$url = $paymentRequest->register($credentials);
						$onlyCheckoutCode = true;
						$code = $paymentRequest->register($credentials, $onlyCheckoutCode);
						
						//self::printPaymentUrl($url);
						
						$layout = $event->Layout;
						$this->Template->setDirectory(App::$root . "app/views/theme/$layout/");
						
						$this->_set('code', $code);
						$this->_set('lightbox', true);
						
						$this->_set('event', $event);
						$this->_set('model', $participant);
						$this->_set('form', $form);
						$this->_set('booked', $booked);
						$this->_set('cart', $shopping);
						$this->_set('total', $total);
						
						return $this->_view('lightbox');
					} 
					catch (PagSeguroServiceException $e) 
					{
						throw new ValidationException($e->getMessage(), 1);
					}
					
					$booked->IsUsed = 1;
					$booked->save();

					//$participant->Status = Format::akatusToEventMania($response->getStatus());
					//$participant->TransactionCode = $response->getTransaction();
					//$participant->save();

					if($participant->isConfirmed())
					{
						$participant = ViewParticipant::get($participant->Id);
						$participant->sendTicketMail();
					}


					////// End PagSeguro //////
					
					
					
					
					/*
					//Akatus
					$receiver = new Receiver(Config::get('akatus_key'), Config::get('akatus_email'));

					//dados do comprador
					$buyer = new Buyer($participant->Name, $participant->Email);

					//telefone do comprador
					$buyer->addNewPhone('residencial', Format::strToNumber(Request::post('Card_Phone')));

					//carrinho e os produtos comprados
					$cart = new Cart($buyer, $receiver);
					foreach($shopping as $item)
						$cart->addNewProduct($event->Name .' - '. $item->getCurrent()->getTitle(), $item->getSalePrice(true), 0)->setId($item->getCurrent()->Id);


					if(Request::post('Payment_Method') == 'boleto' || preg_match('/^tef_/', Request::post('Payment_Method')))
					{
						$cart->getTransaction()->setReference($participant->Id);
						$cart->getTransaction()->setPaymentMethod(Format::post('Payment_Method'));
						$akatusCartApi = new AkatusCartApi ();
					}
					else
					{
						//Transação com Cartão de crédito
						$card = new CardTransaction();
						$card->setReference($participant->Id);
						$card->setPaymentMethod(Format::post('Payment_Method'));

						$card->setNumber(Format::post('Card_Number'));
						$card->setExpiration(Format::post('Card_Month') . '/' . Format::post('Card_Year'));
						$card->setSecurityCode(Format::post('Card_CVV'));
						$card->setInstallments(Format::post('Card_Installment'));

						//Portador
						$holder = $card->holder();
						$holder->setDoc(Format::post('Card_CPF')); //cpf para testes
						$holder->setName(Format::post('Card_Name'));
						$holder->setPhone(Format::strToNumber(Request::post('Card_Phone')));

						$cart->setTransaction($card);
						$akatusCartApi = new AkatusCartApi(new AkatusCartWithCardMarshaller());
					}
					$transaction = $cart->getTransaction();
					$transaction->setFingerprintAkatus(Format::post('fingerprint_akatus'));
					$transaction->setFingerprintPartnerId(Format::post('fingerprint_partner_id'));
					$transaction->setIp(IP);

					//o método test() indica que estamos usando o ambiente de testes,
					//para usar o ambiente de produção, basta não suar o método test.
					if(Config::get('enviroment') === AppController::PRODUCTION_ENVIROMENT)
						$response = $akatusCartApi->execute($cart);
					else
						$response = $akatusCartApi->test()->execute($cart);

					$booked->IsUsed = 1;
					$booked->save();

					$participant->Status = Format::akatusToEventMania($response->getStatus());
					$participant->TransactionCode = $response->getTransaction();
					$participant->save();

					if($participant->isConfirmed())
					{
						$participant = ViewParticipant::get($participant->Id);
						$participant->sendTicketMail();
					}

					if($response->getReturnUrl())
					{
						header('Location: ' . $response->getReturnUrl());
						exit;
					}
					 
					 */
				}
				else
				{
					$participant->Status = 3;
					$participant->save();

					$booked->IsUsed = 1;
					$booked->save();

					$participant = ViewParticipant::get($participant->Id);
					$participant->sendTicketMail();
				}

				$layout = $event->Layout;
				$this->Template->setDirectory(App::$root . "app/views/theme/$layout/");

				$this->_set('event', $event);
				$this->_set('model', $participant);

				return $this->_view('finish');
			}
		}
		catch (RuntimeException $e)
		{
			$log = new ErrorLog();
			$log->EventId = (int)$event->Id;
			$log->Action = 'buy';
			$log->Name = Format::post('Name');
			$log->Email = Format::post('Email');
			$log->Phone = Format::post('Card_Phone');
			$log->Message = $e->getMessage();
			$log->save();

			$this->_flash('alert alert-block alert-error', $e->getMessage());
		}
		catch(ValidationException $e)
		{
			$log = new ErrorLog();
			$log->EventId = (int)$event->Id;
			$log->Action = 'buy';
			$log->Name = Format::post('Name');
			$log->Email = Format::post('Email');
			$log->Phone = Format::post('Card_Phone');
			$log->Message = $e->getMessage();
			$log->save();

			$this->_flash('alert alert-block alert-error', $e->getMessage());
		}
		catch (Exception $e)
		{
			$log = new ErrorLog();
			$log->EventId = (int)$event->Id;
			$log->Action = 'buy';
			$log->Name = Format::post('Name');
			$log->Email = Format::post('Email');
			$log->Phone = Format::post('Card_Phone');
			$log->Message = $e->getMessage();
			$log->save();

			$this->_flash('alert alert-block alert-error', 'Ocorreu um erro e não foi possível efetuar a inscrição.');
		}

		$layout = $event->Layout;
		$this->Template->setDirectory(App::$root . "app/views/theme/$layout/");

		$this->_set('event', $event);
		$this->_set('form', $form);
		$this->_set('booked', $booked);
		$this->_set('cart', $shopping);
		$this->_set('total', $total);

		// for layout
		$this->_set('workshops', $event->getWorkshops(1, 100));
		$this->_set('tickets', $event->getTickets());

		return $this->_view();
	}

	public function installment($card, $price)
	{
		$params = array(
			'email' => Config::get('akatus_email'),
			'api_key' => Config::get('akatus_key'),
			'payment_method' => $card,
			'amount' => $price,
		);

		$options = array(
			CURLOPT_SSL_VERIFYPEER => false,
		);
		
		if(Config::get('enviroment') === AppController::PRODUCTION_ENVIROMENT)
			$url = 'https://www.akatus.com/api/v1/parcelamento/simulacao.json';
		else
			$url = 'http://sandbox.akatus.com/api/v1/parcelamento/simulacao.json';
		
		$response = Request::create($url . '?' . http_build_query($params), 'GET', array(), array(), $options);

		header('Content-type: application/json; charset='. Config::get('charset'));
		return $this->_print($response);
	}
	
	public function notify()
	{
		if(!Request::isPost() || Request::post('token') != Config::get('akatus_nip'))
			throw new BadRequestException('Requisição mal feita.');

		$participant = Participant::get(Request::post('referencia'));
		if(!$participant)
			throw new PageNotFoundException('Participante não encontrado.');

		$status = Format::akatusToEventMania(Request::post('status'));
		$participant->Status = $status;
		$participant->save();

		if($participant->Status === Participant::CONFIRMED_STATUS)
		{
			$participant = ViewParticipant::get($participant->Id);
			$participant->sendTicketMail();
		}

		exit;
	}

	public function notify2()
	{
		$code = Request::post('notificationCode');
		$type = Request::post('notificationType');

		if ($code && $type) 
		{
			$notificationType = new PagSeguroNotificationType($type);
			$strType = $notificationType->getTypeFromValue();
			switch ($strType) 
			{
				case 'TRANSACTION':
					$this->transactionNotification($code);
					break;
				default:
					LogPagSeguro::error("Unknown notification type [" . $notificationType->getValue() . "]");
			}
		} 
		else 
		{
			LogPagSeguro::error("Invalid notification parameters: " . serialize($_POST));
		}
		exit;



		/*if(!Request::isPost() || Request::post('token') != Config::get('akatus_nip'))
			throw new BadRequestException('Requisição mal feita.');

		$participant = Participant::get(Request::post('referencia'));
		if(!$participant)
			throw new PageNotFoundException('Participante não encontrado.');

		$status = Format::akatusToEventMania(Request::post('status'));
		$participant->Status = $status;
		$participant->save();

		if($participant->Status === Participant::CONFIRMED_STATUS)
		{
			$participant = ViewParticipant::get($participant->Id);
			$participant->sendTicketMail();
		}

		exit;*/
	}
	
	private static function transactionNotification($notificationCode) 
			{
		/*
		 * #### Credentials #####
		 * Replace the parameters below with your credentials (e-mail and token)
		 * You can also get your credentials from a config file. See an example:
		 * $credentials = PagSeguroConfig::getAccountCredentials();
		 */
		$credentials = new PagSeguroAccountCredentials(Config::get('pagseguro_email'), Config::get('pagseguro_token'));
		try 
		{
			$transaction = PagSeguroNotificationService::checkTransaction($credentials, $notificationCode);
			// Do something with $transaction
			
			$participant = Participant::get($transaction->getReference());
			if(!$participant)
				throw new PageNotFoundException('Participante não encontrado.');

			$participant->Status = (int)$transaction->getStatus();
			$participant->save();

			if($participant->Status === Participant::CONFIRMED_STATUS)
			{
				$participant = ViewParticipant::get($participant->Id);
				$participant->sendTicketMail();
			}
		} 
		catch (PagSeguroServiceException $e) 
		{
			die($e->getMessage());
		}
	}

	public function iframe($id)
	{
		if(preg_match('/^([\d]+)$/', $id))
			$this->Event = Event::get($id);
		else
			$this->Event = Event::single(array('Slug' => $id));

        if(!$this->Event || !$this->Event->isVisible() || $this->Event->isBlocked())
			throw new PageNotFoundException('O evento não foi encontrado.');

        $layout = $this->Event->Layout;
		$this->Template->setDirectory(App::$root . "app/views/theme/$layout/");

		$this->_set('event', $this->Event);
		$this->_set('workshops', $this->Event->getWorkshops(1, 100));
		$this->_set('tickets', $this->Event->getTickets());
		return $this->_partial();
	}
}
