<?php

class ErrorLogController extends AppController
{
	public function admin_index($p = 1)
	{
		$m = 30;
		$q = Format::get('q', '');
		$logs = ErrorLog::search($p, $m, 'Id', 'DESC', array(
			'Action' => "%$q%",
			'Browser' => "%$q%",
			'Name' => "%$q%",
			'Email' => "%$q%",
			'Message' => "%$q%",
		));
		$this->_set('m', $m);
		$this->_set('q', $q);
		
		return $this->_view($logs);
	}
	
	public function admin_view($id)
	{
		$log = ErrorLog::single(array(
			'Id' => $id
		));
		if(!$log)
			throw new PageNotFoundException('Log não encontrado.');
		
		return $this->_view($log);
	}
}