<?php

class ReviewerController extends AppController
{
    public function index($id, $p = 1)
    {
        $event = Event::get($id);
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado.');
        $this->checkAuthorization($event, Manager::ROLE_REVIEWER, 'Você não pode realizar evaliações neste evento.');

        $m = 20;
        $this->_set('m', $m);
        $this->_set('event', $event);
        return $this->_view($this->User->getReviews($event->Id, $p, $m));
    }

    public function review($reviewId)
    {
        $review = ViewReview::get($reviewId);
        if(!$review) throw new PageNotFoundException('Artigo não foi encontrado.');
        $event = $review->getEvent();
        $this->checkAuthorization($event, Manager::ROLE_REVIEWER, 'Você não é revisor deste evento.');

        if(Request::isPost())
        {
            try
            {
                $review = Review::get($review->Id);
                $review = $this->_data($review);
                $review->save();

                $this->_flash('alert alert-success', 'Avaliação salva com sucesso.');
                return $this->_redirect('~/reviewer/' . $event->Id);
            }
            catch (ValidationException $e)
            {
                $this->_flash('alert alert-error', $e->getMessage());
            }
            catch (Exception $e)
            {
                $this->_flash('alert alert-error', 'Ocorreu um erro ao tentar salvar a avaliação.');
            }
        }

        $this->_set('event', $event);
        return $this->_view($review);
    }

    public function add($paperId, $managmentId)
    {
        if(Request::isPost())
        {
            $paper = ViewPaper::get($paperId);

            if(!$paper) throw new PageNotFoundException('Artigo não foi encontrado.');

            $event = $paper->getEvent();
            $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não pode adicionar revisores.');

            $status = $paper->addReview($managmentId);
            return $this->_json($status);
        }

        return $this->_json(false);
    }

    public function remove($paperId, $managmentId, $force = false)
    {
        if(Request::isPost())
        {
            $paper = ViewPaper::get($paperId);

            if(!$paper) throw new PageNotFoundException('Artigo não foi encontrado.');

            $event = $paper->getEvent();
            $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não pode adicionar revisores.');

            $status = $paper->removeReview($managmentId, $force);

            return $this->_json($status);
        }

        return $this->_json(false);
    }

    public function clear($eventId)
    {
        $event = Event::get($eventId);
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado..');
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não pode adicionar revisores.');

        try
        {
            $event->clearReviews();
            $this->_flash('alert alert-success', 'Revisões excluídas com sucesso.');
        }
        catch (ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch (Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao excluír as revisões.');
        }

        return $this->_back();
    }

    public function notify($eventId)
    {
        $event = Event::get($eventId);
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado..');
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não pode adicionar revisores.');

        try
        {
            $event->notifyReviewers();
            $this->_flash('alert alert-success', 'E-mails enviados com sucesso.');
        }
        catch (ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch (Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao enviar os e-mails de notificação.' . $e->getMessage());
        }

        return $this->_back();
    }

    public function raffle($eventId)
    {
        $event = Event::get($eventId);
        if(!$event) throw new PageNotFoundException('O evento não foi encontrado..');
        $this->checkAuthorization($event, Manager::ROLE_ADMIN, 'Você não pode adicionar revisores.');

        try
        {
            $event->raffleReviewers();
            $this->_flash('alert alert-success', 'Revisores sorteados com sucesso.');
        }
        catch (ValidationException $e)
        {
            $this->_flash('alert alert-error', $e->getMessage());
        }
        catch (Exception $e)
        {
            $this->_flash('alert alert-error', 'Ocorreu um erro ao sortear os revisores.');
        }

        return $this->_back();
    }

    public function my($p = 1)
    {
        $m = 20;
        $this->_set('m', $m);
        return $this->_view($this->User->getReviewEvents($p, $m));
    }
}
