<?php

/** @View("view_participant") */
class ViewParticipant extends Participant implements IShoppingCartItem {

	use PriceTrait;

	/**
	 * @overwrite
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;

	/** @Column(Type="Int") */
	public $EventId;

	/** @Column(Type="String") */
	public $TicketName;

	/** @Column(Type="String") */
	public $EventName;

	/** 
	 * @Column(Type="String") 
	 * @Filter("capitalize")
	 */
	public $AuthorName;

	/** @Column(Type="Int") */
	public $PaymentWay;

	/** @Column(Type="Int") */
	public $Absorb;

	/** @Column(Type="Int") */
	public $MinimumFee;

	/** @Column(Type="String") */
	public $WorkshopName;

	/** @Column(Type="Double") */
	public $WorkshopPrice;

	public function getBuyDate()
	{
		return Format::datetime($this->Date);
	}

    public function getName()
    {
        return $this->TicketName;
    }

    public static function get($id)
    {
    	return self::single(array('Id' => $id));
    }

	protected $_cart;

	public function getCart()
	{
		if(!$this->_cart)
		{
			$event = new Event();
			$event->MinimumFee = $this->MinimumFee;
			$event->Absorb = $this->Absorb;
			$event->PaymentWay = $this->PaymentWay;
			$event->Id = $this->EventId;

			$cart = new ShoppingCart($event, $this);
			$cart->add(new Workshop($event->Id, $this->WorkshopPrice));
		}

		return $cart;
	}

	protected $_ticket = null;

	public function getTicket()
	{
		if(!$this->_ticket)
		{
			$this->_ticket = Ticket::get($this->TicketId);
		}

		return $this->_ticket;
	}

	protected $_event = null;

	public function getEvent()
	{
		if(!$this->_event)
		{
			$this->_event = Event::get($this->EventId);
		}

		return $this->_event;
	}

	public static function countEventParticipants($eventId)
	{
		return self::countTickets($eventId, self::SOLD_STATUSES);
	}

	public static function countSoldTickets($eventId)
	{
		return self::countTickets($eventId, self::SOLD_STATUSES);
	}

	public static function countUnpaidTickets($eventId)
	{
		return self::countTickets($eventId, self::UNPAID_STATUSES);
	}

	public static function countCanceledTickets($eventId)
	{
		return self::countTickets($eventId, self::CANCELED_STATUSES);
	}

	protected static function countTickets($eventId, $statusStr)
	{
		$db = Database::factory();
		return $db->ViewParticipant
					->where('EventId = ? AND Status IN ' . $statusStr, $eventId)
					->count();
	}

	public static function eventSearch($eventId, $p = 1, $m = 25, $q = '', $o = 'Name', $t = 'ASC')
	{
		$p = ($p < 1 ? 1 : $p) - 1;
		$db = Database::factory();
		return $db->ViewParticipant
					->where('EventId = ? AND (Name LIKE ? OR Email LIKE ?)', $eventId, "%$q%", "%$q%")
					->orderBy($o, $t)
					->paginate($p, $m);
	}

	public static function eventSearchConfirmed($eventId, $p = 1, $m = 25, $q = '', $o = 'Name', $t = 'ASC')
	{
		$p = ($p < 1 ? 1 : $p) - 1;
		$db = Database::factory();
		return $db->ViewParticipant
					->where('EventId = ? AND Status IN ' . self::SOLD_STATUSES . ' AND (Name LIKE ? OR Email LIKE ?)', $eventId, "%$q%", "%$q%")
					->orderBy($o, $t)
					->paginate($p, $m);
	}

	public static function eventSearchChecked($eventId, $p = 1, $m = 25, $q = '', $o = 'Name', $t = 'ASC')
	{
		$p = ($p < 1 ? 1 : $p) - 1;
		$db = Database::factory();
		return $db->ViewParticipant
					->where('EventId = ? AND CheckIn = ? AND (Name LIKE ? OR Email LIKE ?)', $eventId, 1, "%$q%", "%$q%")
					->orderBy($o, $t)
					->paginate($p, $m);
	}

	public static function allByEvent($eventId, $siteParticipants = true)
	{
		$op = '<>';
		if($siteParticipants)
			$op = '=';

		$db = Database::factory();
		return $db->ViewParticipant->where('EventId = ? AND AuthorId '. $op .' ' . self::SITE_AUTHOR_ID, $eventId)->all();
	}

	public static function allByEventAndStatus($eventId, $siteParticipants = true, $status = null)
	{
		$op = '<>';
		if($siteParticipants)
			$op = '=';

		if(is_array($status))
			$status = ' AND Status IN (' . implode(', ', $status) . ')';
		else if(is_string($status))
			$status = ' AND Status = ' . $status;
		else
			$status = '';

		$db = Database::factory();
		return $db->ViewParticipant->where('EventId = ? AND AuthorId '. $op .' ' . self::SITE_AUTHOR_ID . $status, $eventId)->all();
	}

	public function getTicketPDF()
	{
		require_once(App::$root . 'app/vendors/dompdf/dompdf_config.inc.php');

		require ROOT . 'app/vendors/phpqrcode/qrlib.php';
		ob_start();
		QRCode::png($this->getNumber(), false, QR_ECLEVEL_L, 4, 0);
		$qrcode = base64_encode(ob_get_contents());
		ob_end_clean();

		$content = Import::view(array('event' => $this->getEvent(), 'model' => $this, 'qrcode' => $qrcode), 'ticket', 'pdf');

		$dompdf = new DOMPDF();
		$dompdf->load_html($content);
		$dompdf->set_paper('a4', 'portrait');
		$dompdf->add_info('Title', 'Ingresso - ' . $this->Id);
		$dompdf->add_info('Author', 'EventMania');
		$dompdf->render();
		return $dompdf->output();
	}

	public function sendTicketMail()
	{
		$mail = new Mail();
		$mail->setSubject('Confirmação de Inscrição - ' . $this->EventName);
		$mail->setFrom('contato@eventmania.com.br', 'Roberta');
		$mail->addTo($this->Email, $this->Name);
		$mail->setTemplate(array(
			'controller' => 'mail',
			'view' => 'ticket',
		), array(
			'name' => $this->getFirstName(),
		), array(
			'controller' => 'mail',
			'view' => 'style',
		));
		$mail->addAttachment('Ingresso-' . $this->Id . '.pdf', 'application/pdf', base64_encode($this->getTicketPDF()));

		return $mail->send();
	}
}
