<?php

/** @Entity("workshop") */
class Workshop extends BaseModel implements IShoppingCartItem
{
    use HappeningTrait, PriceTrait, ImageTrait;

    /**
     * @AutoGenerate()
     * @Column(Type="Int",Key="Primary")
     */
    public $Id;

    /** @Column(Type="String") */
    public $Title;

    /** @Column(Type="String") */
    public $Description;

    /** @Column(Type="Int") */
    public $Amount;

    /** @Column(Type="String") */
    public $Instructor;

    /** @Column(Type="String") */
    public $InstructorUrl;

    /** @Column(Type="Int") */
    public $EventId;

    public function __construct($eventId = 0, $price = 0.0, $title = '')
    {
        $this->EventId = intval($eventId);
        $this->Price = $price;
        $this->Title = $title;
    }

    public function getEvent()
    {
        return Event::get($this->EventId);
    }

    public function getName()
    {
        return $this->Title;
    }

    public function count($statuses)
    {
        $db = Database::factory();
        $count = $db->ViewParticipant
            ->where('EventId = ? AND WorkshopId = ? AND Status IN ' . $statuses, $this->EventId, $this->Id)
            ->count();

        return intval($count);
    }

    public function isAvailable()
    {
        return $this->Amount - $this->count(Participant::SOLD_STATUSES) - $this->count(Participant::UNPAID_STATUSES) > 0;
    }

    public function getTitle()
    {
        return $this->Title;
    }
}
