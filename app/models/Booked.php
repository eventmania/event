<?php

/** @Entity("booked") */
class Booked extends BaseModel
{
	/**
	 * @AutoGenerate()
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;

	/** 
	 * @Column(Type="Int")
	 * @Required()
	 */
	public $EventId;

	/** 
	 * @Column(Type="String")
	 * @Required()
	 */
	public $Code;
	
	/** 
	 * @Column(Type="String")
	 * @Required()
	 */
	public $SessionId;
	
	/** 
	 * @Column(Type="Int")
	 * @Required()
	 */
	public $Date;

	/** 
	 * @Column(Type="Int")
	 * @Required()
	 */
	public $IsUsed = 0;
	
	const TIME = 60;
	
	public function isExpired()
	{
		return $this->IsUsed || ($this->Date + (MINUTE * self::TIME)) < time();
	}
	
	public function getRemainder()
	{
		$remainder = ($this->Date + (MINUTE * self::TIME)) - time();
		return $remainder > 0 ? $remainder : 0;
	}

	public function getItems()
	{
		return BookedItem::all($this->Id);
	}
}