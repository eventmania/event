<?php

/**
 * Implementa as funções de tratamento de imagens.
 */
trait ImageTrait
{
    /** @Column(Type="String") */
    public $Image;

    public function getImageUrl($w = false, $h = false)
    {
        if($h && $w)
        {
            if($this->Image)
            {
                $filename = 'tmp/' . md5($this->Image . $w . $h) . '.png';

                if(!file_exists(App::$wwwroot . $filename))
                {
                    $canvas = new Canvas2();
                    $canvas->carrega(App::$wwwroot . 'uploads/' . $this->Image);
                    $canvas->redimensiona($w, $h, 'crop');
                    $canvas->grava(App::$wwwroot . $filename);
                }

                return Request::getSite() . $filename;
            }

            return $this->getPlaceholderImage($w, $h);
        }

        if($this->Image)
            return Request::getSite() . 'uploads/' . $this->Image;
        return Request::getSite() . 'img/logo.png';
    }

    public function saveImage($imagePath, $w = 200, $h = 200)
    {
        if($imagePath)
        {
            $canvas = new Canvas2();
            $canvas->carrega($imagePath);
            $this->Image = md5(uniqid()) . '.png';
            $canvas->redimensiona($w, $h, 'crop');
            $canvas->grava(App::$wwwroot . 'uploads/' . $this->Image);
        }
    }

    public function getPlaceholderImage($w, $h)
    {
        $number = rand(1, 8);
        $image  = 'placeholder-' . $number . '.jpg';
        $filename = 'tmp/' . md5($image . $w . $h) . '.jpg';

        if(!file_exists(App::$wwwroot . $filename))
        {
            $canvas = new Canvas2();
            $canvas->carrega(App::$wwwroot . 'img/abstract/' . $image);
            $canvas->redimensiona($w, $h, 'crop');
            $canvas->grava(App::$wwwroot . $filename);
        }
        return Request::getSite() . $filename;
    }
}
