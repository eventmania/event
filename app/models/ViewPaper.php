<?php
/**
 * @View("view_paper")
 */
class ViewPaper extends Paper
{
    /**
     * @Column(Type="Int")
     */
    public $EventId;

    /**
     * @Column(Type="String")
     */
    public $EventName;

    /**
     * @Column(Type="String")
     */
    public $EventSlug;

    /**
     * @Column(Type="String")
     */
    public $EventCustomUrl;

    /**
     * @Column(Type="Int")
     */
    public $HasPapers;

    /**
     * @Column(Type="Int")
     */
    public $TicketId;

    /**
     * @Column(Type="String")
     */
    public $UserEmail;

    /**
     * @Column(Type="String")
     */
    public $UserName;

    public function getEvent()
    {
        return Event::get($this->EventId);
    }

    public static function get($id)
    {
        return self::single(array('Id' => $id));
    }

    public function getAuthors()
    {
        $authors = $this->UserName . "[{$this->UserEmail}]; " . $this->Authors;
        return rtrim($authors, '; ');
    }

    public static function allByEvent($eventId)
    {
        $db = Database::factory();
        return $db->ViewPaper->all('EventId = ?', $eventId);
    }

    public function sendReviews()
    {
        $this->loadRecommendations();

        $this->ParticipantName = $this->UserName;
        $this->PaperId = $this->Id;

        $mail = new Mail();
        $mail->setFrom('avisos@eventmania.com.br', 'EventMania');
        $mail->setSubject('Resultado de Submissão de Artigo');
        $mail->setTo($this->UserEmail, $this->UserName);
        $mail->setTemplate(array(
            'controller' => 'mail',
            'view' => $this->Decision ? 'paper-accepted' : 'paper-refused',
        ), array(
            'event' => $this->getEvent(),
            'paper' => $this,
            'recommendations' => $this->Recommendations,
            'needRevision' => $this->NeedRevision
        ), array(
            'controller' => 'mail',
            'view' => 'style',
        ));
        $mail->send();
    }
}
