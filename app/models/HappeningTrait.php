<?php

/**
 * Implementa as funções de formatação de datas e horas de acontecimentos
 * (eventos, workshops, palestras, etc.).
 */
trait HappeningTrait
{
    /**
     * @Column(Type="Int")
     * @Required()
     * @Label("Data de Inicio")
     */
    public $StartDate;

    /**
     * @Column(Type="Int")
     * @Required()
     * @Label("Data de Fim")
     */
    public $EndDate;

    protected function _date($time)
    {
        return Format::date($time);
    }

    public function getStartDate()
    {
        return $this->_date($this->StartDate ? $this->StartDate : time());
    }

    public function getEndDate()
    {
        return $this->_date($this->EndDate ? $this->EndDate : time());
    }

    protected function _getHour($time)
    {
        $hour = date('H', $time);
        $minute = date('i', $time);

        if($minute == '59')
            return $hour . ':59';
        if($minute >= 30)
            return $hour . ':30';

        return $hour . ':00';
    }

    public function getStartHour()
    {
        return $this->_getHour($this->StartDate ? $this->StartDate : time());
    }

    public function getEndHour()
    {
        return $this->_getHour($this->EndDate ? $this->EndDate : strtotime("midnight", time()) - 1);
    }

    private $_remainingDays = null;

    public function getRemainingDays()
    {
        if(!$this->_remainingDays)
        {
            $today = time();
            $remaining = ceil(($this->StartDate - $today) / DAY);
            if($remaining > 0)
                $this->_remainingDays = $remaining;
            else
                $this->_remainingDays = 0;
        }

        return $this->_remainingDays;
    }

    public function setStartDate($startDate, $startHour)
    {
        $this->StartDate = Format::strToTimestamp($startDate . ' ' . $startHour);
    }

    public function setEndDate($endDate, $endHour)
    {
        $this->EndDate = Format::strToTimestamp($endDate . ' ' . $endHour);
    }

    protected function _getShortDate($date)
    {
        return date("D / d M", $date);
    }

    public function getShortDate($date = 'start')
    {
        if($date == 'start')
            return $this->_getShortDate($this->StartDate);
        return $this->_getShortDate($this->EndDate);
    }
}
