<?php
/**
 * @Entity("view_review")
 */
class ViewReview extends Review
{
    /**
     * @Column(Type="String")
     */
    public $Title;

    /**
     * @Column(Type="String")
     */
    public $File;

    /**
     * @Column(Type="String")
     */
    public $Keywords;

    /**
     * @Column(Type="String")
     */
    public $Authors;

    /**
     * @Column(Type="String")
     */
    public $Summary;

    /**
     * @Column(Type="String")
     */
    public $ParticipantName;

    /**
     * @Column(Type="String")
     */
    public $ParticipantEmail;

    /**
     * @Column(Type="Int")
     */
    public $UserId;

    /**
     * @Column(Type="String")
     */
    public $UserName;

    /**
     * @Column(Type="String")
     */
    public $UserEmail;

    /**
     * @Column(Type="Int")
     */
    public $EventId;

    /**
     * @Column(Type="Int")
     */
    public $TotalReviews;

    public static function get($id)
    {
        return self::single(array('Id' => $id));
    }

    public function getEvent()
    {
        return Event::get($this->EventId);
    }

    public function getUrl()
    {
        return Request::getSite() . Paper::SAVE_DIRECTORY . $this->File;
    }

    public function getAuthors()
    {
        $authors = $this->UserName . "[{$this->UserEmail}]; " . $this->Authors;
        return rtrim($authors, '; ');
    }

    public function getOverallRate()
    {
        return $this->Recommendation * 3 + $this->ContentRate + $this->ImpactRate + $this->PresentationRate;
    }

    public static function allByEvent($eventId, $pickPapers = false)
    {
        $db = Database::factory();
        $reviews = $db->ViewReview->all('EventId = ? AND Status = ?', $eventId, self::PUBLISHED_STATUS);

        if($pickPapers)
            $reviews = self::_choose($reviews, $pickPapers);

        return $reviews;
    }

    public static function _choose($reviews, $pick)
    {
        $reviews = self::_evaluate($reviews);
        usort($reviews, function($alpha, $beta){
            if ($alpha->OverallRate > $beta->OverallRate)
                return 1;
            elseif ($alpha->OverallRate === $beta->OverallRate)
                if($alpha->UserName > $beta->UserName)
                    return 1;
                elseif($alpha->UserName === $beta->UserName)
                    return 0;
            return -1;
        });

        $count = count($reviews);
        $i = $count - 1;
        $pick = $count - $pick;
        for (; $i >= $pick && $i >= 0; $i--)
            $reviews[$i]->Decision = 1;

        for (; $i >= 0; $i--)
            $reviews[$i]->Decision = 0;

        return $reviews;
    }

    public static function _evaluate($reviews)
    {
        if($reviews)
        {
            $papers = array();
            foreach ($reviews as $r)
            {
                if(!isset($papers[$r->Id]))
                {
                    $papers[$r->PaperId] = $r;
                    $papers[$r->PaperId]->OverallRate = 0;
                    $papers[$r->PaperId]->Comments = array();
                    $papers[$r->PaperId]->Recommendations = array();
                }

                $papers[$r->PaperId]->OverallRate += $r->getOverallRate();
                $papers[$r->PaperId]->Comments[] = $r->PublicComments;
                $papers[$r->PaperId]->Recommendations[] = $r->getRecommendation();
            }

            return $papers;
        }

        return array();
    }
}
