<?php

/** @Entity("ticket") */
class Ticket extends BaseModel implements IShoppingCartItem
{
	use HappeningTrait, PriceTrait;

	/**
	 * @AutoGenerate()
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;

	/** @Column(Type="Int") */
	public $EventId;

	/**
	 * @Required
	 * @Column(Type="String")
	 * @Label("Nome")
	 * @Regex(Pattern="^(.{5,})$",Message="O Nome do ingresso deve possuir no mínimo 5 caractéres")
	 */
	public $Name;

	/** @Column(Type="String") */
	public $Description;

	/**
	 * @Column(Type="Int")
	 * @Required()
	 * @Label("Quantidade")
	 */
	public $Amount;

	/** @Column(Type="Int") */
	public $ReservedAmount;

	/** @Column(Type="Int") */
	public $IsPrivate;

	/** @Column(Type="Int") */
	public $HasPapers;

	public function __construct($eventId = 0, $title = '', $amount = 0, $price = 0, $description = '', $startDate = '01/01/2013', $startHour = '00:00', $endDate = '01/01/2013', $endHour = '23:30', $visibility = 0)
	{
		$this->EventId = (int) $eventId;
		$this->_setData($title, $amount, $price, $description, $startDate, $startHour, $endDate, $endHour, $visibility);
	}

	public function update($title = '', $amount = 0, $description = '', $startDate = '01/01/2013', $startHour = 0, $endDate = '01/01/2013', $endHour = 0, $visibility = 0)
	{
		$this->_setData($title, $amount, null, $description, $startDate, $startHour, $endDate, $endHour, $visibility);
		$this->save();
	}

	protected function _setData($title = null, $amount = null , $price = null, $description = null, $startDate = null, $startHour = null , $endDate = null, $endHour = null, $visibility = 0)
	{
		if($title)
			$this->Name = $title;
		if($amount)
			$this->Amount = (int) $amount;
		if($price)
			$this->setPrice($price);

		$this->Description = $description;
		if($startDate)
			$this->setStartDate($startDate, $startHour);
		if($endDate)
			$this->setEndDate($endDate, $endHour);
		$this->IsPrivate = $visibility ? 1 : 0;
	}

	protected $_event = null;

	public function getEvent()
	{
		if(!$this->_event)
			$this->_event = Event::get($this->EventId);
		return $this->_event;
	}

	public function getSoldPercentage()
	{
		$solds = $this->countSolds();
		return $solds / $this->Amount * 100;
	}

	public function getName()
	{
		return $this->Name;
	}

	public function hide()
	{
		$this->IsPrivate = 1;
		$this->save();
	}

	public function show()
	{
		$this->IsPrivate = 0;
		$this->save();
	}

	public function isOpen()
	{
		return $this->StartDate <= time() && $this->EndDate >= time();
	}

	public static function countPaidByEvent($eventId)
	{
		$db = Database::factory();
		return $db->Ticket->where('EventId = ? AND Price > 0', $eventId)->count('Id');
	}

	protected $_solds = null;

	public function countSolds($all = false)
	{
		if($this->_solds === null)
		{
			$db = Database::factory();
			$this->_solds = $db->Participant->where('TicketId = ? AND Status IN ' . ($all ? Participant::ALL_STATUSES : Participant::SOLD_STATUSES), $this->Id)->count();
		}

		return intval($this->_solds);
	}

	protected $_unpaidSolds = null;

	public function countUnpaidSolds()
	{
		if($this->_unpaidSolds === null)
		{
			$db = Database::factory();
			$this->_unpaidSolds = $db->Participant->where('TicketId = ? AND Status IN ' . Participant::UNPAID_STATUSES, $this->Id)->count();
		}

		return intval($this->_unpaidSolds);
	}

	public function isAvailable()
	{
		return $this->isOpen() && (self::countSolds() + self::countUnpaidSolds()) < $this->Amount;
	}

    public function getTitle()
    {
        return $this->Name;
    }
}
