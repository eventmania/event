<?php

/** @View("view_tickets_sold_by_date") */
class ViewSoldTicketsByDate extends BaseModel
{
	/** @Column(Type="Int") */
	public $EventId;

	/** @Column(Type="Int") */
	public $PurchaseDay;

	/** @Column(Type="Int") */
	public $PurchaseMonth;

	/** @Column(Type="Int") */
	public $PurchaseYear;

	/** @Column(Type="Int") */
	public $Amount;
}