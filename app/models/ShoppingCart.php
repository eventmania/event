<?php

class ShoppingCart implements Iterator
{
	public $Context;

	protected $_items;

	private $_currentKey = self::CONTEXT_KEY;

	const CONTEXT_KEY = -1;

	const CLIENT_ROLE = 'client';
	const ORGANIZER_ROLE = 'organizer';

	const COOKIE_NAME = 'EventManiaTimerID3';

	private $_cookieId;
	
	private function getCookieId()
	{
		if(isset($_COOKIE[self::COOKIE_NAME]))
			return $_COOKIE[self::COOKIE_NAME];
		
		$this->_cookieId = md5(guid() . uniqid());
		setcookie(self::COOKIE_NAME, $this->_cookieId, 0, '/');
		return $this->_cookieId;
	}

	public function __construct(IShoppingCartContext $context, IShoppingCartItem $item = null)
	{
		$this->Context = $context;
		$this->_items = array();
		$this->_currentKey = self::CONTEXT_KEY;

		if($item)
			$this->add($item);
	}

	protected function _sum()
	{
		$price = 0;

		foreach ($this->_items as $item)
			$price += $item->getPrice();

		return $price;
	}

	protected function _getPrice()
	{
    	if($this->key() == self::CONTEXT_KEY)
    		return $this->_sum();
    	$item = $this->_items[$this->key()];
		return $item->getPrice();
	}

    protected function _getFee(IShoppingCartItem $item)
    {
        if(!$item->getPrice())
            return 0;
        
        $fee = $item->getPrice() * $this->Context->getPaymentWay() / 100;
    	return $fee < $this->Context->getMinimumFee() ? $this->Context->getMinimumFee() : $fee;
    }

	protected function _return($value, $asFloat)
	{
		if($asFloat)
			return $value;
		return Format::money($value);
	}

    protected function _countFreeItems()
    {
        $count = 0;
        
        foreach ($this->_items as $i) 
        {
            if(!$i->getPrice())
                $count++;
        }

        return $count;
    }

	public function add(IShoppingCartItem $item)
	{
		array_push($this->_items, $item);
	}

	public function remove(IShoppingCartItem $item, $recursive = false)
	{
		$class = get_class($item);

		foreach ($this->_items as $key => $_item) 
		{
			if($item->getId() == $_item->getId() && is_a($_item, $class))
			{
				unset($this->_items[$key]);
				$removed = true;
			}

			if($removed && !$recursive)
				break;
		}
	}

    /** 
     * Pega o preço líquido do item atual
     * @param 		$asFloat 		O resultado deve ser retornado como float ou string?
     * @return 		mixed 			O valor líquido do item atual.
     */
    public function getNetPrice($asFloat = false)
    {
    	$price = $this->_getPrice();

		if($this->Context->isAbsorb())
			$price -= $this->getFee('', true);

		return $this->_return($price, $asFloat);
    }

    /** 
     * Pega o preço de venda do item atual
     * @param 		$asFloat 		O resultado deve ser retornado como float ou string?
     * @return 		mixed 			O valor de venda do item atual.
     */
    public function getSalePrice($asFloat = false)
    {
    	$price = $this->_getPrice();

		if(!$this->Context->isAbsorb())
			$price += $this->getFee('', true);

		return $this->_return($price, $asFloat);
    }

    /** 
     * Pega a taxa baseado no papel do item atual
     * @param 		$role 			O pappel a ser utilizado para calcular a taxa, vazio para retornar a taxa independentemente.
     * @param 		$asFloat 		O resultado deve ser retornado como float ou string?
     * @return 		mixed 			A taxa do item atual.
     */
    public function getFee($role = '', $asFloat = false)
    {
    	$fee = 0;

    	if(!(
    		$role == self::CLIENT_ROLE && $this->Context->isAbsorb() ||
    		$role == self::ORGANIZER_ROLE && !$this->Context->isAbsorb()
    	)) {
            if($this->key() === self::CONTEXT_KEY)
            {
                foreach ($this->_items as $item)
                    $fee += $this->_getFee($item);
            }
            else
                $fee = $this->_getFee($this->_items[$this->key()]);
    	}

		return $this->_return($fee, $asFloat);
    }

	public function getCurrent()
	{
		if($this->valid())
		{
			if($this->_currentKey == self::CONTEXT_KEY)
				return $this->Context;
			return $this->_items[$this->_currentKey];
		}

		return null;
	}

    public function length()
    {
        return count($this->_items);
    }

    public function reset()
    {
        $this->_currentKey = self::CONTEXT_KEY;
        return $this;
    }

	/* Iterator */

	public function current()
	{
		return $this;
	}

	public function key()
	{
		return $this->_currentKey;
	}

	public function next()
	{
		$this->_currentKey++;
	}

	public function rewind()
	{
		$this->_currentKey = 0;
	}

	public function valid()
	{
		return $this->_currentKey < $this->length();
	}

	public function saveBooked()
	{
		$booked = new Booked();
		$booked->EventId = (int)$this->Context->getId();
		$booked->Code = md5(guid() . uniqid('', true));
		$booked->SessionId = $this->getCookieId();
		$booked->Date = time();
		$booked->save();


		foreach ($this->_items as $item)
		{
			$bookedItem = new BookedItem();
			$bookedItem->BookedId = (int)$booked->Id;
			$bookedItem->ItemType = get_class($item);
			$bookedItem->ItemId = (int)$item->Id;
			$bookedItem->save();
		}
		
		return $booked;
	}
}