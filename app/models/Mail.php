<?php
class Mail
{
	private $subject = '';
	private $to = array();
	private $from;
	private $message = '';
	private $replyTo = '';
	private $html = '';
	private $attachments = array();

	public function __construct($name = 'Equipe do Eventmania', $email = 'roberta@eventmania.com.br', $replyTo = 'atendimento@eventmania.com.br')
	{
		$this->from = new stdClass();
		$this->from->from_name = $name;
		$this->from->from_email = $email;
		$this->from->replyTo = $replyTo;
	}

	public function setSubject($subject)
	{
		$this->subject = $subject;
	}

	public function setReplyTo($replyTo)
	{
		$this->replyTo = $replyTo;
	}

	public function addTo($email, $name = null)
	{
		$to = new stdClass();
		$to->name	= $name;
		$to->email	= $email;

		$this->to[] = $to;
	}
	
	public function addToAll($emails, $names = array())
	{
		$this->to = array();
		
		foreach ($emails as $i => $email)
		{
			$to = new stdClass();
			$to->email	= $email;
			if(isset($names[$i]))
				$to->name = $names[$i];
			$to->type	= 'bcc';
			$this->to[] = $to;
		}
	}

	public function setTo($email, $name = null)
	{
		$this->to = array();
		$this->addTo($email, $name);
	}

	public function setFrom($email, $name = null)
	{
		$this->from->from_name = $name;
		$this->from->from_email = $email;
	}

	public function setMessage($message)
	{
		$this->message = $message;
	}

	public function setTemplate($template, $vars = array(), $style = false)
	{
		if($style)
			$html = MailFormater::import($template, $style, $vars);
		else
			$html = Import::view($vars, $template['controller'], $template['view']);
		$this->html = $html;
	}

	public function addAttachment($name, $mime, $content)
	{
		$attachment = new stdClass();
		$attachment->name = $name;
		$attachment->type = $mime;
		$attachment->content = $content;

		$this->attachments[] = $attachment;
	}

	public function send()
	{
		$message = array();
		$message['type'] = 'messages';
		$message['call'] = 'send';
		$message['message']['subject']				= $this->subject;
		$message['message']['text']					= $this->message;
		$message['message']['html']					= $this->html;
		$message['message']['from_email']			= $this->from->from_email;
		$message['message']['from_name']			= $this->from->from_name;
		$message['message']['to']					= $this->to;
		$message['message']['attachments']			= $this->attachments;
		$message['message']['headers']['Reply-To']	= $this->replyTo;
		
		$mandrill = new Mindrill(Config::get('mandrill_key'), false);
		return $mandrill->call('/messages/send.json', $message);
	}

	public static function importRecipients($str)
	{
		$matches = array();
		preg_match_all('%([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})%', $str, $matches);
		return array_unique(array_shift($matches));
	}

	public static function broadcast($recipients, $subject, $template, $from = 'Equipe do EventMania', $fromMail = 'service@eventmania.com.br')
	{
		$mail = new Mail($from, $fromMail);
		$mail->setSubject($subject);
		$mail->addToAll($recipients);

		$mail->setTemplate($template, $template['vars'], array(
			'controller' => 'mail',
			'view' => 'style',
		));

		$errors = array();
		
		$response = $mail->send();
		
		if(is_array($response))
		{
			foreach ($response as $r)
			{
				if($r->status === 'rejected' || $r->status === 'invalid')
					$errors[] = $r->email;
			}
		}
		else
		{
			return false;
		}
		return $errors;
	}
}
