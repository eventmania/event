<?php

/**
 * Implementa as funções de formatação de preços.
 */
trait PriceTrait
{
    use ShoppingCartItemTrait;

    public function setPrice($price)
    {
        $this->Price = Format::strToDouble($price);
    }

    public function getFormatedPrice($showCurrency = true)
    {
        if($showCurrency)
            return Format::money($this->Price);
        
        return Format::number($this->Price);
    }
}