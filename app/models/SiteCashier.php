<?php

class SiteCashier implements ICashier
{
    public $Context;

    public function __construct(ICashierContext $context)
    {
        $this->Context = $context;
    }

    protected function _return($value, $asFloat)
    {
        if($asFloat)
            return $value;
        return Format::money($value);
    }

    protected function _hashSatus($status)
    {
        if(is_array($status))
            $status = implode(', ', $status);
        return md5($status);
    }

    protected $_participants = array();

    protected function _allParticipants($status)
    {
        $hash = $this->_hashSatus($status);

        if(!isset($this->_participants[$hash]))
            $this->_participants[$hash] = ViewParticipant::allByEventAndStatus($this->Context->getId(), true, $status);

        return $this->_participants[$hash];
    }

    /** pega toda a receita liquida */
    public function getNetIncome($asFloat = false)
    {
        $total = 0;
        $participants = $this->_allParticipants(array(Participant::CONFIRMED_STATUS, Participant::FINALIZED_STATUS));

        foreach ($participants as $p) 
        {
            $cart = new ShoppingCart($this->Context, $p);
            $cart->add(new Workshop($this->Context->getId(), $p->WorkshopPrice));
            $total += $cart->getNetPrice(true);
        }

        return $this->_return($total, $asFloat);
    }

    /** pega toda a receita bruta */
    public function getGrossIncome($asFloat = false)
    {
        $total = 0;
        $participants = $this->_allParticipants(array(Participant::CONFIRMED_STATUS, Participant::FINALIZED_STATUS));

        foreach ($participants as $p) 
        {
            $cart = new ShoppingCart($this->Context, $p);
            $cart->add(new Workshop($this->Context->getId(), $p->WorkshopPrice));
            $total += $cart->getSalePrice(true);
        }

        return $this->_return($total, $asFloat);
    }

    /** Pega todas as taxas */
    public function getIncomeFees($asFloat = false)
    {
        $total = 0;
        $participants = $this->_allParticipants(array(Participant::CONFIRMED_STATUS, Participant::FINALIZED_STATUS));

        foreach ($participants as $p) 
        {
            $cart = $this->Context->newCart(array($p));
            $total += $cart->getFee('', true);
        }

        return $this->_return($total, $asFloat);
    }

    private $_cachOnHand;

    /** pega o valor da receita que pode ser sacado */
    public function getCashOnHand($asFloat = false)
    {
        if(!$this->_cachOnHand)
        {
            $total = 0;
            $db = Database::factory();
            $participants = $db->ViewParticipant->where('EventId = ? AND Status = ? AND AuthorId = ?', $this->Context->getId(), Participant::FINALIZED_STATUS, Participant::SITE_AUTHOR_ID)->all();

            foreach ($participants as $p) 
            {
                $cart = new ShoppingCart($this->Context, $p);
                $cart->add(new Workshop($this->Context->getId(), $p->WorkshopPrice));
                $total += $cart->getNetPrice(true);
            }

            $this->_cachOnHand = $total - $this->getWithdrawn(true) - $this->getWithdrawnCompleted(true);
        }
        return $this->_return($this->_cachOnHand, $asFloat);
    }


    private $_remaining = null;

    /** Valor restante do OnHand */
    public function getRemaining($asFloat = false)
    {
        if(!$this->_remaining)
        {
            $total = 0;
            $db = Database::factory();
            $participants = $db->ViewParticipant->where('EventId = ? AND Status = ? AND AuthorId = ?', $this->Context->getId(), Participant::CONFIRMED_STATUS, Participant::SITE_AUTHOR_ID)->all();

            foreach ($participants as $p) 
            {
                $cart = new ShoppingCart($this->Context, $p);
                $cart->add(new Workshop($this->Context->getId(), $p->WorkshopPrice));
                $total += $cart->getNetPrice(true);
            }
            $this->_remaining = $total;
        }
        return $this->_return($this->_remaining, $asFloat);
    }

    private $_withdrawn;

    /** valor sacado do OnHand */
    public function getWithdrawn($asFloat = false)
    {
        if(!$this->_withdrawn)
        {
            $db = Database::factory();
            $this->_withdrawn = $db->Transfer
                        ->where('EventId = ? AND Status IN ' . Transfer::WAITING_STATUSES, $this->Context->getId())
                        ->sum('Value');
        }
        return $this->_return($this->_withdrawn, $asFloat);
    }

    private $_withdrawnCompleted;

    /** valor sacado do OnHand */
    public function getWithdrawnCompleted($asFloat = false)
    {
        if(!$this->_withdrawnCompleted)
        {
            $db = Database::factory();
            $this->_withdrawnCompleted = $db->Transfer
                        ->where('EventId = ? AND Status = ?', $this->Context->getId(), Transfer::FINALIZED_STATUS)
                        ->sum('Value');
        }
        return $this->_return($this->_withdrawnCompleted, $asFloat);
    }

    public function hasSales()
    {
        $db = Database::factory();
        return $db->ViewParticipant
                    ->where('EventId = ? AND AuthorId = ?', $this->Context->getId(), Participant::SITE_AUTHOR_ID)
                    ->count();
    }
}