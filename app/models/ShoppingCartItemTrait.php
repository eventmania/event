<?php

trait ShoppingCartItemTrait
{
    /** 
     * @Column(Type="Double") 
     * @Required()
     * @Label("Preço")
     */
    public $Price;

    public function getPrice()
    {
        return $this->Price;
    }
}