<?php

/** @View("view_event_manager") */
class ViewEventManager extends Event
{
	/** @Column(Type="Int") */
	public $ManagementId;

	/** @Column(Type="Int") */
	public $ManagerId;

	/** @Column(Type="Int") */
	public $Role;

    /** @Column(Type="Int") */
    public $ManagerStatus;

    public static function search($p = 1, $m = 20, $o = 'Id', $t = 'DESC', $filters = array(), $operator = 'OR', $distinct = false)
    {
        $events = parent::search($p, $m, $o, $t, $filters, $operator);

        if($distinct && $events->Count)
        {
            $listeds = array();
            $count = 0;

            foreach ($events->Data as $i => $e)
            {
                if(in_array($e->Id, $listeds))
                {
                    $count++;
                    unset($events->Data[$i]);
                }
                else
                    array_push($listeds, $e->Id);
            }

            $events->Count -= $count;
        }

        return $events;
    }
}
