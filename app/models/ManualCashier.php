<?php

class ManualCashier implements ICashier
{
    public $Context;

    public function __construct(ICashierContext $context)
    {
        $this->Context = $context;
    }

    protected function _return($value, $asFloat)
    {
        if($asFloat)
            return $value;
        return Format::money($value);
    }

    protected $_participants = null;

    protected function _allParticipants()
    {
        if(!$this->_participants)
            $this->_participants = ViewParticipant::allByEvent($this->Context->getId(), false);

        return $this->_participants;
    }

    protected $_total = null;

    /** pega toda a receita liquida */
    public function getNetIncome($asFloat = false)
    {
        if($this->_total === null)
        {
            $this->_total = 0;
            $participants = $this->_allParticipants();

            foreach ($participants as $p)
                $this->_total += $p->Price + $p->WorkshopPrice;
        }

        return $this->_return($this->_total, $asFloat);
    }

    /** pega toda a receita bruta */
    public function getGrossIncome($asFloat = false)
    {
        return $this->getNetIncome($asFloat);
    }

    /** Pega todas as taxas */
    public function getIncomeFees($asFloat = false)
    {
        return $this->_return(0, $asFloat);
    }

    /** pega o valor da receita que pode ser sacado */
    public function getCashOnHand($asFloat = false)
    {
        return $this->getNetIncome($asFloat);
    }

    /** Valor restante do OnHand */
    public function getRemaining($asFloat = false)
    {
        return $this->_return(0, $asFloat);
    }

    /** valor sacado do OnHand */
    public function getWithdrawn($asFloat = false)
    {
        return $this->getNetIncome($asFloat);
    }

    public function getManagerIncome($managerId, $asFloat = false)
    {
        $total = 0;
        $db = Database::factory();
        $participants = $db->ViewParticipant->where('EventId = ? AND AuthorId = ?', $this->Context->getId(), $managerId)->all();

        foreach ($participants as $p)
            $total += $p->Price + $p->WorkshopPrice;

        return $this->_return($total, $asFloat);
    }
}