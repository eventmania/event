<?php
/**
 * @Entity("view_event_owner")
 */
class ViewEventOwner extends Event
{
    /** @Column(Type="String") */
    public $OwnerName;

    /** @Column(Type="String") */
    public $OwnerEmail;
}