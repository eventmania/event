<?php

/** @Entity("view_workshop") */
class ViewWorkshop extends Workshop
{
    /** @Column(Type="Int") */
    public $ParticipantsAmount;

    public function getParticipantsPercentage()
    {
        return $this->Amount ? $this->ParticipantsAmount / $this->Amount * 100 : 0;
    }
}