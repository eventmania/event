<?php
/**
 * @Entity("user")
 */
class User extends BaseModel
{
	/**
	 * @AutoGenerated()
	 * @Column(Type="Int", Key="Primary")
	 */
	public $Id;

	/**
	 * @Required()
	 * @Label("Nome")
	 * @Regex(Pattern="^(.{3,45})$",Message="Seu nome deve ter entre 3 e 45 letras.")
	 * @Column(Type="String")
	 */
	public $Name;

	/**
	 * @Required()
	 * @Label("Email")
	 * @Regex(Pattern="^([a-zA-Z0-9\.\-_]+)@([a-zA-Z0-9\.\-]+)\.([a-z]{2,})$",Message="Endereço de e-mail está inválido")
	 * @Column(Type="String")
	 */
	public $Email;

	/**
	 * @Required()
	 * @Column(Type="String")
	 * @Label("Senha")
	 */
	public $Password;

	/** @Column(Type="Int") */
	public $Role = 0;

	/** @Column(Type="Int") */
	public $Status = 0;

	/** @Column(Type="Int") */
	public $CreateDate;

	/** @Column(Type="Int") */
	public $LastLoginDate;

	/**
	 * @Required()
	 * @Column(Type="Int")
	 */
	public $Newsletter;

	/** @Column(Type="String") */
	public $CodeConfirmation;

	public static $Roles = array(0 => 'user', 10 => 'admin');

	private static function _encrypt($pass)
	{
		return md5($pass);
	}

	public function setPassword($pass)
	{
		$this->Password = self::_encrypt($pass);
	}

	public function checkPassword($pass)
	{
		return self::_encrypt($pass) === $this->Password;
	}

	public function resetPassword()
	{	
		$this->CodeConfirmation = md5(uniqid('', true));
		$this->save();
		$this->sendResetPasswordMail();
	}

	public function sendConfirmationMail()
	{
		$mail = new Mail();
		$mail->setSubject('Confirmar Cadastro');
		$mail->setFrom('nao-responda@eventmania.com.br', 'EventMania');
		$mail->addTo($this->Email, $this->Name);
		$mail->setTemplate(array(
			'controller' => 'mail',
			'view' => 'confirm',
		), array(
			'name' => $this->Name,
			'link' => Request::getSite() . 'user/confirm/' . $this->getCode()
		), array(
			'controller' => 'mail',
			'view' => 'style',
		));

		return $mail->send();
	}

	public function sendWelcomeMail()
	{
		$mail = new Mail();
		$mail->setSubject('Olá da equipe do EventMania');
		$mail->setFrom('roberta@eventmania.com.br', 'Roberta');
		$mail->addTo($this->Email, $this->Name);
		$mail->setTemplate(array(
			'controller' => 'mail',
			'view' => 'welcome',
		), array(
			'name' => $this->getFirstName(),
		), array(
			'controller' => 'mail',
			'view' => 'style',
		));

		return $mail->send();
	}

	public function sendInviteMail($event, $type)
	{
		$mail = new Mail();
		$mail->setSubject('Convite para o ' . $event->Name);
		$mail->setFrom('roberta@eventmania.com.br', 'Roberta');
		$mail->addTo($this->Email, $this->Name);
		$mail->setTemplate(array(
			'controller' => 'mail',
			'view' => 'invite-' . $type,
		), array(
			'event' => $event,
			'code' => $this->CodeConfirmation,
		), array(
			'controller' => 'mail',
			'view' => 'style',
		));

		return $mail->send();
	}

	public function sendNewPasswordMail($newPassowrd)
	{
		$mail = new Mail();
		$mail->setSubject('Redefinição de Senha');
		$mail->setFrom('suporte@eventmania.com.br', 'EventMania');
		$mail->addTo($this->Email, $this->Name);
		$mail->setTemplate(array(
			'controller' => 'mail',
			'view' => 'new-password',
		), array(
			'name' => $this->getFirstName(),
			'password' => $newPassowrd,
		), array(
			'controller' => 'mail',
			'view' => 'style',
		));

		return $mail->send();
	}
	
	public function sendResetPasswordMail()
	{
		$mail = new Mail();
		$mail->setSubject('Redefinição de Senha');
		$mail->setFrom('suporte@eventmania.com.br', 'EventMania');
		$mail->addTo($this->Email, $this->Name);
		$mail->setTemplate(array(
			'controller' => 'mail',
			'view' => 'reset-password',
		), array(
			'name' => $this->getFirstName(),
			'code' => $this->CodeConfirmation,
		), array(
			'controller' => 'mail',
			'view' => 'style',
		));

		return $mail->send();
	}

	public function isActive()
	{
		return $this->Status == 1;
	}

	public static function isAvailable($email)
	{
		$db = Database::factory();
		return !$db->User->where('Email = ?', $email)->count();
	}

	public function isAdmin($eventId)
	{
		return $this->checkManagment($eventId, Manager::ROLE_ADMIN);
	}

	public function getCode()
	{
		return Security::encrypt($this->Id, Config::get('salt'));
	}

	public function getFirstName()
	{
		$partes = explode(' ', $this->Name);
		if(isset($partes[0]))
			return $partes[0];
	}

	public static function getByCode($code)
	{
		return User::get(Security::decrypt($code, Config::get('salt')));
	}

	public static function getByEmail($email)
	{
		$db = Database::factory();
		return $db->User->where('Email = ?', $email)->single();
	}

	public function getSubmissionEvents($p, $m)
	{
		$events = ViewPaper::search($p, $m, false, false, array(
			'UserEmail' => $this->Email,
			'HasPapers' => 1
		), 'AND');

		return $events;
	}

	public function getReviewEvents($p, $m)
	{
		$events = ViewEventManager::search($p, $m, false, false, array(
			'ManagerId' => $this->Id,
			'Role' => Manager::ROLE_REVIEWER
		), 'AND');

		return $events;
	}

	public function getReviews($eventId, $p = 1, $m = 20)
	{
		$events = ViewReview::search($p, $m, false, false, array(
			'UserId' => $this->Id,
			'EventId' => $eventId,
		), 'AND');

		return $events;
	}

	public function checkManagment($eventId, $role = Manager::ROLE_VIEWER)
	{
		return Manager::checkManagment($eventId, $this->Id, $role) || $this->Role >= 10;
	}

	public function getAccounts()
	{
		return Account::allByUser($this->Id);
	}

	public function blockSwitch()
	{
		if($this->Status)
			$this->Status = 0;
		else
			$this->Status = 1;

		$this->save();
	}

	public static function cast($object)
	{
		$user = new User();
		if(is_object($object) || is_array($object))
		{
			foreach ($object as $property => $value)
			{
				if(!property_exists($user, $property))
					throw new Exception('Propriedade "' . $property . '" não foi encontrada em User');
				$user->{$property} = $value;
			}
		}
		else
		{
			Auth::clear();
			$this->_redirect('~/login');
		}
		return $user;
	}
}
