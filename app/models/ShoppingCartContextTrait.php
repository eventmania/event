<?php

trait ShoppingCartContextTrait
{
    /** @Column(Type="Int") */
    public $PaymentWay;

    /** @Column(Type="Int") */
    public $Absorb;

    /** @Column(Type="Double") */
    public $MinimumFee;

    public function getPaymentWay()
    {
        return $this->PaymentWay;
    }

    public function isAbsorb()
    {
        return $this->Absorb;
    }

    public function getMinimumFee()
    {
        return $this->MinimumFee;
    }
}