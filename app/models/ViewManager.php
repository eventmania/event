<?php

/** @Entity("view_manager") */
class ViewManager extends Manager
{
    /**
     * @Column(Type="Int",Key="Primary")
     */
    public $Id;

    /** @Column(Type="String") */
    public $Name;

    /** @Column(Type="String") */
    public $Email;

    public function getManagment()
    {
        return Manager::get($this->Id);
    }
}
