<?php

/** @Entity("event") */
class Event extends BaseModel implements ICashierContext
{
	use HappeningTrait, ImageTrait, ShoppingCartContextTrait;

	/**
	 * @AutoGenerate()
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;

	/** @Column(Type="String") */
	public $Name;

	/** @Column(Type="String") */
	public $Organizer;

	/** @Column(Type="String") */
	public $OrganizerEmail;

	/** @Column(Type="Int") */
	public $Visibility;

	/** @Column(Type="Int") */
	public $CategoryId;

	/** @Column(Type="Int") */
	public $Status;

	/** @Column(Type="String") */
	public $Description;

	/** @Column(Type="String") */
	public $Twitter;

	/** @Column(Type="String") */
	public $Facebook;

	/** @Column(Type="Int") */
	public $UserId;

	/** @Column(Type="String") */
	public $Latitude;

	/** @Column(Type="String") */
	public $Longitude;

	/** @Column(Type="String") */
	public $Address;

	/** @Column(Type="Int") */
	public $MapType;

	/** @Column(Type="Int") */
	public $Hits;

	/** @Column(Type="Int") */
	public $CreateDate;

	/** @Column(Type="String") */
	public $CustomUrl;

	/** @Column(Type="Int") */
	public $FakeDeadLine;

	/** @Column(Type="Int") */
	public $DeadLine;

	/** @Column(Type="Int") */
	public $RevisionDeadLine;

	/** @Column(Type="Int") */
	public $ResubmissionDeadLine;

	/** @Column(Type="Int") */
	public $ReviewersPerPaper;

	/** @Column(Type="Int") */
	public $HasPapers;

	/** @Column(Type="String") */
	public $CallForPapers;

	/** @Column(Type="Int") */
	public $AcceptedPapers;

	/** @Column(Type="Int") */
	public $IsSinglePurchase;

	/** @Column(Type="String") */
	public $Layout;

	/** @Column(Type="String") */
	public $LayoutConfig;

	/** @Column(Type="Int") */
	public $Capacity;

	/** @Column(Type="String") */
	public $Slug;

	/** @Column(Type="Int") */
	public $SpotlightIndex;

	/** @Column(Type="String") */
	public $City;

	/** @Column(Type="String") */
	public $State;

	const DEFAULT_FEE = 9;

	const MINIMUM_FEE = 4.0;

	const BLOCKED_STATUS = -1;
	const DRAFT_STATUS = 0;
	const PUBLISHED_STATUS = 1;

	public function __construct($userId = null)
	{
		$this->UserId = (int) $userId;
		$this->PaymentWay = self::DEFAULT_FEE;
		$this->MinimumFee = self::MINIMUM_FEE;
		$this->CreateDate = time();
	}

	public function isAuthorizedManager($userId, $role = Manager::ROLE_VIEWER)
	{
		return Auth::is('admin') || Manager::checkManagment($this->Id, $userId, $role);
	}

    private $_isPaid;

    public function isPaid()
    {
        if($this->_isPaid === null)
            $this->_isPaid = Ticket::countPaidByEvent($this->Id) > 0;
        return $this->_isPaid;
    }

    public function isBlocked()
    {
    	return $this->Status === self::BLOCKED_STATUS;
    }

    public function isPublic()
    {
    	return $this->Status === self::PUBLISHED_STATUS;
    }

    public function isVisible()
    {
    	return $this->isPublic() && $this->Visibility;
    }

   	public function isValidAbsortion($item = null)
   	{
		$isValid = true;

		if($this->Absorb)
		{
			if($item)
				return !$item->Price || $item->Price >= $this->MinimumFee;

			$tickets = $this->getTickets();
			$workshops = $this->getWorkshops();
			$items = array_merge($tickets->Data, $workshops->Data);

			if($items)
				foreach ($items as $t)
				{
					$tIsValid = !$t->Price || $t->Price >= $this->MinimumFee;
					$isValid = $isValid && $tIsValid;
				}
		}

		return $isValid;
   	}

   	protected $_layout = null;

   	public function getId()
   	{
   		return $this->Id;
   	}

   	public function getLayout()
   	{
   		if(!$this->_layout)
   			$this->_layout = new Layout($this->Layout, $this->LayoutConfig);
   		return $this->_layout;
   	}

	public function getCity()
	{
		return 'Palmas - TO';
	}

	public function getSlug()
	{
		return $this->Slug;
	}

	public function getUrl()
	{
		if($this->CustomUrl)
			return $this->CustomUrl;

		if($this->Slug)
			return Request::getSite() . $this->Slug . '/';

		return '';
	}

	public function getName($length = false)
	{
		if($length && strlen($this->Name) > $length)
		{
			$name = trim(substr($this->Name, 0, $length));
			return $name . '...';
		}

		return $this->Name;
	}

	public function getAddress()
	{
		$address = preg_replace('/([\d]{5})-([\d]{3}), /', '', $this->Address);
		$address = str_replace('República Federativa do Brasil', 'Brasil', $address);
		return $address;
	}

	public function getShortDescription()
	{
		$desc = explode('.', $this->Description);
		return array_pop($desc);
	}

	public function getCurrentDeadLine()
	{
		return $this->FakeDeadLine > time() ? $this->getFakeDeadLine() : $this->getDeadLine();
	}

	public function getDeadLine()
	{
        return $this->_date($this->DeadLine ? $this->DeadLine : time());
	}

	public function getFakeDeadLine()
	{
        return $this->_date($this->FakeDeadLine ? $this->FakeDeadLine : time());
	}

	public function getRevisionDeadLine()
	{
        return $this->_date($this->RevisionDeadLine ? $this->RevisionDeadLine : time());
	}

	public function getResubmissionDeadLine()
	{
        return $this->_date($this->ResubmissionDeadLine ? $this->ResubmissionDeadLine : time());
	}

	protected $_soldTicketsPercentage = null;

	public function getSoldTicketPercentage()
	{
		if(!$this->_soldTicketsPercentage)
		{
			if($this->countTickets())
				$this->_soldTicketsPercentage = ($this->countSoldTickets() / $this->countTickets() * 100);
			else
				$this->_soldTicketsPercentage = 0;
		}
		return $this->_soldTicketsPercentage;
	}

	protected $_ticketsByDateReport = null;

	public function getTicketsByDateReport()
	{
		if(!$this->_ticketsByDateReport)
			$this->_ticketsByDateReport = ViewSoldTicketsByDate::search(1, 100, 'PurchaseYear, PurchaseMonth, PurchaseDay', null, array(
												'EventId' => $this->Id
											));
		return $this->_ticketsByDateReport;
	}

	public function getSchedule()
	{
		$start = date("dmY", $this->StartDate);
		$end = date("dmY", $this->EndDate);

		if($start === $end)
			$schedule = ucfirst(strftime("%A, %d de %B de %Y", $this->StartDate) . ', ' . date("H", $this->StartDate) . 'h - ' . date("H", $this->EndDate) . 'h');
		else
			$schedule = ucfirst(strftime("%A, %d de %B de %Y", $this->StartDate) . ' - ' . strftime("%d de %B de %Y", $this->EndDate));

		return htmlentities($schedule);
	}

	protected $_tickets = null;

	public function getTickets($p = 1, $m = 100)
	{
		if(!$this->_tickets)
		{
			$this->_tickets = Ticket::search($p, $m, null, null, array(
				'EventId' => $this->Id
			));
		}

		return $this->_tickets;
	}

	public function getTicketsAssoc()
	{
		$arr = array();
		$tickets = $this->getTickets();

		foreach ($tickets->Data as $t)
		{
			$arr[$t->Id] = $t->Name;
		}

		return $arr;
	}

	public function getWorkshopsAssoc()
	{
		$arr = array('' => 'Selecione um workshop...');
		$workshops = $this->getWorkshops();

		foreach ($workshops->Data as $t)
		{
			$arr[$t->Id] = $t->Title;
		}

		return $arr;
	}

	public function getParticipants($p = 1, $m = 25, $q = '', $o = 'Name, Id', $t = 'ASC')
	{
		return ViewParticipant::eventSearch($this->Id, $p, $m, $q, $o, $t);
	}

	public function getManagers($p = 1, $m = 25, $o = 'Name, Id', $t = 'ASC')
	{
		return ViewManager::search($p, $m, $o, $t, array(
			'EventId' => $this->Id
		));
	}

	private $_transfers;

	public function getTransfers()
	{
		if(!$this->_transfers)
			$this->_transfers = Transfer::search (1, 10, 'Id', 'ASC', array(
				'EventId' => $this->Id
			))->Data;
		return $this->_transfers;
	}

	public function getConfirmedParticipants($p = 1, $m = 25, $q = '', $o = 'Name, Id', $t = 'ASC')
	{
		return ViewParticipant::eventSearchConfirmed($this->Id, $p, $m, $q, $o, $t);
	}

	public function getCheckedParticipants($p = 1, $m = 25, $q = '', $o = 'Name, Id', $t = 'ASC')
	{
		return ViewParticipant::eventSearchChecked($this->Id, $p, $m, $q, $o, $t);
	}

	public function getParticipationsData($ticketId = false, $authorId = false, $workshopId = false, $status = false)
	{
		return ViewParticipantCustomData::search($this->Id, $ticketId, $authorId, $workshopId, $status);
	}

	public function getWorkshops($p = 1, $m = 100)
	{
		return ViewWorkshop::search($p, $m, null, null, array(
			'EventId' => $this->Id
		));
	}

	public function getPapers($p, $m)
	{
		return ViewPaper::search($p, $m, 'Title', 'ASC', array(
			'EventId' => $this->Id
		));
	}

	public function getForm()
	{
		$fields = Field::allByEvent($this->Id);
		return new CustomForm($fields);
	}

	public function getOwner()
	{
		return User::get($this->UserId);
	}

	public static function getSpotlights()
	{
		$db = Database::factory();
		return $db->Event
					->where('SpotlightIndex > ?', 0)
					->orderBy('SpotlightIndex', 'ASC')
					->all();
	}

	public static function getComing()
	{
		$db = Database::factory();
		return $db->Event
					->where('EndDate < ?', time())
					->all();
	}

	public static function getPublicComing()
	{
		$db = Database::factory();
		return $db->Event
					->where('Visibility = 1 AND EndDate < ?', time())
					->orderBy('StartDate')
					->all();
	}

	public function getGrossIncome()
	{
		$site = $this->getSiteCashier()->getGrossIncome(true);
		$manual = $this->getManualCashier()->getGrossIncome(true);
		return Format::money($site + $manual);
	}

	public function getNetIncome()
	{
		$site = $this->getSiteCashier()->getNetIncome(true);
		$manual = $this->getManualCashier()->getNetIncome(true);
		return Format::money($site + $manual);
	}

	public function getSiteCashier()
	{
		return new SiteCashier($this);
	}

	public function getManualCashier()
	{
		return new ManualCashier($this);
	}

	protected $_countParticipants = null;

	public function countParticipants()
	{
		if(!$this->_countParticipants)
		{
			$this->_countParticipants = ViewParticipant::countEventParticipants($this->Id);
		}
		return $this->_countParticipants;
	}

	public function countTickets()
	{
		return $this->Capacity;
	}

	protected $_countSoldTickets = null;

    public function countSoldTickets()
    {
        if(!$this->_countSoldTickets)
            $this->_countSoldTickets = ViewParticipant::countSoldTickets($this->Id);
        return $this->_countSoldTickets;
    }

    protected $_unpaidTickets = null;

    public function countUnpaidTickets()
    {
        if(!$this->_unpaidTickets)
            $this->_unpaidTickets = ViewParticipant::countUnpaidTickets($this->Id);
        return $this->_unpaidTickets;
    }

    protected $_canceledTickets = null;

    public function countCanceledTickets()
    {
        if(!$this->_canceledTickets)
            $this->_canceledTickets = ViewParticipant::countCanceledTickets($this->Id);
        return $this->_canceledTickets;
    }

	public function addTickets($titles, $amounts, $prices, $descriptions, $startDates, $startHours, $endDates, $endHours, $visibilities)
	{
		$tickets = array();

		foreach ($titles as $i => $t)
		{
			$ticket = $this->addTicket($t, $amounts[$i], $prices[$i], $descriptions[$i], $startDates[$i], $startHours[$i], $endDates[$i], $endHours[$i], $visibilities[$i]);
			array_push($tickets, $ticket);
		}

		return $tickets;
	}

	public function addTicket($title, $amount, $price, $description, $startDate, $startHour, $endDate, $endHour, $visibility)
	{
		if(!$this->Id)
			throw new Exception("O evento deve ser salvo antes de adicionar os tickets.", 500);

		$ticket = new Ticket($this->Id, $title, $amount, $price, $description, $startDate, $startHour, $endDate, $endHour, $visibility);
		$ticket->save();
		return $ticket;
	}

	public function addReviewers($reviewersIds)
	{
		if($reviewersIds)
		{
			foreach ($reviewersIds as $id)
			{
				$this->addManager((int) $id, Manager::ROLE_REVIEWER);
			}
		}
	}

	public function addManager($userId, $role)
	{
		if(!$this->Id)
			throw new Exception("O evento deve ser salvo antes de adicionar os gerentes.", 500);

		if(!is_integer($userId))
			$userId = $userId->Id;

		$management = new Manager($userId, $this->Id, $role);
		$management->Status = Manager::ACTIVE_STATUS;
		$management->save();
	}

	public function allPapersAndReviewers($p = 1, $m = 20, $o = 'Title', $t = 'ASC')
	{
		$papers = ViewReview::search($p, $m, $o, $t, array('EventId' => $this->Id));
		$reviewers = $this->allReviewers(Manager::ROLE_REVIEWER);

		$results = array();
		$countReviewerRevviews = array();
		if($papers->Count)
			foreach ($papers->Data as $p)
			{
				if(!isset($results[$p->PaperId]))
					$results[$p->PaperId] = Paper::getMock(array(
						'Id' => $p->PaperId,
						'File' => $p->File,
						'Title' => $p->Title,
						'Keywords' => $p->Keywords
					));

				if(!isset($results[$p->PaperId]->Reviewers))
					$results[$p->PaperId]->Reviewers = array();

				if($p->UserId && !isset($results[$p->PaperId]->Reviewers[$p->UserId]))
				{
					$countReviewerRevviews[$p->UserId] = $p->TotalReviews;
					$results[$p->PaperId]->Reviewers[$p->UserId] = User::getMock(array(
						'Id' => $p->UserId,
						'ManagmentId' => $p->ReviewerId,
						'Name' => $p->UserName,
						'Email' => $p->UserEmail,
						'CountReviews' => $p->TotalReviews,
						'IsChecked' => true
					));
				}
			}

		foreach ($results as $paper)
		{
			foreach ($reviewers as $reviewer)
			{
				if($reviewer->UserId && !isset($paper->Reviewers[$reviewer->UserId]))
				{
					$count = isset($countReviewerRevviews[$reviewer->UserId]) ? $countReviewerRevviews[$reviewer->UserId] : 0;
					$paper->Reviewers[$reviewer->UserId] = User::getMock(array(
						'Id' => $reviewer->UserId,
						'ManagmentId' => $reviewer->Id,
						'Name' => $reviewer->Name,
						'Email' => $reviewer->Email,
						'CountReviews' => $count,
						'IsChecked' => false
					));
				}
			}
		}

		return (object) array(
			'Data' => $results,
			'Count' => $papers->Count
		);
	}

	public function allParticipants()
	{
		$db = Database::factory();
		return $db->ViewParticipant->all('EventId = ?', $this->Id);
	}

	public function allManagers($role = false)
	{
		$db = Database::factory();

		if($role)
			return $db->ViewManager->all('EventId = ? AND Role = ?', $this->Id, $role);
		return $db->ViewManager->all('EventId = ?', $this->Id);
	}

	public function allReviewers()
	{
		$db = Database::factory();
		return $db->ViewManager->all('EventId = ? AND Role = ?', $this->Id, Manager::ROLE_REVIEWER);
	}

	public function allPapers()
	{
		$db = Database::factory();
		return $db->ViewPaper->all('EventId = ?', $this->Id);
	}

	public function toggle($status)
	{
		if($status === '1' && !$this->isVisible())
		{
			$this->Visibility = 1;
			$this->setStatus(1);
		}
		else if($status === '0' && $this->isVisible())
		{
			$this->setStatus(0);
		}

		$this->save();
	}

	public function setStatus($status)
	{
		if(!$this->isBlocked())
			$this->Status = intval($status);
	}

	public function setSlug($slug)
	{
		if(self::checkSlug($slug, $this->Id))
		{
			$this->Slug = $slug;
		}
		else
		{
			$this->Slug = self::slugify($slug, $this->Id);
		}
	}

	public function setAbsorb($absorb)
	{
		$absorb = intval($absorb);
		if($absorb != $this->Absorb && $this->getSiteCashier()->hasSales())
			throw new ValidationException('A política de pagamento das taxas de serviços não pode mais ser alterada, pois já foram realizadas vendas.');

		$this->Absorb = $absorb;

		if(!$this->isValidAbsortion())
			throw new ValidationException('Para você pagar as taxas do serviço todos os ingressos devem possuir valor superior a '. Format::money($this->MinimumFee) .'.');

		$this->save();
	}

    public function setFakeDeadLine($fakeDeadLine)
    {
    	if($fakeDeadLine)
        	$this->FakeDeadLine = Format::strToTimestamp($fakeDeadLine . ' 23:59:59', 'd/m/Y H:i:s');
    }

    public function setDeadLine($deadLine)
    {
    	if($deadLine)
        	$this->DeadLine = Format::strToTimestamp($deadLine . ' 23:59:59', 'd/m/Y H:i:s');
    }

    public function setResubmissionDeadLine($deadLine)
    {
    	if($deadLine)
        	$this->ResubmissionDeadLine = Format::strToTimestamp($deadLine . ' 23:59:59', 'd/m/Y H:i:s');
    }

    public function setRevisionDeadLine($deadLine)
    {
    	if($deadLine)
        	$this->RevisionDeadLine = Format::strToTimestamp($deadLine . ' 23:59:59', 'd/m/Y H:i:s');
    }

	public static function setSpotlights($spotligths)
	{
		if ($spotligths)
		{
			$olds = Event::getSpotlights();

			foreach ($olds as $old)
			{
				$old->SpotlightIndex = 0;
				$old->save();
			}

			foreach ($spotligths as $index => $id)
			{
				if($id)
				{
					$new = Event::get($id);
					$new->SpotlightIndex = $index;
					$new->save();
				}
				else
				{
					$unset = Event::single(array(
						'SpotlightIndex' => $index,
					));

					if($unset)
					{
						$unset->SpotlightIndex = 0;
						$unset->save();
					}
				}
			}
		}
	}

	public static function checkSlug($slug, $id = 0)
	{
		$db = Database::factory();
		$event = $db->Event->where('Slug = ?', $slug)->single();
		return !$event || $event->Id === $id;
	}

	public function checkDraftSubscription()
	{
        if($this->isPublic())
        {
            Subscription::unsubscribe(EventController::EVENT_DRAFT_SUBSCRITION, $this->Id, 'Event');
        }
	}

	public function checkDeadLine()
	{
		return time() <= $this->DeadLine;
	}

	public function checkResubmissionDeadLine()
	{
		return time() <= $this->ResubmissionDeadLine;
	}

	public static function slugify($str, $id = 0)
	{
		$slug = Format::slugify($str);

		while (!self::checkSlug($slug, $id))
		{
			$slug .= strval(rand(0, 9));
		}

		return $slug;
	}

	public function blockSwitch()
	{
		$subject = $view = null;

		if($this->isBlocked())
		{
			$subject = 'Desbloqueio de Evento.';
			$view = 'unblock-event';
			$this->Status = self::DRAFT_STATUS;
		}
		else
		{
			$subject = 'Bloqueio de Evento.';
			$view = 'block-event';
			$this->Status = self::BLOCKED_STATUS;
		}

		$owner = $this->getOwner();

		$mail = new Mail();
		$mail->setFrom('suporte@eventmania.com.br', 'EventMania');
		$mail->addTo($owner->Email, $owner->Name);
		$mail->setSubject($subject);
		$mail->setTemplate(array(
			'controller' => 'mail',
			'view' => $view,
		), array(
			'name' => $owner->getFirstName(),
			'event' => $this
		), array(
			'controller' => 'mail',
			'view' => 'style',
		));

		$this->save();
		$mail->send();
	}

	public function save()
	{
		$this->LayoutConfig = $this->getLayout()->toJson();
		parent::save();
	}

	protected $_tmpCart = null;

	public function newCart(array $items)
	{
		$this->_tmpCart = new ShoppingCart($this);

		foreach ($items as $i)
		{
			$this->_tmpCart->add($i);

			if(is_a($i, 'ViewParticipant') && $i->WorkshopId)
				$this->_tmpCart->add(new Workshop($this->Id, $i->WorkshopPrice, $i->WorkshopName));
		}

		return $this->_tmpCart;
	}

	public function lastCart()
	{
		return $this->_tmpCart;
	}

	public function raffleReviewers()
	{
		$papers = $this->allPapers();

		$reviewers = $this->allManagers(Manager::ROLE_REVIEWER);
		$count = count($reviewers);
		$bound = $count - 1;

		if($this->ReviewersPerPaper > $count)
			throw new ValidationException('Você definiu mais revisores por artigo do que estão cadastrados, adicione mais ' . ($this->ReviewersPerPaper - $count) . ' revisor(es).');

		foreach ($papers as $paper)
		{
			$used = array();
			for ($i = $paper->countReviewers(); $i < $this->ReviewersPerPaper; $i++)
			{
				$rand = rand(0, $bound);
				while (array_search($rand, $used) !== false || Review::single(array(
					'ReviewerId' => $reviewers[$rand]->Id,
					'PaperId' => $paper->Id
				)))
				{
					$rand = rand(0, $bound);
				}
				array_push($used, $rand);

				$reviewer = $reviewers[$rand];
				$review = new Review($paper->Id, $reviewer->Id);
				$review->save();
			}
		}
	}

	public function checkReviewers($reviewersIds)
	{
		$db = Database::factory();
		$reviewers = $db->Manager->all('EventId = ? AND Role = ?', $this->Id, Manager::ROLE_REVIEWER);
		if($reviewersIds)
		{
			foreach ($reviewers as $r)
			{
				if(array_search($r->Id, $reviewersIds) === false)
				{
					$r->Status = Manager::BLOCKED_STATUS;
					$r->save();
				}
			}
		}
		else
		{
			foreach ($reviewers as $r)
			{
				$r->Status = Manager::BLOCKED_STATUS;
				$r->save();
			}
		}
	}

	public function clearReviews()
	{
		Review::clear($this->Id);
	}

	public function notifyReviewers()
	{
		$reviewers = $this->allManagers(Manager::ROLE_REVIEWER);

		if(!$reviewers)
			throw new ValidationException('Você ainda não cadastrou nenhum revisor.');

        $mail = new Mail();
        $mail->setFrom('avisos@eventmania.com.br', 'EventMania');
        $mail->setSubject('Notificação de Avaliação de Artigo');
        $mail->setTemplate(array(
            'controller' => 'mail',
            'view' => 'reviewer-notification',
        ), array(
            'event' => $this
        ), array(
            'controller' => 'mail',
            'view' => 'style',
        ));

		foreach ($reviewers as $r)
		{
        	$mail->setTo($r->Email, $r->Name);
        	$mail->send();
		}
	}

	public function sendPapersByMail($user)
	{
		$papers = ViewPaper::allByEvent($this->Id);

		$mail = new Mail();
        $mail->setFrom('avisos@eventmania.com.br', 'EventMania');
        $mail->setSubject('Artigos para Download');
    	$mail->setTo($user->Email, $user->Name);
        $mail->setTemplate(array(
            'controller' => 'mail',
            'view' => 'papers',
        ), array(
            'event' => $this,
            'papers' => $papers,
            'name' => $user->getFirstName()
        ), array(
            'controller' => 'mail',
            'view' => 'style',
        ));

    	return $mail->send();
	}

	public function sendReviews()
	{
		$papers = ViewReview::allByEvent($this->Id, $this->AcceptedPapers);

		$mail = new Mail();
        $mail->setFrom('avisos@eventmania.com.br', 'EventMania');
        $mail->setSubject('Resultado de Submissão de Artigo');

		foreach ($papers as $p)
		{
			$tmpPaper = Paper::get($p->PaperId);
			$tmpPaper->Decision = (int) $p->Decision;
			$tmpPaper->NeedRevision = (int) ($this->ResubmissionDeadLine > time());
			$tmpPaper->save();

	    	$mail->setTo($p->ParticipantEmail, $p->ParticipantName);
	        $mail->setTemplate(array(
	            'controller' => 'mail',
	            'view' => $p->Decision ? 'paper-accepted' : 'paper-refused',
	        ), array(
	            'event' => $this,
	            'paper' => $p,
	            'recommendations' => $p->Recommendations,
	            'needRevision' => $tmpPaper->NeedRevision
	        ), array(
	            'controller' => 'mail',
	            'view' => 'style',
	        ));
	        $mail->send();
		}
	}
}
