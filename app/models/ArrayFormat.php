<?php

class ArrayFormat
{
    public static $keyProp;

    public static $valueProp;

    /**
     * Mapeia as chaves de um array de forma que todas as chaves sejam dadas pelo valor armazenado no subarray (ou objeto) na chave indicada em $keyprop.
     * @param   array   $array      Array a ser mapeado.
     * @param   string  $keyProp    Propriedade de onde serão tiradas as novas chaves.
     * @param   string  $valueProp  Se especificado os valores originais dos subarrays serão sobrepostos pelo valor da propriedade especificada.
     * @return  array   O array mapeado.
     */
    public static function map($array, $keyProp, $valueProp = null)
    {
        self::$keyProp = $keyProp;
        self::$valueProp = $valueProp;

        return array_reduce($array, function(&$result, $item){
            $itemArray = (array) $item;

            if(!(is_string($itemArray[ArrayFormat::$keyProp]) || is_numeric($itemArray[ArrayFormat::$keyProp])))
            {
                throw new ArrayFormatException('Definição de propriedade inválida para mapear array.', 500);
            }

            if(ArrayFormat::$valueProp)
            {
                $result[$itemArray[ArrayFormat::$keyProp]] = $itemArray[ArrayFormat::$valueProp];
            }
            else
            {
                $result[$itemArray[ArrayFormat::$keyProp]] = $item;
            }

            return $result;
        });
    }

    /**
     * Retorna um array contendo uma propriedade especifica dos subarrays (ou objetos).
     * @param   array   $array      O array a ser mapeado.
     * @param   string  $keyProp    A propriedade dos subarrays que será retornada.
     * @return  array   O array mapeado.
     */
    public static function selectedProperty($array, $keyProp)
    {
        self::$keyProp = $keyProp;

        return array_reduce($array, function(&$result, $item){
            $item = (array) $item;
            $result[] = $item[ArrayFormat::$keyProp];
            return $result;
        });
    }
}