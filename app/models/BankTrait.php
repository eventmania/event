<?php

/**
 * Implementa as funções de conta bancárias.
 */
trait BankTrait
{
    private $_banks = array(
		'001' => 'Banco do Brasil',
		'237' => 'Bradesco',
		'104' => 'Caixa Econômica',
		'341' => 'Itaú',
		'033' => 'Santander',
		Account::PAYMENT_TOOL => Account::PAYMENT_TOOL,
	);
	
	private $_accountTypes = array(
		0 => 'Conta Corrente',
		1 => 'Conta Poupança',
	);
	
	private $_accountTypesMin = array(
		0 => 'CC',
		1 => 'CP',
	);
	
	public function getBanks()
	{
		return $this->_banks;
	}
	
	public function getBank()
	{
		return $this->_banks[$this->Bank];
	}
	
	public function isBank()
	{
		return $this->Bank != Account::PAYMENT_TOOL;
	}
	
	public function getDetails()
	{
		$details = '';
		if(!$this->isBank())
			$details = 'E-mail: ' . $this->Agency;
		else
			$details = 'Agência: ' . $this->Agency . ' | Conta: ' . $this->Account . ' | Favorecido: ' . $this->Name;
		return $details;
	}

	public function getAccountTypes()
	{
		return $this->_accountTypes;
	}
	
	public function getAccountType()
	{
		return $this->_accountTypes[$this->AccountType];
	}
	
	public function getAccountTypeMin()
	{
		return $this->_accountTypesMin[$this->AccountType];
	}
}