<?php

/** @Entity("manager") */
class Manager extends BaseModel
{
	/**
	 * @AutoGenerate()
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;

	/** @Column(Type="Int") */
	public $UserId;

	/** @Column(Type="Int") */
	public $EventId;

	/** @Column(Type="Int") */
	public $Role;

	/** @Column(Type="Int") */
	public $Status;

	/* Regras possíveis */
	const ROLE_REVIEWER = 100;
	const ROLE_VIEWER = 200;
	const ROLE_EXCHANGER = 300;
	const ROLE_ADDER = 400;
	const ROLE_ADMIN = 500;
	const ROLE_OWNER = 600;

	protected static $_roles = array(
		self::ROLE_REVIEWER => 'Avaliador de Artigo',
		self::ROLE_VIEWER => 'Vizualizador',
		self::ROLE_EXCHANGER => 'Trocador',
		self::ROLE_ADDER => 'Vendedor',
		self::ROLE_ADMIN => 'Administrador',
		self::ROLE_OWNER => 'Criador/Administrador',
	);

	const ACTIVE_STATUS = 1;
	const BLOCKED_STATUS = 0;
	const WAITING_STATUS = 3;

	public static $_status = array(
		self::ACTIVE_STATUS => 'Ativo',
		self::BLOCKED_STATUS => 'Bloqueado',
		self::WAITING_STATUS => 'Aguardando Confirmação',
	);

	public static $_statusLabels = array(
		self::ACTIVE_STATUS => 'label-success',
		self::BLOCKED_STATUS => 'label-important',
		self::WAITING_STATUS => '',
	);

	public function __construct($userId = 0, $eventId = 0, $role = 0)
	{
		$this->UserId = (int) $userId;
		$this->EventId = (int) $eventId;
		$this->Role = (int) $role;
	}

	public static function checkManagment($eventId, $userId, $role = self::ROLE_VIEWER)
	{
		$db = Database::factory();
		return $db->Manager
					->where('EventId = ? AND UserId = ? AND Role >= ?', $eventId, $userId, $role)
					->count() > 0;
	}

	public function getRole()
	{
		return self::$_roles[$this->Role];
	}

	public static function getRoles()
	{
		$roles = self::$_roles;
		unset($roles[self::ROLE_REVIEWER]);
		unset($roles[self::ROLE_OWNER]);
		return $roles;
	}

	public function getEvent()
	{
		return Event::get($this->EventId);
	}

	public function setRole($role)
	{
		$this->Role = intval($role);
	}

	public static function getManagement($eventId, $userId)
	{
		$db = Database::factory();
		return $db->Manager->where('EventId = ? AND UserId = ?', $eventId, $userId)->single();
	}

	public static function exists($eventId, $userId)
	{
		$db = Database::factory();
		return $db->Manager->where('EventId = ? AND UserId = ?', $eventId, $userId)->count() == 1;
	}

	public function isOwner()
	{
		return $this->Role == self::ROLE_OWNER;
	}

	public function isActive()
	{
		return $this->Status == self::ACTIVE_STATUS;
	}

	public function getStatus()
	{
		return self::$_status[$this->Status];
	}

	public function getStatusLabel()
	{
		return self::$_statusLabels[$this->Status];
	}

    public function countReviews()
    {
    	if($this->Role !== self::ROLE_REVIEWER)
    		throw new Exception("This manager is not reviewer", 500);

        $db = Database::factory();
        return $db->Review
            ->where('ReviewerId = ?', $this->Id)
            ->count();
    }

	public function blockSwitch()
	{
		if($this->Status)
			$this->Status = 0;
		else
			$this->Status = 1;

		$this->save();
	}
}
