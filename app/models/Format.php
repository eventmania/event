<?php

class Format
{
	public static function number($number)
	{
		$currency = Config::get('currency');
		return number_format($number, $currency['decimals'], $currency['dec_separator'], $currency['thousand_separator']);
	}

	public static function money($number)
	{
		$currency = Config::get('currency');
		return $currency['currency'] . ' ' . self::number($number);
	}

	public static function moneyToDouble($money)
	{
		$currency = Config::get('currency');
		$string = ltrim($money, $currency['currency'] . ' ');
		return (double)str_replace(array('.', ','), array('', '.'), $string);
	}

	public static function date($time)
	{
		return date('d/m/Y', $time);
	}

	public static function datetime($time)
	{
		return date('d/m/Y H:i', $time);
	}

	public static function strToTimestamp($str, $format = 'd/m/Y H:i')
	{
		if(preg_match('@H:i$@', $format) && !preg_match('@([0-9]{2}):([0-9]{2})$@', $str))
			$str .= ' 00:00';

		$date = DateTime::createFromFormat($format, $str);
		return $date->getTimestamp();
	}

	public static function strToDouble($str)
	{
		return (double) preg_replace('@,([0-9]{0,2})$@', '.$1', $str);
	}

	public static function strToNumber($str)
	{
		return preg_replace('/\D/', '', $str);
	}

	//It was taken from symfony's jobeet tutorial.
	public static function slugify($text, $lowercase = true)
	{
		// replace non letter or digits by -
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);

		// trim
		$text = trim($text, '-');

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// lowercase
		if($lowercase)
			$text = strtolower($text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		if (empty($text))
		{
			return 'n-a';
		}

		return $text;
	}

	/**
	 * Converte status de texto, vindo do Akatus, para número conforme o EventMania
	 */
	public static function akatusToEventMania($status)
	{
		$statuses = array(
			'Processando' => Participant::PROCCESSING_STATUS,
			'Aguardando Pagamento' => Participant::WAITING_STATUS,
			'Em Análise' => Participant::ANALYSIS_STATUS,
			'Aprovado' => Participant::CONFIRMED_STATUS,
			'Completo' => Participant::FINALIZED_STATUS,
			'Cancelado' => Participant::CANCELED_STATUS,
			'Devolvido' => Participant::RETURNED_STATUS,
			'Estornado' => Participant::REVERSED_STATUS,
			'Chargeback' => Participant::CHARGEBACK_STATUS,
			'Divergente' => Participant::DIVERGENT_STATUS,
			'Em disputa' => Participant::CONTEST_STATUS,
		);
		return $statuses[$status];
	}

	/**
	 * Converte os status que estão em texto em array. Por exemplo (1, 2, 3) viram array(1, 2, 3)
	 */
	public static function statusToArray($statuses)
	{
		$matches = array();
		if(preg_match_all('/[\d]+/', $statuses, $matches));
			return array_shift($matches);
		return array();
	}

	public static function stripTags($str)
	{
		return strip_tags($str, '<a><span><address><em><strong><b><i><big><small><sub><sup><cite><code><img><ul><ol><li><dl><lh><dt><dd><br><p><table><th><td><tr><pre><blockquote><nowiki><h1><h2><h3><h4><h5><h6><hr>');
	}
	
	public static function post($key, $default = null)
	{
		$posts = $default;
		if(isset($_POST[$key]))
			$posts = self::_removerTags($_POST[$key]);
		return $posts;
	}
	
	public static function get($key, $default = null)
	{
		$posts = $default;
		if(isset($_GET[$key]))
			$posts = self::_removerTags($_GET[$key]);
		return $posts;
	}
	
	private static function _removerTags($posts)
	{
		if(is_array($posts))
		{
			foreach ($posts as $key => $post)
				$posts[$key] = self::_removerTags($post);
		}
		else
		{
			$posts = strip_tags($posts);
		}
		return $posts;
	}

	public static function capitalize($text)
	{
		return ucwords(strtolower($text));
	}
}
