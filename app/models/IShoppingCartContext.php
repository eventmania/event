<?php

interface IShoppingCartContext
{
    public function getPaymentWay();

    public function isAbsorb();

    public function getMinimumFee();
}