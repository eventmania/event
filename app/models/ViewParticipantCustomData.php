<?php 
/**
 * @Entity("view_participant_custom_data")
 */
class ViewParticipantCustomData extends Participant
{
    /**
     * @Column(Type="Int", Key="Primary")
     */
    public $Id;

    /**
     * @Column(Type="String")
     */
    public $TicketName;

    /** 
     * @Column(Type="String") 
     * @Filter("capitalize")
     */
    public $AuthorName;

    /**
     * @Column(Type="String")
     */
    public $Workshop;

    /**
     * @Column(Type="String")
     */
    public $Fields;

    /**
     * @Column(Type="String")
     */
    public $FieldsIds;

    /**
     * @Column(Type="String")
     */
    public $Values;

    /**
     * @Column(Type="Int")
     */
    public $EventId;

    public static function search($eventId, $ticketId = false, $authorId = false, $workshopId = false, $status = false)
    {
        $query = array();
        $params = array();

        array_push($query, 'EventId = ?');
        array_push($params, $eventId);
        
        if($ticketId)
        {
            array_push($query, 'TicketId = ?');
            array_push($params, $ticketId);
        }

        if($authorId)
        {
            array_push($query, 'AuthorId = ?');
            array_push($params, $authorId);
        }

        if($workshopId)
        {
            array_push($query, 'WorkshopId = ?');
            array_push($params, $workshopId);
        }

        if($status)
        {
            array_push($query, 'Status = ?');
            array_push($params, $status);
        }

        $db = Database::factory();
        $rs = $db->ViewParticipantCustomData->whereArray(implode(' AND ', $query), $params)->orderBy('Name, Id')->all();

        return self::_pack($rs);
    }

    protected static function _pack($participants)
    {
        $bag = array();
        $separator = '>>';

        if($participants)
            foreach ($participants as $p) 
            {
                $fieldId = explode($separator, $p->FieldsIds);
                $field = explode($separator, $p->Fields);
                $data = explode($separator, $p->Values);
                $count = count($field);

                $fieldId = array_pad($fieldId, $count, '');
                $data = array_pad($data, $count, '');

                $custom = array();
                for ($i = 0; $i < $count; $i++) 
                { 
                    $custom[$fieldId[$i]] = array(
                        'Title' => $field[$i],
                        'Value' => $data[$i],
                    );
                }

                $default = array();
                $default['Number'] = array(
                    'Title' => 'Cod. Ingresso',
                    'Value' => $p->getNumber(),
                );
                $default['TicketName'] = array(
                    'Title' => 'Ingresso',
                    'Value' => $p->TicketName,
                );
                $default['AuthorName'] = array(
                    'Title' => 'Vendedor',
                    'Value' => $p->AuthorName,
                );
                $default['Name'] = array(
                    'Title' => 'Nome',
                    'Value' => $p->Name,
                );
                $default['Email'] = array(
                    'Title' => 'Email',
                    'Value' => $p->Email,
                );
                $default['Date'] = array(
                    'Title' => 'Data',
                    'Value' => $p->getDate(),
                );
                $default['CheckInDate'] = array(
                    'Title' => 'Check-In',
                    'Value' => $p->getCheckInDate(),
                );
                $default['Status'] = array(
                    'Title' => 'Status',
                    'Value' => $p->getStatus(),
                );
                $default['Workshop'] = array(
                    'Title' => 'Workshop',
                    'Value' => $p->Workshop,
                );

                array_push($bag, $default + $custom);
            }

        return $bag;
    }
}