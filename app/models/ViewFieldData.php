<?php
/**
 * @View("view_field_data")
 */
class ViewFieldData extends Field
{
    /**
     * @Column(Type="Int")
     */
    public $FieldId;

    /**
     * @Column(Type="Int")
     */
    public $ParticipantId;

    /**
     * @Column(Type="String")
     */
    public $Value;
}
