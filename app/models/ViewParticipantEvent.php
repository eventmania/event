<?php

/** @View("view_participant_event") */
class ViewParticipantEvent extends Event
{
	/** @Column(Type="Int") */
	public $ParticipantId;

	/** @Column(Type="Int") */
	public $TicketId;
}