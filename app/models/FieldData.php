<?php
/**
 * @Entity("field_data")
 */
class FieldData extends BaseModel
{
    /**
     * @Column(Type="Int", Key="Primary")
     */
    public $FieldId;

    /**
     * @Column(Type="Int", Key="Primary")
     */
    public $ParticipantId;

    /**
     * @Column(Type="String")
     */
    public $Value;

	public function save()
	{
		$db = Database::factory();

        if($this->_isNew())
		  $db->FieldData->insert($this);
        else
            $db->FieldData->update($this);

		$db->save();
	}
}
