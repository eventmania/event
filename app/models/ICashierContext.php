<?php

interface ICashierContext extends IShoppingCartContext
{
    public function getId();
}