<?php

class BaseModel extends Model
{
    public static function single(array $filters)
    {
        $db = Database::factory();
        $query = '';
        $conditions = array();

        foreach ($filters as $k => $f)
        {
            $filter = '';

            if(!is_array($f))
                if(strpos($f, '%') === false)
                    $filter = array($k, '=', '?');
                else
                    $filter = array($k, 'LIKE', '?');

            $query .= ' AND ' . implode(' ', $filter);
            array_push($conditions, $f);
        }

        $query = trim($query, ' AND ');

        $clazz = get_called_class();
        return $db->$clazz->whereArray($query, $conditions)->single();
    }

    public static function getMock(array $data)
    {
        $clazz = get_called_class();
        $mock = new $clazz();

        foreach ($data as $key => $value)
            $mock->$key = $value;

        return $mock;
    }
}
