<?php

/** @Entity("booked_item") */
class BookedItem extends BaseModel
{
	/**
	 * @AutoGenerate()
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;
	
	/** 
	 * @Column(Type="Int")
	 * @Required()
	 */
	public $BookedId;

	/** 
	 * @Column(Type="String")
	 * @Required()
	 */
	public $ItemType;

	/** 
	 * @Column(Type="Int")
	 * @Required()
	 */
	public $ItemId;

	public static function all($id)
	{
		$db = Database::factory();
		return $db->BookedItem->where('BookedId = ?', $id)->all();
	}
}