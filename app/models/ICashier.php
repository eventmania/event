<?php

interface ICashier
{
    public function __construct(ICashierContext $context);

    /** pega toda a receita liquida */
    public function getNetIncome($asFloat);

    /** pega toda a receita bruta */
    public function getGrossIncome($asFloat);

    /** Pega todas as taxas */
    public function getIncomeFees($asFloat);

    /** pega o valor da receita que pode ser sacado */
    public function getCashOnHand($asFloat);

    /** Valor restante do OnHand */
    public function getRemaining($asFloat);

    /** valor sacado do OnHand */
    public function getWithdrawn($asFloat);
}