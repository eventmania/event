<?php 
/**
 * @View("view_transfer")
 */
class ViewTransfer extends Transfer
{
	/** @Column(Type="Int") */
	public $EventId;
	
	/** @Column(Type="String") */
	public $EventName;
	
	/** @Column(Type="Int") */
    public $EventStartDate;
	
	/** @Column(Type="Int") */
    public $EventEndDate;
	
	/** @Column(Type="String") */
    public $UserName;
	
	/** @Column(Type="String") */
	public $UserEmail;
}