<?php

interface IShoppingCartItem
{
    public function getPrice();
    
    public function getName();
}