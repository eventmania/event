<?php

/** @Entity("participant") */
class Participant extends BaseModel {

	/**
	 * @AutoGenerate()
	 * @Column(Type="Int",Key="Primary")
	 */
	public $Id;

	/** @Column(Type="Int") */
	public $TicketId;

	/** 
	 * @Column(Type="String") 
	 * @Filter("capitalize")
	 */
	public $Name;

	/** 
	 * @Column(Type="String") 
	 * @Filter("strtolower")
	 */
	public $Email;

	/** @Column(Type="Int") */
	public $Date;

	/** @Column(Type="Int") */
	public $CheckIn;

	/** @Column(Type="Int") */
	public $CheckInDate;

	/** @Column(Type="Int") */
	public $Status;

	/** @Column(Type="Int") */
	public $AuthorId;

	/** @Column(Type="Int") */
	public $WorkshopId;

	/** @Column(Type="String") */
	public $TransactionCode;

	/** @Column(Type="String") */
	public $PaymentMethod;

	/** @Column(Type="Int") */
	public $InvitationStatus;

	const SOLD_STATUSES = '(3, 4)';
	const UNPAID_STATUSES = '(1)';
	const CANCELED_STATUSES = '(6, 7, 8, 9, 12, 13, 14)';
	const ALL_STATUSES = '(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14)';

	const UNFINISHED_STATUS = 0;
	const WAITING_STATUS = 1;
	const ANALYSIS_STATUS = 2;
	const CONFIRMED_STATUS = 3;
	const FINALIZED_STATUS = 4;
	const CONTEST_STATUS = 5;
	const RETURNED_STATUS = 6;
	const CANCELED_STATUS = 7;
	const ORG_CANCELED_STATUS = 8;
	const ADMIN_CANCELED_STATUS = 9;
	const ORG_ANALYSIS_STATUS = 10;

	const PROCCESSING_STATUS = 11;
	const REVERSED_STATUS = 12;
	const CHARGEBACK_STATUS = 13;
	const DIVERGENT_STATUS = 14;

	const SITE_AUTHOR_ID = 0;

	const INVITATION_DAYS_REMAINING = 3;

	const INVITATION_SEND = 1;
	const INVITATION_FINALIZED = 2;

	public static $_status = array(
		self::UNFINISHED_STATUS => 'Não Finalizada',
		self::PROCCESSING_STATUS => 'Processando',
		self::WAITING_STATUS => 'Aguardando Pagamento',
		self::ANALYSIS_STATUS => 'Em Análise',
		self::CONFIRMED_STATUS => 'Confirmado',
		self::FINALIZED_STATUS => 'Finalizado', //saque disponível
		self::CONTEST_STATUS => 'Em Disputa',
		self::RETURNED_STATUS => 'Devolvido',
		self::CANCELED_STATUS => 'Cancelado',
		self::ORG_CANCELED_STATUS => 'Cancelado p/ Organização',
		self::ADMIN_CANCELED_STATUS => 'Cancelado p/ EventMania',
		self::ORG_ANALYSIS_STATUS => 'Em análise p/ Organização',
		self::REVERSED_STATUS => 'Estornado',
		self::CHARGEBACK_STATUS => 'Chargeback',
		self::DIVERGENT_STATUS => 'Divergente',
	);

	public static $_statusLabels = array(
		self::UNFINISHED_STATUS => '',
		self::PROCCESSING_STATUS => '',
		self::WAITING_STATUS => '',
		self::ANALYSIS_STATUS => 'label-warning',
		self::CONFIRMED_STATUS => 'label-success',
		self::FINALIZED_STATUS => 'label-success', //saque disponível
		self::CONTEST_STATUS => 'label-important',
		self::RETURNED_STATUS => 'label-important',
		self::CANCELED_STATUS => 'label-important',
		self::ORG_CANCELED_STATUS => 'label-important',
		self::ADMIN_CANCELED_STATUS => 'label-important',
		self::ORG_ANALYSIS_STATUS => 'label-important',
		self::REVERSED_STATUS => 'label-important',
		self::CHARGEBACK_STATUS => 'label-important',
		self::DIVERGENT_STATUS => 'label-important',
	);

	public function __construct($userId = null, $status = 3)
	{
		$this->Date = time();
		$this->CheckIn = 0;
		$this->AuthorId = $userId;
		$this->Status = $status;
	}

	public static function getStatuses()
	{
		return self::$_status;
	}

	public function getStatus()
	{
		return self::$_status[$this->Status];
	}

	public function getStatusLabel()
	{
		return self::$_statusLabels[$this->Status];
	}

	public function isConfirmed()
	{
		return in_array($this->Status, Format::statusToArray(self::SOLD_STATUSES));
	}

	public function isCanceled()
	{
		return in_array($this->Status, Format::statusToArray(self::CANCELED_STATUSES));
	}

	private $_event = null;

	public function getEvent()
	{
		if(!$this->_event)
		{
			$db = Database::factory();
			$this->_event = $db->ViewParticipantEvent->single('ParticipantId = ?', $this->Id);
		}

		return $this->_event;
	}

	public function getNumber()
	{
		$id = base_convert($this->Id, 10, 35);
		$date = base_convert($this->Date, 10, 35);
		$number = $id . $date;
		return strtoupper($number);
	}

	public function getDate()
	{
		if($this->Date)
			return Format::datetime($this->Date);
	}

	public function getCheckInDate()
	{
		if($this->CheckInDate)
			return Format::datetime($this->CheckInDate);
	}

	public static function getByNumber($number)
	{
		$number = strtoupper($number);
		$date = base_convert(substr($number, -6), 35, 10);
		$id = base_convert(substr($number, 0, -6), 35, 10);
		$participant = self::get($id);
		if($participant && $participant->Date == $date)
			return $participant;
	}

	public function getCustomData()
	{
		$data = ViewFieldData::search(1, 100, false, false, array('ParticipantId' => $this->Id));
		return $data->Data;
	}

	public function addFieldsData(array $fields)
	{
		foreach ($fields as $id => $value)
			$this->addFieldData($id, $value);
	}

	public function addFieldData($id, $value)
	{
		$field = new FieldData();
		$field->FieldId = (int)$id;
		$field->ParticipantId = (int)$this->Id;
		$field->Value = $value;
		$field->save();
	}

	public function editFieldsData(array $fields)
	{
		foreach ($fields as $id => $value)
			$this->editFieldData($id, $value);
	}

	public function editFieldData($id, $value)
	{
		$field = FieldData::single(array(
			'ParticipantId' => $this->Id,
			'FieldId' => $id
		));

		if(!$field)
			return $this->addFieldData($id, $value);

		$field->Value = $value;
		$field->save();
	}

	public function checkFieldsData(array $fields)
	{
		foreach ($fields as $id => $value)
			$this->checkFieldData($id, $value);
	}

	public function checkFieldData($id, $value)
	{
		$field = Field::get($id);
		if(!$field)
			throw new BadRequestException('Requisição mal feita.');

		if($field->IsRequired == 1 && empty($value))
			throw new ValidationException('O campo "' . $field->Title . '" é obrigatório.');
	}

	public function getFirstName()
	{
		$partes = explode(' ', $this->Name);
		if(isset($partes[0]))
			return $partes[0];
	}

	public function invite($message = '')
	{
		$mail = new Mail();
		$mail->setSubject('Convite para o ' . $this->getEvent()->Name);
		$mail->setFrom('avisos@eventmania.com.br', 'EventMania');
		$mail->addTo($this->Email, $this->Name);
		$mail->setTemplate(array(
			'controller' => 'mail',
			'view' => 'invitation',
		), array(
			'model' => $this,
			'event' => $this->getEvent(),
			'message' => $message
		), array(
			'controller' => 'mail',
			'view' => 'style',
		));

		return $mail->send();
	}
}
