<?php
/**
 * Tipo do drive do banco de dados, pode assumir os seguintes valores: mysql
 */
Config::set('database', array(
    'default' => array(
        'type' => 'mysql',
        'host' => 'localhost',
        'name' => 'event',
        'user' => 'root',
        'pass' => '',
        'validate' => true
    )
));

Config::set('mandrill_key', '');

Config::set('facebook_appid', '');
Config::set('facebook_secret', '');

Config::set('enviroment', 'production'); // development ou production

Config::set('akatus_email', '');
Config::set('akatus_key', '');
Config::set('akatus_nip', '');
Config::set('akatus_public_token', '');

session_set_cookie_params(Config::get('session_time') * 60, '/', '.eventmania.com.br', true, true);
