SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `account` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `Type` int(11) NOT NULL COMMENT '0 - MoIP\n1- Conta bancária',
  `Bank` varchar(16) NOT NULL COMMENT 'Código do banco (ex.: 001 - banco do brasil)',
  `Agency` varchar(128) DEFAULT NULL,
  `Account` varchar(16) DEFAULT NULL,
  `AccountType` int(11) DEFAULT NULL COMMENT '0 - Conta-corente\n1 - Poupança',
  `Name` varchar(128) DEFAULT NULL,
  `CPF` varchar(16) DEFAULT NULL COMMENT 'CPF ou CNPJ',
  `IsMain` int(11) NOT NULL COMMENT '0 - Não\n1 - Sim',
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

INSERT INTO `account` (`Id`, `UserId`, `Type`, `Bank`, `Agency`, `Account`, `AccountType`, `Name`, `CPF`, `IsMain`) VALUES
(3, 2, 1, '001', '5921-8', '5944-7', 0, 'Valdirene Neves Jr', '025.716.751-09', 0),
(4, 2, 1, 'Akatus', 'vaneves@vaneves.com', '', 0, '', '', 0),
(5, 2, 1, '237', '3664-1', '0009108-1', 0, 'Valdirene da Cruz Neves Jun', '025.716.751-09', 0);

CREATE TABLE IF NOT EXISTS `booked` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EventId` int(11) NOT NULL,
  `Code` varchar(64) CHARACTER SET latin1 NOT NULL,
  `SessionId` varchar(126) CHARACTER SET latin1 NOT NULL,
  `Date` int(11) NOT NULL,
  `IsUsed` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=67 ;

CREATE TABLE IF NOT EXISTS `booked_item` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `BookedId` int(11) NOT NULL,
  `ItemType` varchar(32) NOT NULL,
  `ItemId` int(11) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;


CREATE TABLE IF NOT EXISTS `error_log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EventId` int(11) NOT NULL,
  `Action` varchar(16) NOT NULL,
  `Date` int(11) NOT NULL,
  `IP` varchar(32) NOT NULL,
  `Browser` varchar(256) NOT NULL,
  `Name` varchar(128) DEFAULT NULL,
  `Email` varchar(128) DEFAULT NULL,
  `Phone` varchar(64) DEFAULT NULL,
  `Message` text,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `event` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `Image` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Organizer` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `Visibility` int(11) DEFAULT NULL COMMENT '0 - evento privado, na qual só permite inscrições de convidados ou pela organização\n1 - evento público',
  `CategoryId` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL COMMENT '0 - rascunho\n1 - incompleto\n2 - publicado',
  `Description` text CHARACTER SET utf8,
  `Twitter` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Facebook` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `StartDate` int(11) DEFAULT NULL,
  `EndDate` int(11) DEFAULT NULL,
  `UserId` int(11) NOT NULL,
  `Latitude` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Longitude` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MapType` int(11) NOT NULL COMMENT '0 - none; 1 - address; 2 - online;',
  `Hits` int(11) DEFAULT NULL COMMENT 'Visualizações ao hotsite',
  `CreateDate` int(11) DEFAULT NULL,
  `CustomUrl` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PaymentWay` int(11) NOT NULL,
  `FakeDeadLine` int(11) DEFAULT NULL,
  `DeadLine` int(11) DEFAULT NULL,
  `IsSinglePurchase` int(11) DEFAULT '0',
  `Layout` varchar(64) COLLATE utf8_unicode_ci DEFAULT 'default',
  `Capacity` int(11) DEFAULT NULL,
  `Slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SpotlightIndex` int(11) DEFAULT NULL,
  `Absorb` int(11) DEFAULT NULL,
  `MinimumFee` float DEFAULT NULL,
  `OrganizerEmail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LayoutConfig` text COLLATE utf8_unicode_ci,
  `HasPapers` int(11) NOT NULL,
  `AcceptedPapers` int(11) DEFAULT NULL,
  `RevisionDeadLine` int(11) NOT NULL,
  `ResubmissionDeadLine` int(11) NOT NULL,
  `ReviewersPerPaper` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_event_user` (`UserId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=20 ;

CREATE TABLE IF NOT EXISTS `field` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(32) NOT NULL,
  `Type` varchar(32) NOT NULL,
  `IsRequired` int(11) NOT NULL COMMENT '0 - opcional\n1 - obrigatório',
  `Index` int(11) NOT NULL COMMENT 'numeração da ordem em que os campos apareceram',
  `EventId` int(11) NOT NULL,
  `Content` text,
  PRIMARY KEY (`Id`),
  KEY `fk_field_event` (`EventId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='campos personalizados na inscrição do evento' AUTO_INCREMENT=8 ;

CREATE TABLE IF NOT EXISTS `field_data` (
  `FieldId` int(11) NOT NULL,
  `ParticipantId` int(11) NOT NULL,
  `Value` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`FieldId`,`ParticipantId`),
  KEY `fk_field_data_field` (`FieldId`),
  KEY `fk_field_data_participant` (`ParticipantId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `manager` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` int(11) NOT NULL,
  `EventId` int(11) NOT NULL,
  `Role` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_manager_user` (`UserId`),
  KEY `fk_manager_event` (`EventId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=14 ;

CREATE TABLE IF NOT EXISTS `message` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EventId` int(11) DEFAULT NULL COMMENT 'Se for 0 a mensagem vai para o EventMania.\nSe for maior que 0 vai para a organização do evento referente.',
  `Name` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Message` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Date` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_message_event` (`EventId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `paper` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ParticipantId` int(11) NOT NULL,
  `Title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `File` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Summary` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `Authors` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NeedRevision` int(11) DEFAULT '0',
  `Decision` int(11) DEFAULT '0',
  PRIMARY KEY (`Id`),
  KEY `fk_paper_participant` (`ParticipantId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `participant` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TicketId` int(11) NOT NULL,
  `Name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `Email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `Date` int(11) DEFAULT NULL,
  `CheckIn` int(11) NOT NULL DEFAULT '0' COMMENT '0 - Não presente 1 - Presente (checkin)',
  `CheckInDate` int(11) DEFAULT NULL,
  `Status` int(11) NOT NULL,
  `AuthorId` int(11) NOT NULL,
  `WorkshopId` int(11) DEFAULT NULL,
  `TransactionCode` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `PaymentMethod` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_participant_ticket` (`TicketId`),
  KEY `fk_participant_author` (`AuthorId`),
  KEY `fk_participant_workshop` (`WorkshopId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=772 ;

CREATE TABLE IF NOT EXISTS `review` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ReviewerId` int(11) NOT NULL,
  `PaperId` int(11) NOT NULL,
  `PresentationRate` int(11) DEFAULT NULL,
  `ContentRate` int(11) DEFAULT NULL,
  `ImpactRate` int(11) DEFAULT NULL,
  `PublicComments` text,
  `PrivateComments` text,
  `Recommendation` int(11) DEFAULT NULL,
  `Status` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_review_user` (`ReviewerId`),
  KEY `fk_review_paper` (`PaperId`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

CREATE TABLE IF NOT EXISTS `subscription` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `SendDate` int(11) DEFAULT '0',
  `Sent` int(11) DEFAULT '0',
  `Type` varchar(255) DEFAULT NULL,
  `Template` varchar(255) DEFAULT NULL,
  `ModelId` int(11) DEFAULT NULL,
  `ModelType` varchar(64) DEFAULT NULL,
  `Subject` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

CREATE TABLE IF NOT EXISTS `ticket` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EventId` int(11) NOT NULL,
  `Name` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `Price` float NOT NULL,
  `Description` varchar(256) CHARACTER SET latin1 DEFAULT NULL,
  `StartDate` int(11) DEFAULT NULL,
  `EndDate` int(11) DEFAULT NULL,
  `Amount` int(11) DEFAULT NULL,
  `ReservedAmount` int(11) DEFAULT NULL,
  `IsPrivate` int(11) NOT NULL DEFAULT '0' COMMENT '0 - somente convidados podem adquirir\n1 - qualquer pessoa pode adquirir',
  `HasPapers` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_ticket_event` (`EventId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=35 ;

CREATE TABLE IF NOT EXISTS `transfer` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(64) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Date` int(11) NOT NULL,
  `Value` float NOT NULL,
  `Bank` varchar(16) NOT NULL,
  `Agency` varchar(16) DEFAULT NULL,
  `Account` varchar(16) DEFAULT NULL,
  `AccountType` int(11) DEFAULT NULL COMMENT '0 - Conta-corente\n1 - Poupança',
  `Name` varchar(128) DEFAULT NULL,
  `CPF` varchar(16) DEFAULT NULL,
  `ExternalCode` varchar(128) DEFAULT NULL,
  `Status` int(11) NOT NULL COMMENT '0 - Iniciado; 1 - Aguardando Pagamento; 2 - Aprovado; 3 - Executado; 4 - Cancelado',
  `EventId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

CREATE TABLE IF NOT EXISTS `transfer_history` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TransferId` int(11) NOT NULL,
  `Date` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Status` int(11) NOT NULL,
  `Description` text,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

CREATE TABLE IF NOT EXISTS `user` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) CHARACTER SET utf8 DEFAULT NULL,
  `Email` varchar(128) CHARACTER SET utf8 DEFAULT NULL,
  `Password` varchar(32) CHARACTER SET utf8 DEFAULT NULL,
  `Role` int(11) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  `CreateDate` int(11) DEFAULT NULL,
  `LastLoginDate` int(11) DEFAULT NULL,
  `Newsletter` int(11) DEFAULT NULL,
  `CodeConfirmation` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

INSERT INTO `user` (`Id`, `Name`, `Email`, `Password`, `Role`, `Status`, `CreateDate`, `LastLoginDate`, `Newsletter`, `CodeConfirmation`) VALUES
(2, 'Diego Oliveira', 'diegopso2@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 10, 1, 0, 0, 0, ''),
(3, 'Fulano', 'fulano@gmail.com', '1014d5082f55b9a9580886e7654a81a3', 0, 1, 1386379103, 1386379103, 0, ''),
(4, 'Site', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Van Neves', 'vaneves@vaneves.com', '3c086f596b4aee58e1d71b3626fefc87', 0, 1, NULL, NULL, NULL, NULL),
(6, 'Valdirene Neves Jr', 'van@palmas.to', 'e10adc3949ba59abbe56e057f20f883e', 0, 1, NULL, NULL, NULL, NULL);

CREATE TABLE IF NOT EXISTS `view_event_manager` (
`Id` int(11)
,`Name` varchar(256)
,`Slug` varchar(255)
,`Image` varchar(45)
,`Organizer` varchar(128)
,`Visibility` int(11)
,`CategoryId` int(11)
,`Status` int(11)
,`Description` text
,`Twitter` varchar(45)
,`Facebook` varchar(256)
,`StartDate` int(11)
,`EndDate` int(11)
,`UserId` int(11)
,`Address` varchar(256)
,`Hits` int(11)
,`CreateDate` int(11)
,`CustomUrl` varchar(255)
,`PaymentWay` int(11)
,`FakeDeadLine` int(11)
,`DeadLine` int(11)
,`IsSinglePurchase` int(11)
,`Layout` varchar(64)
,`Capacity` int(11)
,`ManagementId` int(11)
,`ManagerId` int(11)
,`Role` int(11)
);CREATE TABLE IF NOT EXISTS `view_event_owner` (
`Id` int(11)
,`Name` varchar(256)
,`Image` varchar(45)
,`Organizer` varchar(128)
,`Visibility` int(11)
,`CategoryId` int(11)
,`Status` int(11)
,`Description` text
,`Twitter` varchar(45)
,`Facebook` varchar(256)
,`StartDate` int(11)
,`EndDate` int(11)
,`UserId` int(11)
,`Latitude` varchar(32)
,`Longitude` varchar(32)
,`Address` varchar(256)
,`MapType` int(11)
,`Hits` int(11)
,`CreateDate` int(11)
,`CustomUrl` varchar(255)
,`PaymentWay` int(11)
,`FakeDeadLine` int(11)
,`DeadLine` int(11)
,`IsSinglePurchase` int(11)
,`Layout` varchar(64)
,`Capacity` int(11)
,`Slug` varchar(255)
,`OwnerName` varchar(45)
,`OwnerEmail` varchar(128)
);CREATE TABLE IF NOT EXISTS `view_manager` (
`Id` int(11)
,`UserId` int(11)
,`EventId` int(11)
,`Role` int(11)
,`Name` varchar(45)
,`Email` varchar(128)
);CREATE TABLE IF NOT EXISTS `view_paper` (
`Id` int(11)
,`ParticipantId` int(11)
,`UserEmail` varchar(128)
,`UserName` varchar(64)
,`Title` varchar(255)
,`File` varchar(255)
,`Keywords` varchar(255)
,`Summary` varchar(512)
,`Authors` varchar(255)
,`NeedRevision` int(11)
,`Decision` int(11)
,`EventId` int(11)
,`EventName` varchar(256)
,`EventSlug` varchar(255)
,`EventCustomUrl` varchar(255)
,`HasPapers` int(11)
,`TicketId` int(11)
);CREATE TABLE IF NOT EXISTS `view_participant` (
`Id` int(11)
,`TicketId` int(11)
,`Name` varchar(64)
,`Email` varchar(128)
,`Date` int(11)
,`CheckIn` int(11)
,`CheckInDate` int(11)
,`Status` int(11)
,`AuthorId` int(11)
,`WorkshopId` int(11)
,`EventId` int(11)
,`TicketName` varchar(45)
,`Price` float
,`Absorb` int(11)
,`EventName` varchar(256)
,`PaymentWay` int(11)
,`MinimumFee` float
,`AuthorName` varchar(45)
,`WorkshopName` varchar(128)
,`WorkshopPrice` float
);CREATE TABLE IF NOT EXISTS `view_participant_custom_data` (
`Id` int(11)
,`TicketId` int(11)
,`TicketName` varchar(45)
,`AuthorName` varchar(45)
,`AuthorId` int(11)
,`Name` varchar(64)
,`Email` varchar(128)
,`Date` int(11)
,`CheckInDate` int(11)
,`Status` int(11)
,`WorkshopId` int(11)
,`Workshop` varchar(128)
,`Fields` text
,`FieldsIds` text
,`Values` text
,`EventId` int(11)
);CREATE TABLE IF NOT EXISTS `view_participant_event` (
`Id` int(11)
,`Name` varchar(256)
,`Image` varchar(45)
,`Organizer` varchar(128)
,`Visibility` int(11)
,`CategoryId` int(11)
,`Status` int(11)
,`Description` text
,`Twitter` varchar(45)
,`Facebook` varchar(256)
,`StartDate` int(11)
,`EndDate` int(11)
,`UserId` int(11)
,`Address` varchar(256)
,`Hits` int(11)
,`CreateDate` int(11)
,`CustomUrl` varchar(255)
,`PaymentWay` int(11)
,`FakeDeadLine` int(11)
,`DeadLine` int(11)
,`IsSinglePurchase` int(11)
,`Layout` varchar(64)
,`Capacity` int(11)
,`ParticipantId` int(11)
,`TicketId` int(11)
);CREATE TABLE IF NOT EXISTS `view_review` (
`Id` int(11)
,`ReviewerId` int(11)
,`PresentationRate` int(11)
,`ContentRate` int(11)
,`ImpactRate` int(11)
,`PublicComments` text
,`PrivateComments` text
,`Recommendation` int(11)
,`Status` int(11)
,`PaperId` int(11)
,`File` varchar(255)
,`Title` varchar(255)
,`Keywords` varchar(255)
,`Summary` varchar(512)
,`ParticipantName` varchar(64)
,`ParticipantEmail` varchar(128)
,`UserName` varchar(45)
,`UserEmail` varchar(128)
,`UserId` int(11)
,`EventId` int(11)
,`TotalReviews` bigint(21)
);CREATE TABLE IF NOT EXISTS `view_tickets_sold_by_date` (
`PurchaseDay` int(2)
,`PurchaseMonth` int(2)
,`PurchaseYear` int(4)
,`Amount` bigint(21)
,`EventId` int(11)
);CREATE TABLE IF NOT EXISTS `view_transfer` (
`Id` int(11)
,`Code` varchar(64)
,`UserId` int(11)
,`Date` int(11)
,`Value` float
,`Bank` varchar(16)
,`Agency` varchar(16)
,`Account` varchar(16)
,`AccountType` int(11)
,`Name` varchar(128)
,`CPF` varchar(16)
,`ExternalCode` varchar(128)
,`Status` int(11)
,`EventId` int(11)
,`EventName` varchar(256)
,`EventStartDate` int(11)
,`EventEndDate` int(11)
,`UserName` varchar(45)
,`UserEmail` varchar(128)
);CREATE TABLE IF NOT EXISTS `view_workshop` (
`Id` int(11)
,`Title` varchar(128)
,`Description` text
,`Amount` int(11)
,`Price` float
,`Instructor` varchar(256)
,`EventId` int(11)
,`ParticipantsAmount` bigint(21)
);
CREATE TABLE IF NOT EXISTS `workshop` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(128) NOT NULL,
  `Description` text,
  `Amount` int(11) NOT NULL COMMENT 'quantidade de vagas disponíveis',
  `Price` float NOT NULL COMMENT 'caso seja cobrado um valor a mais para participar do workshop',
  `Instructor` varchar(256) DEFAULT NULL COMMENT 'nome do instrutor do workshop',
  `EventId` int(11) NOT NULL,
  `StartDate` int(11) DEFAULT NULL,
  `EndDate` int(11) DEFAULT NULL,
  `Image` varchar(255) DEFAULT NULL,
  `InstructorUrl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `fk_workshop_event` (`EventId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

INSERT INTO `workshop` (`Id`, `Title`, `Description`, `Amount`, `Price`, `Instructor`, `EventId`, `StartDate`, `EndDate`, `Image`, `InstructorUrl`) VALUES
(1, 'CriaÃ§Ã£o de Jogos com Unity3D', '', 30, 5, 'Van Neves', 10, 5, 6, NULL, NULL),
(2, 'CriaÃ§Ã£o de Jogos com Unity3D', '', 30, 0, 'Van Neves', 9, 29, 29, NULL, NULL),
(3, 'Desenvolvimento Web com Trilado PHP', 'Desenvolvimento Web com Trilado PHP\r\n:)', 30, 0, 'Van Neves', 10, 31, 31, '56863bccb38d5097d152c502882020bf.png', 'http://vaneves.com'),
(4, 'CriaÃ§Ã£o de Jogos com Unity3D', '', 30, 25, 'Van Neves', 12, 7, 8, '', 'http://vaneves.com'),
(5, 'CriaÃ§Ã£o de Jogos com Unity3D', 'fdgdfgvdads', 10, 10, 'Van Neves', 19, 1399154400, 1399500000, '5d9cb2013ba32fc75461b3f0a9f1bcc1.png', '');
DROP TABLE IF EXISTS `view_event_manager`;

CREATE VIEW `view_event_manager` AS select `e`.`Id` AS `Id`,`e`.`Name` AS `Name`,`e`.`Slug` AS `Slug`,`e`.`Image` AS `Image`,`e`.`Organizer` AS `Organizer`,`e`.`Visibility` AS `Visibility`,`e`.`CategoryId` AS `CategoryId`,`e`.`Status` AS `Status`,`e`.`Description` AS `Description`,`e`.`Twitter` AS `Twitter`,`e`.`Facebook` AS `Facebook`,`e`.`StartDate` AS `StartDate`,`e`.`EndDate` AS `EndDate`,`e`.`UserId` AS `UserId`,`e`.`Address` AS `Address`,`e`.`Hits` AS `Hits`,`e`.`CreateDate` AS `CreateDate`,`e`.`CustomUrl` AS `CustomUrl`,`e`.`PaymentWay` AS `PaymentWay`,`e`.`FakeDeadLine` AS `FakeDeadLine`,`e`.`DeadLine` AS `DeadLine`,`e`.`IsSinglePurchase` AS `IsSinglePurchase`,`e`.`Layout` AS `Layout`,`e`.`Capacity` AS `Capacity`,`m`.`Id` AS `ManagementId`,`m`.`UserId` AS `ManagerId`,`m`.`Role` AS `Role` from (`event` `e` join `manager` `m` on((`e`.`Id` = `m`.`EventId`)));
DROP TABLE IF EXISTS `view_event_owner`;

CREATE VIEW `view_event_owner` AS select `e`.`Id` AS `Id`,`e`.`Name` AS `Name`,`e`.`Image` AS `Image`,`e`.`Organizer` AS `Organizer`,`e`.`Visibility` AS `Visibility`,`e`.`CategoryId` AS `CategoryId`,`e`.`Status` AS `Status`,`e`.`Description` AS `Description`,`e`.`Twitter` AS `Twitter`,`e`.`Facebook` AS `Facebook`,`e`.`StartDate` AS `StartDate`,`e`.`EndDate` AS `EndDate`,`e`.`UserId` AS `UserId`,`e`.`Latitude` AS `Latitude`,`e`.`Longitude` AS `Longitude`,`e`.`Address` AS `Address`,`e`.`MapType` AS `MapType`,`e`.`Hits` AS `Hits`,`e`.`CreateDate` AS `CreateDate`,`e`.`CustomUrl` AS `CustomUrl`,`e`.`PaymentWay` AS `PaymentWay`,`e`.`FakeDeadLine` AS `FakeDeadLine`,`e`.`DeadLine` AS `DeadLine`,`e`.`IsSinglePurchase` AS `IsSinglePurchase`,`e`.`Layout` AS `Layout`,`e`.`Capacity` AS `Capacity`,`e`.`Slug` AS `Slug`,`u`.`Name` AS `OwnerName`,`u`.`Email` AS `OwnerEmail` from (`event` `e` join `user` `u` on((`e`.`UserId` = `u`.`Id`)));
DROP TABLE IF EXISTS `view_manager`;

CREATE VIEW `view_manager` AS select `m`.`Id` AS `Id`,`m`.`UserId` AS `UserId`,`m`.`EventId` AS `EventId`,`m`.`Role` AS `Role`,`u`.`Name` AS `Name`,`u`.`Email` AS `Email` from (`manager` `m` join `user` `u` on((`m`.`UserId` = `u`.`Id`)));
DROP TABLE IF EXISTS `view_paper`;

CREATE VIEW `view_paper` AS select `p`.`Id` AS `Id`,`pt`.`Id` AS `ParticipantId`,`pt`.`Email` AS `UserEmail`,`pt`.`Name` AS `UserName`,`p`.`Title` AS `Title`,`p`.`File` AS `File`,`p`.`Keywords` AS `Keywords`,`p`.`Summary` AS `Summary`,`p`.`Authors` AS `Authors`,`p`.`NeedRevision` AS `NeedRevision`,`p`.`Decision` AS `Decision`,`e`.`Id` AS `EventId`,`e`.`Name` AS `EventName`,`e`.`Slug` AS `EventSlug`,`e`.`CustomUrl` AS `EventCustomUrl`,`e`.`HasPapers` AS `HasPapers`,`t`.`Id` AS `TicketId` from (((`participant` `pt` join `ticket` `t` on((`t`.`Id` = `pt`.`TicketId`))) join `event` `e` on((`e`.`Id` = `t`.`EventId`))) left join `paper` `p` on((`p`.`ParticipantId` = `pt`.`Id`)));
DROP TABLE IF EXISTS `view_participant`;

CREATE VIEW `view_participant` AS select `p`.`Id` AS `Id`,`p`.`TicketId` AS `TicketId`,`p`.`Name` AS `Name`,`p`.`Email` AS `Email`,`p`.`Date` AS `Date`,`p`.`CheckIn` AS `CheckIn`,`p`.`CheckInDate` AS `CheckInDate`,`p`.`Status` AS `Status`,`p`.`AuthorId` AS `AuthorId`,`p`.`WorkshopId` AS `WorkshopId`,`t`.`EventId` AS `EventId`,`t`.`Name` AS `TicketName`,`t`.`Price` AS `Price`,`e`.`Absorb` AS `Absorb`,`e`.`Name` AS `EventName`,`e`.`PaymentWay` AS `PaymentWay`,`e`.`MinimumFee` AS `MinimumFee`,`u`.`Name` AS `AuthorName`,`w`.`Title` AS `WorkshopName`,`w`.`Price` AS `WorkshopPrice` from ((((`event` `e` join `ticket` `t` on((`t`.`EventId` = `e`.`Id`))) join `participant` `p` on((`t`.`Id` = `p`.`TicketId`))) left join `user` `u` on((`u`.`Id` = `p`.`AuthorId`))) left join `workshop` `w` on((`p`.`WorkshopId` = `w`.`Id`)));
DROP TABLE IF EXISTS `view_participant_custom_data`;

CREATE VIEW `view_participant_custom_data` AS select `p`.`Id` AS `Id`,`p`.`TicketId` AS `TicketId`,`t`.`Name` AS `TicketName`,`u`.`Name` AS `AuthorName`,`p`.`AuthorId` AS `AuthorId`,`p`.`Name` AS `Name`,`p`.`Email` AS `Email`,`p`.`Date` AS `Date`,`p`.`CheckInDate` AS `CheckInDate`,`p`.`Status` AS `Status`,`p`.`WorkshopId` AS `WorkshopId`,`w`.`Title` AS `Workshop`,group_concat(`f`.`Title` separator '>>') AS `Fields`,group_concat(`f`.`Id` separator '>>') AS `FieldsIds`,group_concat(`fd`.`Value` separator '>>') AS `Values`,`t`.`EventId` AS `EventId` from (((((`participant` `p` join `ticket` `t` on((`t`.`Id` = `p`.`TicketId`))) left join `user` `u` on((`p`.`AuthorId` = `u`.`Id`))) left join `workshop` `w` on((`p`.`WorkshopId` = `w`.`Id`))) left join `field` `f` on((`f`.`EventId` = `t`.`EventId`))) left join `field_data` `fd` on(((`f`.`Id` = `fd`.`FieldId`) and (`p`.`Id` = `fd`.`ParticipantId`)))) group by `p`.`Id`;
DROP TABLE IF EXISTS `view_participant_event`;

CREATE VIEW `view_participant_event` AS select `e`.`Id` AS `Id`,`e`.`Name` AS `Name`,`e`.`Image` AS `Image`,`e`.`Organizer` AS `Organizer`,`e`.`Visibility` AS `Visibility`,`e`.`CategoryId` AS `CategoryId`,`e`.`Status` AS `Status`,`e`.`Description` AS `Description`,`e`.`Twitter` AS `Twitter`,`e`.`Facebook` AS `Facebook`,`e`.`StartDate` AS `StartDate`,`e`.`EndDate` AS `EndDate`,`e`.`UserId` AS `UserId`,`e`.`Address` AS `Address`,`e`.`Hits` AS `Hits`,`e`.`CreateDate` AS `CreateDate`,`e`.`CustomUrl` AS `CustomUrl`,`e`.`PaymentWay` AS `PaymentWay`,`e`.`FakeDeadLine` AS `FakeDeadLine`,`e`.`DeadLine` AS `DeadLine`,`e`.`IsSinglePurchase` AS `IsSinglePurchase`,`e`.`Layout` AS `Layout`,`e`.`Capacity` AS `Capacity`,`p`.`Id` AS `ParticipantId`,`t`.`Id` AS `TicketId` from ((`participant` `p` join `ticket` `t` on((`p`.`TicketId` = `t`.`Id`))) join `event` `e` on((`t`.`EventId` = `e`.`Id`)));
DROP TABLE IF EXISTS `view_review`;

CREATE VIEW `view_review` AS select `r`.`Id` AS `Id`,`r`.`ReviewerId` AS `ReviewerId`,`r`.`PresentationRate` AS `PresentationRate`,`r`.`ContentRate` AS `ContentRate`,`r`.`ImpactRate` AS `ImpactRate`,`r`.`PublicComments` AS `PublicComments`,`r`.`PrivateComments` AS `PrivateComments`,`r`.`Recommendation` AS `Recommendation`,`r`.`Status` AS `Status`,`p`.`Id` AS `PaperId`,`p`.`File` AS `File`,`p`.`Title` AS `Title`,`p`.`Keywords` AS `Keywords`,`p`.`Summary` AS `Summary`,`pt`.`Name` AS `ParticipantName`,`pt`.`Email` AS `ParticipantEmail`,`u`.`Name` AS `UserName`,`u`.`Email` AS `UserEmail`,`u`.`Id` AS `UserId`,`t`.`EventId` AS `EventId`,(select count(`review`.`Id`) from `review` where (`review`.`ReviewerId` = `m`.`Id`)) AS `TotalReviews` from (((((`paper` `p` join `participant` `pt` on((`p`.`ParticipantId` = `pt`.`Id`))) join `ticket` `t` on((`pt`.`TicketId` = `t`.`Id`))) left join `review` `r` on((`r`.`PaperId` = `p`.`Id`))) left join `manager` `m` on((`m`.`Id` = `r`.`ReviewerId`))) left join `user` `u` on((`m`.`UserId` = `u`.`Id`)));
DROP TABLE IF EXISTS `view_tickets_sold_by_date`;

CREATE VIEW `view_tickets_sold_by_date` AS select dayofmonth(from_unixtime(`p`.`Date`)) AS `PurchaseDay`,month(from_unixtime(`p`.`Date`)) AS `PurchaseMonth`,year(from_unixtime(`p`.`Date`)) AS `PurchaseYear`,count(`p`.`Id`) AS `Amount`,`t`.`EventId` AS `EventId` from (`ticket` `t` join `participant` `p` on((`p`.`TicketId` = `t`.`Id`))) where ((`p`.`Status` = 3) or (`p`.`Status` = 4)) group by `t`.`EventId`,`p`.`Status`,year(from_unixtime(`p`.`Date`)),month(from_unixtime(`p`.`Date`)),dayofmonth(from_unixtime(`p`.`Date`));
DROP TABLE IF EXISTS `view_transfer`;

CREATE VIEW `view_transfer` AS select `t`.`Id` AS `Id`,`t`.`Code` AS `Code`,`t`.`UserId` AS `UserId`,`t`.`Date` AS `Date`,`t`.`Value` AS `Value`,`t`.`Bank` AS `Bank`,`t`.`Agency` AS `Agency`,`t`.`Account` AS `Account`,`t`.`AccountType` AS `AccountType`,`t`.`Name` AS `Name`,`t`.`CPF` AS `CPF`,`t`.`ExternalCode` AS `ExternalCode`,`t`.`Status` AS `Status`,`t`.`EventId` AS `EventId`,`e`.`Name` AS `EventName`,`e`.`StartDate` AS `EventStartDate`,`e`.`EndDate` AS `EventEndDate`,`u`.`Name` AS `UserName`,`u`.`Email` AS `UserEmail` from ((`transfer` `t` join `event` `e` on((`t`.`EventId` = `e`.`Id`))) join `user` `u` on((`t`.`UserId` = `u`.`Id`)));
DROP TABLE IF EXISTS `view_workshop`;

CREATE VIEW `view_workshop` AS select `w`.`Id` AS `Id`,`w`.`Title` AS `Title`,`w`.`Description` AS `Description`,`w`.`Amount` AS `Amount`,`w`.`Price` AS `Price`,`w`.`Instructor` AS `Instructor`,`w`.`EventId` AS `EventId`,(select count(`participant`.`Id`) from `participant` where (`participant`.`WorkshopId` = `w`.`Id`)) AS `ParticipantsAmount` from `workshop` `w`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;