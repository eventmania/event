var config  = require('../../_modules/config').get();
var app = require('../../_modules/app');

casper.test.begin('Teste se o form de cadastro é exibido na hora certa', 3, {
    test: function(tester) {
        casper.start(config.root, function() {
            var phrase = this.evaluate(function() {
                return $('.main h2').text();
            });

            tester.assertEquals('A melhor ferramenta para organizar seus eventos', phrase, 'Foi carregada a página certa.');
            tester.assertExists('.main form', 'O form de cadastro está na home.');
        });

        app.login(casper);

        casper.thenOpen(config.root, function() {
            tester.assertDoesntExist('.main form', 'O form de cadastro sumiu da home após login.');
        });

        app.logout(casper);

        casper.run(function() {
            tester.done();
        });
    },
    verbose: true,
    logLevel: "info"
});
