var config  = require('../../_modules/config').get();
var app = require('../../_modules/app');
var session = {};

casper.test.begin('Teste se a data de final das vendas de ingressos é atualizada corretamente.', 12, {
    test: function(tester) {
        casper.start();

        app.login(casper);

        casper.thenOpen(config.root + 'event/add/', function() {
            tester.assertEquals(config.root + 'event/add/', this.getCurrentUrl(), 'Acessou a area de criação de eventos.');
        });

        casper.thenClick('.wizard a:last-of-type', function() {
            tester.assertEquals(config.root + 'event/add/', this.getCurrentUrl(), 'Não alterou a página ao clicar no wizard antes de salvar evento.');
        });

        casper.thenClick('#StartDate', function() {
            tester.assertExists('div.datepicker.datepicker-dropdown', 'O dropdown do datepicker abriu.');
            session.startDate = this.evaluate(function() {
                return $('#StartDate').val();
            })
        });

        casper.thenClick('td.new.day:last-of-type', function() {
            var date = this.evaluate(function() {
                return $('#StartDate').val();
            });

            tester.assertNotEquals(date, session.startDate, 'A data mudou após clicar no datepicker.');
            tester.assertMatch(date, /([0-9]{2})\/([0-9]{2})\/([0-9]{4})/, 'A nova data está no formato correto.');

            session.startDate = date;
        });

        casper.thenClick('a.btn-ticket-details', function() {
            var date = this.evaluate(function() {
                return $('input.react').val();
            });

            tester.assertEquals(date, session.startDate, 'A data de fim das vendas de ingressos foi alterada corretamente após a alteração da data inicial do evento.');
        });

        casper.thenClick('input.react', function() {
            tester.assertExists('div.datepicker.datepicker-dropdown', 'O dropdown do datepicker da data final de vendas dos ingressos abriu.');
        });

        casper.thenClick('td.day:first-of-type', function() {
            var date = this.evaluate(function() {
                return $('input.react').val();
            });

            tester.assertNotEquals(date, session.startDate, 'A data final das vendas de tickets mudou após clicar no datepicker.');
            tester.assertMatch(date, /([0-9]{2})\/([0-9]{2})\/([0-9]{4})/, 'A nova data está no formato correto.');
            session.ticketEndDate = date;
        });

        casper.thenClick('#StartDate', function() {
            tester.assertExists('div.datepicker.datepicker-dropdown', 'O dropdown do datepicker abriu.');
        });

        casper.thenClick('td.day:nth-of-type(2)', function() {
            var startDate = this.evaluate(function() {
                return $('#StartDate').val();
            });
            var ticketEndDate = this.evaluate(function() {
                return $('input.react').val();
            });

            tester.assertNotEquals(startDate, session.startDate, 'A data final das vendas de tickets mudou após clicar no datepicker.');
            tester.assertEquals(ticketEndDate, session.ticketEndDate, 'A data final das vendas de ingressos não mudou após mudar o inicio do evento.');
        });

        app.logout(casper);

        casper.run(function() {
            tester.done();
        });
    },
    verbose: true,
    logLevel: "info"
});
