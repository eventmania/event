exports.login = function(casper) {
    casper.waitFor(function() {
        return this.open(config.root + 'login');
    });

    casper.waitFor(function() {
        this.fill('.container form', {
            'Email': config.auth.email,
            'Password': config.auth.pass
        }, true);
        return true;
    }, function() {
        console.log('Logged in!');
    }, function() {
        console.log('Login Timeout!');
    });
};

exports.logout = function(casper) {
    casper.thenOpen(config.root + 'logout', function() {
        console.log('Logged out!');
    });
}

exports.getInEvent = function(casper) {
    casper.thenOpen(config.root + 'event', function() {

        var link = this.evaluate(function() {
            return __utils__.findOne('table td+td a').getAttribute('href');
        });

        session.eventId = link.replace(/[^0-9]*/, '');

        casper.waitFor(function() {
            return this.open(config.getFullUrl(link));
        }, function() {
            var isRight = this.evaluate(function() {
                return __utils__.findOne('#statistic').getAttribute('id');
            });

            if(isRight)
                console.log('Em algum evento!');
            else
                console.log('Não foi possível acessar algum evento!');
        }, function() {
            console.log('Não foi possível acessar algum evento (timeout)!');
        });

    });
};
