/* 08/05 */
ALTER TABLE `manager` ADD COLUMN `Status` INT NULL COMMENT '0 - Bloquado\n1 - Ativo'  AFTER `Role` ;
UPDATE `manager` SET `Status`=1 WHERE `Id`> 0;

DROP VIEW view_manager;
CREATE VIEW `view_manager` AS select `m`.`Id` AS `Id`,`m`.`UserId` AS `UserId`,`m`.`EventId` AS `EventId`,`m`.`Role` AS `Role`,`m`.`Status` AS `Status`,`u`.`Name` AS `Name`,`u`.`Email` AS `Email` from (`manager` `m` join `user` `u` on((`m`.`UserId` = `u`.`Id`)));

DROP VIEW view_event_manager;

CREATE VIEW `view_event_manager` AS select `e`.`Id` AS `Id`,`e`.`Name` AS `Name`,`e`.`Slug` AS `Slug`,`e`.`Image` AS `Image`,`e`.`Organizer` AS `Organizer`,`e`.`Visibility` AS `Visibility`,`e`.`CategoryId` AS `CategoryId`,`e`.`Status` AS `Status`,`e`.`Description` AS `Description`,`e`.`Twitter` AS `Twitter`,`e`.`Facebook` AS `Facebook`,`e`.`StartDate` AS `StartDate`,`e`.`EndDate` AS `EndDate`,`e`.`UserId` AS `UserId`,`e`.`Address` AS `Address`,`e`.`Hits` AS `Hits`,`e`.`CreateDate` AS `CreateDate`,`e`.`CustomUrl` AS `CustomUrl`,`e`.`PaymentWay` AS `PaymentWay`,`e`.`FakeDeadLine` AS `FakeDeadLine`,`e`.`DeadLine` AS `DeadLine`,`e`.`IsSinglePurchase` AS `IsSinglePurchase`,`e`.`Layout` AS `Layout`,`e`.`Capacity` AS `Capacity`,`m`.`Id` AS `ManagementId`,`m`.`UserId` AS `ManagerId`,`m`.`Role` AS `Role`, `m`.`Status` AS `ManagerStatus` from (`event` `e` join `manager` `m` on((`e`.`Id` = `m`.`EventId`)));

UPDATE `manager` SET `Role`=600 WHERE Role = 60;
UPDATE `manager` SET `Role`=500 WHERE Role != 600;

/* 10/05 */
ALTER TABLE `event`
ADD COLUMN `CallForPapers` TEXT NULL AFTER `HasPapers`;

/* 13/05 */
ALTER TABLE `event` ADD COLUMN `City` VARCHAR(128) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL  AFTER `ReviewersPerPaper` , ADD COLUMN `State` CHAR(2) NULL  AFTER `City` ;

/* 14/05 */
CREATE  OR REPLACE VIEW `view_field_data` AS
SELECT
    *
FROM
    field f
    LEFT JOIN field_data fd ON f.Id = fd.FieldId
;

ALTER TABLE `participant`
ADD COLUMN `InvitationStatus` INT NULL DEFAULT 0 AFTER `PaymentMethod`;

/* 06/06 */
CREATE
     OR REPLACE ALGORITHM = UNDEFINED
    DEFINER = `root`@`localhost`
    SQL SECURITY DEFINER
VIEW `view_workshop` AS
    select
        `w`.`Id` AS `Id`,
        `w`.`Title` AS `Title`,
        `w`.`Description` AS `Description`,
        `w`.`Amount` AS `Amount`,
        `w`.`Price` AS `Price`,
        `w`.`Instructor` AS `Instructor`,
        `w`.`EventId` AS `EventId`,
         w.Image,
        (select
                count(`participant`.`Id`)
            from
                `participant`
            where
                (`participant`.`WorkshopId` = `w`.`Id`)) AS `ParticipantsAmount`
    from
        `workshop` `w`;
